#ifndef _SHAREDLITEGENERICPROCESS_H_0B4952EA_A871_4e85_86ED_838BFBCE4316_INCLUDED
#define _SHAREDLITEGENERICPROCESS_H_0B4952EA_A871_4e85_86ED_838BFBCE4316_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Jun-2016 at 11:53:21a, UTC+7, Phuket, Rawai, Monday;
	This is Shared Lite Library (Internext project) Generic Process class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject 29-Feb-2020 at 8:40:13a, UTC+7, Novosibirsk, Tulenina, Saturday;
*/
#include <Shellapi.h>
#include <TlHelp32.h>
#include <Psapi.h>
#pragma comment(lib, "Psapi.lib")

#include "shared.gen.sys.err.h"
#include "shared.gen.proc.flt.h"

namespace shared { namespace process
{
	using shared::sys_core::CError;

	class CGenericProcess
	{
	private:
		DWORD         m_dwId;
		CAtlString    m_module;
		CAtlString    m_desc;
		CAtlString    m_name;
	public:
		 CGenericProcess(void);
		 CGenericProcess(const DWORD dwId);
		~CGenericProcess(void);
	public:
		LPCTSTR       Desc    (void) const ;
		HRESULT       Desc    (LPCTSTR)    ;
		DWORD         ExitCode(void) const ;
		DWORD         Id      (void) const ;
		HRESULT       Id      (const DWORD);
		CAtlString    IdAsText(void) const ;
		bool          Running (void) const ;
		LPCTSTR       Module  (void) const ;
		HRESULT       Module  (LPCTSTR)    ;            // sets full path of the module
		LPCTSTR       Name    (void) const ;
		HRESULT       Name    (LPCTSTR)    ;            // sets the executable name (without path), i.e. file name
		HRESULT       Run     (LPCTSTR pszModule, LPCTSTR pszCmdLine, const bool bWithAdminRights);
	};

	typedef ::std::vector<CGenericProcess> TGenericProcessList;

	class CGenericProcessEnum
	{
	private:
		mutable CError       m_error;
		TGenericProcessList  m_proc_list;
	public:
		 CGenericProcessEnum(void);
		~CGenericProcessEnum(void);
	public:
		HRESULT              Apply       (const CProcessFilter&, TGenericProcessList& _result) const;
		HRESULT              Enumerate   (void);
		TErrorRef            Error       (void)const;
		CGenericProcess      GetResourceLock(LPCTSTR pszFile, LPCTSTR pszFilter);
		const
		TGenericProcessList& List        (void)const;
		HRESULT              TakeSnapshot(void);
	};
}}

#endif/*_SHAREDLITEGENERICPROCESS_H_0B4952EA_A871_4e85_86ED_838BFBCE4316_INCLUDED*/