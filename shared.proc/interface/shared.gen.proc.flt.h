#ifndef _SHAREDGENPROCFLT_H_C8580D98_CB3B_4C84_ACD5_173E32A056F7_INCLUDED
#define _SHAREDGENPROCFLT_H_C8580D98_CB3B_4C84_ACD5_173E32A056F7_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Mar-2020 at 4:33:46p, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is Ebo Pack generic process list data filter interface declaration file.
*/
#include "shared.gen.sys.err.h"
namespace shared { namespace process {
	using shared::sys_core::CError;

	struct Comparator {
		bool operator()(const CAtlString& _path, const CAtlString& _name) const {
			return  (-1 != _path.Find(_name.GetString()));
		}
	};

	typedef ::std::map<CAtlString, bool> TCriteria;// key is process name/part of name; value is a check to apply;

	class CProcessFilter_Base {
	protected:
		TCriteria  m_values;
		CError     m_error ;

	protected:
		 CProcessFilter_Base (void);
		~CProcessFilter_Base (void);

	public:
		HRESULT   Append(const ::std::pair<CAtlString, bool>&);
		HRESULT   Append(LPCTSTR _lp_sz_value, const bool _b_apply = false);
		HRESULT   Clear (void)      ;
		bool      Empty (void) const;
		TErrorRef Error (void) const;
		bool      Has   (const CAtlString& _cs_path) const;
		HRESULT   Remove(LPCTSTR _lp_sz_value);
		HRESULT   Set   (const TCriteria&    );
		HRESULT   Use   (const CAtlString& _cs_path, const bool _b_apply);

	public:
		const
		TCriteria&  Criteria(void) const;

	public:
		CProcessFilter_Base& operator += (LPCTSTR _lp_sz_value);
		CProcessFilter_Base& operator -= (LPCTSTR _lp_sz_value);
		CProcessFilter_Base& operator << (const TCriteria&);

	public:
		bool operator ==(const CAtlString& _cs_path) const;
	};

	class CProcFilter_Exclude : public CProcessFilter_Base {
	                           typedef CProcessFilter_Base TBase;
	public:
		 CProcFilter_Exclude (void);
		~CProcFilter_Exclude (void);
	};

	typedef CProcFilter_Exclude TFltExclude;

	class CProcFilter_Include : public CProcessFilter_Base {
	                           typedef CProcessFilter_Base TBase;
	public:
		 CProcFilter_Include (void);
		~CProcFilter_Include (void);
	};

	typedef CProcFilter_Include TFltInclude;

	class CProcessFilter {
	private:
		CError    m_error;
		CProcFilter_Exclude  m_exc_flt;
		CProcFilter_Include  m_inc_flt;

	public:
		 CProcessFilter(void);
		~CProcessFilter(void);

	public:
		TErrorRef          Error  (void) const;
		const TFltExclude& Exclude(void) const;
		      TFltExclude& Exclude(void)      ;
		const TFltInclude& Include(void) const;
		      TFltInclude& Include(void)      ;
	};
}}
#endif/*_SHAREDGENPROCFLT_H_C8580D98_CB3B_4C84_ACD5_173E32A056F7_INCLUDED*/