/*
	This is shared lite library resource Zw locker class implementation file.
*/
#include "StdAfx.h"
#include "Shared_ZwResourceLocker.h"
#include "Shared_RawData.h"
#include "Shared_GenericHandle.h"
#include "Shared_SystemCore.h"

using namespace shared;
using namespace shared::lite;
using namespace shared::lite::common;
using namespace shared::lite::process;
using namespace shared::lite::process::Zw;
using namespace shared::lite::data;

#if !defined(HANDLE_TYPE_FILE)
	#define  HANDLE_TYPE_FILE 28
#endif

#if !defined(HANDLE_TYPE_FILE_EX)
	#define  HANDLE_TYPE_FILE_EX 25
#endif

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace process { namespace Zw { namespace details
{
	struct ProcessObject_NameInfo
	{
		UNICODE_STRING	uszName;
	};

	struct ProcessObject_BaseInfo
	{
		ULONG           Attributes;
		ACCESS_MASK     GrantedAccess;
		ULONG           HandleCount;
		ULONG           PointerCount;
		ULONG           PagedPoolUsage;
		ULONG           NonPagedPoolUsage;
		ULONG           Reserved[3];
		ULONG           NameInformationLength;
		ULONG           TypeInformationLength;
		ULONG           SecurityDescriptorLength;
		LARGE_INTEGER   CreateTime;
	};

	class  ProcessObject_PoolType
	{
	public:
		enum _e{
			NonPagedPool,
			NonPagedPoolSession = 32,
			NonPagedPoolCacheAlignedMustSSession = NonPagedPoolSession + 6
		};
	};

	struct ProcessObject_TypeInfo
	{
		UNICODE_STRING  Name;
		ULONG           ObjectCount;
		ULONG           HandleCount;
		ULONG           Reserved1[4];
		ULONG           PeakObjectCount;
		ULONG           PeakHandleCount;
		ULONG           Reserved2[4];
		ULONG           InvalidAttributes;
		GENERIC_MAPPING GenericMapping;
		ULONG           ValidAccess;
		UCHAR           Unknown;
		BOOLEAN         MaintainHandleDatabase;
		ProcessObject_PoolType::_e    PoolType;
		ULONG           PagedPoolUsage;
		ULONG           NonPagedPoolUsage;
	};

	class  ProcessObject_InfoClass
	{
	public:
		enum _e{
			ObjectBasicInformation,
			ObjectNameInformation,
			ObjectTypeInformation,
			ObjectAllTypesInformation,
			ObjectHandleInformation
		};
	};

	class  ProcessObject_FileInfoClass
	{
	public:
		enum _e{
			FileNameInformation = 9,
		};
	};

	struct ProcessObject_FileInfo
	{
		ULONG FileNameLength;
		WCHAR FileName[1];
	};

	class  ProcessObject_SystemInfoClass
	{
	public:
		enum _e{
			SystemHandleInformation = 16,
		};
	};

	struct ProcessObject_SystemHandleInfo
	{
		DWORD           dwCount;
		CSystemHandle   Handles[1];
	};
}}}}}
////////////////////////////////////////////////////////////////////////////

bool    CQuerySystemInfoFunctor::IsValid(void)const
{
	return (NULL != m_pFunction);
}

////////////////////////////////////////////////////////////////////////////

CProcessObjectInfo::CProcessObjectInfo(void) :
	m_hObject(NULL),
	m_uNameInfoSize(0),
	m_uTypeInfoSize(0)
{
}

CProcessObjectInfo::CProcessObjectInfo(const HANDLE hObject, const CQueryObjectFunctor& fn_init) :
	m_hObject(hObject),
	m_uNameInfoSize(0),
	m_uTypeInfoSize(0)
{
	details::ProcessObject_BaseInfo basic_info = {0};
	ULONG  ulReqLength = 0;

	m_error = fn_init(
			m_hObject,
			details::ProcessObject_InfoClass::ObjectBasicInformation,
			&basic_info,
			sizeof(basic_info),
			&ulReqLength
		);
	if (!m_error)
	{
		m_uNameInfoSize = basic_info.NameInformationLength; if (!m_uNameInfoSize) m_uNameInfoSize = _MAX_PATH * sizeof(TCHAR);
		m_uTypeInfoSize = basic_info.TypeInformationLength;
	}
}

////////////////////////////////////////////////////////////////////////////

bool             CProcessObjectInfo::IsFile(void)const
{
	return (!m_type.IsEmpty() && 0 == m_type.CompareNoCase(_T("File")));
}

HRESULT          CProcessObjectInfo::Name(LPCTSTR pszName)
{
	if (!pszName || ::_tcslen(pszName) < 1)
		return E_INVALIDARG;
	m_name = pszName;
	return S_OK;
}

HRESULT          CProcessObjectInfo::Name(const CQueryObjectFunctor& fn_query)
{
	if (!fn_query.IsValid())
		return (m_error = E_INVALIDARG);

	CRawData raw_name(m_uNameInfoSize);
	if (!raw_name.IsValid())
		return (m_error = raw_name.Error());

	ULONG  ulReqLength = 0;

	m_error = fn_query(
			m_hObject,
			details::ProcessObject_InfoClass::ObjectNameInformation,
			raw_name.GetData(),
			m_uNameInfoSize,
			&ulReqLength
		);
	if (m_error)
		return  m_error;
	details::ProcessObject_NameInfo* pNameInfo = reinterpret_cast<details::ProcessObject_NameInfo*>(raw_name.GetData());
	if (pNameInfo)
	{
		::ATL::CAtlString cs_name(
				pNameInfo->uszName.Buffer,
				pNameInfo->uszName.Length / sizeof(TCHAR)
			);
		m_name = cs_name;
	}
	return m_error;
}

HRESULT          CProcessObjectInfo::Type(const CQueryObjectFunctor& fn_query)
{
	if (!fn_query.IsValid())
		return (m_error = E_INVALIDARG);
	if (!m_uTypeInfoSize)
		return (m_error = OLE_E_BLANK);

	CRawData raw_type(m_uTypeInfoSize);
	if (!raw_type.IsValid())
		return (m_error = raw_type.Error());

	ULONG  ulReqLength = 0;

	m_error = fn_query(
			m_hObject,
			details::ProcessObject_InfoClass::ObjectTypeInformation,
			raw_type.GetData(),
			m_uTypeInfoSize,
			&ulReqLength
		);
	if (m_error)
		return  m_error;
	details::ProcessObject_TypeInfo* pTypeInfo = reinterpret_cast<details::ProcessObject_TypeInfo*>(raw_type.GetData());
	if (pTypeInfo)
	{
		::ATL::CAtlString cs_type(
				pTypeInfo->Name.Buffer,
				pTypeInfo->Name.Length / sizeof(TCHAR)
			);
		m_type = cs_type;
	}

	return m_error;
}

////////////////////////////////////////////////////////////////////////////

CProcessObjectInfo& CProcessObjectInfo::operator= (const CProcessObjectInfo& obj_ref)
{
	this->m_hObject = obj_ref.m_hObject;
	this->m_uNameInfoSize = obj_ref.m_uNameInfoSize;
	this->m_uTypeInfoSize = obj_ref.m_uTypeInfoSize;
	return *this;
}

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace process { namespace Zw { namespace details
{
	HRESULT    ProcessObject_PopulateSysHandles(CRawData& raw_handles)
	{
		CQuerySystemInfoFunctor fn_query_sys;
		if (!fn_query_sys.IsValid())
			return E_NOTIMPL;

		DWORD dwBlockSize  = 0;
		DWORD dwReqLength  = sizeof(details::ProcessObject_SystemHandleInfo);
		NTSTATUS nt_status = NT_STATUS_SUCCESS;
		do
		{
			dwBlockSize += dwReqLength;
			raw_handles.Create(dwBlockSize);
			if (!raw_handles.IsValid())
				return raw_handles.Error();

			nt_status = fn_query_sys(
					details::ProcessObject_SystemInfoClass::SystemHandleInformation,
					raw_handles.GetData(),
					raw_handles.GetSize(),
					&dwReqLength
				);
			if (!dwReqLength)
				return E_OUTOFMEMORY;

		} while (NT_STATUS_INFO_LENGTH_MISMATCH == nt_status);

		ex_def::CRtlNtError err_obj;
		err_obj = nt_status;
		return err_obj;
	}

	HRESULT    ProcessObject_PopulateProcessList(const CRawData& raw_handles, TProcessList& proc_list)
	{
		if (!proc_list.empty())proc_list.clear();
		details::ProcessObject_SystemHandleInfo* pInfo = 
			reinterpret_cast<details::ProcessObject_SystemHandleInfo*>(raw_handles.GetData());

		for (DWORD i_ = 0; i_ < pInfo->dwCount; i_++)
		{
			const CSystemHandle& handle = pInfo->Handles[i_];
			if (0x0012019f == handle.GrantedAccess) // excludes the handles that can lead to a hanging (named pipes)
				continue;
			if (0x00120196 == handle.GrantedAccess) // excludes the handles that can lead to a hanging (named pipes)
				continue;
			TProcessList::iterator it_ = proc_list.find(handle.ProcessId);

			try
			{
				if (it_ != proc_list.end())
					it_->second.push_back(handle);
				else
				{
					THandleList handles;
					handles.push_back(handle);
					proc_list.insert(::std::make_pair(handle.ProcessId, handles));
				}
			}catch(::std::bad_alloc&){return E_OUTOFMEMORY;}
		}
		return S_OK;
	}

	CAtlString ProcessObject_NormalizeTargetFileName(LPCTSTR pszResource)
	{
		::ATL::CAtlString cs_file;
		{
			TCHAR szFile[_MAX_FNAME] = {0};
			TCHAR szTail[_MAX_EXT]   = {0};

			::_tsplitpath(pszResource, NULL, NULL, szFile, szTail);
			cs_file.Format(_T("%s%s"), szFile, szTail);
		}
		return cs_file;
	}

	using shared::lite::runnable::CGenericRunnableObject;

	static UINT WINAPI ProcessObject_ThreadFunction(void* pObject)
	{
		if (NULL == pObject)
			return 1;
		CGenericRunnableObject* pRunnable = NULL;
		CResourceLocker* pResLocker  = NULL;
		try
		{
			pRunnable  = reinterpret_cast<CGenericRunnableObject*>(pObject);
			pResLocker = reinterpret_cast<CResourceLocker*>(pRunnable);
		}
		catch (::std::bad_cast&) { return 1; }
		shared::lite::sys_core::CComAutoInitializer com_lib(false);
		if (!com_lib.IsSuccess())
			return 1;

		CQueryObjectFunctor   fn_query_obj;
		if (!fn_query_obj.IsValid())
			return 1;

		CResourceLocker& locker_obj = *pResLocker;
		CThreadCrtData&  crt_data = locker_obj.ThreadData();

		TProcessList::const_iterator& it_ = crt_data.ProcessListIter();
		TProcessList& proc_list = crt_data.ProcessList();

		CSysError err_obj;

		::WaitForSingleObject( crt_data.EventStart(), INFINITE );
		::ResetEvent( crt_data.EventStart() );

		for (it_; it_ != proc_list.end(); ++it_)
		{
			const DWORD dwProcId = it_->first;
			CAutoHandle hProcess = ::OpenProcess( PROCESS_DUP_HANDLE , FALSE, dwProcId );
			if (!hProcess.IsValid())
				continue;

			const THandleList& handles = it_->second;

			for (size_t i_ = crt_data.HandleIter(); i_ < handles.size(); i_++, crt_data.HandleIter()++)
			{
				const HANDLE hResource = reinterpret_cast<HANDLE>(handles[i_].wValue);
				CAutoHandle hDuplicate;
				const BOOL bResult = ::DuplicateHandle(
										hProcess,
										hResource,
										::GetCurrentProcess(),
										&hDuplicate,
										0,
										FALSE,
										DUPLICATE_SAME_ACCESS
									);
				if (!bResult || !hDuplicate.IsValid())
					continue;

				CProcessObjectInfo obj_info(hDuplicate, fn_query_obj);
				err_obj = obj_info.Type(fn_query_obj);

				if (err_obj)
					continue;

				if (!obj_info.IsFile())
					err_obj;

				err_obj = obj_info.Name(fn_query_obj);
				if (err_obj)
					continue;
				CProcessObjectBase& obj_base = obj_info;
				LPCTSTR pszName = obj_base.Name();
				if (crt_data.IsMatched(pszName))
				{
					obj_base.Name(dwProcId);
					ex_def::CProcessObjectPath proc_path;
					::ATL::CAtlString cs_path =  proc_path.DeviceToDrivePath(obj_base.Name());
					obj_info.Name(cs_path);
					try
					{
						crt_data.MatchedObjects().push_back(obj_info);
					}
					catch(::std::bad_alloc&){ break; }
				}
				::SetEvent( crt_data.EventFinish() );
			}
			crt_data.HandleIter() = 0;
		}
		crt_data.MarkAsCompleted();
		locker_obj.MarkCompleted();
		return 0;
	}
}}}}}

////////////////////////////////////////////////////////////////////////////

CThreadCrtData::CThreadCrtData(void) : 
//	m_proc_iter(NULL) ,
	m_handle_iter(0)  ,
	m_evt_start(NULL) ,
	m_evt_finish(NULL),
	m_completed(false)
{
}

CThreadCrtData::~CThreadCrtData(void)
{
	this->Clear();
}

////////////////////////////////////////////////////////////////////////////

HRESULT        CThreadCrtData::Clear(void)
{
	m_proc_iter = NULL;
	m_handle_iter = 0;
	if (!m_proc_lst.empty())m_proc_lst.clear();
	if (!m_matched_objs.empty())m_matched_objs.clear();
	if (!m_target.IsEmpty())m_target.Empty();
	if ( m_evt_start )
	{
		::CloseHandle(m_evt_start); m_evt_start = NULL;
	}
	if ( m_evt_finish )
	{
		::CloseHandle(m_evt_finish); m_evt_finish = NULL;
	}
	m_error.Reset();
	m_completed = false;
	return S_OK;
}

TErrorRef      CThreadCrtData::Error(void)const
{
	return m_error;
}

HANDLE&        CThreadCrtData::EventFinish(void)
{
	return m_evt_finish;
}

HANDLE&        CThreadCrtData::EventStart(void)
{
	return m_evt_start;
}

size_t&        CThreadCrtData::HandleIter(void)
{
	return m_handle_iter;
}

bool           CThreadCrtData::IsCompleted(void)const
{
	return m_completed;
}

HRESULT        CThreadCrtData::Initialize(const TProcessList& proc_list)
{
	if ( m_error )m_error.Clear();

	if (!m_evt_start)  m_evt_start  = ::CreateEvent(0, TRUE, FALSE, _T("zw_t_dat_evt_start"));
	if (!m_evt_start)  return (m_error = ::GetLastError());

	if (!m_evt_finish) m_evt_finish = ::CreateEvent(0, TRUE, FALSE, _T("zw_t_dat_evt_finish"));
	if (!m_evt_finish) return (m_error = ::GetLastError());

	m_proc_lst = proc_list;
	m_proc_iter = m_proc_lst.begin();
	m_handle_iter = 0;

	return m_error;
}

bool           CThreadCrtData::IsMatched(LPCTSTR pszDevicePath)const
{
	if (!pszDevicePath || !::_tcslen(pszDevicePath))
		return false;
	if (m_target.IsEmpty())
		return false;
	return (NULL != ::_tcsstr(pszDevicePath, m_target));
}

VOID           CThreadCrtData::MarkAsCompleted(void)
{
	m_completed = true;
}

TProcessInfos& CThreadCrtData::MatchedObjects(void)
{
	return m_matched_objs;
}

TProcessList&  CThreadCrtData::ProcessList(void)
{
	return m_proc_lst;
}

TProcessIter&  CThreadCrtData::ProcessListIter(void)
{
	return m_proc_iter;
}

CAtlString&    CThreadCrtData::TargetFile(void)
{
	return m_target;
}

////////////////////////////////////////////////////////////////////////////

CResourceLocker::CResourceLocker(void):
	CGenericRunnableObject(details::ProcessObject_ThreadFunction, *this, _variant_t((long)::GetTickCount()))
{
}

CResourceLocker::~CResourceLocker(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CResourceLocker::GenericEvent_OnNotify(const _variant_t v_evt_id)
{
	v_evt_id;
	return S_OK;
}

////////////////////////////////////////////////////////////////////////////

TErrorRef          CResourceLocker::Error(void)const
{
	return m_error;
}

CProcessObjectBase CResourceLocker::GetLock(LPCTSTR pszResource, LPCTSTR pszFilter)
{
	pszFilter;

	CProcessObjectInfo      lock_obj;

	// (1) gets the list of handles
	CRawData raw_handles;
	m_error = details::ProcessObject_PopulateSysHandles(raw_handles);
	if (m_error)
		return lock_obj;

	// (2) builds the list of processes
	TProcessList proc_list;
	m_error = details::ProcessObject_PopulateProcessList(raw_handles, proc_list);
	if (m_error)
		return lock_obj;

	// (3) prepares the file name for search
	::ATL::CAtlString cs_file = details::ProcessObject_NormalizeTargetFileName(pszResource);

	// (4) initializes thread run-time data structure
	m_error = m_t_data.Clear();
	m_error = m_t_data.Initialize(proc_list);
	if (m_error)
		return lock_obj;

	static const bool bForceToTerminate = true;

	m_t_data.TargetFile() = cs_file;
	if (!TRunnable::IsStopped()) m_error = TRunnable::Stop(bForceToTerminate);
	if ( TRunnable::IsStopped()) m_error = TRunnable::Start();
	if (m_error)
		return lock_obj;

	while (!m_t_data.IsCompleted())
	{
		::ResetEvent(m_t_data.EventFinish());
		::SetEvent(m_t_data.EventStart());

		if( WAIT_TIMEOUT == ::WaitForSingleObject( m_t_data.EventFinish(), 100 ))
		{
			m_error = TRunnable::Stop(bForceToTerminate);
			m_error = TRunnable::Start();
			if (m_error)
				break;
			continue;
		}
	}

	// (4) iterates through all processes and gets paths for file type handles
	
	return lock_obj;
}

CThreadCrtData&    CResourceLocker::ThreadData(void)
{
	return  m_t_data;
}