#ifndef _SHAREDLITEZWFUNCTORS_H_459D60A2_30AC_4a01_A255_974FD9959078_INCLUDED
#define _SHAREDLITEZWFUNCTORS_H_459D60A2_30AC_4a01_A255_974FD9959078_INCLUDED
/*
	This is shared lite library Zw functor class(es) declaration file.
*/
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0501
#endif

#include <Winternl.h>

namespace shared { namespace lite { namespace process { namespace Zw
{
	class CQueryObjectFunctor
	{
		typedef NTSTATUS (WINAPI *PNtQueryObject)(HANDLE, LONG, PVOID, ULONG , PULONG);
	private:
		PNtQueryObject     m_pFunction;
	public:
		CQueryObjectFunctor(void);
		~CQueryObjectFunctor(void);
	public:
		NTSTATUS operator()(
				__in_opt   HANDLE  hObject,
				__in       LONG    clsObjectInfo,
				__out_bcount_opt(ulObjectInfoLength) PVOID pvObjectInfo,
				__in       ULONG   ulObjectInfoLength,
				__out_opt  PULONG  puReturnLength
			)const;
	public:
		bool    IsValid(void)const;
	};

	class CQuerySystemInfoFunctor
	{
		typedef NTSTATUS (WINAPI *PNtQuerySystemInfo)(LONG, PVOID, ULONG, PULONG);
	private:
		PNtQuerySystemInfo m_pFunction;
	public:
		CQuerySystemInfoFunctor(void);
		~CQuerySystemInfoFunctor(void);
	public:
		NTSTATUS operator()(
				__in       LONG    clsSystemInfo,
				__out      PVOID   pvSystemInfo,
				__in       ULONG   ulSystemInfoLength,
				__in_opt   PULONG  puReturnLength
			)const;
	public:
		bool    IsValid(void)const;
	};
}}}}

#endif/*_SHAREDLITEZWFUNCTORS_H_459D60A2_30AC_4a01_A255_974FD9959078_INCLUDED*/