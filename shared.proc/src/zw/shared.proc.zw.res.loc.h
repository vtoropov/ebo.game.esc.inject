#ifndef _SHAREDEXRESOURCELOCKER_H_C5F0F537_BE81_4fb8_8871_097D788CDA7D_INCLUDED
#define _SHAREDEXRESOURCELOCKER_H_C5F0F537_BE81_4fb8_8871_097D788CDA7D_INCLUDED
/*
	This is shared lite library resource Zw-based (system internal service) locker class declaration file.
*/
#include "Shared_ZwFunctors.h"
#include "Shared_SystemError.h"
#include "Shared_ExtCommonDefs.h"
#include "Shared_GenericRunnableObject.h"

namespace shared { namespace lite { namespace process { namespace Zw
{
	using shared::lite::runnable::CGenericRunnableObject;
	using shared::lite::runnable::IGenericEventNotify;

	using shared::lite::process::ex_def::CProcessObjectBase;
	using shared::lite::process::ex_def::CRtlNtError;

	class CSystemHandle
	{
	public:
		ULONG           ProcessId;
		UCHAR           ObjectTypeNumber;
		UCHAR           Flags;
		WORD            wValue;
		PVOID           Object;
		ACCESS_MASK     GrantedAccess;
	};

	class CProcessObjectInfo : public CProcessObjectBase
	{
		typedef CProcessObjectBase TBaseObject;
	private:
		HANDLE             m_hObject;
		ULONG              m_uNameInfoSize; // cached name information structure size (in bytes)
		ULONG              m_uTypeInfoSize; // cached type information structure size (in bytes)
	public:
		CProcessObjectInfo(void);
		CProcessObjectInfo(const HANDLE hObject, const CQueryObjectFunctor& fn_init);
	public:
		bool               IsFile(void)const;
		HRESULT            Name(LPCTSTR);
		HRESULT            Name(const CQueryObjectFunctor&);
		HRESULT            Type(const CQueryObjectFunctor&);
	public:
		CProcessObjectInfo& operator= (const CProcessObjectInfo&);
	};

	typedef ::std::vector<CProcessObjectInfo> TProcessInfos;
	typedef ::std::vector<CSystemHandle>      THandleList;
	typedef ::std::map<DWORD, THandleList>    TProcessList;
	typedef TProcessList::const_iterator      TProcessIter;

	class CThreadCrtData
	{
	private:
		TProcessList       m_proc_lst;
		TProcessIter       m_proc_iter;
		size_t             m_handle_iter;
		TProcessInfos      m_matched_objs;
		HANDLE             m_evt_start;
		HANDLE             m_evt_finish;
		CRtlNtError        m_error;
		CAtlString         m_target;
		volatile bool      m_completed;
	public:
		CThreadCrtData(void);
		~CThreadCrtData(void);
	public:
		HRESULT            Clear(void);
		TErrorRef          Error(void)const;
		HANDLE&            EventFinish(void);
		HANDLE&            EventStart(void);
		size_t&            HandleIter(void);
		HRESULT            Initialize(const TProcessList&);
		bool               IsCompleted(void)const;
		bool               IsMatched(LPCTSTR pszDevicePath)const;
		VOID               MarkAsCompleted(void);
		TProcessInfos&     MatchedObjects(void);
		TProcessList&      ProcessList(void);
		TProcessIter&      ProcessListIter(void);
		CAtlString&        TargetFile(void);
	private:
		CThreadCrtData(const CThreadCrtData&);
		CThreadCrtData& operator= (const CThreadCrtData&);
	};

	class CResourceLocker:
		public  CGenericRunnableObject,
		public  IGenericEventNotify
	{
		typedef CGenericRunnableObject TRunnable;
	private:
		mutable
		CRtlNtError        m_error;
		CThreadCrtData     m_t_data;
	public:
		CResourceLocker(void);
		~CResourceLocker(void);
	private: //IGenericEventNotify
		virtual HRESULT    GenericEvent_OnNotify(const _variant_t v_evt_id) override sealed;
	public:
		TErrorRef          Error(void)const;
		CProcessObjectBase GetLock(LPCTSTR pszResource, LPCTSTR pszFilter = NULL);
		CThreadCrtData&    ThreadData(void);
	};
}}}}

#endif/*_SHAREDEXRESOURCELOCKER_H_C5F0F537_BE81_4fb8_8871_097D788CDA7D_INCLUDED*/