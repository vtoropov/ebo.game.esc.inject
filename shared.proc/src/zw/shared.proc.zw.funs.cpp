/*
	This is shared lite library Zw functor class(es) implementation file.
*/
#include "StdAfx.h"
#include "Shared_ZwFunctors.h"

using namespace shared;
using namespace shared::lite;
using namespace shared::lite::process;
using namespace shared::lite::process::Zw;

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace process { namespace Zw { namespace details
{
	LPCTSTR     FunctorService_GetModuleName(void)
	{
		static LPCTSTR pszServiceModuleName = _T("ntdll.dll");
		return pszServiceModuleName;
	}
}}}}}

////////////////////////////////////////////////////////////////////////////

CQueryObjectFunctor::CQueryObjectFunctor(void) : m_pFunction(NULL)
{
	const HMODULE hModule = ::GetModuleHandle(details::FunctorService_GetModuleName());
	if (hModule)
		m_pFunction = reinterpret_cast<PNtQueryObject>(::GetProcAddress(hModule, "NtQueryObject"));
}

CQueryObjectFunctor::~CQueryObjectFunctor(void)
{
}

////////////////////////////////////////////////////////////////////////////

NTSTATUS CQueryObjectFunctor::operator()(
				__in_opt  HANDLE   hObject,
				__in      LONG     clObjectInfoCls,
				__out_bcount_opt(ulObjectInfoLength) PVOID pvObjectInfo,
				__in      ULONG    ulObjectInfoLength,
				__out_opt PULONG   puReturnLength
			)const
{
	if (!m_pFunction)
		return E_NOTIMPL;
	const NTSTATUS nt_status = m_pFunction(hObject, clObjectInfoCls, pvObjectInfo, ulObjectInfoLength, puReturnLength);
	return nt_status;
}

////////////////////////////////////////////////////////////////////////////

bool    CQueryObjectFunctor::IsValid(void)const
{
	return (NULL != m_pFunction);
}

////////////////////////////////////////////////////////////////////////////

CQuerySystemInfoFunctor::CQuerySystemInfoFunctor(void) : m_pFunction(NULL)
{
	const HMODULE hModule = ::GetModuleHandle(details::FunctorService_GetModuleName());
	if (hModule)
		m_pFunction = reinterpret_cast<PNtQuerySystemInfo>(::GetProcAddress(hModule, "NtQuerySystemInformation"));
}

CQuerySystemInfoFunctor::~CQuerySystemInfoFunctor(void)
{
}

////////////////////////////////////////////////////////////////////////////

NTSTATUS CQuerySystemInfoFunctor::operator()(
				__in       LONG    clsSystemInfo,
				__out      PVOID   pvSystemInfo,
				__in       ULONG   ulSystemInfoLength,
				__in_opt   PULONG  puReturnLength
			)const
{
	if (!this->IsValid())
		return E_NOTIMPL;
	const NTSTATUS nt_status = m_pFunction(clsSystemInfo, pvSystemInfo, ulSystemInfoLength, puReturnLength);
	return nt_status;
}