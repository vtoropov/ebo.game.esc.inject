#ifndef _SYSPROCSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED
#define _SYSPROCSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Jun-2016 at 11:53:21a, UTC+7, Phuket, Rawai, Monday;
	This is System Process Wrapper Library (Intenext project) precompiled headers declaration file. (v2)
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject 29-Feb-2020 at 8:19:28a, UTC+7, Novosibirsk, Tulenina, Saturday;
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comdef.h>
#include <comutil.h>
#include <vector>
#include <map>

#include <atlapp.h>
#include <atltheme.h>

#pragma comment(lib, "__shared.lite_v15.lib")

#endif/*_SYSPROCSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED*/