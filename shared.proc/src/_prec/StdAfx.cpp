/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Jun-2016 at 9:40:30a, UTC+7, Phuket, Rawai, Monday;
	This is System Process Wrapper library (Intenext project) precompiled headers implementation file. (v2)
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject 29-Feb-2020 at 8:32:22a, UTC+7, Novosibirsk, Tulenina, Saturday;
*/
#include "StdAfx.h"
// https://docs.microsoft.com/en-us/cpp/build/reference/gm-enable-minimal-rebuild?view=vs-2019
#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)