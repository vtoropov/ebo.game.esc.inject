#ifndef _SHAREDLITERMFUNCTORS_H_B583825F_AE0C_41fe_B0A3_AB43B7F21DD0_INCLUDED
#define _SHAREDLITERMFUNCTORS_H_B583825F_AE0C_41fe_B0A3_AB43B7F21DD0_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Dec-2014 at 2:41:48a, UTC+3, Saint-Petersburh, Tuesday;
	This is shared lite library (Internext project) extended Rm (restart manager) functor class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject 29-Feb-2020 at 1:25:42p, UTC+7, Novosibirsk, Tulenina, Saturday;
*/
#include <RestartManager.h>

namespace shared { namespace process { namespace Rm
{
	class CStartSessionFunctor
	{
		typedef DWORD (WINAPI *PFRmStartSession)(DWORD*, DWORD, WCHAR[]);
	private:
		PFRmStartSession   m_pFunction;
	public:
		CStartSessionFunctor(void);
		~CStartSessionFunctor(void);
	public:
		DWORD operator()(
				__out      DWORD* pSessionHandle,
				__reserved DWORD  dwSessionFlags,
				__out_ecount(CCH_RM_SESSION_KEY + 1) WCHAR strSessionKey[]
			)const;
	public:
		bool    IsValid(void)const;
	};

	class CRegisterResourcesFunctor
	{
		typedef DWORD (WINAPI *PFRmRegisterResource)(DWORD, UINT, LPCWSTR[], UINT, RM_UNIQUE_PROCESS[], UINT, LPCWSTR[]);
	private:
		PFRmRegisterResource m_pFunction;
	public:
		CRegisterResourcesFunctor(void);
		~CRegisterResourcesFunctor(void);
	public:
		DWORD operator()(
				__in      DWORD dwSessionHandle,
				__in      UINT  nFiles,
				__in_ecount_opt(nFiles) LPCWSTR rgsFileNames[],
				__in      UINT  nApplications,
				__in_ecount_opt(nApplications) RM_UNIQUE_PROCESS rgApplications[],
				__in      UINT  nServices,
				__in_ecount_opt(nServices) LPCWSTR rgsServiceNames[]
			)const;
	public:
		bool    IsValid(void)const;
	};

	class CGetListFunctor
	{
		typedef DWORD (WINAPI *PFRmGetList)(DWORD, UINT*, UINT*, RM_PROCESS_INFO[], LPDWORD);
	private:
		PFRmGetList       m_pFunction;
	public:
		CGetListFunctor(void);
		~CGetListFunctor(void);
	public:
		DWORD operator()(
				__in      DWORD   dwSessionHandle,
				__out     UINT*   pnProcInfoNeeded,
				__inout   UINT*   pnProcInfo,
				__inout_ecount_opt(*pnProcInfo) RM_PROCESS_INFO rgAffectedApps[],
				__out     LPDWORD lpdwRebootReasons
			)const;
	public:
		bool    IsValid(void)const;
	};

	class CEndSessionFunctor
	{
		typedef DWORD (WINAPI *PFRmEndSession)(DWORD);
	private:
		PFRmEndSession    m_pFunction;
	public:
		CEndSessionFunctor(void);
		~CEndSessionFunctor(void);
	public:
		DWORD operator()(
				__in DWORD dwSessionHandle
			)const;
	public:
		bool    IsValid(void)const;
	};
}}}

#endif/*_SHAREDLITERMFUNCTORS_H_B583825F_AE0C_41fe_B0A3_AB43B7F21DD0_INCLUDED*/