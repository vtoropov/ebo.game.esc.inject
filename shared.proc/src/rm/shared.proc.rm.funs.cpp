/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Dec-2014 at 2:44:00a, UTC+3, Saint-Petersburh, Tuesday;
	This is shared lite extended Rm (restart manager) functor class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject 29-Feb-2020 at 1:42:33p, UTC+7, Novosibirsk, Tulenina, Saturday;
*/
#include "StdAfx.h"
#include "shared.proc.rm.funs.h"

using namespace shared::process;
using namespace shared::process::Rm;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace process { namespace Rm { namespace details
{
	CAtlString  FunctorService_DecodeName(LPCTSTR pszEncoded)
	{
		CAtlString cs_name;
		for (size_t i_ = 0; i_ <= ::_tcslen(pszEncoded) - 3; i_ += 3)
			cs_name += pszEncoded[i_ + 2];

		return cs_name;
	}

	LPCTSTR     FunctorService_GetModuleName(void)
	{
		static CAtlString cs_module;
		if (cs_module.IsEmpty())
		{
			LPCTSTR pszPattern = _T("Rstrtmgr.dll");
			cs_module = FunctorService_DecodeName(pszPattern);
		}
		return cs_module;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CStartSessionFunctor:: CStartSessionFunctor(void) : m_pFunction(NULL)
{
	HMODULE hModule = ::LoadLibrary(details::FunctorService_GetModuleName());
	if (hModule)
	{
		LPCTSTR pszPattern = _T("RmStartSession");
		CAtlStringA cs_func = details::FunctorService_DecodeName(pszPattern);
		m_pFunction = reinterpret_cast<CStartSessionFunctor::PFRmStartSession>(::GetProcAddress(hModule, cs_func));
		// we do not free library here because we want to keep it in memory for further calls;
	}
}

CStartSessionFunctor::~CStartSessionFunctor(void) {}

/////////////////////////////////////////////////////////////////////////////

DWORD CStartSessionFunctor::operator()(
				__out      DWORD* pSessionHandle,
				__reserved DWORD  dwSessionFlags,
				__out_ecount(CCH_RM_SESSION_KEY + 1) WCHAR strSessionKey[]
			)const
{
	if (!m_pFunction)
		return ERROR_INVALID_FUNCTION;

	const DWORD dwResult = m_pFunction(pSessionHandle, dwSessionFlags, strSessionKey);
	return dwResult;
}

/////////////////////////////////////////////////////////////////////////////

bool    CStartSessionFunctor::IsValid(void) const { return (NULL != m_pFunction); }

/////////////////////////////////////////////////////////////////////////////

CRegisterResourcesFunctor:: CRegisterResourcesFunctor(void) : m_pFunction(NULL)
{
	HMODULE hModule = ::LoadLibrary(details::FunctorService_GetModuleName());
	if (hModule) {
		LPCTSTR pszPattern  = _T("RmRegisterResources");
		CAtlStringA cs_func = details::FunctorService_DecodeName(pszPattern);
		m_pFunction = reinterpret_cast<CRegisterResourcesFunctor::PFRmRegisterResource>(::GetProcAddress(hModule, cs_func));
		::FreeLibrary(hModule); hModule = NULL;
	}
}

CRegisterResourcesFunctor::~CRegisterResourcesFunctor(void) {}

/////////////////////////////////////////////////////////////////////////////

DWORD CRegisterResourcesFunctor::operator()(
				__in      DWORD dwSessionHandle,
				__in      UINT  nFiles,
				__in_ecount_opt(nFiles) LPCWSTR rgsFileNames[],
				__in      UINT  nApplications,
				__in_ecount_opt(nApplications) RM_UNIQUE_PROCESS rgApplications[],
				__in      UINT  nServices,
				__in_ecount_opt(nServices) LPCWSTR rgsServiceNames[]
			)const
{
	if (!m_pFunction)
		return ERROR_INVALID_FUNCTION;
	const DWORD dwResult = m_pFunction(dwSessionHandle, nFiles, rgsFileNames, nApplications, rgApplications, nServices, rgsServiceNames);
	return dwResult;
}

/////////////////////////////////////////////////////////////////////////////

bool    CRegisterResourcesFunctor::IsValid(void) const { return (NULL != m_pFunction); }

/////////////////////////////////////////////////////////////////////////////

CGetListFunctor:: CGetListFunctor(void) : m_pFunction(NULL)
{
	HMODULE hModule = ::LoadLibrary(details::FunctorService_GetModuleName());
	if (hModule)
	{
		LPCTSTR pszPattern = _T("RmGetList");
		CAtlStringA cs_func = details::FunctorService_DecodeName(pszPattern);
		m_pFunction = reinterpret_cast<CGetListFunctor::PFRmGetList>(::GetProcAddress(hModule, cs_func));
		::FreeLibrary(hModule); hModule = NULL;
	}
}

CGetListFunctor::~CGetListFunctor(void) {}

/////////////////////////////////////////////////////////////////////////////

DWORD CGetListFunctor::operator()(
				__in      DWORD   dwSessionHandle,
				__out     UINT*   pnProcInfoNeeded,
				__inout   UINT*   pnProcInfo,
				__inout_ecount_opt(*pnProcInfo) RM_PROCESS_INFO rgAffectedApps[],
				__out     LPDWORD lpdwRebootReasons
			) const {
	if (!m_pFunction)
		return ERROR_INVALID_FUNCTION;
	const DWORD dwResult = m_pFunction(dwSessionHandle, pnProcInfoNeeded, pnProcInfo, rgAffectedApps, lpdwRebootReasons);
	return dwResult;
}

/////////////////////////////////////////////////////////////////////////////

bool    CGetListFunctor::IsValid(void) const { return (NULL != m_pFunction); }

/////////////////////////////////////////////////////////////////////////////

CEndSessionFunctor:: CEndSessionFunctor(void) : m_pFunction(NULL)
{
	HMODULE hModule = ::LoadLibrary(details::FunctorService_GetModuleName());
	if (hModule)
	{
		LPCTSTR pszPattern = _T("RmEndSession");
		CAtlStringA cs_func = details::FunctorService_DecodeName(pszPattern);
		m_pFunction = reinterpret_cast<CEndSessionFunctor::PFRmEndSession>(::GetProcAddress(hModule, cs_func));
		::FreeLibrary(hModule); hModule = NULL;
	}
}

CEndSessionFunctor::~CEndSessionFunctor(void) {}

/////////////////////////////////////////////////////////////////////////////

DWORD   CEndSessionFunctor::operator()(
				__in DWORD dwSessionHandle
			)const
{
	if (!m_pFunction)
		return ERROR_INVALID_FUNCTION;
	const DWORD dwResult = m_pFunction(dwSessionHandle);
	return dwResult;
}

/////////////////////////////////////////////////////////////////////////////

bool    CEndSessionFunctor::IsValid(void) const { return (NULL != m_pFunction); }