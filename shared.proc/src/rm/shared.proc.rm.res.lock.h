#ifndef _SHAREDEXRESOURCELOCKER_H_B1F6E45B_FFC4_4546_B2F0_2A049ABB99E5_INCLUDED
#define _SHAREDEXRESOURCELOCKER_H_B1F6E45B_FFC4_4546_B2F0_2A049ABB99E5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Dec-2014 at 3:15:14a, UTC+7, Saint-Petersburh, Tuesday;
	This is shared process library (Internext) resource Rm-based (restart manager) locker class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject 29-Feb-2020 at 2:12:38p, UTC+7, Novosibirsk, Tulenina, Saturday;
*/
#include "shared.gen.sys.err.h"
#include "shared.proc.com.defs.h"

namespace shared { namespace process { namespace Rm
{
	using shared::sys_core::CError;
	using shared::process::CProcessObjectBase;

	class CProcessObjectInfo : public CProcessObjectBase {
	                          typedef CProcessObjectBase TBaseObject;
	public:
		CProcessObjectInfo(void);

	public:
		HRESULT            Name(LPCTSTR pszModuleName);
	};

	class CResourceLocker
	{
	private:
		mutable CError  m_error;
	public:
		 CResourceLocker(void);
		~CResourceLocker(void);
	public:
		TErrorRef          Error(void)const;
		CProcessObjectInfo GetLock(LPCTSTR pszResource, LPCTSTR pszFilter = NULL);
	};
}}}

#endif/*_SHAREDEXRESOURCELOCKER_H_B1F6E45B_FFC4_4546_B2F0_2A049ABB99E5_INCLUDED*/