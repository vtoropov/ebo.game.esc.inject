/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Dec-2014 at 3:16:00a, UTC+7, Saint-Petersburh, Tuesday;
	This is shared process library (Internext) resource Rm-based (restart manager) locker class implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject 29-Feb-2020 at 4:09:12p, UTC+7, Novosibirsk, Tulenina, Saturday;
*/
#include "StdAfx.h"
#include "shared.proc.rm.res.lock.h"
#include "shared.proc.rm.funs.h"
#include "shared.gen.han.h"

using namespace shared::process;
using namespace shared::process::Rm;
using namespace shared::common;

/////////////////////////////////////////////////////////////////////////////

CProcessObjectInfo::CProcessObjectInfo(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT CProcessObjectInfo::Name(LPCTSTR pszModuleName)
{
	if (!pszModuleName || ::_tcslen(pszModuleName) < 1)
		return (m_error = E_INVALIDARG);
	TBaseObject::m_name = pszModuleName;
	return (m_error = S_OK);
}

/////////////////////////////////////////////////////////////////////////////

CResourceLocker:: CResourceLocker(void) {}
CResourceLocker::~CResourceLocker(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CResourceLocker::Error(void)const { return m_error; }

CProcessObjectInfo CResourceLocker::GetLock(LPCTSTR pszResource, LPCTSTR pszFilter) {
	pszFilter;
	CProcessObjectInfo lock_obj;

	CStartSessionFunctor      fn_ses_start;
	CRegisterResourcesFunctor fn_reg_res;
	CGetListFunctor           fn_get_list;
	CEndSessionFunctor        fn_end_ses;

	if (!fn_ses_start.IsValid()
		|| !fn_reg_res.IsValid()
			|| !fn_get_list.IsValid()
				|| !fn_end_ses.IsValid())
	{
				m_error = ERROR_INVALID_FUNCTION;
	}
	else {
		DWORD dwSession = 0;
		TCHAR szSessionKey[CCH_RM_SESSION_KEY + 1] = {0};

		m_error = fn_ses_start(&dwSession, 0, szSessionKey);
		if (m_error == false) {
			m_error = fn_reg_res(dwSession, 1, &pszResource, 0, NULL, 0, NULL);
			if (m_error == false) {
				DWORD dwReason = 0;
				DWORD dwGetListResult = 0;
				UINT  nProcInfoNeeded = 0;
				UINT  nProcInfoLength = 1;

				::std::vector<RM_PROCESS_INFO> processes(nProcInfoLength);
				while(ERROR_MORE_DATA == (dwGetListResult = fn_get_list(
					dwSession         ,
					&nProcInfoNeeded  ,
					&nProcInfoLength  ,
					&processes.front(),
					&dwReason
				))) {
					nProcInfoLength = nProcInfoNeeded;
					processes.resize(nProcInfoLength);
				}
				m_error = dwGetListResult;
				if (m_error == false) {
					for (size_t i_ = 0; i_ < processes.size(); i_++) {

						const RM_PROCESS_INFO& rm_info = processes[i_];
						CProcessObjectBase& proc_obj = lock_obj;

						m_error = proc_obj.Name(rm_info.Process.dwProcessId);
						if (m_error)
							break;

						CProcessObjectPath   obj_path;
						CAtlString cs_path = obj_path.DeviceToDrivePath(proc_obj.Name());

						if (::_tcslen(rm_info.strServiceShortName) > 0)
						{
							::ATL::CAtlString cs_name;
							cs_name.Format(_T("[%s] %s"), rm_info.strServiceShortName, (LPCTSTR)cs_path);
							lock_obj.Name(cs_name);
						}
						else
							lock_obj.Name(cs_path);
						break;
					}
				}
			}
		}
	}
	return lock_obj;
}