/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Jun-2016 at 11:53:21a, UTC+7, Phuket, Rawai, Monday;
	This is Shared Lite Library (Internext project) Generic Process class implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject 29-Feb-2020 at 8:40:13a, UTC+7, Novosibirsk, Tulenina, Saturday;
*/
#include "StdAfx.h"
#include "shared.gen.proc.h"
#include "shared.gen.raw.buf.h"
#include "shared.proc.rm.res.lock.h"
#if 0
#include "shared.proc.zw.res.loc.h"
#endif
#if (0)
#include "Shared_Func_Shell.h"
#include "Shared_Func_PsApi.h"
#include "Shared_Func_Kernel.h"
#endif
#include "shared.gen.han.h"

using namespace shared::common;
using namespace shared::process;

#include "shared.gen.app.obj.h"

using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

CGenericProcess:: CGenericProcess(void) : m_dwId(0) { }
CGenericProcess:: CGenericProcess(const DWORD dwId) : m_dwId(dwId) {}
CGenericProcess::~CGenericProcess(void) {}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR     CGenericProcess::Desc(void) const { return m_desc.GetString(); }
HRESULT     CGenericProcess::Desc(LPCTSTR pszDesc) {
	m_desc = pszDesc;
	return (m_desc.IsEmpty() ? OLE_E_BLANK : S_OK);
}

DWORD       CGenericProcess::ExitCode(void) const {
	DWORD dwExitCode = (DWORD)-1;
	HANDLE hProcess  = ::OpenProcess(PROCESS_ALL_ACCESS, TRUE, m_dwId);
	if (hProcess)
	{
		::GetExitCodeProcess(hProcess, &dwExitCode);
		::CloseHandle(hProcess);
	}
	return dwExitCode;
}

DWORD       CGenericProcess::Id  (void) const { return m_dwId; }
HRESULT     CGenericProcess::Id  (const DWORD dwId) {
	if (!dwId)
		return E_INVALIDARG;
	m_dwId = dwId;
	return S_OK;
}

CAtlString  CGenericProcess::IdAsText(void) const {
	static LPCTSTR lp_sz_pat = _T("%d");
	CAtlString cs_id; cs_id.Format(lp_sz_pat, (DWORD)this->Id());
	return cs_id;
}

bool        CGenericProcess::Running(void)const
{
	bool bResult = false;
	HANDLE hProcess = ::OpenProcess(SYNCHRONIZE, FALSE, m_dwId);
	if (hProcess)
	{
		const DWORD dwResult = ::WaitForSingleObject(hProcess, 0);
		bResult = (WAIT_TIMEOUT == dwResult);
		::CloseHandle(hProcess);
	}
	return bResult;
}

LPCTSTR     CGenericProcess::Module(void) const { return m_module.GetString(); }

HRESULT     CGenericProcess::Module(LPCTSTR pszModule)
{
	if (!pszModule || !::_tcslen(pszModule))
		return E_INVALIDARG;
	m_module = pszModule;
	return S_OK;
}

LPCTSTR     CGenericProcess::Name(void) const { return m_name.GetString(); }
HRESULT     CGenericProcess::Name(LPCTSTR pszName)
{
	m_name = pszName;
	return (m_name.IsEmpty() ? OLE_E_BLANK : S_OK);
}

HRESULT     CGenericProcess::Run(LPCTSTR pszModule, LPCTSTR pszCmdLine, const bool bWithAdminRights)
{
	if (!pszModule || !::_tcslen(pszModule))
		return E_INVALIDARG;
	m_module = pszModule;
	m_module.Replace(_T('/'), _T('\\'));

	::ATL::CAtlString cs_cmd_line(pszCmdLine);

	SHELLEXECUTEINFO info_ = {0};

	info_.cbSize = sizeof(SHELLEXECUTEINFO);
	info_.lpVerb = (bWithAdminRights ? _T("runas") : _T("open"));
	info_.lpFile = m_module.GetString();
	info_.lpParameters = cs_cmd_line.GetString();
	info_.hwnd   = HWND_DESKTOP;
	info_.nShow  = SW_NORMAL;
	info_.fMask  = SEE_MASK_NOCLOSEPROCESS;

	const BOOL bResult = ::ShellExecuteEx(&info_);
	if (bResult)
	{
		m_dwId = ::GetProcessId(info_.hProcess);
		return S_OK;
	}
	else
		return HRESULT_FROM_WIN32(::GetLastError());
}

/////////////////////////////////////////////////////////////////////////////

CGenericProcessEnum:: CGenericProcessEnum(void) {}
CGenericProcessEnum::~CGenericProcessEnum(void) {
	if (!m_proc_list.empty())m_proc_list.clear();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT         CGenericProcessEnum::Apply (const CProcessFilter& _filter, TGenericProcessList& _result) const {
	m_error << __MODULE__ << S_OK;

	if (_filter.Exclude().Empty() && _filter.Include().Empty())
		return (m_error = __DwordToHresult(ERROR_INVALID_STATE)) = _T("Filter criteria are empty;");

	if (m_proc_list.empty())
		return (m_error = __DwordToHresult(ERROR_NO_DATA)) = _T("Process list is empty;");

	for (size_t i_ = 0; i_ < m_proc_list.size(); i_++) {

		const CGenericProcess& proc_ = m_proc_list[i_];
		if (_filter.Exclude().Empty() == false &&
		    _filter.Exclude() == proc_.Module()) {
			continue;
		}
		if (_filter.Include().Empty() == false &&
		    _filter.Include() == proc_.Module()) {
			try {
				_result.push_back(proc_);
			}
			catch (const ::std::bad_alloc&) {
				m_error = E_OUTOFMEMORY; break;
			}
		}
		else if (_filter.Include().Empty()) {
			try {
				_result.push_back(proc_);
			}
			catch (const ::std::bad_alloc&) {
				m_error = E_OUTOFMEMORY; break;
			}
		}
	}

	return m_error;
}

HRESULT         CGenericProcessEnum::Enumerate(void) {
	m_error << __MODULE__ << S_OK;

	if (!m_proc_list.empty())m_proc_list.clear();

	CRawData raw_ids;
	DWORD dw_len = sizeof(DWORD) * 1024;
	do {
		raw_ids.Create(dw_len);
		if (!raw_ids.IsValid())
			return (m_error = raw_ids.Error());

		if (!::EnumProcesses(reinterpret_cast<PDWORD>(raw_ids.GetData()), raw_ids.GetSize(), &dw_len))
		{
			m_error = ::GetLastError();
			break;
		}
		else if (raw_ids.GetSize() == dw_len)
			dw_len *= 2;
	} while (raw_ids.GetSize() <= dw_len);

	if (m_error)
		return m_error;

	const PDWORD pdwIds  = reinterpret_cast<PDWORD>(raw_ids.GetData());
	const  DWORD dwCount = dw_len / sizeof(DWORD);
	const  DWORD dwPath  = _MAX_DRIVE + _MAX_DIR + _MAX_FNAME + _MAX_EXT;

	for (DWORD i_ = 0; i_ < dwCount; i_++)
	{
		const DWORD dwProcId = pdwIds[i_];
		if (!dwProcId)
			continue;

		const CAutoHandle hProcess = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwProcId);
		if (!hProcess.IsValid())
			continue;
		TCHAR szPath[dwPath] = {0};
		if (!::GetModuleFileNameEx(hProcess, NULL, szPath, dwPath))
			continue;
		const CApplication::CVersion ver_(szPath);
		try {
			CGenericProcess proc_obj;
			proc_obj.Id    (dwProcId);
			proc_obj.Module(szPath);
			proc_obj.Desc  (ver_.FileDescription());
			proc_obj.Name  (ver_.InternalName());

			m_proc_list.push_back(proc_obj);
		}
		catch(::std::bad_alloc&){ m_error = E_OUTOFMEMORY; break; }
	}

	return  m_error;
}

TErrorRef       CGenericProcessEnum::Error(void)const { return m_error; }

CGenericProcess CGenericProcessEnum::GetResourceLock(LPCTSTR pszFile, LPCTSTR pszFilter)
{
	CGenericProcess res_lock;

	if (!pszFile || ::_tcslen(pszFile) < _MAX_DRIVE)
	{
		m_error = E_INVALIDARG;
		return res_lock;
	}

	CProcessObjectBase proc_info;

	if (WTL::RunTimeHelper::IsVista())
	{
		Rm::CResourceLocker lock_obj;
		proc_info = lock_obj.GetLock(pszFile, pszFilter);
		m_error = lock_obj.Error();
	}
	else
	{
		/*Zw::CResourceLocker lock_obj;
		proc_info = lock_obj.GetLock(pszFile, pszFilter);
		m_error = lock_obj.Error();*/
		m_error = S_OK;
	}
	if (!m_error)
		res_lock.Module(proc_info.Name());
	return res_lock;
}

const
TGenericProcessList&
                CGenericProcessEnum::List(void)const { return m_proc_list; }

HRESULT         CGenericProcessEnum::TakeSnapshot(void) {
	m_error << __MODULE__ << S_OK;

	if (!m_proc_list.empty())m_proc_list.clear();

	CAutoHandle hProcessSnap = ::CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, NULL );
	if (!hProcessSnap.IsValid())
		return (m_error = ::GetLastError());

	const DWORD dwSize  = sizeof(PROCESSENTRY32);
	PROCESSENTRY32 pe32 = {0}; pe32.dwSize = dwSize;

	if (!::Process32First(hProcessSnap, &pe32))
		return (m_error = ::GetLastError());

	typedef ::std::map<::ATL::CAtlString, INT> TUnique_Key;
	TUnique_Key unique_key;
	do {
		::ATL::CAtlString cs_path  = pe32.szExeFile;
		TUnique_Key::const_iterator it_ = unique_key.find(cs_path);
		if (it_ == unique_key.end()) {
			try {
				unique_key.insert(::std::make_pair(cs_path, 1));
			}
			catch (::std::bad_alloc&){m_error = E_OUTOFMEMORY; break;}
			const CAutoHandle hProcess = ::OpenProcess(PROCESS_QUERY_INFORMATION|PROCESS_VM_READ, FALSE, pe32.th32ProcessID);
			if (hProcess.IsValid())
			{
				TCHAR szPath[_MAX_DRIVE + _MAX_DIR + _MAX_FNAME + _MAX_EXT] = {0};
				if (::GetModuleFileNameEx(hProcess, NULL, szPath, _countof(szPath)))
					cs_path = szPath;
			}

			const CApplication::CVersion ver_(cs_path);
			CGenericProcess proc_obj;
			proc_obj.Module(cs_path);
			proc_obj.Desc(ver_.FileDescription());
			if (proc_obj.Desc() == NULL)
				proc_obj.Desc(ver_.ProductName());
			proc_obj.Name(pe32.szExeFile);
			proc_obj.Id  (pe32.th32ProcessID);
			try {
				m_proc_list.push_back(proc_obj);
			}
			catch(::std::bad_alloc&){ m_error = E_OUTOFMEMORY; break; }
			{
				::memset((void*)&pe32, 0, dwSize);
				pe32.dwSize = dwSize;
			}
		}
	} while (::Process32Next( hProcessSnap, &pe32 ));

	return m_error;
}