#ifndef _SHAREDEXIMPLCOMMONDEFS_H_EB9BDCB2_39B3_4013_A0BC_26263B6D5155_INCLUDED
#define _SHAREDEXIMPLCOMMONDEFS_H_EB9BDCB2_39B3_4013_A0BC_26263B6D5155_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Jun-2016 at 10:38:05a, UTC+7, Phuket, Rawai, Monday;
	This is shared generic process library (Internext) extended common defenition declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject 29-Feb-2020 at 3:33:16p, UTC+7, Novosibirsk, Tulenina, Saturday;
*/
#include "shared.gen.sys.err.h"

#if !defined(NTSTATUS)
	#define  NTSTATUS LONG
#endif

#if !defined(NT_STATUS_SUCCESS)
	#define  NT_STATUS_SUCCESS ((NTSTATUS)0x00000000L)
#endif

#if !defined(NT_STATUS_INFO_LENGTH_MISMATCH)
	#define  NT_STATUS_INFO_LENGTH_MISMATCH ((NTSTATUS)0xc0000004L)
#endif

namespace shared { namespace process {

	using shared::sys_core::CError;

	class CRtlNtError : public CError {
	                   typedef CError TError;
	protected:
		typedef ULONG (WINAPI *PFRtlNtStatusToDosError)(LONG);

	protected:
		PFRtlNtStatusToDosError    m_pFunction;
	public:
		 CRtlNtError(void);
		 CRtlNtError(const DWORD dwError);
		 CRtlNtError(const HRESULT);
		 CRtlNtError(const CRtlNtError& );
		~CRtlNtError(void);

	public:
		CRtlNtError& operator= (const CError&);
		CRtlNtError& operator= (const CRtlNtError&);
		CRtlNtError& operator= (const LONG lNtStatus);    // convert NT status code to Win32/HResult error;
	private:
		VOID     Initialize(void);
	};

	class CProcessObjectBase
	{
	protected:
		CAtlString         m_name;                        // actually, the full path of the module that spawns the process
		CAtlString         m_type;                        // the module type, we are especially interested in file type
		CRtlNtError        m_error;
	public:
		 CProcessObjectBase(void);
		~CProcessObjectBase(void);

	public:
		virtual HRESULT    Name(const DWORD dwProcessId); // sets device path from object PID

	public:
		void               Clear(void);
		TErrorRef          Error(void)const;
		bool               IsValid(void)const;
		LPCTSTR            Name(void)const;
		LPCTSTR            Type(void)const;
	};

	class CProcessObjectPath {
		typedef ::std::vector<CAtlString> TDrvLetters;
	private:
		TDrvLetters     m_drives;
	public:
		 CProcessObjectPath(void);
		~CProcessObjectPath(void);
	public:
		CAtlString      DeviceToDrivePath(LPCTSTR pszDevicePath)const;
	};
}}

#endif/*_SHAREDEXIMPLCOMMONDEFS_H_EB9BDCB2_39B3_4013_A0BC_26263B6D5155_INCLUDED*/