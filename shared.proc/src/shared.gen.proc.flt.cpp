/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Mar-2020 at 4:46:23p, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is Ebo Pack generic process list data filter interface implementation file.
*/
#include "StdAfx.h"
#include "shared.gen.proc.flt.h"

using namespace shared::process;

/////////////////////////////////////////////////////////////////////////////

CProcessFilter_Base:: CProcessFilter_Base(void) { m_error << __MODULE__ << S_OK; }
CProcessFilter_Base::~CProcessFilter_Base(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CProcessFilter_Base::Append(const ::std::pair<CAtlString, bool>& _data) {
	m_error << __MODULE__ << S_OK;

	if (_data.first.IsEmpty())
		return (m_error = E_INVALIDARG) = _T("Process name is invalid;");

	if (this->Has(_data.first))
		return m_error;

	try {
		m_values.insert(_data);
	}
	catch (const ::std::bad_alloc&) {
		m_error = E_OUTOFMEMORY;
	}

	return m_error;
}

HRESULT   CProcessFilter_Base::Append(LPCTSTR _lp_sz_value, const bool _b_apply) {
	
	::std::pair<CAtlString, bool> data_ = ::std::make_pair(CAtlString(_lp_sz_value), _b_apply);

	return this->Append(data_);
}

HRESULT   CProcessFilter_Base::Clear (void) {
	m_error << __MODULE__ << S_OK;

	if (this->Empty())
		return (m_error = S_FALSE);

	return m_error;
}

bool      CProcessFilter_Base::Empty (void) const {
	if (this->m_values.empty())
		return true;

	for (TCriteria::const_iterator it_ = m_values.begin(); it_ != m_values.end(); ++it_) {
		if (it_->second)
			return false;
	}

	return true;
}
TErrorRef CProcessFilter_Base::Error (void) const { return m_error; }
bool      CProcessFilter_Base::Has   (const CAtlString& _cs_path) const {

	bool b_result = false;

	if (_cs_path.IsEmpty()) return (b_result);
	CAtlString cs_what(_cs_path); cs_what.MakeLower();

	for (TCriteria::const_iterator it_ = m_values.begin(); it_ != m_values.end(); ++it_) {

		if (it_->second == false)
			continue;

		CAtlString cs_value(it_->first); cs_value.MakeLower();

		if (-1 != cs_what.Find(cs_value))
			return (b_result = true);
	}

	return b_result;
}

HRESULT   CProcessFilter_Base::Remove(LPCTSTR _lp_sz_value) {
	m_error << __MODULE__ << S_OK;

	if (NULL == _lp_sz_value || !::_tcslen(_lp_sz_value))
		return (m_error = E_INVALIDARG) = _T("The value being removed is invalid;");

	TCriteria::iterator it_ = m_values.find(CAtlString(_lp_sz_value));
	if (it_ != m_values.end()) {
		m_values.erase(it_);
	}
	else {
		(m_error = __DwordToHresult(ERROR_NOT_FOUND));
	}

	return m_error;
}

HRESULT   CProcessFilter_Base::Set   (const TCriteria& _values) {
	m_error << __MODULE__ << S_OK;
	if (m_values.empty() == false)
		m_values.clear();

	try {
		m_values = _values;
	}
	catch (const ::std::bad_alloc&) {
		m_error = E_OUTOFMEMORY;
	}

	return m_error;
}
HRESULT   CProcessFilter_Base::Use   (const CAtlString& _cs_path, const bool _b_apply) {
	m_error << __MODULE__ << S_OK;

	CAtlString cs_what(_cs_path); cs_what.MakeLower();

	for (TCriteria::iterator it_ = m_values.begin(); it_ != m_values.end(); ++it_) {
		CAtlString cs_value(it_->first); cs_value.MakeLower();

		if (-1 != cs_what.Find(cs_value))
			return (it_->second = _b_apply);
	}

	return m_error;
}


/////////////////////////////////////////////////////////////////////////////
const
TCriteria&   CProcessFilter_Base::Criteria(void) const { return m_values; }

/////////////////////////////////////////////////////////////////////////////

CProcessFilter_Base&   CProcessFilter_Base::operator += (LPCTSTR _lp_sz_value) { this->Append(_lp_sz_value); return *this; }
CProcessFilter_Base&   CProcessFilter_Base::operator -= (LPCTSTR _lp_sz_value) { this->Remove(_lp_sz_value); return *this; }
CProcessFilter_Base&   CProcessFilter_Base::operator << (const TCriteria& _data) {
	this->Set(_data);
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

bool CProcessFilter_Base::operator ==(const CAtlString& _cs_path) const { return this->Has(_cs_path); }

/////////////////////////////////////////////////////////////////////////////

CProcFilter_Exclude:: CProcFilter_Exclude(void) : TBase() {}
CProcFilter_Exclude::~CProcFilter_Exclude(void) {}

/////////////////////////////////////////////////////////////////////////////

CProcFilter_Include:: CProcFilter_Include(void) : TBase() {}
CProcFilter_Include::~CProcFilter_Include(void) {}

/////////////////////////////////////////////////////////////////////////////

CProcessFilter:: CProcessFilter(void) { m_error << __MODULE__ << S_OK; }
CProcessFilter::~CProcessFilter(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef          CProcessFilter::Error  (void) const { return m_error  ; }
const TFltExclude& CProcessFilter::Exclude(void) const { return m_exc_flt; }
      TFltExclude& CProcessFilter::Exclude(void)       { return m_exc_flt; }
const TFltInclude& CProcessFilter::Include(void) const { return m_inc_flt; }
      TFltInclude& CProcessFilter::Include(void)       { return m_inc_flt; }