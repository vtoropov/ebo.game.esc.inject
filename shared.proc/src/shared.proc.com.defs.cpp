/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Jun-2016 at 10:38:05a, UTC+7, Phuket, Rawai, Monday;
	This is shared generic process library (Internext) extended common defenition implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject 29-Feb-2020 at 3:33:16p, UTC+7, Novosibirsk, Tulenina, Saturday;
*/
#include "StdAfx.h"
#include "shared.proc.com.defs.h"
#include "shared.gen.han.h"

using namespace shared::common;
using namespace shared::process;

#include <Psapi.h>
/////////////////////////////////////////////////////////////////////////////

CRtlNtError:: CRtlNtError(void) : TError(), m_pFunction(NULL) { this->Initialize(); }

CRtlNtError:: CRtlNtError(const DWORD dwError) : TError(), m_pFunction(NULL) {
	TError::Code(dwError); this->Initialize();
}

CRtlNtError:: CRtlNtError(const HRESULT hError) : TError(), m_pFunction(NULL) {
	TError::Result(hError); this->Initialize();
}

CRtlNtError:: CRtlNtError(const CRtlNtError& _err) : CRtlNtError() { *this = _err; }

CRtlNtError::~CRtlNtError(void) {}

/////////////////////////////////////////////////////////////////////////////

CRtlNtError& CRtlNtError::operator= (const CError& err_ref) {
	*((CError*)this) = err_ref;
	return *this;
}

CRtlNtError& CRtlNtError::operator= (const CRtlNtError& _err) {
	((CError&)*this) = ((CError&)_err);
	return *this;
}

CRtlNtError& CRtlNtError::operator= (const LONG lNtStatus) {
	DWORD dwError = ERROR_INVALID_FUNCTION;
	if (m_pFunction)
		dwError = m_pFunction(lNtStatus);
	((CError&)*this) = (NT_STATUS_SUCCESS == lNtStatus ? S_OK : E_FAIL);
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

VOID CRtlNtError::Initialize(void) {
	// due to a lot of mismatching internal error codes with win32 ones
	// and aggressive spaming to std output, this functionality is disabled, because it is useless
#if 0
	const HMODULE hModule = ::GetModuleHandle(_T("ntdll.dll"));
	if (hModule)
		m_pFunction = reinterpret_cast<PFRtlNtStatusToDosError>(::GetProcAddress(hModule, "RtlNtStatusToDosError"));
#endif
}

/////////////////////////////////////////////////////////////////////////////

CProcessObjectBase:: CProcessObjectBase(void) { }
CProcessObjectBase::~CProcessObjectBase(void) { }

/////////////////////////////////////////////////////////////////////////////

void      CProcessObjectBase::Clear(void) {
	m_error = S_OK;
	if (!m_name.IsEmpty())m_name.Empty();
	if (!m_type.IsEmpty())m_type.Empty();
}

TErrorRef CProcessObjectBase::Error(void)const { return m_error; }
LPCTSTR   CProcessObjectBase::Name (void)const { return m_name.GetString(); }
HRESULT   CProcessObjectBase::Name (const DWORD dwProcessId) {
	if (!dwProcessId)
		return (m_error = E_INVALIDARG);

	CAutoHandle hProcess = ::OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, dwProcessId);

	if (!hProcess.IsValid())
		return (m_error = ::GetLastError());

	static const DWORD dwFullPathLength = (_MAX_DRIVE + _MAX_DIR + _MAX_FNAME + _MAX_EXT);

	TCHAR szModuleFileName[dwFullPathLength] = {0};
	DWORD dwBufferLength = _countof(szModuleFileName);
	const DWORD dwResult = ::GetProcessImageFileName(hProcess, szModuleFileName, dwBufferLength);
	if (!dwResult)
		m_error = ::GetLastError();
	else {
		m_error = S_OK;
		m_name  = szModuleFileName;
	}
	return m_error;
}

LPCTSTR   CProcessObjectBase::Type(void)const { return m_type.GetString(); }

/////////////////////////////////////////////////////////////////////////////

CProcessObjectPath::CProcessObjectPath(void)
{
	static const INT  max_drive_len = 4 * 26 + 1; // 4 chars per one letter (including 0) are multiplied by 26 letters + trailing zero;
	TCHAR szDrvBuffer[max_drive_len]= {0};

	if (::GetLogicalDriveStrings(_countof(szDrvBuffer) - 1, szDrvBuffer))
	{
		PTCHAR pszDrive = szDrvBuffer;
		for (INT i_ = 0; i_ < max_drive_len; i_ += 4)
		{
			if (!pszDrive)
				break;
			::ATL::CAtlString cs_drive(pszDrive);
			if (cs_drive.IsEmpty() || !cs_drive.GetLength())
				break;
			cs_drive = cs_drive.Left(2);
			try
			{
				m_drives.push_back(cs_drive);
			} catch(::std::bad_alloc&){ break; }

			pszDrive += 4;
		}
	}
}

CProcessObjectPath::~CProcessObjectPath(void) {}

/////////////////////////////////////////////////////////////////////////////

CAtlString      CProcessObjectPath::DeviceToDrivePath(LPCTSTR pszDevicePath)const
{
	CAtlString szDrivePath;

	if (m_drives.empty())
		return szDrivePath;

	for (size_t i_ = 0; i_ < m_drives.size(); i_++)
	{
		TCHAR szDevice[_MAX_PATH] = {0};
		if (::QueryDosDevice(m_drives[i_].GetString(), szDevice, _countof(szDevice)))
		{
			const bool bDeviceFound = (0 == ::_tcsnicmp(pszDevicePath, szDevice, ::_tcslen(szDevice)));
			if (bDeviceFound)
			{
				szDrivePath.Format(_T("%s%s"), m_drives[i_].GetString(), pszDevicePath + ::_tcslen(szDevice));
				break;
			}
		}
	}
	return szDrivePath;
}