#ifndef _SHAREDLITELIBRARYREGISTRYDATAPROVIDER_H_2F09C567_4655_42e2_9729_6E40697EF2B8_INCLUDED
#define _SHAREDLITELIBRARYREGISTRYDATAPROVIDER_H_2F09C567_4655_42e2_9729_6E40697EF2B8_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Apr-2015 at 1:01:25pm, GMT+3, Taganrog, Wednesday;
	This is Shared Lite Library Registry Data Provider class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 15-Jun-2018 at 1:38:09p, UTC+7, Phuket, Rawai, Friday;
	Adopted to Ebo Pack on 2-Oct-2019 at 1:46:18p, UTC+7, Novosibirsk, Tulenina, Wednesday;
*/
#include "shared.gen.sys.err.h"
#include "shared.gen.raw.buf.h"
#include "shared.reg.hive.defs.h"
#include "shared.reg.hive.key.h"

namespace shared { namespace registry {

	using shared::sys_core::CError;
	using shared::common::CRawData;

	class CRegistryStg : public CRegistryBase {
	                    typedef CRegistryBase TBase;
	public:
		 CRegistryStg(const HKEY hRoot, const DWORD dwOptions = CRegOptions::eNone);
		~CRegistryStg(void);
	public:
		HRESULT      Load     (LPCTSTR pszFolder, LPCTSTR pszValueName, ::ATL::CAtlString& _out_value) const;
		HRESULT      Load     (LPCTSTR pszFolder, LPCTSTR pszValueName, LONG& _out_value, const LONG _default = 0) const;
		HRESULT      Remove   (LPCTSTR pszFolder);
		HRESULT      Remove   (LPCTSTR pszFolder, LPCTSTR pszValueName);
		HRESULT      Save     (LPCTSTR pszFolder, LPCTSTR pszValueName, const ::ATL::CAtlString& _in_value);
		HRESULT      Save     (LPCTSTR pszFolder, LPCTSTR pszValueName, const DWORD _in_value);             // saves as dword;
		HRESULT      Save     (LPCTSTR pszFolder, LPCTSTR pszValueName, const LONG  _in_value);             // saves as string;
		HRESULT      Save     (LPCTSTR pszFolder, LPCTSTR pszValueName, const TMultiString& _in_value);     // saves string array as a multi-string registry data type;
	private:
		CRegistryStg(const CRegistryStg&);
		CRegistryStg& operator= (const CRegistryStg&);
	};

	class CStorage {
	protected:
		mutable
		CError     m_error;

	public:
		 CStorage (void) ;
		~CStorage (void) ;

	public:
		TErrorRef   Error (void ) const;
		HRESULT     Info  (CKey&)      ; // gets an information of when key is created or changed;
		HRESULT     Read  (CKey&)      ;
		HRESULT     Read  (const CKey& , CRawData& _out_data) const;                   // loads binary data;
		HRESULT     Read  (const CKey& , PBYTE& _out_value, ULONG& _out_size) const;   // loads binary data;
		HRESULT     Write (const CKey&);
		HRESULT     Write (const CKey& , const CRawData& _out_data);                   // saves binary data;
		HRESULT     Write (const CKey& , const PBYTE _in_value, const ULONG _in_size); // saves binary data;
	};
}}

#endif/*_SHAREDLITELIBRARYREGISTRYDATAPROVIDER_H_2F09C567_4655_42e2_9729_6E40697EF2B8_INCLUDED*/