#ifndef _GENERICSTGDATA_H_4916FDBD_664D_48f6_A467_B1F03F356B69_INCLUDED
#define _GENERICSTGDATA_H_4916FDBD_664D_48f6_A467_B1F03F356B69_INCLUDED
/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 6:56:06am, GMT+4, Taganrog, Wednesday;
	This is Ebo Pack shared library generic raw data class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 11:58:22a, UTC+7, Phuket, Rawai, Monday;
	Adopted to sound-bin-trans project on 14-Apr-2019 at 0:01:19a, UTC+7, Phuket, Rawai, Sunday;
	Adopted to communicate project on 23-May-2019 at 11:58:54a, UTC+7, Phuket, Rawai, Thursday;
	Adopted to shared memory project on 8-Jun-2019 at 1:43:30a, UTC+7, Novosibirsk, Tulenina, Saturday;
	Adopted to Ebo Game Dyna Inject on 5-Mar-2020 at 1:53:51p, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "shared.gen.sys.err.h"
#include <vector>

namespace shared { namespace common
{
	using shared::sys_core::CError;

	typedef const PBYTE PCBYTE;

	class CRawBuffer {
	protected:
		PBYTE       m_pData;
		DWORD       m_dwSize;
	protected:
		mutable CError m_error;
	protected:
		CRawBuffer(void);
	public:
		HRESULT     Clear  (void);
		bool        IsEmpty(void) const;
		bool        IsValid(void) const;
	};

	class CRawData : public  CRawBuffer {
	                 typedef CRawBuffer TBuffer;
	public:
	    CRawData(void);
	    CRawData(const _variant_t&);       // creates from bstr data
	    CRawData(const DWORD dwSize);
	    CRawData(const PBYTE pData, const DWORD dwSize);
	   ~CRawData(void);
	public:
		HRESULT     Append (const CRawData&);
		HRESULT     Append (const PBYTE pData, const DWORD dwSize);
		HRESULT     CopyToVariantAsArray(_variant_t&)const; // data is interpreted as binary and is copied to safe array as is;
		HRESULT     CopyToVariantAsUtf16(_variant_t&)const; // data is interpreted as Utf8 text and is converted to Utf16 bstr;
		HRESULT     CopyToVariantAsUtf8 (_variant_t&)const; // data is interpreted as Utf8 text and is copied to bstr as is;
		HRESULT     Create (const DWORD dwSize);
		HRESULT     Create (const PBYTE pData, const DWORD dwSize);
		LPBYTE      Detach (void)      ;
		TErrorRef   Error  (void) const;
		PCBYTE      GetData(void) const;
		PBYTE       GetData(void)      ;
		DWORD       GetSize(void) const;
		HRESULT     Reset  (void)      ;
		HRESULT     ToFile (LPCTSTR _lp_sz_path) const;  // saves data dto file specified; 
		HRESULT     ToStringUtf16(CAtlStringW& ) const;  // assumes the buffer contains unicode string;
		HRESULT     ToStringUtf8 (CAtlStringA& ) const;  // assumes the buffer contains ansi string;
	public:
		operator const    bool (void)const;              // returns a validity state of the object;
		operator const    DWORD(void)const;              // returns a buffer size;
		operator const    PBYTE(void)const;              // returns a buffer data pointer (ra);
		operator          PBYTE(void)     ;              // returns a buffer data pointer (rw);
	public:
		CRawData(const CRawData&);
		CRawData& operator+=(const CRawData&);
		CRawData& operator= (const CRawData&);
		CRawData& operator= (const _variant_t&);         // creates from bstr data;
		CRawData& operator= (const ::std::vector<BYTE>&);
	};

	class CSafeArrayEx;
	class CSafeArrayBase {
	protected:
		friend class CSafeArrayEx;
		mutable
		CError        m_error;
		LPSAFEARRAY   m_psa;
		UCHAR HUGEP*  m_data_ptr;
	protected:
		CSafeArrayBase(void);

	public:
		TErrorRef     Error(VOID)   const;  // gets error object reference;
		bool          IsValid(void) const;  // checks the validity of the data accessor object;
	};

	class CSafeArrayEx : public CSafeArrayBase {
	                    typedef CSafeArrayBase TBase;
	public:
		class CElements {
		private:
			const
			CSafeArrayBase&  m_data_ref;
			mutable LONG     m_lbound;    // lower boundery;
			mutable LONG     m_ubound;    // upper boundary;
		public:
			CElements(const CSafeArrayBase& _data_ref);
		public:
			LONG        Count(void)const;
			HRESULT     Item (const LONG _ndx, _variant_t&) const;
			VARTYPE     Type (void)const;
		};

	private:
		CElements       m_elements;
	public:
		CSafeArrayEx(const _variant_t&);                 // variant type must be VT_ARRAY|VT_UI1
		CSafeArrayEx(LPSAFEARRAY);                       // safe array must contain VT_UI1 data
		~CSafeArrayEx(void);
	public:
		const
		CSafeArrayBase& Base (void) const;
		PBYTE           Data (void);                     // gets access to safe array data pointer
		const CElements&Elements(void) const;            // elements reference of safe array;
		HRESULT         Size (LONG& n_size_ref) const;   // gets the size of the safe array data in bytes
	public:
		operator const CSafeArrayBase& (void) const;
	private:
		CSafeArrayEx(const CSafeArrayEx&);
		CSafeArrayEx& operator= (const CSafeArrayEx&);
	};
}}

#endif/*_GENERICSTGDATA_H_4916FDBD_664D_48f6_A467_B1F03F356B69_INCLUDED*/