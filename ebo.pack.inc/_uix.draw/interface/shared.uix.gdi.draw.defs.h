#ifndef _UIXDRAWCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED
#define _UIXDRAWCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED
/*
	Created by Tech_dog (VToropov) on 5-Feb-2015 at 11:41:14pm, GMT+3, Taganrog, Thursday;
	This is UIX Draw library common definition/class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 11-Aug-2018 at 4:14:00p, UTC+7, Novosibirsk, Tulenina, Saturday;
	Adopted to Ebo Pack on 12-Nov-2019 at 12:54:10p, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include <gdiplus.h>

#define __HRESULT_FROM_LASTERROR()   HRESULT_FROM_WIN32(::GetLastError())
#define __H(rc__)    (rc__.bottom - rc__.top)
#define __W(rc__)    (rc__.right - rc__.left)

#define RGBA(r,g,b,a)	( RGB(r,g,b) | (((DWORD)(BYTE)(a))<<24) )
#define GetAValue(_clr)	( 0 == LOBYTE((_clr)>>24) ? 255 : LOBYTE((_clr)>>24) )

#define HexToRgb(_hex)  ( RGB( LOBYTE((_hex)>>16),LOBYTE((_hex)>>8),LOBYTE((_hex))) )
#define H2C(_hex) HexToRgb(_hex)

namespace ex_ui { namespace draw {

	class eAlphaValue
	{
	public:
		enum _e {
			eTransparent     = 0x00,      // transparent
			eTransparent95   = 0x0D,      // 05% opacity
			eTransparent90   = 0x19,      // 10% opacity
			eTransparent85   = 0x26,      // 15% opacity
			eTransparent80   = 0x33,      // 20% opacity
			eTransparent75   = 0x40,      // 25% opacity
			eTransparent70   = 0x4C,      // 30% opacity
			eTransparent65   = 0x59,      // 35% opacity
			eTransparent60   = 0x66,      // 40% opacity
			eTransparent55   = 0x73,      // 45% opacity
			eSemitransparent = 0x7F,      // 50% opacity
			eTransparent45   = 0x8C,      // 55% opacity
			eTransparent40   = 0x99,      // 60% opacity
			eTransparent35   = 0xA6,      // 65% opacity
			eTransparent30   = 0xB2,      // 70% opacity
			eTransparent25   = 0xBF,      // 75% opacity
			eTransparent20   = 0xCC,      // 80% opacity
			eTransparent15   = 0xD9,      // 85% opacity
			eTransparent10   = 0xE5,      // 90% opacity
			eTransparent05   = 0xF2,      // 95% opacity
			eOpaque          = 0xFF       //100% opacity
		};
	};

namespace defs
{
	interface IRenderer
	{
		virtual   HRESULT  DrawBackground(const HDC hSurface, const RECT& rcDrawArea) {hSurface; rcDrawArea; return E_NOTIMPL;}
		virtual   HRESULT  DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea) const PURE;
	};
}}}

#endif/*_UIXDRAWCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED*/