#ifndef _UIXDRAWPNGLOADER_H_E1B2893F_B891_4e86_9457_A6C667A0EA5F_INCLUDED
#define _UIXDRAWPNGLOADER_H_E1B2893F_B891_4e86_9457_A6C667A0EA5F_INCLUDED
/*
	Created by Tech_dog (VToropov) on 6-Feb-2015 at 1:16:22am, GMT+3, Taganrog, Friday;
	This is UIX draw library PNG wrapper common class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 11:16:45p, UTC+7, Phuket, Rawai, Monday;
*/
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace draw { namespace common
{
	class CPngBitmap
	{
	public:
		Gdiplus::Bitmap* m_pBitmap;
	public:
		CPngBitmap(void);
		CPngBitmap(const INT nWidth, const INT nHeight);
		CPngBitmap(LPCWSTR pszFile);
		virtual ~CPngBitmap(void);
	public:
		operator Gdiplus::Bitmap*(void) const;
	public:
		HRESULT                 Attach(Gdiplus::Bitmap*);
		HBITMAP                 Clone(void) const;
		Gdiplus::Bitmap*        Detach(void);
		VOID                    Empty(void);
		HRESULT                 GetHandle(HBITMAP&) const;
		Gdiplus::Bitmap*        GetPtr(void) const;
		const Gdiplus::Bitmap&  GetRef(void) const;
		Gdiplus::Bitmap&        GetRef(void);
		SIZE                    GetSize(void) const;
		bool                    IsValid(void) const;
		HRESULT                 Load(LPCWSTR pszFile);
	};

	class CPngBitmapPtr
	{
	private:
		CPngBitmap*   m_pBitmap;
		HRESULT       m_hResult;
	public:
		CPngBitmapPtr(const bool bCreateObject = false);
		~CPngBitmapPtr(void);
	public:
		HRESULT                 Attach(CPngBitmap*);
		HRESULT                 Attach(Gdiplus::Bitmap*);
		HRESULT                 Create(const INT nWidth, const INT nHeight);
		HRESULT                 Create(const UINT resId, const HMODULE hResourceModule = NULL);
		HRESULT                 Destroy(void);
		CPngBitmap*             Detach(void);
		HRESULT                 GetLastResult(void) const;
		CPngBitmap*             GetObject(void) const;
		const CPngBitmap&       GetObjectRef(void) const;
		CPngBitmap&             GetObjectRef(void);
		bool                    IsValidObject(void) const;
	public:
		operator CPngBitmap*() const { return m_pBitmap; }
	public:
		static HRESULT          CreateObject(CPngBitmap*&);
		static HRESULT          CreateObject(const INT nWidth, const INT nHeight, CPngBitmap*&);
		static HRESULT          DestroyObject_Safe(CPngBitmap*&);
	};
}}}

#endif/*_UIXDRAWPNGLOADER_H_E1B2893F_B891_4e86_9457_A6C667A0EA5F_INCLUDED*/