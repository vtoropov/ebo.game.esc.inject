#ifndef _UIXDRAWRENDERER_H_B10C71E6_F9B5_49d6_A9EE_82013C781AAB_INCLUDED
#define _UIXDRAWRENDERER_H_B10C71E6_F9B5_49d6_A9EE_82013C781AAB_INCLUDED
/*
	Created by Tech_dog (VToropov) on 6-Feb-2015 at 00:32:11AM, GMT+3, Taganrog, Friday;
	This is Ebo Pack shared UIX draw library renderer interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 11:18:17p, UTC+7, Phuket, Rawai, Monday;
*/
#include "shared.uix.gdi.draw.defs.h"
#include "shared.uix.gdi.provider.h"
#include "shared.uix.png.wrap.h"

namespace ex_ui { namespace draw { namespace renderers {
	using ex_ui::draw::common::CPngBitmapPtr;
	using ex_ui::draw::CZBuffer;
	//
	// TODO: renderer implementation is very simplistic here - it uses only one image for getting
	//       background draw completed; unfortunately, it cannot be used for composing a background from several images;
	//       this approach must be re-viewed;
	//
	class CBackgroundRendererDefImpl:
		public ex_ui::draw::defs::IRenderer
	{
	protected:
		mutable
		CPngBitmapPtr     m_bkgnd_cache;
	protected:
		::ATL::CWindow&   m_owner_ref;
		INT               m_x_shift;
		INT               m_y_shift;
	protected:
		 CBackgroundRendererDefImpl(::ATL::CWindow&  owner_ref);
		~CBackgroundRendererDefImpl(void);
	protected:
		virtual   HRESULT DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea) const override;
	public:
		HRESULT   GetImageSize(SIZE& __in_out_ref) const;
		HRESULT   SetVerticalShift(const INT);

	public:
		static   HRESULT  ImageSize(CPngBitmapPtr&, SIZE&);
	private:
		CBackgroundRendererDefImpl(const CBackgroundRendererDefImpl&);
		CBackgroundRendererDefImpl& operator= (const CBackgroundRendererDefImpl&);
	};

	class CBackgroundTileRenderer:
		public CBackgroundRendererDefImpl
	{
		typedef CBackgroundRendererDefImpl TBase;
	private:
		mutable
		CPngBitmapPtr     m_tile_ptr;
	public:
		 CBackgroundTileRenderer(::ATL::CWindow&  owner_ref);
		 CBackgroundTileRenderer(const UINT nResId, ::ATL::CWindow&  owner_ref);
		~CBackgroundTileRenderer(void);
	private:
#pragma warning(disable:4481)
		virtual HRESULT   DrawBackground(const HDC hSurface, const RECT& rcDrawArea) override sealed;
#pragma warning(default:4481)
	public:
		HRESULT           Draw(CZBuffer&, const RECT& rcDrawArea);
		HRESULT           InitializeFromFile(LPCTSTR pszFilePath);
		bool              IsValid (void) const;
		HRESULT           LoadTile(const UINT nResId);
		SIZE              TileSize(void) const;
	private:
		CBackgroundTileRenderer(const CBackgroundTileRenderer&);
		CBackgroundTileRenderer& operator= (const CBackgroundTileRenderer&);
	};

	class CBkgTileRender_Simple {
	public:
		 CBkgTileRender_Simple(void);
		~CBkgTileRender_Simple(void);
	public:
		HRESULT           Draw(CZBuffer&, const RECT& _area, Gdiplus::Bitmap* const);
		HRESULT           Draw(CZBuffer&, const RECT& _area, Gdiplus::Bitmap* const, const POINT& _ancher);
	};
}}}

#endif/*_UIXDRAWRENDERER_H_B10C71E6_F9B5_49d6_A9EE_82013C781AAB_INCLUDED*/