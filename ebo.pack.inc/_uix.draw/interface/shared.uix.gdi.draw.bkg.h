#ifndef _SHAREDUIXGDIDRAWBKG_H_079105A7_B853_4C4B_92B8_9CD3AF2A0D63_INCLUDED
#define _SHAREDUIXGDIDRAWBKG_H_079105A7_B853_4C4B_92B8_9CD3AF2A0D63_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jan-2019 at 8:10:28a, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Ebo Pack shared UIX draw library rendering background interface declaration file.
*/
#include "shared.uix.gdi.object.h"
#include "shared.gen.sys.err.h"

namespace ex_ui { namespace draw {

	using shared::sys_core::CError;

	class CBkgSegment {
	public:
		enum _pos : UINT {
			e_none     = 0x0000,   // no segment;
			e_left     = 0x0001,   // left          side;
			e_top      = 0x0002,   // top           side;
			e_right    = 0x0004,   // right         side;
			e_bottom   = 0x0008,   // bottom        side;
			e_middle   = 0x0010,   // middle|center;
			e_top_lft  = 0x0020,   // top   |left   corner;
			e_top_rht  = 0x0040,   // top   |right  corner;
			e_btm_lft  = 0x0080,   // bottom|left   corner;
			e_btm_rht  = 0x0100,   // bottom|right  corner;
		};
	protected:
		_pos     m_pos;
		WORD     m_res;

	public:
		 CBkgSegment(const _pos, const WORD _res_id);
		~CBkgSegment(void);

	public:
		bool      IsValid (void) const;
		_pos      Position(void) const;
		HRESULT   Position(const _pos);
		WORD      Resource(void) const;
		HRESULT   Resource(const WORD);

	public:
		CBkgSegment& operator<<(const _pos);
		CBkgSegment& operator<<(const WORD);
		operator const _pos(void) const;
		operator const WORD(void) const;
		operator const bool(void) const;
	};

	typedef ::std::map<CBkgSegment::_pos, WORD> TSegments;   // a key is segment identifier, a value is a resource identifier;

	class CBkgSegmentMap {
	private:
		CError      m_error;
		TSegments   m_segs ;

	public:
		 CBkgSegmentMap(void);
		~CBkgSegmentMap(void);

	public:
		HRESULT     Append (const CBkgSegment::_pos, const WORD _res_id);
		HRESULT     Append (const CBkgSegment&);
		VOID        Clear  (void);
		TErrorRef   Error  (void) const;
		bool        IsValid(void) const;
		const
		TSegments&  Map (void) const;

	public:
		CBkgSegmentMap& operator+= (const ::std::pair<CBkgSegment::_pos, WORD>&);
		CBkgSegmentMap& operator+= (const CBkgSegment&);
	public:
		operator const TSegments&  (void) const;
		operator const bool        (void) const;  // if map is valid, returns true;
	};

	class CBkgImages : public CImageCache {
	                  typedef CImageCache TCache;
	private:
		CError      m_error;
	public:
		 CBkgImages(void);
		~CBkgImages(void);

	public:
		HRESULT     Create (const TSegments&);   // creates a background images from resource identifier(s) provided;
		TErrorRef   Error  (void) const;         // gets last error object;
		bool        IsEmpty(void) const;         // checks image cache count, if the count is zero, it returns true;
	};

	class CBkgDrawArea {
	private:
		POINT       m_ancher;
		RECT        m_clip  ;

	public:
		 CBkgDrawArea(void);
		 CBkgDrawArea(const RECT& _clip);
		 CBkgDrawArea(const RECT& _clip, const POINT& _ancher);
		~CBkgDrawArea(void);

	public:
		const
		POINT&      Ancher(void) const;
		POINT&      Ancher(void)      ;
		const
		RECT&       Clip  (void) const;
		RECT&       Clip  (void)      ;

	public:
		operator const RECT& (void) const;
		operator       RECT& (void)      ;
	};

	typedef ::std::map<CBkgSegment::_pos, CBkgDrawArea> TLayout;

	class CBkgLayout {
	private:
		TLayout   m_layout;
	public:
		 CBkgLayout(const RECT& _rc_area, const CBkgImages&);
		~CBkgLayout(void);

	public:
		const
		TLayout&   Do(const RECT _rc_area, const CBkgImages&);

	public:
		operator const TLayout& (void) const;
	};
	//
	// TODO: Functinality must be extended;
	//       (a) only top and middle are used for this version (01/10/19);
	//
	class CBkgRenderer {
	private:
		CError       m_error;
		CBkgImages   m_src;

	public:
		 CBkgRenderer(void);
		~CBkgRenderer(void);

	public:
		HRESULT     Draw   (const HDC _h_dc, const RECT& _rc_area);
		TErrorRef   Error  (void) const;
		const
		CBkgImages& Images (void) const;
		CBkgImages& Images (void)      ;
		bool        IsValid(void) const;   // checks image cache for emptiness; if the image list is empty, background cannot be drawn;
	};
}}

#endif/*_SHAREDUIXGDIDRAWBKG_H_079105A7_B853_4C4B_92B8_9CD3AF2A0D63_INCLUDED*/