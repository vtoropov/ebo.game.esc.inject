#ifndef _SHAREDUIXCTRLLABELEXT_H_1B30B1A9_C44B_47FD_AE61_5A3AC57E717E_INCLUDED
#define _SHAREDUIXCTRLLABELEXT_H_1B30B1A9_C44B_47FD_AE61_5A3AC57E717E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Sep-2018 at 9:20:42p, UTC+7, Novosibirsk, Rodniki, Tulenina, Friday;
	This is shared UIX library custom label control extension interface declaration file. 
*/
#include "shared.uix.gen.hsl.h"
#include "shared.uix.ctrl.defs.h"

namespace ex_ui { namespace controls {

	interface ILabel_Events {
		virtual HRESULT   ILabel_OnBkgChanged (void) PURE;
		virtual HRESULT   ILabel_OnFontChanged(void) PURE;
	};

	class CLab_Format {
	public:
		enum _r : ULONG {
			eNa = 0xFFFF,    // any resource has word-based identifier, which value is from 1 to 0xFFFF (65535);
		};
	private:
		ILabel_Events& m_callee   ;
		COLORREF       m_back     ;
		COLORREF       m_fore     ;
		DWORD          m_size     ;
		CAtlString     m_family   ;
		bool           m_underline;
		eHorzAlign::_e m_hz_align ;
		UINT           m_bkg_id   ;        // background image resource identifier;
		eVertAlign::_e m_vt_align ;
		CMargins       m_margins  ;

	public:
		 CLab_Format(ILabel_Events&);
		~CLab_Format(void);

	public:
		COLORREF       BkgColour  (void) const;
		HRESULT        BkgColour  (const COLORREF);
		UINT           BkgImage   (void) const;          // gets background image resource identifier;
		HRESULT        BkgImage   (const UINT u_res_id); // sets background image resource identifier;
		LPCTSTR        Family     (void) const;
		HRESULT        Family     (LPCTSTR)   ;
		DWORD          FontSize   (void) const;
		HRESULT        FontSize   (const DWORD);
		COLORREF       ForeColour (void) const;
		HRESULT        ForeColour (const COLORREF);
		eHorzAlign::_e HorzAlign  (void) const;
		VOID           HorzAlign  (const eHorzAlign::_e);
		const
		CMargins&      Margins    (void) const;
		CMargins&      Margins    (void)      ;
		bool           Underline  (void) const;
		VOID           Underline  (const bool);
		eVertAlign::_e VertAlign  (void) const;
		VOID           VertAlign  (const eVertAlign::_e);
	};

}}

#endif/*_SHAREDUIXCTRLLABELEXT_H_1B30B1A9_C44B_47FD_AE61_5A3AC57E717E_INCLUDED*/