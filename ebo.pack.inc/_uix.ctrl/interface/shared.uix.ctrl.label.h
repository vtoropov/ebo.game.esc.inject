#ifndef _SHAREDUIXCTRLLABEL_H_02422FCB_4FB2_4303_84E7_AD5F1867A8AA_INCLUDED
#define _SHAREDUIXCTRLLABEL_H_02422FCB_4FB2_4303_84E7_AD5F1867A8AA_INCLUDED
/*
	Created by Tech_dog (VToropov) on 16-Mar-2015 at 10:02:24pm, GMT+3, Taganrog, Monday;
	This is shared UIX library extended label control interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 19-Aug-2018 at 7:35:08p, UTC+7, Novosibirsk, Rodniki, Tulenina, Sunday;
*/
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.ctrl.label.ext.h"
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace controls {

	using ex_ui::draw::defs::IRenderer;

	class CLabel_Ex : public ILabel_Events
	{
	protected:
		HANDLE        m_wnd_ptr;
		CLab_Format   m_format ;

	public:
		 CLabel_Ex(void);
		~CLabel_Ex(void);

	public:
		HRESULT       Create (const HWND hParent, const RECT& rcArea, LPCTSTR lpszText);
		HRESULT       Destroy(void);
		const
		CLab_Format&  Format (void) const;
		CLab_Format&  Format (void)      ;
		const bool    Is     (void) const;
		HRESULT       ParentRenderer (IRenderer*  const );
		HRESULT       Refresh(const bool b_async = false); // refreshes content of a label control window;
		SIZE          ReqSize(LPCTSTR _lp_sz_txt) const  ; // returns a size of a text; margins are also taken into account;
		CAtlString    Text   (void) const;
		HRESULT       Text   (LPCTSTR)   ;
		CWindow       Window (void) const;

	private: // ILabel_Events
		virtual HRESULT   ILabel_OnBkgChanged (void) override;
		virtual HRESULT   ILabel_OnFontChanged(void) override;

	public:
		static SIZE  ReqSize(LPCTSTR _lp_sz_txt, const CLab_Format&); // uses desktop window DC, format font and margins;
	private:
		CLabel_Ex(const CLabel_Ex&);
		CLabel_Ex& operator= (const CLabel_Ex&);
	};
}}

#endif/*_SHAREDUIXCTRLLABEL_H_02422FCB_4FB2_4303_84E7_AD5F1867A8AA_INCLUDED*/