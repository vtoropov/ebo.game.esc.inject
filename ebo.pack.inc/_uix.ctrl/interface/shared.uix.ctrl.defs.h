#ifndef _UIXCTRLCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED
#define _UIXCTRLCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED
/*
	Created by Tech_dog (VToropov) on 9-Feb-2015 at 7:23:30pm, GMT+3, Taganrog, Monday;
	This is Ebo Pack UIX shared library control common interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-Jul-2018 at 0:25:31a, UTC+7, Novosibirsk, Rodniki, Tulenina, Saturday;
*/
#include "shared.uix.gdi.provider.h"
#include "shared.uix.gdi.object.h"

#include "shared.gen.sys.err.h"

namespace ex_ui { namespace controls { 

	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::CColour;

	using shared::sys_core::CError;

	class CMargins {
	public:
		enum _side   : UINT {
			e_left   = 0x0,
			e_top    = 0x1,
			e_right  = 0x2,
			e_bottom = 0x3,
		};
	protected:
		INT    m_sides[e_bottom + 1];

	public:
		 CMargins(const INT _lft = 0, const INT _top = 0, const INT _rht = 0, const INT _btm = 0);
		~CMargins(void);

	public: // acceessor(s);
		const INT&   Bottom (void)  const;
		      INT&   Bottom (void)       ;
		const INT&   Left   (void)  const;
		      INT&   Left   (void)       ;
		const INT&   Right  (void)  const;
		      INT&   Right  (void)       ;
		const INT&   Top    (void)  const;
		      INT&   Top    (void)       ;

	public:
		const void   ApplyTo(RECT&) const;
		const bool   IsValid(void)  const;

	public:
		operator const bool (void)  const;
		const CMargins& operator<<(RECT&) const;
	};

	class CProperties {
	protected:
		DWORD    m_value;

	protected:
		CProperties(const DWORD dwValue = 0);
		virtual ~CProperties(void);
	public:
		bool     Has   (const DWORD) const;
		VOID     Modify(const DWORD, const bool _bApply);
	public:
		const DWORD&   Value(void) const;
		      DWORD&   Value(void)      ;

	public:
		operator const DWORD(void) const;
		CProperties&   operator = (const DWORD);
		CProperties&   operator+= (const DWORD);
		CProperties&   operator-= (const DWORD);
	};

	class CControlState : public CProperties {
	public:
		enum _e : ULONG {
			eNormal      = 0x00, // default value that indicates no action, which is affected a state, is applied; 
			eDisabled    = 0x01,
			eSelected    = 0x02,
			eHovered     = 0x04,
			ePressed     = 0x08,
		};
	public:
		 CControlState(const DWORD dwValue = _e::eNormal);
		~CControlState(void);

	public:
		bool  IsDisabled(void) const;
		bool  IsHovered (void) const;
		bool  IsPressed (void) const;
		bool  IsSelected(void) const;
	};

	class CControlStyle : public CProperties {
	public:
		enum _e : ULONG {
			eNone        = 0x00,  // default, no style is applied;
			eBorder      = 0x01,
		};
	private:
		DWORD       m_value;

	public:
		 CControlStyle(const DWORD dwStyle = _e::eNone);
		~CControlStyle(void);

	public:
		bool   IsBordered(void) const;
		VOID   IsBordered(const bool);
	};

	class eHorzAlign {
	public:
		enum _e {
			eLeft        = DT_LEFT  ,
			eCenter      = DT_CENTER,
			eRight       = DT_RIGHT ,
		};
	};

	class eVertAlign {
	public:
		enum _e {
			eMiddle      = DT_VCENTER,
			eTop         = DT_TOP    ,
			eBottom      = DT_BOTTOM ,
		};
	};

	interface IControlEvent
	{
		virtual  HRESULT  IControlEvent_OnClick(const UINT ctrlId) PURE;
		virtual  HRESULT  IControlEvent_OnClick(const UINT ctrlId, const LONG_PTR nData) PURE;
	};

	class CControlCrt
	{
	protected:
		UINT              m_ctrlId;
		CColour           m_bk_clr;
		CControlState     m_state ;
		CControlStyle     m_style ;

	public:
		 CControlCrt(void);
		~CControlCrt(void);

	public:
		const CColour&    BackColor(void)const;
		CColour&          BackColor(void);
		VOID              BackColor(const CColour&);
		VOID              BackColor(const COLORREF, const BYTE _alpha);
		UINT              CtrlId(void) const;
		HRESULT           CtrlId(const UINT);

	public:
		const
		CControlState&    State(void) const;
		CControlState&    State(void)      ;
		const
		CControlStyle&    Style(void) const;
		CControlStyle&    Style(void)      ;
	};

}}

#endif/*_UIXCTRLCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED*/