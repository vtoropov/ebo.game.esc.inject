#ifndef _SHAREDUIXCTRLBASERND_H_52AD29E8_4263_4CE0_A167_79F1A5C3B3D9_INCLUDED
#define _SHAREDUIXCTRLBASERND_H_52AD29E8_4263_4CE0_A167_79F1A5C3B3D9_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Jul-2018 at 10:47:11a, UTC+7, Novosibirsk, Sunday;
	This is UIX shared library control common renderer interface declaration file;
*/
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.img.wrap.h"

namespace ex_ui { namespace controls {

	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::CZBuffer;
	using ex_ui::draw::CImageCache;

	class CControlRenderer : public IRenderer {
	protected:
		CWindow             m_wnd;
		CControlCrt&        m_crt;
		CImageCache         m_images;

	public:
		 CControlRenderer(CControlCrt&);
		~CControlRenderer(void);

	public:
		virtual HRESULT     DrawBackground(const HDC hSurface, const RECT& rcDrawArea) override;
		virtual HRESULT     DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea) const override;

	public:
#if (0)
		virtual HRESULT     DrawBkgnd(CZBuffer&);
#endif
		virtual HRESULT     DrawImage(CZBuffer&);
		virtual HRESULT     DrawImage(CZBuffer&, const RECT& rcDrawArea);
		virtual HRESULT     DrawImage(CZBuffer&, const RECT& rcDrawArea, const DWORD dwState);

	public:
		const
		CImageCache&   Images(void) const;
		CImageCache&   Images(void)      ;
		const
		CWindow&       Host  (void) const;
		CWindow&       Host  (void)      ;
		SIZE           RequiredSize(void) const;
	};
}}

#endif/*_SHAREDUIXCTRLBASERND_H_52AD29E8_4263_4CE0_A167_79F1A5C3B3D9_INCLUDED*/