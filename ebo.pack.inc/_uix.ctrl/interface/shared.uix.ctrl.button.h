#ifndef _UIXCTRLBUTTONCTRL_H_81EC4C24_8073_49ef_9939_3D5C401193D4_INCLUDED
#define _UIXCTRLBUTTONCTRL_H_81EC4C24_8073_49ef_9939_3D5C401193D4_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Feb-2015 at 6:22:03pm, GMT+3, Taganrog, Sunday;
	This is UIX library custom button control class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack constrol library on 21-Nov-2019 at 11:18:39a, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.gdi.draw.defs.h"
#include "shared.uix.ctrl.base.rnd.h"

namespace ex_ui { namespace controls {

	using ex_ui::controls::IControlEvent;
	using ex_ui::controls::CControlRenderer;

	using ex_ui::draw::defs::IRenderer ;

	typedef CControlRenderer TButtonRnd;

	class CButton_Ex {
	protected:
		HANDLE     m_wnd_ptr;
		CControlCrt    m_crt;
		TButtonRnd     m_rnd;

	public:
		 CButton_Ex(IControlEvent&);
		 CButton_Ex(IControlEvent&, IRenderer& parent_rnd);
		~CButton_Ex(void);
	public:
		HRESULT        Create  (const HWND hParent, const LPRECT, const bool bHidden);
		HRESULT        Destroy (void);
		const bool     Enabled (void) const;
		HRESULT        Enabled (const bool);
		HRESULT        Refresh (const bool b_async = false); // refreshes content of a button control window;
		const
		TButtonRnd&    Renderer(void) const;
		TButtonRnd&    Renderer(void)      ;
		const SIZE     ReqSize (void) const;
		const
		CControlCrt&   Runtime (void) const;
		CControlCrt&   Runtime (void)      ;
		HRESULT        Subclass(::ATL::CWindow&);
		ATL::CWindow   Window  (void) const;
	private:
		CButton_Ex(const CButton_Ex&);
		CButton_Ex& operator= (const CButton_Ex&);
	};
}}

#endif/*_UIXCTRLBUTTONCTRL_H_81EC4C24_8073_49ef_9939_3D5C401193D4_INCLUDED*/