#ifndef _SHAREDGENCON_IF_H_9B902161_A600_4D48_A7F7_9279DBC23FCD_INCLUDED
#define _SHAREDGENCON_IF_H_9B902161_A600_4D48_A7F7_9279DBC23FCD_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Feb-2019 at 7:27:51a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is FIX Engine shared library test desktop console app interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to sound-bin-trans project on 5-Apr-2019 at 11:39:24a, UTC+7, Phuket, Rawai, Thursday;
	Adopted to serial port data project on 21-May-2019 at 7:07:46p, UTC+7, Phuket, Rawai, Tuesday;
	Adopted to RDP automation project on 3-Jun-2019 at 2:01:26p, UTC+7, Seoul, INC airport, Monday;
	Adopted to shared memory project on 8-Jun-2019 at 1:37:15a, UTC+7, Novosibirsk, Tulenina, Saturday;
	Adopted to FakeGPS driver project on 13-Dec-2019 at 9:47:05a, UTC+7, Novosibirsk, Tulenina, Friday;
	Adopted to File Touch app on 2-Feb-2020 at 8:15:18p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include "shared.gen.sys.err.h"

namespace shared { namespace common { namespace ui {

	static LPCTSTR lp_sz_sep = _T("\n\t\t");

	class CConsole_Ex {
	public:
		enum _tp {
			e_info = 0x0,
			e_warn = 0x1,
			e_err  = 0x2,
		};
	public:
		 CConsole_Ex(void);
		~CConsole_Ex(void);

	public:
		HRESULT   OnClose  (void);
		HRESULT   OnCreate (LPCTSTR    _lp_sz_cap);
		HRESULT   OnWait   (LPCTSTR    _lp_sz_msg);
		HRESULT   SetIcon  (const WORD _w_res_id );
		HRESULT   Write    (const _tp  _t, LPCTSTR _lp_sz_msg, LPCTSTR _lp_sz_sep = lp_sz_sep);
		HRESULT   WriteErr (TErrorRef  _err);
		HRESULT   WriteErr (LPCTSTR _lp_sz_msg, LPCTSTR  _lp_sz_sep = lp_sz_sep);
		HRESULT   WriteInfo(LPCTSTR _lp_sz_msg, LPCTSTR  _lp_sz_sep = lp_sz_sep);
		HRESULT   WriteWarn(LPCTSTR _lp_sz_msg, LPCTSTR  _lp_sz_sep = lp_sz_sep);

	public:
		CConsole_Ex& operator << (const WORD);        // info message;
		CConsole_Ex& operator << (LPCTSTR   );        // info message;
		CConsole_Ex& operator << (TErrorRef );        // error message;
		CConsole_Ex& operator << (const CAtlString&); // info message;
	
	};

}}}

#endif/*_SHAREDGENCON_IF_H_9B902161_A600_4D48_A7F7_9279DBC23FCD_INCLUDED*/