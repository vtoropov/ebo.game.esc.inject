#ifndef _SHAREDLITELIBGENERICEVENT_H_DF255B0D_D53A_4917_B1EC_8F7D231AA515_INCLUDED
#define _SHAREDLITELIBGENERICEVENT_H_DF255B0D_D53A_4917_B1EC_8F7D231AA515_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Aug-2014 at 8:03:27pm, GMT+3, Taganrog, Wednesday;
	This is Shared Lite Library Generic Event class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Fake GPS project on 11-Dec-2019 at 7:59:09p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	Adopted to File Touch project on 6-Feb-2020 at 1:11:55a, UTC+7, Novosibirsk, Tulenina, Thursday;
*/

namespace shared { namespace runnable
{
	interface IGenericEventNotify
	{
		virtual HRESULT  GenericEvent_OnNotify(const _variant_t v_evt_id) PURE;
		virtual HRESULT  GenericEvent_OnNotify(const LONG n_evt_id, const _variant_t v_data) {n_evt_id; v_data; return E_NOTIMPL; }
	};
	class CMarshaller
	{
		enum {
			eInternalMsgId = WM_USER + 1,
		};
	protected:
		class CMessageHandler:
			public ::ATL::CWindowImpl<CMessageHandler>
		{
		private:
			IGenericEventNotify&    m_sink_ref;
			const _variant_t        m_event_id;
			DWORD                   m_threadId;
		public:
			BEGIN_MSG_MAP(CMessageHandler)
				MESSAGE_HANDLER(CMarshaller::eInternalMsgId, OnGenericEventNotify)
			END_MSG_MAP()
		private:
			virtual LRESULT  OnGenericEventNotify(UINT, WPARAM, LPARAM, BOOL&);
		public:
			CMessageHandler(IGenericEventNotify&, const _variant_t& v_evt_id);
			virtual ~CMessageHandler(void);
		public:
			DWORD       _OwnerThreadId(void) const;
		};
	protected:
		CMessageHandler  m_handler;
	public:
		CMarshaller(IGenericEventNotify&, const _variant_t& v_evt_id);
		virtual ~CMarshaller(void);
	public:
		virtual HRESULT  Create(void);
		virtual HRESULT  Destroy(void);
		virtual HWND     GetHandle_Safe(void) const;
	private:
		CMarshaller(const CMarshaller&);
		CMarshaller& operator= (const CMarshaller&);
	public:
		virtual HRESULT  Fire(const bool bAsynch = true);
		static  HRESULT  Fire(const HWND hHandler, const bool bAsynch = true);
	public:
		virtual HRESULT  Fire2(void);
	};

	class CDelayEvent
	{
	public:
		enum _frm : DWORD {
			e_na      = 0,
			eInfinite = (DWORD)-1
		};
	protected:
		volatile DWORD     m_nTimeSlice;    // tima space in milliseconds;
		volatile DWORD     m_nTimeFrame;    // total time to wait for;
	protected:
		DWORD    m_nCurrent;                // current time;
	public:
		CDelayEvent(const DWORD nTimeSlice, const DWORD nTimeFrame);
		virtual ~CDelayEvent(void);
	public:
		virtual bool Elapsed(void) const;
		virtual bool IsReset(void) const;
		virtual VOID Reset  (const DWORD nTimeSlice = _frm::e_na, const DWORD nTimeFrame = _frm::e_na);
		virtual VOID Wait   (void);

	public:
		CDelayEvent& operator= (const DWORD) ; // sets time frame value for waiting for;
	};
}}

#endif/*_SHAREDLITELIBGENERICEVENT_H_DF255B0D_D53A_4917_B1EC_8F7D231AA515_INCLUDED*/