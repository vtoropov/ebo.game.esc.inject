#ifndef _SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED
#define _SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Feb-2019 at 6:52:05a, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is FIX Engine shared library configuration command line interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to sound-bin-trans project on 5-Apr-2019 at 11:52:11a, UTC+7, Phuket, Rawai, Thursday;
	Adopted to serial port project on 21-May-2019 at 7:12:03p, UTC+7, Phuket, Rawai, Tuesday;
	Adopted to RDP automation project on 3-Jun-2019 at 2:01:26p, UTC+7, Seoul, INC airport, Monday;
	Adopted to shared memory project on 8-Jun-2019 at 1:34:01a, UTC+7, Novosibirsk, Tulenina, Saturday;
	Adopted to FakeGPS driver project on 13-Dec-2019 at 9:43:27a, UTC+7, Novosibirsk, Tulenina, Friday;
	Adopted to File Touch app on 2-Feb-2020 at 8:15:18p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/

namespace shared { namespace common {

	typedef ::std::map<::ATL::CAtlString, ::ATL::CAtlString> TCmdLineArgs;

	class CCommandLine {
	private:
		CAtlString   m_module_full_path;
		TCmdLineArgs m_args;
	public:
		 CCommandLine(void);
		~CCommandLine(void);
	public:
		HRESULT      Append(LPCTSTR _lp_sz_nm, LPCTSTR _lp_sz_val);
		CAtlString   Arg   (LPCTSTR _lp_sz_nm) const;
		LONG         Arg   (LPCTSTR _lp_sz_nm, const LONG _def_val)const;
		TCmdLineArgs Args  (void)const;     // returns a copy of command line argument collection
		VOID         Clear (void)     ;
		INT          Count (void)const;
		bool         Has   (LPCTSTR pszArgName)const;
		CAtlString   ModuleFullPath(void)const;
		CAtlString   ToString (LPCTSTR _lp_sz_sep = NULL) const;
	};
}}

#endif/*_SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED*/