/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Nov-2014 at 0:42:03am, GMT+3, Taganrog, Saturday;
	This is Shared Lite Library Generic Handle class implementation file.
	-----------------------------------------------------------------------------
	Adopted to File Touch project on 7-Feb-2020 at 9:37:54a, UTC+7, Novosibirsk, Tulenina, Friday;
	Adopted to Ebo Game Dyna Inject 29-Feb-2020 at 9:14:36a, UTC+7, Novosibirsk, Tulenina, Saturday;
*/
#include "StdAfx.h"
#include "shared.gen.han.h"

using namespace shared::common;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace common { namespace _impl
{
	static HRESULT  Shared_CloseHandleSafe(HANDLE& handle_)
	{
		if (!handle_ || INVALID_HANDLE_VALUE == handle_)
			return E_INVALIDARG;
		if (!::CloseHandle(handle_))
		{
			return HRESULT_FROM_WIN32(::GetLastError());
		}
		handle_ = NULL;
		return S_OK;
	}

	static HANDLE&  Shared_GetInvalidHandleRef(void)
	{
		static HANDLE handle = INVALID_HANDLE_VALUE;
		return handle;
	}
}}}
using namespace shared::common::_impl;
/////////////////////////////////////////////////////////////////////////////

CAutoHandle:: CAutoHandle(void): m_handle(NULL) {}
CAutoHandle:: CAutoHandle(const HANDLE handle_): m_handle(handle_) {}
CAutoHandle::~CAutoHandle(void) {
	Shared_CloseHandleSafe(m_handle);
}

/////////////////////////////////////////////////////////////////////////////

CAutoHandle& CAutoHandle::operator=(const HANDLE h_)
{
	Shared_CloseHandleSafe(m_handle);
	m_handle = h_;
	return *this;
}

CAutoHandle::operator HANDLE(void) { return m_handle; }
CAutoHandle::operator HANDLE(void) const { return m_handle; }

/////////////////////////////////////////////////////////////////////////////

PHANDLE    CAutoHandle::operator&(void) { return &m_handle; }

/////////////////////////////////////////////////////////////////////////////

HANDLE     CAutoHandle::Handle (void) const { return m_handle; }
bool       CAutoHandle::IsValid(void) const {
	return (NULL != m_handle && INVALID_HANDLE_VALUE != m_handle);
}

void       CAutoHandle::Reset(void) {
	Shared_CloseHandleSafe(m_handle);
}

/////////////////////////////////////////////////////////////////////////////

CAutoHandleArray::CAutoHandleArray(const DWORD dwSize) : m_handles(NULL), m_size(0) {
	m_error << __MODULE__ << S_OK;
	if (dwSize) {
		try {
			m_handles = new HANDLE[dwSize];
			m_size = dwSize;
		}
		catch(::std::bad_alloc&){ m_error = E_OUTOFMEMORY; }
	}
	else
		m_error = E_INVALIDARG;
}

CAutoHandleArray::~CAutoHandleArray(void)
{
	if (m_handles) {
		try { delete [] m_handles; m_handles = NULL; } catch(...){}
	}
	m_size = 0;
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef  CAutoHandleArray::Error(void) const { return m_error;}

bool       CAutoHandleArray::IsValid(void)const {
	return (NULL != m_handles && m_size);
}

PHANDLE    CAutoHandleArray::Handles(void)const { return m_handles; }
DWORD      CAutoHandleArray::Size(void)const { return m_size; }

HANDLE     CAutoHandleArray::operator[] (const INT nIndex) const {
	return (!this->IsValid() || 0 > nIndex ||  INT(m_size - 1) < nIndex ? 0 : m_handles[nIndex]);
}

HANDLE&    CAutoHandleArray::operator[] (const INT nIndex) {
	return (!this->IsValid() || 0 > nIndex ||  INT(m_size - 1) < nIndex ? Shared_GetInvalidHandleRef() : m_handles[nIndex]);
}