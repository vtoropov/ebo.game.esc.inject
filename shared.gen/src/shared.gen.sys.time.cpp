/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Jan-2016 at 2:49:52am, GMT+7, Phuket, Rawai, Sunday;
	This is (thefileguardian.com) shared lite library date & time related class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to File Touch project on 7-Feb-2020 at 9:55:50a, UTC+7, Novosibirsk, Tulenina, Friday;
	Adopted to Ebo Game Dyna Inject 29-Feb-2020 at 10:54:24a, UTC+7, Novosibirsk, Tulenina, Saturday;
*/
#include "StdAfx.h"
#include "shared.gen.sys.time.h"

using namespace shared::com_ex;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace com_ex { namespace details
{
	bool DateTime_IsEqual(const SYSTEMTIME& _t0, const SYSTEMTIME& _t1, const WORD _msec_threshold)
	{
		if (_t0.wYear   != _t1.wYear  ) return false;
		if (_t0.wMonth  != _t1.wMonth ) return false;
		if (_t0.wDay    != _t1.wDay   ) return false;
		if (_t0.wHour   != _t1.wHour  ) return false;
		if (_t0.wMinute != _t1.wMinute) return false;
		if (_t0.wSecond != _t1.wSecond) return false;
		if (_t0.wMilliseconds > _t1.wMilliseconds
		 && _t0.wMilliseconds - _t1.wMilliseconds > _msec_threshold)
				return false;
		if (_t1.wMilliseconds > _t0.wMilliseconds
		 && _t1.wMilliseconds - _t0.wMilliseconds > _msec_threshold)
				return false;

		return true;
	}

	bool DateTime_LeftLaterRight(const SYSTEMTIME& _t0, const SYSTEMTIME& _t1, const WORD _msec_threshold)
	{
		if (_t0.wYear   >  _t1.wYear) return true;
		if (_t0.wYear   == _t1.wYear   && _t0.wMonth  > _t1.wMonth ) return true;
		if (_t0.wMonth  == _t1.wMonth  && _t0.wDay    > _t1.wDay   ) return true;
		if (_t0.wDay    == _t1.wDay    && _t0.wHour   > _t1.wHour  ) return true;
		if (_t0.wHour   == _t1.wHour   && _t0.wMinute > _t1.wMinute) return true;
		if (_t0.wMinute == _t1.wMinute && _t0.wSecond > _t1.wSecond) return true;

		if (_t0.wMilliseconds >= _t1.wMilliseconds
		 && _t0.wMilliseconds  - _t1.wMilliseconds <= _msec_threshold)
			return true;

		if (_t1.wMilliseconds  > _t0.wMilliseconds
		 && _t1.wMilliseconds  - _t0.wMilliseconds <= _msec_threshold)
			return true;

		return false;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CSystemTime::CSystemTime(void) {
	::memset((SYSTEMTIME*)this, 0, sizeof(SYSTEMTIME));
}

CSystemTime::CSystemTime(const FILETIME& _ft)
{
	if (_ft.dwHighDateTime && _ft.dwLowDateTime)
		::FileTimeToSystemTime(&_ft, (SYSTEMTIME*)this);
}

CSystemTime::CSystemTime(const SYSTEMTIME& _time) { *this = _time;}

/////////////////////////////////////////////////////////////////////////////

bool        CSystemTime::IsValid(void)const
{
	return (TBase::wYear != 0 && TBase::wYear != 1601 && TBase::wMonth != 0 && TBase::wDay != 0 && TBase::wMilliseconds != 0);
}

/////////////////////////////////////////////////////////////////////////////

CSystemTime& CSystemTime::operator= (const SYSTEMTIME& _time)
{
	const DWORD size_ = sizeof(SYSTEMTIME);
	::memcpy_s(
			this  ,
			size_ ,
			&_time,
			size_
		);
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CDateTime::CDateTime(void) {
	::GetLocalTime(&m_time);
}

CDateTime::CDateTime(const SYSTEMTIME& _st) {
	::memcpy_s(&m_time, sizeof(SYSTEMTIME), &_st, sizeof(SYSTEMTIME));
}

CDateTime::CDateTime(const time_t _time)
{
	::memset(&m_time, 0, sizeof(SYSTEMTIME));

	LONGLONG ll_ = Int32x32To64(_time, 10000000) + 116444736000000000;
	FILETIME ft_ = {0};
	ft_.dwLowDateTime  = (DWORD)ll_;
	ft_.dwHighDateTime = ll_ >> 32;

	::FileTimeToSystemTime(&ft_, &m_time);
}

/////////////////////////////////////////////////////////////////////////////

SYSTEMTIME  CDateTime::GetCurrent(void)const { return m_time; }

CAtlString  CDateTime::GetTimeAsUnix(void) const {
	// YYYY-MM-DDThh:mm:ss.sTZD (eg 1997-07-16T19:20:30.45+01:00)
	// http://www.w3.org/TR/NOTE-datetime
	static LPCTSTR p_date = _T("%04d-%02d-%02dT%02d:%02d:%02d.%02d%s%02d:%02d");

	TIME_ZONE_INFORMATION tzi = {0};
	::GetTimeZoneInformation(&tzi);

	LONG l_bias = tzi.Bias;

	const LONG l_bias_h = abs(l_bias) / 60;
	const LONG l_bias_m = abs(l_bias) % 60;

	::ATL::CAtlString cs_timestamp;
	cs_timestamp.Format(
			p_date        ,
			m_time.wYear  ,
			m_time.wMonth ,
			m_time.wDay   ,
			m_time.wHour  ,
			m_time.wMinute,
			m_time.wSecond,
			(WORD)m_time.wMilliseconds/10 , // needs only two digits, not three ones
			l_bias < 0 ? _T("+") : _T("-"), // UTC = local time + bias, i.e. for time zones after GMT the bias is negative; 
			l_bias_h,
			l_bias_m
		);
	return cs_timestamp;
}

VOID        CDateTime::Refresh(void) { ::GetLocalTime(&m_time); }

/////////////////////////////////////////////////////////////////////////////

CDateTime&  CDateTime::operator= (const CDateTime& _rh_ref) {

	const DWORD size_ = sizeof(SYSTEMTIME);
	::memcpy_s(
		&this->m_time, size_ , &_rh_ref.m_time, size_
	);
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

INT    CDateTime::Compare(const FILETIME& _t0, const FILETIME& _t1, const WORD _msec_threshold)
{
	SYSTEMTIME t0_ = {0}; ::FileTimeToSystemTime(&_t0, &t0_);
	SYSTEMTIME t1_ = {0}; ::FileTimeToSystemTime(&_t1, &t1_);

	const INT result_ = CDateTime::Compare(t0_, t1_, _msec_threshold);
	return result_;
}

INT    CDateTime::Compare(const SYSTEMTIME& _t0, const SYSTEMTIME& _t1, const WORD _msec_threshold)
{
	if (details::DateTime_IsEqual(_t0, _t1, _msec_threshold))
		return  0;
	if (details::DateTime_LeftLaterRight(_t0, _t1, _msec_threshold))
		return -1;
	else
		return  1;
}

time_t CDateTime::FileTimeToUnix(const FILETIME& _ftime)
{
	SYSTEMTIME st_ = {0};
	::FileTimeToSystemTime(&_ftime, &st_);

	time_t result_ = CDateTime::SystemTimeToUnix(st_);
	return result_;
}

time_t CDateTime::SystemTimeToUnix(const SYSTEMTIME& _st)
{
	struct tm tmp_ = {0};
	tmp_.tm_mday   = _st.wDay;
	tmp_.tm_mon    = _st.wMonth - 1;
	tmp_.tm_year   = _st.wYear  - 1900;

	tmp_.tm_sec    = _st.wSecond;
	tmp_.tm_min    = _st.wMinute;
	tmp_.tm_hour   = _st.wHour;

	time_t result_ = std::mktime(&tmp_);
	return result_;
}