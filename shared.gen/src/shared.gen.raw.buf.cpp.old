/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Mar-2014 at 8:55:09am, GMT+3, Taganrog, Wednesday;
	This is Shared Lite Library raw data class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 12:00:40p, UTC+7, Phuket, Rawai, Monday;
	Adopted to sound-bin-trans project on 14-Apr-2019 at 0:05:11a, UTC+7, Phuket, Rawai, Sunday;
	Adopted to Ebo Game Dyna Inject 29-Feb-2020 at 9:06:55a, UTC+7, Novosibirsk, Tulenina, Saturday;
*/
#include "StdAfx.h"
#include "shared.gen.raw.buf.h"

using namespace shared::common;
/////////////////////////////////////////////////////////////////////////////

CRawBuffer::CRawBuffer(void): m_pData(NULL), m_dwSize(0) { m_error << __MODULE__ << OLE_E_BLANK; }

/////////////////////////////////////////////////////////////////////////////

CRawData:: CRawData(void) : TBuffer() {}
CRawData:: CRawData(const _variant_t& _data) : TBuffer() {

	if (_data.vt == VT_BSTR)
	{
		this->Create(
				reinterpret_cast<PBYTE>(_data.bstrVal),
				static_cast<DWORD>(::SysStringByteLen(_data.bstrVal))
			);
	}
}

CRawData:: CRawData(const DWORD dwSize) : TBuffer() { this->Create(dwSize);}

CRawData:: CRawData(const PBYTE pData, const DWORD dwSize) : TBuffer() { this->Create(pData, dwSize); }

CRawData::~CRawData(void) { this->Reset(); }

/////////////////////////////////////////////////////////////////////////////

HRESULT     CRawData::Append (const CRawData& _raw_data) {
	m_error << __MODULE__ << S_OK;
	HRESULT hr_ = this->Append(
			_raw_data.GetData(),
			_raw_data.GetSize()
		);
	return hr_;
}

HRESULT     CRawData::Append(const PBYTE pData, const DWORD dwSize) {
	m_error << __MODULE__ << S_OK;
	if (NULL == pData || 0 == dwSize)
		return (m_error = E_INVALIDARG);

	PBYTE pCurrentLocator = NULL;

	if (this->IsValid())
	{
		const DWORD dwNewSize = m_dwSize + dwSize;
		pCurrentLocator = reinterpret_cast<PBYTE>(
					::HeapReAlloc(::GetProcessHeap(), HEAP_ZERO_MEMORY, m_pData, dwNewSize)
				);
		if (pCurrentLocator)
		{
			m_pData = pCurrentLocator;
			pCurrentLocator += m_dwSize;
			m_dwSize = dwNewSize;
		}
		else
			m_error = E_OUTOFMEMORY;
	}
	else {
		m_dwSize = dwSize;
		m_pData = reinterpret_cast<PBYTE>(
					::HeapAlloc(::GetProcessHeap(), HEAP_ZERO_MEMORY, dwSize)
				);
		if (m_pData)
			pCurrentLocator = m_pData;
		else
			m_error = E_OUTOFMEMORY;
	}
	if (m_error == false)
	{
		const errno_t err_ = ::memcpy_s(pCurrentLocator, dwSize, pData, dwSize);
		if (err_)
			m_error = E_OUTOFMEMORY;
	}
	return m_error;
}

HRESULT     CRawData::CopyToVariantAsArray(_variant_t& v_data) const {
	m_error << __MODULE__ << S_OK;

	if (!this->IsValid())
		return (m_error = OLE_E_BLANK);

	if (VT_EMPTY != v_data.vt)v_data.Clear();

	v_data.vt     = VT_ARRAY|VT_UI1;
	v_data.parray = NULL;

	SAFEARRAYBOUND bound[1] ={0};
	bound[0].lLbound        = 0;
	bound[0].cElements      = m_dwSize;

	v_data.parray = ::SafeArrayCreate(VT_UI1, 1, bound);
	if (!v_data.parray)
		return (m_error = E_OUTOFMEMORY);

	UCHAR HUGEP* pData = NULL;
	HRESULT hr_ = ::SafeArrayAccessData(v_data.parray, (void HUGEP**)&pData);
	if (SUCCEEDED(hr_))
	{
		const errno_t err_no = ::memcpy_s((void*)pData, m_dwSize, (const void*)v_data.parray, m_dwSize);
		if (err_no)
			m_error = E_OUTOFMEMORY;
		::SafeArrayUnaccessData(v_data.parray);
		pData = NULL;
	}

	if (m_error == true) {
		::SafeArrayDestroy(v_data.parray);
		v_data.parray = NULL;
		v_data.vt = VT_EMPTY;
	}

	return m_error;
}

HRESULT     CRawData::CopyToVariantAsUtf16(_variant_t& v_data) const {
	m_error << __MODULE__ << S_OK;

	if (!this->IsValid())
		return (m_error = OLE_E_BLANK);

	if (VT_EMPTY != v_data.vt)v_data.Clear();

	USES_CONVERSION;
	v_data.Clear();
	v_data.vt = VT_BSTR;
	v_data.bstrVal = ::SysAllocStringLen(
				::A2BSTR(reinterpret_cast<LPCSTR>(m_pData)),
				static_cast<UINT>(m_dwSize)
			);
	if (NULL == v_data.bstrVal) {
		v_data.vt = VT_EMPTY; m_error = E_OUTOFMEMORY;
	}
	return m_error;
}

HRESULT     CRawData::CopyToVariantAsUtf8 (_variant_t& v_data)const {
	m_error << __MODULE__ << S_OK;

	if (!this->IsValid())
		return (m_error = OLE_E_BLANK);

	if (VT_EMPTY != v_data.vt)v_data.Clear();

	v_data.vt = VT_BSTR;
	v_data.bstrVal = ::SysAllocStringByteLen((LPCSTR)m_pData, m_dwSize);

	if (v_data.bstrVal)
		m_error = S_OK;
	else {
		v_data.vt = VT_EMPTY; m_error = E_OUTOFMEMORY;
	}
	return m_error;
}

HRESULT     CRawData::Create(const DWORD dwSize) {
	m_error << __MODULE__ << S_OK;

	if (this->IsValid())
		this->Reset();

	if (dwSize) {
		m_pData = reinterpret_cast<PBYTE>(
					::HeapAlloc(::GetProcessHeap(), HEAP_ZERO_MEMORY, dwSize)
				);
		if (NULL == m_pData)
			m_error  = ::GetLastError();
		else {
			m_dwSize = dwSize;
		}
	}
	return m_error;
}

HRESULT     CRawData::Create(const PBYTE pData, const DWORD dwSize) {
	m_error << __MODULE__ << S_OK;
	this->Reset();
	return this->Append(pData, dwSize);
}

TErrorRef   CRawData::Error  (void) const { return m_error ; }
LPBYTE      CRawData::Detach (void) {
	m_error << __MODULE__ << S_OK;
	if (this->IsValid() == false) {
		m_error = OLE_E_BLANK; return NULL;
	}
	LPBYTE p_ret = TBuffer::m_pData; TBuffer::m_pData = NULL; TBuffer::m_dwSize = 0; m_error = OLE_E_BLANK;
	return p_ret;
}
const PBYTE CRawData::GetData(void) const { return m_pData ; }
PBYTE       CRawData::GetData(void)       { return m_pData ; }
DWORD       CRawData::GetSize(void) const { return m_dwSize; }
bool        CRawData::IsValid(void) const { return (NULL != m_pData && 0 != m_dwSize); }

HRESULT     CRawData::Reset(void) {
	m_error << __MODULE__ << S_OK;
	m_dwSize = 0;
	if (m_pData) {
		if (!::HeapFree(::GetProcessHeap(), 0, m_pData))
			m_error = ::GetLastError();

		m_pData = NULL;
		if (m_error)
			return m_error;
	}
	(m_error = OLE_E_BLANK) = _T("No data");
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT     CRawData::ToStringUtf16(CAtlStringW& _utf16)const {
	m_error << __MODULE__ << S_OK;

	if (this->IsValid() == false)
		return (m_error = OLE_E_BLANK);

	_utf16.Append(
			reinterpret_cast<LPCTSTR>(this->GetData()),
			static_cast<INT>(this->GetSize()/sizeof(TCHAR))
		);

	return m_error;
}

HRESULT     CRawData::ToStringUtf8 (CAtlStringA& _utf8)const {
	m_error << __MODULE__ << S_OK;

	if (this->IsValid() == false)
		return (m_error = OLE_E_BLANK);

	_utf8.Append(
			reinterpret_cast<LPCSTR>(this->GetData()),
			static_cast<INT>(this->GetSize())
		);

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CRawData::operator const    PBYTE(void)const { return m_pData; }
CRawData::operator          PBYTE(void)      { return m_pData; }

/////////////////////////////////////////////////////////////////////////////

CRawData::CRawData(const CRawData& raw_ref)  { *this = raw_ref;}

CRawData& CRawData::operator+=(const CRawData& raw_ref) { this->Append( raw_ref ); return *this; }
CRawData& CRawData::operator= (const CRawData& raw_ref) { this->Create(raw_ref.GetData(), raw_ref.GetSize()); return *this; }

CRawData& CRawData::operator= (const _variant_t& _data) {
	if (_data.vt == VT_BSTR)
	{
		this->Create(
				reinterpret_cast<PBYTE>(_data.bstrVal),
				static_cast<DWORD>(::SysStringByteLen(_data.bstrVal))
			);
	}
	return *this;
}

CRawData& CRawData::operator= (const ::std::vector<BYTE>& _vec) {
	if (_vec.empty() == true)
		return *this;

	this->Create(
			(const PBYTE)_vec.data(), static_cast<DWORD>(_vec.size()) * sizeof(BYTE)
		);
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CSafeArrayBase::CSafeArrayBase(void) : m_psa(NULL), m_data_ptr(NULL) { m_error << __MODULE__ << S_OK; }

/////////////////////////////////////////////////////////////////////////////

TErrorRef CSafeArrayBase::Error  (void) const { return m_error; }
bool      CSafeArrayBase::IsValid(void) const { return (m_error.Result() == S_OK); }

/////////////////////////////////////////////////////////////////////////////

CSafeArrayEx::CElements::CElements(const CSafeArrayBase& _data_ref) : m_data_ref(_data_ref), m_lbound(0), m_ubound(0) {}

/////////////////////////////////////////////////////////////////////////////

LONG      CSafeArrayEx::CElements::Count(void)const {

	UINT count_ = 0;
	if (m_data_ref.IsValid() == false)
		return count_;

	HRESULT hr_ = S_OK;

	m_lbound = 0;
	hr_ = ::SafeArrayGetLBound(m_data_ref.m_psa, 1, &m_lbound);
	if (FAILED(hr_))
		return count_;

	m_ubound = 0;
	hr_ = ::SafeArrayGetUBound(m_data_ref.m_psa, 1, &m_ubound);
	if (FAILED(hr_))
		return count_;

	count_ = (m_ubound - m_lbound + 1);

	return count_;
}

HRESULT   CSafeArrayEx::CElements::Item (const LONG _ndx, _variant_t& _item) const {

	HRESULT hr_ = S_OK;

	if (_ndx + m_lbound < m_lbound) hr_ = DISP_E_BADINDEX;
	if (_ndx + m_lbound > m_ubound) hr_ = DISP_E_BADINDEX;

	if (FAILED(hr_))
		return (m_data_ref.m_error = hr_);

	const VARTYPE type_ = this->Type();

	switch (type_) {
	case VT_BSTR :
		{
			LONG ndx_ = _ndx + m_lbound;

			_item.Clear();
			hr_ = ::SafeArrayGetElement(
					m_data_ref.m_psa, &ndx_, &_item.bstrVal
				);
			if (SUCCEEDED(hr_))
				_item.vt = VT_BSTR;

		} break;
	default:
		m_data_ref.m_error = DISP_E_BADVARTYPE;
	}

	return  hr_;
}

VARTYPE   CSafeArrayEx::CElements::Type(void) const {

	VARTYPE vType = VT_EMPTY;

	if (m_data_ref.IsValid() == false)
		return vType;

	HRESULT hr_ = ::SafeArrayGetVartype(m_data_ref.m_psa, &vType);
	if (FAILED(hr_))
		vType = VT_EMPTY;

	return vType;
}

/////////////////////////////////////////////////////////////////////////////

CSafeArrayEx:: CSafeArrayEx(const _variant_t& var_ref) : m_elements(*this) {
	m_error << __MODULE__ << S_OK;

	if (!(VT_ARRAY & var_ref.vt) || !(VT_UI1 & var_ref.vt)) {
		m_error = DISP_E_TYPEMISMATCH;
	}
	else if (!var_ref.parray) {
		m_error = E_INVALIDARG;
	}
	else {
		HRESULT hr_ = ::SafeArrayAccessData(var_ref.parray, (void HUGEP**)&m_data_ptr);
		if (FAILED(hr_))
			m_error = hr_;
		else
			m_psa = var_ref.parray;
	}
}

CSafeArrayEx:: CSafeArrayEx(SAFEARRAY* psa) : m_elements(*this) {
	m_error << __MODULE__ << S_OK;

	if (psa) {
		HRESULT hr_ = ::SafeArrayAccessData(psa, (void HUGEP**)&m_data_ptr);
		if (FAILED(hr_))
			m_error = hr_;
		else
			m_psa = psa;
	}
	else
		m_error = E_INVALIDARG;
}

CSafeArrayEx::~CSafeArrayEx(void) {
	if (NULL != m_psa) {
		::SafeArrayUnaccessData(m_psa);
		m_psa = NULL;
	}
	m_data_ptr = NULL;
}

/////////////////////////////////////////////////////////////////////////////
const
CSafeArrayBase&
          CSafeArrayEx::Base (void) const { return *this; }
PBYTE     CSafeArrayEx::Data (void)       { return reinterpret_cast<PBYTE>(m_data_ptr); }
const
CSafeArrayEx::CElements&
          CSafeArrayEx::Elements(void) const { return m_elements; }
HRESULT   CSafeArrayEx::Size(LONG& n_size_ref) const {
	m_error << __MODULE__ << S_OK;

	if (this->IsValid() == false)
		return (m_error = OLE_E_BLANK);

	HRESULT hr_ = S_OK;
	n_size_ref  = 0;
	{
		const UINT n_el_size = ::SafeArrayGetElemsize(m_psa);
		if (0 == n_el_size)
			return (m_error = (DWORD)ERROR_INVALID_DATA);

		LONG n_lbound = 0;
		hr_ = ::SafeArrayGetLBound(m_psa, 1, &n_lbound);
		if (FAILED(hr_))
			return (m_error = hr_);

		LONG n_ubound = 0;
		hr_ = ::SafeArrayGetUBound(m_psa, 1, &n_ubound);
		if (FAILED(hr_))
			return (m_error = hr_);

		n_size_ref = (n_ubound - n_lbound + 1) * (LONG)n_el_size;
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CSafeArrayEx::operator const CSafeArrayBase& (void) const { return this->Base(); }