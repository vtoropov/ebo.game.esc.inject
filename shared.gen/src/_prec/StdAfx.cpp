/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Apr-2019 at 8:08:27a, UTC+7, Phuket, Rawai, Wednesday;
	This is generic shared library precompiled headers implementation file.
*/
#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)