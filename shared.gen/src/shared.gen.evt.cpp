/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Aug-2014 at 8:04:23pm, GMT+3, Taganrog, Wednesday;
	This is Shared Lite Library Generic Event class implementation file.
	-----------------------------------------------------------------------------
	Adopted to Fake GPS project on 11-Dec-2019 at 8:02:19p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	Adopted to File Touch project on 6-Feb-2020 at 1:12:07a, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "StdAfx.h"
#include "shared.gen.evt.h"
#include "shared.gen.sys.err.h"

using namespace shared::runnable;
using namespace shared::sys_core;

/////////////////////////////////////////////////////////////////////////////

CMarshaller::CMessageHandler::CMessageHandler(IGenericEventNotify& sink_ref, const _variant_t& v_evt_id):
	m_sink_ref(sink_ref),
	m_event_id(v_evt_id),
	m_threadId(::GetCurrentThreadId()) {
}

CMarshaller::CMessageHandler::~CMessageHandler(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT  CMarshaller::CMessageHandler::OnGenericEventNotify(UINT, WPARAM, LPARAM, BOOL&)
{
	m_sink_ref.GenericEvent_OnNotify(m_event_id);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

DWORD    CMarshaller::CMessageHandler::_OwnerThreadId(void)const { return m_threadId; }

/////////////////////////////////////////////////////////////////////////////

CMarshaller:: CMarshaller(IGenericEventNotify& sink_ref, const _variant_t& v_evt_id):
	m_handler(sink_ref, v_evt_id) {}

CMarshaller::~CMarshaller(void) { Destroy();}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CMarshaller::Create(void)
{
	if (m_handler.IsWindow())
		return S_OK;
	m_handler.Create(HWND_MESSAGE);
	return (m_handler.IsWindow() ? S_OK : HRESULT_FROM_WIN32(::GetLastError()));
}

HRESULT  CMarshaller::Destroy(void)
{
	if (!m_handler.IsWindow())
		return S_OK;
	if (m_handler.DestroyWindow())
		m_handler.m_hWnd = NULL;
	else
	{
		CError err_obj; err_obj = (::GetLastError());
		ATLASSERT(0);
	}
	return S_OK;
}

HWND     CMarshaller::GetHandle_Safe(void) const
{
	return (m_handler.IsWindow() ? m_handler.m_hWnd : NULL);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CMarshaller::Fire(const bool bAsynch)
{
	const HWND hHandler = this->GetHandle_Safe();
	return CMarshaller::Fire(hHandler, bAsynch);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CMarshaller::Fire2(void)
{
	const HWND  hHandler = this->GetHandle_Safe();
	if (NULL == hHandler)
		return OLE_E_INVALIDHWND;
	if (!::PostThreadMessage(
			m_handler._OwnerThreadId(),
			CMarshaller::eInternalMsgId,
			NULL,
			NULL
		))
		return __HRESULT_FROM_WIN32(::GetLastError());
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CMarshaller::Fire(const HWND hHandler, const bool bAsynch)
{
	if (!::IsWindow(hHandler))
		return OLE_E_INVALIDHWND;
	if (bAsynch)
		::PostMessage(hHandler, CMarshaller::eInternalMsgId, (WPARAM)0, (LPARAM)0);
	else
		::SendMessage(hHandler, CMarshaller::eInternalMsgId, (WPARAM)0, (LPARAM)0);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CDelayEvent:: CDelayEvent(const DWORD nTimeSlice, const DWORD nTimeFrame):
	m_nTimeSlice(nTimeSlice), m_nTimeFrame(nTimeFrame), m_nCurrent(0) {
	ATLASSERT(m_nTimeSlice);
	ATLASSERT(m_nTimeFrame);
}

CDelayEvent::~CDelayEvent(void) {}

/////////////////////////////////////////////////////////////////////////////

bool CDelayEvent::Elapsed(void) const { return (m_nCurrent >= m_nTimeFrame); }
bool CDelayEvent::IsReset(void) const { return (m_nCurrent == 0); }

VOID CDelayEvent::Reset  (const DWORD nTimeSlice, const DWORD nTimeFrame)
{
	if (0 < nTimeSlice) m_nTimeSlice = nTimeSlice; ATLASSERT(m_nTimeSlice);
	if (0 < nTimeFrame) m_nTimeFrame = nTimeFrame; ATLASSERT(m_nTimeFrame);

	m_nCurrent = 0;
}

void CDelayEvent::Wait(void) {
	::Sleep(m_nTimeSlice);
	m_nCurrent += m_nTimeSlice;
}

/////////////////////////////////////////////////////////////////////////////

CDelayEvent& CDelayEvent::operator= (const DWORD _v) { if (_v) { m_nTimeFrame = _v; }  return *this; }