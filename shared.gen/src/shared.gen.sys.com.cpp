/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Mar-2014 at 10:52:48am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite library operating system core interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 11-Jul-2018 at 10:29:17a, UTC+7, Phuket, Rawai, Wednesday;
	Adopted to sound-bin-trans project on 8-Apr-2019 at 1:09:29p, UTC+7, Phuket, Rawai, Monday;
	Adopted to FakeGPS driver project on 13-Dec-2019 at 10:18:13a, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "StdAfx.h"
#include "shared.gen.sys.com.h"

using namespace shared::sys_core;
/////////////////////////////////////////////////////////////////////////////

CCoIniter:: CCoIniter(const bool bMultiThreaded) {
	m_error << __MODULE__ << S_OK;
	if (bMultiThreaded) {
		m_error = ::CoInitializeEx(0, COINIT_MULTITHREADED);
	}
	else {
		m_error = ::CoInitialize(NULL);
	}
}

CCoIniter:: CCoIniter(const DWORD _flag) {
	m_error << __MODULE__ << S_OK;
	m_error = ::CoInitializeEx(0, _flag);
}

CCoIniter::~CCoIniter(void) {
	if (IsSuccess()) {
		::CoUninitialize();
	}
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef  CCoIniter::Error(void) const     { return ( m_error); }
bool       CCoIniter::IsSuccess(void) const { return (!m_error); }

/////////////////////////////////////////////////////////////////////////////

CCoIniter::operator const bool (void) const { return this->IsSuccess(); }

/////////////////////////////////////////////////////////////////////////////

CCoApartmentThreaded::CCoApartmentThreaded(void):TBase((DWORD)COINIT_APARTMENTTHREADED) {}

/////////////////////////////////////////////////////////////////////////////

CCoSecurityProvider::CCoSecurityProvider(void) { m_error << __MODULE__ << S_OK; }

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CCoSecurityProvider::Error(VOID) CONST { return m_error; }
HRESULT     CCoSecurityProvider::Init(const CCoSecAuthLevel::_e _lvl, const CCoSecImpLevel::_e _imp) {
	m_error << __MODULE__ << S_OK;
	m_error = S_OK;

	HRESULT hr_ = ::CoInitializeSecurity(
		NULL     ,                   // security descriptor;
		-1       ,                   // COM negotiates authentication service;
		NULL     ,                   // authentication services;
		NULL     ,                   // reserved;
		_lvl     ,
		_imp     ,
		NULL     ,
		EOAC_NONE,
		NULL
	);
	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}

HRESULT     CCoSecurityProvider::InitDefault(void) { return this->Init(CCoSecAuthLevel::eDefault, CCoSecImpLevel::eImpersonate); }
HRESULT     CCoSecurityProvider::InitNoIdentity(void) { return this->Init(CCoSecAuthLevel::eNone, CCoSecImpLevel::eIdentity);}