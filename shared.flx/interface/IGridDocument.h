#ifndef _FGRIDDOCUMENT_H_F5C5AA48_823D_45E9_81BA_338DB94BB882_INCLUDED
#define _FGRIDDOCUMENT_H_F5C5AA48_823D_45E9_81BA_338DB94BB882_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Feb-2020 at 5:35:58a, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is Flex grid control cell document layer composition interface declaration file.
*/
namespace UILayer { namespace ext {

	enum eElementType {
		e_Text  = 0,  // default;       | no event source
		e_Web   = 1,  // web link;      | event on click
		e_Email = 2,  // email address; | event on click
		e_Image = 3,  // image;         | event on click
	};

	interface IDocElement {
		virtual UINT         Id   (void) const           PURE;
		virtual HRESULT      Id   (const UINT)           PURE;
		virtual HRESULT      Image(const WORD _w_res_id) PURE;
		virtual HRESULT      Text (LPCTSTR)              PURE;
		virtual HRESULT      Text (const WORD _w_res_id) PURE;
		virtual eElementType Type (void) const           PURE;
		virtual HRESULT      Type (const eElementType)   PURE;
	};

	interface IDocLayer {// a layer that is container for elements, which are resided horizontally;
		virtual HRESULT      Append (const eElementType) PURE;
		virtual IDocElement& Element(const UINT _ndx)    PURE;
		virtual UINT         ElementCount(void) const    PURE;
	};

	interface IDocument {
		virtual HRESULT      Append(void)                PURE;
		virtual IDocLayer&   Layer (const UINT _ndx)     PURE;
		virtual UINT         LayerCount(void) const      PURE; // get layers count;
	};
}}

#endif/*_FGRIDDOCUMENT_H_F5C5AA48_823D_45E9_81BA_338DB94BB882_INCLUDED*/