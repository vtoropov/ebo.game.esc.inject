#ifndef _IGRIDINTERFACETHM_H_30E0F8AE_E008_4E4D_B3C4_147E67FC62C9_INCLUDED
#define _IGRIDINTERFACETHM_H_30E0F8AE_E008_4E4D_B3C4_147E67FC62C9_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Nov-2019 at 7:21:52p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack flex grid control theme interface declaration file.
*/
#include "IGridInterface.fmt.h"

#ifndef RGBA
#define RGBA(r,g,b,a)	( RGB(r,g,b) | (((DWORD)(BYTE)(a))<<24) )
#endif
#ifndef GetAValue
#define GetAValue(_clr)	( 0 == LOBYTE((_clr)>>24) ? 255 : LOBYTE((_clr)>>24) )
#endif

namespace UILayer { namespace ext {

	enum eThemeAttribute
	{
		eAttBlinkBack    =  0,
		eAttBlinkFore    =  1,
		eAttSolidBack    =  2,
		eAttSolidFore    =  3,
		eAttSolidGrid    =  4,
		eAttStatusFore   =  5,
		/*---extended attribute set---*/
		eAttDrawGradient =  6, // drawing gradient mode {1 - on | 0 - off}
		eAttDrawGradType =  7, // gradient type {0 - vertical | 1 - horizontal | 2 - stripes}
		eAttDrawHorzDivs =  8, // drawing horizontal lines {1 - on | 0 - off}
		eAttDrawVertDivs =  9, // drawing vertical lines {1 - on | 0 - off}
		eAttTransCols    = 10, // number columns per gradient transition { 0 - off | n > 0 - number}
		eAttTransRows    = 11, // number rows per gradient transition { 0 - off | n > 0 - number}
		eAttHorzDivClr0  = 12, // start color for gradient drawing of horizontal lines
		eAttHorzDivClr1  = 13, // end color for gradient drawing of horizontal lines
		eAttVertDivClr0  = 14, // start color for gradient drawing of vertical lines
		eAttVertDivClr1  = 15, // end color for gradient drawing of vertical lines
		eAttBackClr0     = 16, // start color for gradient drawing of background
		eAttBackClr1     = 17  // end color for gradient drawing of background
	};
/////////////////////////////////////////////////////////////////////////////
	interface IColorTheme  {
		virtual HRESULT  CloneTo     (IColorTheme&)                     const PURE;
		virtual UINT     GetAttCount (void)                             const PURE;
		virtual COLORREF GetAttribute(const eThemeAttribute eAttribute) const PURE;
		virtual UINT     GetThemeID  (void)                             const PURE;
		virtual HRESULT  SetAttribute(const eThemeAttribute eAttribute, const DWORD _value) PURE;
	};
/////////////////////////////////////////////////////////////////////////////
	interface IColorTheme2 // supports a gradient
	{
		virtual bool     ApplyTo(IFormatObject4&, const eTextSizes) const                 = 0;
		virtual bool     ApplyTo(IFormatObject6&) const                                   = 0;
		virtual UINT     GetAssociatedCommandId(void) const                               = 0;
		virtual bool     GetAttribute   (const eThemeAttribute, COLORREF& clr) const      = 0;
		virtual bool     GetAttribute   (const eThemeAttribute, INT&      val) const      = 0;
		virtual bool     GetPaletteColor(const long key_, COLORREF&) const                = 0;
		virtual bool     GetRowSelection(COLORREF&, INT& alpha) const                     = 0;
		virtual UINT     GetThemeID     (void) const                                      = 0;
	};
/////////////////////////////////////////////////////////////////////////////
	enum ePredefinedTheme
	{
		eGradientBlack = 7060,
		eGradientBlue  = 7062,
		eGradientBrown = 7064,
		eGradientGreen = 7066,
		eGradientTeal  = 7068,
		eGradientWhite = 7070,
		eSolidBlack    = 7072,
		eSolidBlue     = 7076,
		eSolidBrown    = 7080,
		eSolidGreen    = 7078,
		eSolidWhite    = 7074,
		eAsphaltStripes= 7082,
		eDefault       = eAsphaltStripes,
	};
/////////////////////////////////////////////////////////////////////////////
	interface IThemeManager
	{
		virtual IColorTheme2&  GetActiveTheme(void) const                                 = 0;
		virtual IColorTheme2&  GetDefaultTheme(void) const                                = 0;
		virtual IColorTheme2&  GetPredefinedTheme(const ePredefinedTheme) const           = 0;
		virtual IColorTheme2&  GetPredefinedThemeFromCommandId(const UINT uCmdId) const   = 0;
		virtual IColorTheme2&  GetPredefinedThemeFromNumeric(const LONG) const            = 0;
	};
}}

#endif/*_IGRIDINTERFACETHM_H_30E0F8AE_E008_4E4D_B3C4_147E67FC62C9_INCLUDED*/