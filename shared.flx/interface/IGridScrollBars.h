#ifndef _IGRIDSCROLLBARS_H_5DA06D9B_BCA1_4C0C_88D9_4C494E5255B7_INCLUDED
#define _IGRIDSCROLLBARS_H_5DA06D9B_BCA1_4C0C_88D9_4C494E5255B7_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Mar-2019 at 4:28:42p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack flex grid control scrollbar management interface declaration file.
*/
#include "IGridInterface.thm.h"
namespace UILayer { namespace ext {

	enum eGridScrollBarShow
	{
		e2_ScrollBarShowNever  = 0,
		e2_ScrollBarShowAuto   = 1, // default;
		e2_ScrollBarShowAlways = 2
	};

	interface IScrollBarTheme {
		virtual IColorTheme&   Customize(const ePredefinedTheme) PURE ;    // returns a reference to object that can be customized;
	};
	typedef   IScrollBarTheme TScrollTheme;

	interface IScrollBars {
		virtual VOID           Show   (eGridScrollBarShow eMode) PURE ;    // sets scrollbar show/hide options;
		virtual VOID           Theme  (const IColorTheme&)       PURE ;    // apply theme to scrollbar look & feel;
		virtual TScrollTheme&  Theme  (void)                     PURE ;    // customizes scrollbar theme;
	};

}}

#endif/*_IGRIDSCROLLBARS_H_5DA06D9B_BCA1_4C0C_88D9_4C494E5255B7_INCLUDED*/