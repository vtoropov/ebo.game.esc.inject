#ifndef _IGRIDINTERFACEROWSEL_H_0899D633_48D4_4393_BD6C_A02BEB2317E5_INCLUDED
#define _IGRIDINTERFACEROWSEL_H_0899D633_48D4_4393_BD6C_A02BEB2317E5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Nov-2019 at 6:31:00p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack flex grid control row selector interface declaration file.
*/
#include "IGridInterface.fmt.h"
#include "IGridInterface.thm.h"
#include "IGridInterface.row.h"

namespace UILayer { namespace ext {

	enum eSelectedObject
	{
		eSelelectedRow   = 0,
		eSelelectedGroup = 1
	};
/////////////////////////////////////////////////////////////////////////////
	interface IRowSelection
	{
		virtual UINT            GetCount   (void)const             = 0;
		virtual IFormatObject6* GetFormat  (eSelectedObject eType) = 0;
		virtual IRow*           GetSelected(INT uIndex)            = 0;
		virtual void            SelectAll  (void)                  = 0;
		virtual bool            IsSelectAll(void) const            = 0;
	};
/////////////////////////////////////////////////////////////////////////////
	
	interface IRowSelectorTheme {
		virtual IColorTheme&    Customize(const ePredefinedTheme) PURE;  // returns a reference to object that can be customized;
	}; 

/////////////////////////////////////////////////////////////////////////////
	interface IRowSelector {
		virtual void           ApplyTheme  (const IColorTheme&)                          PURE ;
		virtual void           EnableShortcutMenu(bool bEnable = false)                    = 0;
		virtual IRow*          GetRow      (const POINT& pt)                       const PURE ;
		virtual IRow*          GetRow      (UINT uRow, bool bWithNotVisible=false) const PURE ;
		virtual UINT           GetRowCount (void)                                  const PURE ;
		virtual UINT           GetRowHeight(INT uRow = -1)                         const PURE ;
		virtual HWND           GetSafeHwnd (void)                                  const PURE ;
		virtual IRowSelection* GetSelection(void)                                          = 0;
		virtual IRowSelectorTheme& GetTheme(void)                                        PURE ;
		virtual UINT           GetTopRow   (void)                                  const PURE ;
		virtual UINT           GetVisibleRows(void)                                const PURE ;
		virtual UINT           GetWidth    (bool bLogical = false)                 const PURE ;
		virtual bool           IsNumbered  (void)                                  const PURE ;
		virtual bool           IsVisible   (void)                                  const PURE ;
		virtual void           Refresh     (void)                                          = 0;
		virtual void           SetFixedNumberedState(bool bFixed)                          = 0;
		virtual void           SetRowHeight(INT iHeight, INT uRow = -1)                    = 0;
		virtual void           SetTextSize (eTextSizes eSize, bool bWithForceOption=false) = 0;
		virtual bool           SetTopRow   (UINT uRow)                                     = 0;
		virtual void           SetVisible  (bool bState)                                   = 0;
		virtual void           SetWidth    (UINT uWidth, bool bWithUpdateOption = false)   = 0;
		virtual void           ShowCtxMenuForSelectRow(HWND hWnd, const POINT& point)      = 0;
		virtual void           ShowNumbers (bool bState)                                   = 0;
		virtual void           UpdateLayout(LPRECT lpRect=0)                               = 0;
	};
}}

#endif/*_IGRIDINTERFACEROWSEL_H_0899D633_48D4_4393_BD6C_A02BEB2317E5_INCLUDED*/