#ifndef _IGRIDINTERFACECELL_H_04C105CB_84E7_40B8_A949_AA68853A2A0A_INCLUDED
#define _IGRIDINTERFACECELL_H_04C105CB_84E7_40B8_A949_AA68853A2A0A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Nov-2019 at 11:11:19a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack flex grid control cell interface declaration file.
*/
#include <string>
typedef ::std::wstring  string_t;

#include "IGridInterface.fmt.h"
#include "IGridInterface.thm.h"
namespace UILayer { namespace ext {

	struct POSITION {
		UINT uRow;
		UINT uCol;
		POSITION(void) : uCol(0), uRow(0) {}
		POSITION(const UINT _row, const UINT _col) : uRow(_row), uCol(_col){}
	};

	enum eCellOption {
	     eNone         = 0,
	     eSingleBorder = 1
	};
/////////////////////////////////////////////////////////////////////////////
	interface IActiveCellTheme {
		virtual IColorTheme&    Customize(const ePredefinedTheme)      PURE ;    // returns a reference to object that can be customized;
	};
	typedef IActiveCellTheme TCellTheme;
/////////////////////////////////////////////////////////////////////////////
	interface ICell {
		virtual VOID           ApplyTheme  (const IColorTheme&)        PURE ;
		virtual UINT           GetColIndex (void)                        = 0;
		virtual IFormatObject* GetFormat   (void)                        = 0;
		virtual DWORD          GetOptions  (void)                const PURE ;
		virtual VOID           GetPlacement(LPRECT lpRect)               = 0;
		virtual VOID           GetPosition (POSITION* pPos)              = 0;
		virtual UINT           GetRowIndex (void)                        = 0;
		virtual VOID           GetText(TCHAR* lpText, UINT iBufferLen)   = 0;
		virtual string_t       GetText     (void)                        = 0;
		virtual TCellTheme&    GetTheme    (void)                      PURE ;
		virtual bool           IsChanged   (ICell* pCell)                = 0;
		virtual bool           IsChanged   (const POSITION& refPos)      = 0;
		virtual bool           IsChanged   (UINT uRow, UINT uCol)        = 0;
		virtual VOID           SetColIndex (UINT uCol)                   = 0;
		virtual VOID           SetFormat   (IFormatObject* pFormatter)   = 0;
		virtual VOID           SetOptions  (const DWORD _value )       PURE ;
		virtual VOID           SetPosition (const POSITION& pos)         = 0;
		virtual VOID           SetPosition (UINT uRow, UINT uCol)        = 0;
		virtual VOID           SetPlacement(const RECT& rect)            = 0;
		virtual VOID           SetRowIndex (UINT uRow)                   = 0;
		virtual VOID           SetText     (const TCHAR* lpText)         = 0;
	};

/////////////////////////////////////////////////////////////////////////////
	interface ICellEditor
	{
		virtual void CancelEdit (void)                            = 0;
		virtual HWND GetSafeHwnd(void)                            = 0;
		virtual bool IsEditMode (void)                            = 0;
		virtual void SetStartTooltip(wchar_t* szTooltipTextW)     = 0;
		virtual void StartEditRowGroup(bool bGetCellText = false) = 0;
		virtual void StartEdit(ICell* pICell, bool bSelectAll)    = 0;
	};
/////////////////////////////////////////////////////////////////////////////
	interface ICellRenderer
	{
		virtual void DrawCell(HDC hDC, const RECT& rcCell, UINT uiRowID, bool bHovered = false) = 0;
		virtual IFormatObject* GetFormat(void)                                                  = 0;
		virtual void SetFormat(IFormatObject* pFormat)                                          = 0;
	};
/////////////////////////////////////////////////////////////////////////////
	enum eCellLongParams
	{
		e_Param_None              = 0,
		e_Param_Error             = 1,
		e_Param_Not_Verified      = 2,
		e_Param_Not_Enough_Data   = 3,
		e_Param_Rtd_Received      = 4,
		e_Param_Rtd_Dismiss       = 5,
		e_Param_ForeColor_Def     = 6,	// used default color for text
		e_Param_ForeColor_Palette = 7	// used default color for text
	};
}}

#endif/*_IGRIDINTERFACECELL_H_04C105CB_84E7_40B8_A949_AA68853A2A0A_INCLUDED*/