#ifndef _IGRIDINTERFACECONST_H_CCABDF6D_79E0_4677_A44C_5976CE321EC1_INCLUDED
#define _IGRIDINTERFACECONST_H_CCABDF6D_79E0_4677_A44C_5976CE321EC1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Mar-2020 at 1:40:51p, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is Flex grid control pre-defined constant declaration file.
*/
namespace UILayer {
	#pragma region "CONSTANTS"
	
	static const double _NULL_DOUBLE                    = (std::numeric_limits<double>::max)();
	static const long   _NULL_LONG                      = 0xfffffffa;
	static const UINT _NULL_PARAM                       = 0xfffffffe;
	static const UINT _PERSISTENT_DATA                  = 0xfffffffb;
	static const UINT _SUB_COLUMN_ID                    = 1000000;
	static const UINT _UPDATE_FQ_AUTO                   = 0xffffffff;
	static const UINT _COLUMN_WIDTH_AUTO                = 0xffffffff;
	static const UINT _CLMN_DEF_WIDTH                   = 85; // 80 bars x 1px width + left & right margins by 4px - 1px grid

	
	static const UINT _MK_LBUTTON_GRID                  = 0x0100;
	static const UINT _MK_LBUTTON_HEADER                = 0x0200;
	static const UINT _MK_LBUTTON_ROWSEL                = 0x0400;
	static const UINT _MK_LBUTTON_INVCOL                = 0x1000; // mouse click is outside of columns
	static const UINT _MK_LBUTTONDBLCLK_GRID            = 0x1100;

	static const UINT WM_COLUMN_WIDTH_CHANGED           = (WM_USER + 0x10);
	static const UINT WM_COLUMN_WIDTH_AUTOFIT           = (WM_USER + 0x11);
	static const UINT WM_COLUMN_LBUTTONDBLCLK           = (WM_USER + 0x12);
	static const UINT WM_COLUMN_DRAG_AND_DROP           = (WM_USER + 0x13);
	static const UINT WM_COLUMN_BUTTONPRESSED           = (WM_USER + 0x14);
	static const UINT WM_COLUMN_BEFORECTXMENU           = (WM_USER + 0x15);
	static const UINT WM_COLUMN_MENUCOMMANDEX           = (WM_USER + 0x16);

	static const UINT WM_GRID_BEFORE_EDIT               = (WM_USER + 0x17);
	static const UINT WM_GRID_AFTER_EDIT                = (WM_USER + 0x18);
	static const UINT WM_GRID_DELETE                    = (WM_USER + 0x19);
	static const UINT WM_GRID_GET_INPLACE_TOOLTIP       = (WM_USER + 0x20);
	static const UINT WM_DATA_NOT_FOUND                 = (WM_USER + 0x21);

	static const UINT _uiToolTipBufLen = 80;
	static const UINT _uiTextBufLen    = 80;
	
	namespace HD_menu
	{
		static const UINT CMD_HALIGN_LEFT               = 0xff00;  // aligns a text to the left side
		static const UINT CMD_HALIGN_CENTER             = 0xff01;  // aligns a text on the column center
		static const UINT CMD_HALIGN_RIGHT              = 0xff02;  // aligns a text to the right side
		static const UINT CMD_F2T                       = 0xff03;  // fits column width to the longest text in it
		static const UINT CMD_HALIGN                    = 0xff04;  // 'Alignment' sub-menu identifier
	};
	namespace RS_menu
	{
		static const UINT CMD_HIDE                      = 0xff20;
		static const UINT CMD_CUT                       = 0xff21;
		static const UINT CMD_COPY                      = 0xff22;
		static const UINT CMD_PASTE                     = 0xff23;
		static const UINT CMD_INSERT                    = 0xff24;
		static const UINT CMD_CLEAR                     = 0xff25;
		static const UINT CMD_DELETE                    = 0xff26;
		static const UINT CMD_NUMBER                    = 0xff27;
	};
#pragma endregion
}
#endif/*_IGRIDINTERFACECONST_H_CCABDF6D_79E0_4677_A44C_5976CE321EC1_INCLUDED*/