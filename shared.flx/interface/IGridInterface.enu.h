#ifndef _IGRIDUNTERFACEENU_H_435FD704_2999_4E2B_BDC5_42CE784F7B5E_INCLUDED
#define _IGRIDUNTERFACEENU_H_435FD704_2999_4E2B_BDC5_42CE784F7B5E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Nov-2019 at 6:44:00p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack flex grid control common enumeration(s) and structure(s) declaration file.
*/

namespace UILayer { namespace ext {

	enum eGradientType
	{
		eGT__Vertical   = 0,       // default
		eGT__Horizontal = 1,
		eGT__Striped    = 2
	};
	enum eGradientEffect
	{
		GE_Concave      = 0,       // more 3-D effects (for gradient themes)
		GE_Flatten      = 1        // little 3-D effects (for non-gradient themes)
	};
	enum eFacetType
	{
		FT_Left         = 0,
		FT_Top          = 1,
		FT_Right        = 2,
		FT_Bottom       = 4
	};

	typedef struct tag__OverlayData
	{
		COLORREF       __c__0;     // color 1 for gradient
		COLORREF       __c__1;     // color 2 for gradient
		BYTE           __btAlpha;  // alpha channel
		UINT           __grTypes;  // mask of gradient types the overlay is applyed to

		tag__OverlayData(void) : __c__0(CLR_NONE), __c__1(CLR_NONE), __btAlpha(255), __grTypes(eGT__Striped) {}

		__inline bool __IsValid(void) const { return (CLR_NONE != __c__0 && CLR_NONE != __c__1);}
	} __OverlayData;

/////////////////////////////////////////////////////////////////////////////
	enum eTextSizes
	{
		e2_TextSizeSmaller  = 0,  // default, 100%
		e2_TextSizeMedium   = 1,
		e2_TextSizeLargest  = 2,
		e2_TextSizeLarger   = 3,
		e2_TextSizeSmallest = 4,
		e2_TextSizeDefault  = e2_TextSizeSmaller,
	};
}}

#endif/*_IGRIDUNTERFACEENU_H_435FD704_2999_4E2B_BDC5_42CE784F7B5E_INCLUDED*/