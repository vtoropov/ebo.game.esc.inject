#ifndef _IGRIDINTERFACEDEF_H_DD819C22_D166_48EE_9ED0_71255526BFE6_INCLUDED
#define _IGRIDINTERFACEDEF_H_DD819C22_D166_48EE_9ED0_71255526BFE6_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-May-2008 at 07:50:23p, UTC+3, Rostov-on-Don, Tuesday;
	This is an abstraction layer for Flex Grid control;
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Flex grid control on 4-Nov-2019 at 4:42:42p, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include <limits>
#include <ComDef.h>
#include "IGridInterface.enu.h"
#include "IGridInterface.fmt.h"
#include "IGridInterface.thm.h"
#include "IGridInterface.col.h"
#include "IGridInterface.cell.h"
#include "IGridInterface.row.h"
#include "IGridInterface.row.sel.h"
#include "IGridInterface.stg.h"
#include "IGridInterface.shared.h"
#include "IGridScrollBars.h"

extern ::WTL::CMessageLoop* get_MessageLoop();
extern HINSTANCE get_HINSTANCE();

namespace data { namespace cache {

	enum ESortOrder {
		e_Sort_Order_Asc         =  1,
		e_Sort_Order_None        =  0,
		e_Sort_Order_Desc        = -1,
	};

	enum EFieldStatus {
		e_Status_Not_Use         = -5,
		e_Status_Empty           = -4,
		e_Status_Error           = -3,
		e_Status_Not_Enough_Data = -2,
		e_Status_Not_Verified    = -1,
		e_Status_Ok              =  0,
	};

	enum eCellAttribute
	{
		e_Cell_Att_None             = 0x00,  // no attribute
		e_Cell_Att_Blink            = 0x01,  // blinking
		e_Cell_Att_ForeClr_Def      = 0x02,  // used a default forecolor
		e_Cell_Att_ForeClr_Palette  = 0x04,  // used a forecolor from palette
	};
}}

#include "IGridInterface.const.h"

namespace UILayer
{
	using namespace UILayer::ext;
/////////////////////////////////////////////////////////////////////////////
	
	struct RANGE
	{
		INT iStart;
		INT iEnd;
		RANGE(void) : iStart(0), iEnd(0){}
		RANGE(INT i_min, INT i_max) : iStart(i_min), iEnd(i_max) {}
		__inline bool Has    (INT i_) const { return (iStart <= i_ && i_ <= iEnd); }
		__inline bool IsEmpty(void  ) const { return (0==(iEnd - iStart)); }
	};
/////////////////////////////////////////////////////////////////////////////
	enum eShortcutMenus {
		e2_shortcutToHeader      = 0x1,
		e2_shortcutToRowSelector = 0x2
	};
/////////////////////////////////////////////////////////////////////////////
	interface IGrid;
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
	enum eGridGroupStyles
	{
		e2_GroupStyleNone         = 0, // no style, default
		e2_GroupStyleImmovable    = 1, // group object is not movable
		e2_GroupStyleNotUpdatable = 2  // group object cannot be updated
	};
/////////////////////////////////////////////////////////////////////////////
	interface IColumnGroup
	{
		virtual IColumn* AddColumn(LPCTSTR lpCaption, UINT CID)                     = 0;
		virtual void     Clear(void)                                                = 0;
		virtual void     Destroy(void)                                              = 0;
		virtual IColumn* GetColumn(UINT iColIndex)                                  = 0;
		virtual IColumn* GetColumn(UINT iColID, INT iReserved)                      = 0;
		virtual UINT     GetColumnCount(void)                                       = 0;
		virtual UINT     GetID(void)                                                = 0;
		virtual UINT     GetIndex(void)                                             = 0;
		virtual IColumn* GetLast(void)                                              = 0;
		virtual UINT     GetLeft(void)                                              = 0;
		virtual UINT     GetStyle(void)                                             = 0;
		virtual void     GetTitle(TCHAR* lpTitle, UINT iBufferLen)                  = 0;
		virtual LPCTSTR  GetTitle(void)                                             = 0;
		virtual ULONG    GetTag(void)                                               = 0;
		virtual UINT     GetWidth(UINT iFrom = 0)                                   = 0;
		virtual bool     HasStyle(DWORD dwStyle)                                    = 0;
		virtual void     RemoveColumn(IColumn* pColumn)                             = 0;
		virtual void     RemoveColumn(UINT iColumnID)                               = 0;
		virtual void     RemoveColumn(UINT iColIndex, UINT iReserved)               = 0;
		virtual void     SetID(UINT uID)                                            = 0;
		virtual void     SetIndex(UINT uIndex)                                      = 0;
		virtual void     SetLeft(UINT iLeft)                                        = 0;
		virtual void     SetStyle(UINT uStyle, bool bSet = true)                    = 0;
		virtual void     SetTitle(const TCHAR* lpTitle)                             = 0;
		virtual void     SetTag(ULONG ulTag)                                        = 0;
		virtual void     Update(void)                                               = 0;
	};
/////////////////////////////////////////////////////////////////////////////
	namespace ext {
	interface IColumnHeaderTheme {
		virtual IColorTheme&    Customize(const ePredefinedTheme) PURE;  // returns a reference to object that can be customized;
	};}
	typedef UILayer::ext::IColumnHeaderTheme THeadTheme;
/////////////////////////////////////////////////////////////////////////////
	interface IColumnHeader
	{
		virtual IColumnGroup* AddGroup  (LPCTSTR lpTitle)                           = 0; // creates a new group, adds into internal array and returns its pointer
		virtual void          ApplyTheme(const IColorTheme&)                      PURE ;
		virtual void          Clear     (void)                                      = 0;
		virtual IColumn*      GetColumn (INT uCol)                                  = 0;
		virtual IColumn*      GetColumn (UINT iColID, INT nReserved)                = 0;
		virtual IColumn*      GetColumn (const POINT& pt)                           = 0;
		virtual UINT          GetColumnCount(void)                                  = 0;
		virtual UINT          GetColumnWidth(INT iFromIndex=-1, INT iMaxWidth = -1) = 0;
		virtual UINT          GetFixedCols(void)                                    = 0;
		virtual IColumnGroup* GetGroup(INT iGroupIndex)                             = 0;
		virtual IColumnGroup* GetGroup(UINT iGroupID, INT nReserved)                = 0;
		virtual UINT          GetGroupCount (void)                                  = 0;
		virtual UINT          GetHeight     (void)                           const PURE;
		virtual UINT          GetLeftCol    (void)                                  = 0;
		virtual UINT          GetLines      (void)                                  = 0;
		virtual HWND          GetSafeHwnd   (void)                                  = 0;
		virtual THeadTheme&   GetTheme      (void)                                PURE ;
		virtual UINT          GetVisibleCols(void)                                  = 0;
		virtual bool          IsScrollable(void)                                    = 0;
		virtual bool          MoveColumnGroup(IColumnGroup* pWhat, IColumnGroup* pBefore) = 0;
		virtual void          Refresh(void)                                         = 0;
		virtual void          RecalcLayout(bool bWithRefreshOption = true)          = 0;
		virtual void          RemoveGroup(IColumnGroup* pGroup)                     = 0;
		virtual bool          ReplaceColumnGroup(IColumnGroup* pWith, IColumnGroup* pWhat) = 0;
		virtual void          SetFixedCols(UINT _val)                               = 0;
		virtual void          SetHeight(UINT iHeight)                               = 0;
		virtual bool          SetLeftCol(UINT uCol)                                 = 0;
		virtual void          SetTextSize(eTextSizes eSize, bool bWithForceOption=false) = 0;
		virtual void          SetLinesCount(UINT iLinesCount)                       = 0;
		virtual void          UpdateLayout(LPRECT lpRect=0)                         = 0;
	};
/////////////////////////////////////////////////////////////////////////////
	interface IRowGroup
	{
		virtual void            Collapse  (void)            PURE;
		virtual void            Expand    (void)            PURE;
		virtual long            GetID     (void)            PURE;
		virtual LPCTSTR         GetName   (void)            PURE;
		virtual RANGE&          GetRange  (void)            PURE;
		virtual bool            IsExpanded(void)            PURE;
		virtual void            SetID     (long lID)        PURE;
		virtual void            SetName   (LPCTSTR lpName ) PURE;
		virtual void            SetRange  (RANGE& refRange) PURE;
		virtual void            Toggle    (void)            PURE;
	};
/////////////////////////////////////////////////////////////////////////////
	interface IRowGroupCollection
	{
		virtual void            Clear      (INT uRow)       PURE;
		virtual bool            CollapseAll(void)           PURE;
		virtual UINT            Count      (void)           PURE;
		virtual bool            ExpandAll  (void)           PURE;
		virtual IRowGroup*      GetAt      (INT uRow)       PURE;
		virtual IRowGroup*      GetAtByID  (long lID)       PURE;
		virtual IFormatObject4* GetFormat  (void)           PURE;
		virtual bool            Insert     (INT uRow)       PURE;
		virtual bool            IsGroup    (INT uRow) const PURE;
		virtual bool            Remove     (INT uRow)       PURE;
		virtual bool            ToggleAll  (void)           PURE;
	};
/////////////////////////////////////////////////////////////////////////////
	interface IEventSink
	{
		virtual LRESULT OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)=0;
	};
/////////////////////////////////////////////////////////////////////////////
	interface IFind
	{
		virtual void Find(IColumn* pColumn, bool bShow = true, bool bDestroy = false) = 0;
		virtual bool IsActiveWnd(void)                                                = 0;
		virtual bool IsFind(bool& bIsVisible)                                         = 0;
		virtual void SetHWNDOwner(HWND hWndOwner)                                     = 0;
		virtual void SetLockFindWnd(bool bLock)                                       = 0;
		virtual void SetTitle(LPCTSTR lpTitle)                                        = 0;
	};
/////////////////////////////////////////////////////////////////////////////
	enum eCmdMenuGroup
	{
		e2_ExportToExcel   = 0,
		e2_CopyToClipboard = 1,
		e2_SelectAll       = 2,
		e2_TextSize        = 3,
		e2_Themes          = 4
	};
	interface IMenuBuilder
	{
		virtual bool AppendCommandToContextMenu(const HMENU, const HINSTANCE hResModule, const eCmdMenuGroup* pCmdGroup, const INT cGroup) = 0;   // appends to the end necessary menu items for built-in command
		virtual bool UpdateAllMenuState(const HMENU) = 0;
	};
/////////////////////////////////////////////////////////////////////////////
	interface ICommandDefaultHandler
	{
		virtual  bool             IsRegisterted              (const UINT) const   PURE;
		virtual  bool             IsTextSizeRegistered       (const UINT) const   PURE;
		virtual  bool             IsThemeRegistered          (const UINT) const   PURE;
		virtual  bool             IsExportToExcelRegister    (const UINT) const   PURE;
		virtual  bool             IsCopyToClipboardRegister  (const UINT) const   PURE;
		virtual  bool             IsSelectAllRegister        (const UINT) const   PURE;
		virtual  HRESULT          OnRegistered               (const UINT)         PURE;
		virtual  HRESULT          OnTextSizeRegistered       (const UINT)         PURE;
		virtual  HRESULT          OnThemeRegistered          (const UINT)         PURE;
		virtual  HRESULT          OnExportToExcelRegister    (const UINT)         PURE;
		virtual  HRESULT		  OnCopyToClipboardRegister  (const UINT)         PURE;
		virtual  HRESULT          OnSelectAllRegister        (const UINT)         PURE;
		virtual  eTextSizes       RegisteredToTextSize       (const UINT) const   PURE;
		virtual  ePredefinedTheme RegisteredToPredefinedTheme(const UINT) const   PURE;
	};
/////////////////////////////////////////////////////////////////////////////
	enum eNotifyPriority
	{
		eNotifyPriorityLow  = 0,
		eNotifyPriorityHigh = 1,
	};
/////////////////////////////////////////////////////////////////////////////
	enum eAcceleratorGroup
	{
		e2_None = 0,
		e2_NoneModifyCmd = 1,
		e2_All = 2
	};
	interface IAcceleratorManager {
		virtual bool SetAcceleratorGroup(const eAcceleratorGroup eGroup) = 0;
	};

	interface IClipboard {
		virtual bool           CopyTo   (void) PURE;
		virtual void           MoveTo   (void) PURE;
		virtual void           PasteFrom(void) PURE;
	};
/////////////////////////////////////////////////////////////////////////////
	
/////////////////////////////////////////////////////////////////////////////
	interface IGrid : public IEventSink
	{
		virtual void           AddRow(void)                                               = 0;
		virtual void           Advise(IEventSink* const pSink, const eNotifyPriority = eNotifyPriorityLow) = 0;
		virtual void           ApplyObject(IColumn* pColumn, INT nReserved = _NULL_PARAM) = 0;
		virtual void           ApplyObject(IColumnGroup* pGroup)                          = 0;
		virtual void           ApplyObject(IRow* pRow)                                    = 0;
		virtual void           ApplyObject(IColorTheme2* pTheme)                          = 0;
		virtual void           ClearColumn(UINT iColIndex)                                = 0;
		virtual void           ClearColumn(UINT iColID, INT nReserved)                    = 0;
		virtual void           ClearLine(void)                                            = 0;
		virtual void           ClearRow(UINT iRowIndex)                                   = 0;
		virtual void           ClearRow(UINT iRowID, INT nReserved)                       = 0;
		virtual void           ClearRowByIndex(UINT iRowID)                               = 0;
		virtual void           ClearSort(void)                                            = 0;
		virtual IClipboard&    Clipboard(void)                                           PURE;
		virtual void           DeleteAllLine(void)                                        = 0;
		virtual void           DeleteLine(void)                                           = 0; 
		virtual void           DeleteObject(ICell* pCell)                                 = 0;
		virtual void           DeleteObject(IRow* pRow)                                   = 0;
		virtual void           DeleteRow(void)                                            = 0;
		virtual void           Destroy(void)                                              = 0;
		virtual void           EnableRowGrouping(bool)                                    = 0; // enables/disables grouping mode in the grid
		virtual void           EnsureVisible(void)                                        = 0; // makes current cell visible (auto-scroll)
		virtual HRESULT        Export2Excel(void)                                         = 0;
		virtual IAcceleratorManager& GetAcceleratorManager(void)                          = 0;
		virtual ICell*         GetActiveCell(void)                                        = 0;
		virtual ICell*         GetActiveCell(INT nReserved)                               = 0;
		virtual ICell*         GetCell(UINT uRow, UINT uCol)                              = 0;
		virtual ICellEditor*   GetCellEditor(void)                                        = 0;
		virtual long           GetCellLong(UINT uRow, UINT uCol)                          = 0;
		virtual IColorTheme2*  GetColorTheme(void)const                                   = 0;
		virtual IColumn*       GetColumn(UINT iColIndex)                                  = 0;
		virtual IColumn*       GetColumn(UINT iColID, INT iReserved)                      = 0;
		virtual UINT           GetColumnCount(void)                                       = 0;
		virtual IColumnGroup*  GetColumnGroup(INT iGroupIndex)                            = 0;
		virtual IColumnGroup*  GetColumnGroup(UINT iGroupID, INT iReserved)               = 0;
		virtual UINT           GetColumnGroupCount(DWORD dwStyle=0)                       = 0;
		virtual IColumnHeader* GetColumnHeader(void)                                      = 0;
		virtual IColumnHeader& GetColumnHeader_Ref(void)                                  = 0;
		virtual HMENU          GetCtxMenuHandle(eShortcutMenus eShortcut) const           = 0;
		virtual ICommandDefaultHandler& GetCommandDefaultHandlerRef(void)                 = 0;
		virtual IFind*         GetFind(void)                                              = 0;
		virtual IRow*          GetLastRow(void)                                           = 0;
		virtual IMenuBuilder&  GetMenuBuilderRef(void)                                    = 0;
		virtual bool           GetPersistData(ePersistDataTypes eType, VARIANT* pvData)   = 0;
		virtual IPersistent*   GetPersistent(void)                                        = 0;
		virtual IPersistent&   GetPersistent_Ref(void)                                    = 0;
		virtual IRow*          GetRow(UINT iRowIndex)                                     = 0;
		virtual IRow*          GetRow(UINT iRowID, INT iReserved)                         = 0;
		virtual UINT           GetRowCount(void)                                          = 0;
		virtual IThemeManager& GetThemeManagerRef(void)                                   = 0;
		virtual UINT           GetTotalRowCount(void)                                     = 0;
		virtual IRowGroupCollection* GetRowGroupCollection(void)                          = 0;
		virtual IRowGroupCollection& GetRowGroupCollection_Ref(void)                      = 0;
		virtual IRowSelector*  GetRowSelector(void)                                       = 0;
		virtual const RANGE&   GetRowRange(void)                                          = 0; // returns row available range (iStart - minimal row number, iEnd - maximum row number)
		virtual HWND           GetSafeHwnd(void)                                          = 0;
		virtual IColumn*       GetSortIndex(INT iter)                                     = 0;
		virtual INT            GetSortIndexCount(void)                                    = 0;
		virtual HRESULT        GetText(const UINT _row_ndx, const UINT _col_ndx, CAtlString&  ) const PURE;
		virtual HRESULT        GetText(const UINT _row_ndx, const UINT _col_ndx, PTCHAR _p_buf, const UINT _u_len) const PURE;
		virtual bool           GetTextInverse(void) const                                 = 0;
		virtual eTextSizes     GetTextSize(void) const                                    = 0;
		virtual void           GetValue(UINT uRow, UINT uCol, _variant_t& vtValue, INT* pPrecision=0, long* pclrFore=0, long* pclrBack=0) = 0;
		virtual IRow*          InsertRow(UINT iRowID, UINT iBefore)                       = 0;
		virtual bool           IsDataInSelectRowOrActiveCell(void)                        = 0;
		virtual bool           IsSupported(DWORD dwAttributes)                            = 0;
		virtual bool           ModifyCellLong(UINT iRowID, UINT iColID, long eParams, bool bSet)=0;
		virtual bool           MoveColumnGroup(IColumnGroup* pGroup, UINT iBefore)        = 0;
		virtual void           Refresh(void)                                              = 0;
		virtual void           RemoveColumnGroup(IColumnGroup* pGroup)                    = 0;
		virtual void           RemoveColumnGroup(UINT iGroupIndex)                        = 0;
		virtual void           RemoveRow(UINT iRowIndex)                                  = 0;
		virtual void           RemoveRow(UINT iRowID, INT nReserved)                      = 0;
		virtual void           RemoveLastRowInDefaultGroup(void)                          = 0;
		virtual IScrollBars&   ScrollBars(void)                                          PURE;
		virtual bool           SetActiveCell(ICell* pCell)                                = 0;
		virtual bool           SetActiveCell(UINT uRow, UINT uCol)                        = 0;
		virtual void           SetCellForeColor(UINT uRow, UINT uCol, COLORREF clrFore)   = 0;
		virtual void           SetCellLong(UINT iRowID, UINT iColID, long eParams)        = 0;
		virtual void           SetCellRenderer(ICellRenderer* pRenderer)                  = 0;
		virtual void           SetRowRange(const RANGE&)                                  = 0; // sets flexible row available range (iStart - minimal row count, iEnd - maximum row count)
		virtual void           SetRows(UINT nCount)                                       = 0; // sets fixed row available range (internally iStart & iEnd are assigned to nCount)
		virtual void           SetText(UINT uRow, UINT uCol, const TCHAR* lpText)         = 0;
		virtual void           SetText(UINT iRowID, UINT iColID, LPCTSTR lpText, long clrFore) = 0;
		virtual void           SetText(UINT iRowID, UINT iColID, const _variant_t& vtValue, INT iPrecision = 5, long clrFore = CLR_NONE, long clrBack = CLR_NONE) = 0;
		virtual void           SetTextInverse(bool bOn)                                   = 0;
		virtual void           SetTextSize(eTextSizes eSize)                              = 0;
		virtual void           ShowRowSelector(bool bShow)                                = 0;
		virtual void           Sort(void)                                                 = 0;
		virtual void           StopSort(bool bStop, bool bLockDataChange = true)          = 0;
		virtual void           Unadvise(IEventSink* pSink)                                = 0;
		virtual void           Update(ICell* pCell)                                       = 0;
		virtual void           UpdateBatch(INT iFreq = _UPDATE_FQ_AUTO/*in millisec*/)    = 0;
		virtual void           UpdateLayout(const RECT&)                                  = 0;
		virtual void           UpdateLayout(HWND hwndFrame)                               = 0;
		virtual void           UpdateVisibleColumn(void)                                  = 0;
		virtual bool           UseAutoRowHeight(void)                              const PURE;
		virtual void           UseAutoRowHeight(const bool _b_set)                       PURE;
		virtual bool           UseMultilineText(void)                              const PURE;
		virtual void           UseMultilineText(const bool _b_set)                       PURE;
	};
/////////////////////////////////////////////////////////////////////////////
	IGrid*                     CreateGridInstance(HWND hFrame, const DWORD _max_columns = 100, const WORD _ctrl_id = 0);
	HRESULT                    DestroyGrid_Safe(IGrid**);
	const
	ICommandDefaultHandler&    GetDefaultHandler_Ref(void);
/////////////////////////////////////////////////////////////////////////////
}

typedef UILayer::SCellPersistData  TCellPersistData ;
typedef UILayer::SColPersistData   TColPersistData  ;
typedef UILayer::SGroupPersistData TGroupPersistData;
typedef UILayer::SRowPersistData   TRowPersistData  ;

#endif/*_IGRIDINTERFACEDEF_H_DD819C22_D166_48EE_9ED0_71255526BFE6_INCLUDED*/