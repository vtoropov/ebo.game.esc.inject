#ifndef _IGRIDINTERFACESTG_H_14978255_05A3_4A5D_85DC_2189B4D2A49B_INCLUDED
#define _IGRIDINTERFACESTG_H_14978255_05A3_4A5D_85DC_2189B4D2A49B_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Nov-2019 at 7:21:52p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack flex grid control persistent storage interface declaration file.
*/
#include "IGridInterface.col.h"

namespace UILayer { namespace ext {

	enum eSupportedAttributes {
		e2_attPersistRowData = 1
	};
/////////////////////////////////////////////////////////////////////////////
	enum ePersistDataTypes {
		ePersDataTypeNone    = 0,
		ePersDataTypeTheme   = 1,
		ePersDataTypeTopRow  = 2
	};
/////////////////////////////////////////////////////////////////////////////
	typedef struct tagCellPersistentData {
		UINT uRow;
		UINT uCol;
		tagCellPersistentData(void) : uRow(0), uCol(0) {}
	} SCellPersistData;
/////////////////////////////////////////////////////////////////////////////
	typedef struct tagColumnPersistentData {
		UINT uID     ;
		UINT uWidth  ;
		UINT uHAlign ;
		bool bVisible;
		bool bNumber ;

		tagColumnPersistentData(void) : uID(0), uWidth(0), uHAlign(0), bVisible(true), bNumber(false) {}
		static eColumnHAlignTypes ConvertInt2Type(UINT iType)
		{
			eColumnHAlignTypes eType = e2_clmnHAlignGeneral;
			switch(iType) {
			case 1: eType = e2_clmnHAlignLeft;    break;
			case 2: eType = e2_clmnHAlignRight;   break;
			case 3: eType = e2_clmnHAlignCenter;  break;
			}
			return eType;
		}
	} SColPersistData;
/////////////////////////////////////////////////////////////////////////////
	typedef struct tagGroupPersistentData {
		UINT uIndex;
		UINT uID   ;
	} SGroupPersistData;
/////////////////////////////////////////////////////////////////////////////
	typedef struct tagRowPersistentData {
		UINT uIndex;
		UINT uID   ;
		tagRowPersistentData(void) : uIndex(0), uID(0) {}
	} SRowPersistData;
/////////////////////////////////////////////////////////////////////////////
	typedef struct tagRowGroupPersistentData {
		UINT        uIndex ;
		CAtlString  strName;
		bool        bExpand;
	}SRowGroupPersistentData;
/////////////////////////////////////////////////////////////////////////////
	interface IPersistent
	{
		virtual SCellPersistData*  GetActiveCell(void)   = 0;
		virtual SColPersistData*   GetColumn(UINT UID)   = 0;
		virtual SGroupPersistData* GetColGroup(UINT UID) = 0;
		virtual SRowPersistData*   GetRow(UINT UID)      = 0;

		virtual HRESULT Deserialize(IUnknown* pStorage, IUnknown* pContext) = 0;
		virtual HRESULT DeserializeRowGroupState(void)                      = 0;
		virtual HRESULT Serialize  (IUnknown* pStorage, IUnknown* pContext) = 0;
	};
}}

#endif/*_IGRIDINTERFACESTG_H_14978255_05A3_4A5D_85DC_2189B4D2A49B_INCLUDED*/