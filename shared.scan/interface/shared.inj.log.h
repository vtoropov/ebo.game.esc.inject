#ifndef _SHAREDINJLOG_H_1F143CBB_93B3_4D66_998B_E7E834B66459_INCLUDED
#define _SHAREDINJLOG_H_1F143CBB_93B3_4D66_998B_E7E834B66459_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is inject event log declaration file (https://github.com/guided-hacking/GuidedHacking-Injector);
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 26-Mar-2020 at 5:02:07p, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "shared.nt.sys.log.h"
#include "shared.inj.error.h"
namespace shared { namespace log { namespace inject {
	using shared::log::CLog_Base;
	using shared::inject::CErrorInfo;

	class CLog : public CLog_Base {
	            typedef CLog_Base TBase;
	private:
		FILE*       m_file;

	public:
		 CLog (const DWORD _opts = COption::e_one_file);
		~CLog (void);

	public:
		VOID   Write (const CErrorInfo&);

	public:
		CLog&  operator << (const CErrorInfo&);
	};

}}}

#endif/*_SHAREDINJLOG_H_1F143CBB_93B3_4D66_998B_E7E834B66459_INCLUDED*/