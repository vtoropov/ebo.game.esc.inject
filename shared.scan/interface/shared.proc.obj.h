#ifndef _SHAREDPROCMOD_H_0879EB0D_AC59_4E4E_A0D8_D0D3870DE1CF_INCLUDED
#define _SHAREDPROCMOD_H_0879EB0D_AC59_4E4E_A0D8_D0D3870DE1CF_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is base import handler interface declaration file (https://github.com/guided-hacking/GuidedHacking-Injector);
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 4-Mar-2020 at 6:05:06p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	Tech_dog: actually, it is not import handler, but just an observer of what to import;
*/
// https://docs.microsoft.com/en-us/windows/win32/api/tlhelp32/ns-tlhelp32-moduleentry32
#include <tlhelp32.h>
#include <strsafe.h >
#include "shared.nt.sys.err.h"
namespace shared { namespace process {
    using shared::sys_core::CError;

	class CObject_Base {
	protected:
		mutable
		CError_Inj   m_j_error;
		HANDLE       m_h_proc ; // target procedure handle;

	protected:
		 CObject_Base (const HANDLE _h_target_proc);
		~CObject_Base (void);

	public:
		virtual HINSTANCE   ModuleHandle(LPCTCH  lpModuleName) PURE;
		virtual bool        ProcAddress (LPCTCH  lpModuleName, LPCCH szProcName, PVOID& pOut) PURE;
		virtual bool        ProcAddress (HMODULE hModule     , LPCCH szProcName, PVOID& pOut) PURE;
	public:
		TJErrorRef   Error  (void) const;
		bool         IsValid(void) const;
	};

	class CObject : public CObject_Base {
	               typedef CObject_Base TImport;
	public:
		 CObject (const HANDLE _h_target_proc) ;
		~CObject (void) ;

	public:
#pragma warning (disable:4481)
		HINSTANCE    ModuleHandle(LPCTCH  lpModuleName) override sealed;
		bool         ProcAddress (LPCTCH  lpModuleName, LPCCH szProcName, PVOID& pOut) override sealed;
		bool         ProcAddress (HMODULE hModule     , LPCCH szProcName, PVOID& pOut) override sealed;
#pragma warning (default:4481)
	};

	class CObject_WOW64 : public CObject_Base {
	                     typedef CObject_Base TImport;
	public:
		 CObject_WOW64 (const HANDLE _h_target_proc) ;
		~CObject_WOW64 (void) ;

	public:
#pragma warning (disable:4481)
		HINSTANCE    ModuleHandle(LPCTCH  lpModuleName) override sealed;
		bool         ProcAddress (LPCTCH  lpModuleName, LPCCH szProcName, PVOID& pOut) override sealed;
		bool         ProcAddress (HMODULE hModule     , LPCCH szProcName, PVOID& pOut) override sealed;
#pragma warning (default:4481)
	};
#if (0)
	// uses CreateToolHelp32Snapshot and Module32First/Next to retrieve the baseaddress of an image.
	HINSTANCE GetModuleHandleEx(HANDLE hTargetProc, const TCHAR* lpModuleName);
	// a function which tries to get the address of a function by parsing the export directory of the specified module.
	bool GetProcAddressEx(HANDLE hTargetProc, const TCHAR* szModuleName, const char* szProcName, void* &pOut);
	bool GetProcAddressEx(HANDLE hTargetProc, HINSTANCE hModule, const char* szProcName, void* &pOut);
	// uses CreateToolHelp32Snapshot and Module32First/Next to retrieve the baseaddress of an image.
	HINSTANCE GetModuleHandleEx_WOW64(HANDLE hTargetProc, const TCHAR* lpModuleName);
	// a function which tries to get the address of a function by parsing the export directory of the specified module.
	bool GetProcAddressEx_WOW64(HANDLE hTargetProc, const TCHAR* szModuleName, const char* szProcName, void* &pOut);
	bool GetProcAddressEx_WOW64(HANDLE hTargetProc, HINSTANCE hModule, const char* szProcName, void* &pOut);
#endif
}}

#endif/*_SHAREDPROCMOD_H_0879EB0D_AC59_4E4E_A0D8_D0D3870DE1CF_INCLUDED*/