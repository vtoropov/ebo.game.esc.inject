#ifndef _SHAREDINJBASE_H_EB3E967A_E139_45B5_91D0_047E22F9CDCB_INCLUDED
#define _SHAREDINJBASE_H_EB3E967A_E139_45B5_91D0_047E22F9CDCB_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is internal NT function interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 28-Feb-2020 at 2:14:56a, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "shared.inj.start.h"

namespace shared { namespace inject {

#ifndef EXPORT_FUNCTION
#define EXPORT_FUNCTION(export_name, link_name) comment(linker, "/EXPORT:" export_name "=" link_name)
#endif

	using shared::inject::launch::eLaunch;
	enum INJECTION_MODE {
		IM_LoadLibrary,
		IM_LdrLoadDll ,
		IM_ManualMap  ,
	};
	typedef INJECTION_MODE eInject;

#define INJ_ERASE_HEADER            0x0001
#define INJ_FAKE_HEADER             0x0002
#define INJ_UNLINK_FROM_PEB         0x0004
#define INJ_SHIFT_MODULE            0x0008
#define INJ_CLEAN_DATA_DIR          0x0010
#define INJ_THREAD_CREATE_CLOAKED   0x0020
#define INJ_SCRAMBLE_DLL_NAME       0x0040
#define INJ_LOAD_DLL_COPY           0x0080
#define INJ_HIJACK_HANDLE           0x0100

#define INJ_MAX_FLAGS               0x01FF

#define RELOC_FLAG86(RelInfo) ((RelInfo >> 0x0C) == IMAGE_REL_BASED_HIGHLOW)
#define RELOC_FLAG64(RelInfo) ((RelInfo >> 0x0C) == IMAGE_REL_BASED_DIR64)
#ifdef _WIN64
#define RELOC_FLAG RELOC_FLAG64
#else
#define RELOC_FLAG RELOC_FLAG86
#endif

#ifndef ALIGN_UP
#define ALIGN_UP(X, A) (X + (A - 1)) & (~(A - 1))
#define ALIGN_IMAGE_BASE_X64(Base) ALIGN_UP(Base, 0x10)
#define ALIGN_IMAGE_BASE_X86(Base) ALIGN_UP(Base, 0x08)
#endif

	static  const DWORD dw_buf_sz_req = MAX_PATH * 2;

	struct  INJECTIONDATAA
	{
		DWORD       dwLastErrCode;              // used to store the error code of the injection;
		CHAR        szDllPath[dw_buf_sz_req];   // fullpath to the dll to inject;
		DWORD       dwProcessID  ;              // process identifier of the target process;
		eInject     eMode        ;              // injection mode;
		eLaunch     eMethod      ;              // method to execute the remote shellcode;
		DWORD       dwFlags      ;              // combination of the flags defined above;
		DWORD       hHandleValue ;              // optional value to identify a handle in a process;
		HINSTANCE   hDllOut      ;              // returned image base of the injection;
	};
	typedef INJECTIONDATAA inj_data_a;

	// the additional member lp_sz_Ignored should be ignored since it's only used for error logging.
	struct  INJECTIONDATAW
	{
		DWORD       dwLastErrCode;
		WCHAR       szDllPath[dw_buf_sz_req];
		WCHAR*      lp_sz_Ignored;              // exe name of the target process;
		DWORD       dwProcessID  ;
		eInject     eMode        ;
		eLaunch     eMethod      ;
		DWORD       dwFlags      ;
		DWORD       hHandleValue ;
		HINSTANCE   hDllOut      ;
	};
	typedef INJECTIONDATAW inj_data_w;

	DWORD __stdcall InjectA(INJECTIONDATAA* pData);
	DWORD __stdcall InjectW(INJECTIONDATAW* pData);

	bool  __lnk_warn_4221_block_3(void);

}}

#endif/*_SHAREDINJBASE_H_EB3E967A_E139_45B5_91D0_047E22F9CDCB_INCLUDED*/