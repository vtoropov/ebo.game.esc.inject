#ifndef _SHAREDNTSYSLOG_H_92184384_7407_4DB4_8DE7_8511AB126420_INCLUDED
#define _SHAREDNTSYSLOG_H_92184384_7407_4DB4_8DE7_8511AB126420_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is internal NT error codes declaration file (https://github.com/guided-hacking/GuidedHacking-Injector);
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 3-Mar-2020 at 7:27:41p, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "shared.nt.sys.err.h"
#include "shared.gen.han.h"
#include "shared.gen.raw.buf.h"
namespace shared { namespace log {
	using shared::sys_core::CError;
	using shared::common::CAutoHandle;
	using shared::common::CRawData;

	class CLocator {
	private:
		mutable
		CError     m_error;
	public:
		 CLocator(void);
		~CLocator(void);

	public:
		CAtlString  File  (void) const;
		CAtlString  Folder(void) const;
		TErrorRef   Error (void) const;
	};

	class COption {
	public:
		enum _opt : DWORD {
			e_one_file  = 0,
			e_overwrite = 1,
			e_bufferred = 2,
		};
	public:
		bool IsOverwrite(const DWORD _ops) const;
	};

	class CLog_Base {
	protected:
		mutable
		CError      m_error;
		DWORD       m_opts;
	
	protected:
		 CLog_Base(const DWORD _opts);
		~CLog_Base(void);

	public:
		TErrorRef   Error (void) const;
		CAtlString  Path  (void) const;
	}; 

	class CDump : public CLog_Base {
	             typedef CLog_Base TBase;
	private:
		CAutoHandle m_file;
		
	public:
		 CDump (const DWORD _opts);
		~CDump (void);

	public:
		HRESULT   Write (const _variant_t&);
		HRESULT   Write (const CRawData&  );
		HRESULT   Write (const PBYTE _data, const DWORD _sz);

	public:
		CDump&    operator << (const CRawData&);
	};

}}

#endif/*_SHAREDNTSYSLOG_H_92184384_7407_4DB4_8DE7_8511AB126420_INCLUDED*/