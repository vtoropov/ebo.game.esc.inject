#ifndef _SHAREDPROCDEFS_H_649A13B5_A035_4C8E_8899_CFB6ECD40EBE_INCLUDED
#define _SHAREDPROCDEFS_H_649A13B5_A035_4C8E_8899_CFB6ECD40EBE_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Feb-2020 at 0:42:11a, UTC+7, Novosibirsk, Tulenina, Friday;
	This is Ebo Game Dyna injection project process information definition file;
*/
#include "shared.gen.sys.err.h"
namespace shared { namespace process {
	using shared::sys_core::CError;
	// https://docs.microsoft.com/en-us/windows/win32/api/winternl/nf-winternl-ntqueryinformationprocess
	// https://docs.microsoft.com/en-us/windows/win32/api/winternl/ns-winternl-peb
	// https://docs.microsoft.com/en-us/windows/win32/api/winternl/ns-winternl-peb_ldr_data
	// https://docs.microsoft.com/en-us/windows/win32/api/ntdef/ns-ntdef-_unicode_string
	typedef enum _KWAIT_REASON
	{
		Executive         = 0x00,
		FreePage          = 0x01,
		PageIn            = 0x02,
		PoolAllocation    = 0x03,
		DelayExecution    = 0x04,
		Suspended         = 0x05,
		UserRequest       = 0x06,
		WrExecutive       = 0x07,
		WrFreePage        = 0x08,
		WrPageIn          = 0x09,
		WrPoolAllocation  = 0x0A,
		WrDelayExecution  = 0x0B,
		WrSuspended       = 0x0C,
		WrUserRequest     = 0x0D,
		WrEventPair       = 0x0E,
		WrQueue           = 0x0F,
		WrLpcReceive      = 0x10,
		WrLpcReply        = 0x11,
		WrVirtualMemory   = 0x12,
		WrPageOut         = 0x13,
		WrRendezvous      = 0x14,
		WrCalloutStack    = 0x19,
		WrKernel          = 0x1A,
		WrResource        = 0x1B,
		WrPushLock        = 0x1C,
		WrMutex           = 0x1D,
		WrQuantumEnd      = 0x1E,
		WrDispatchInt     = 0x1F,
		WrPreempted       = 0x20,
		WrYieldExecution  = 0x21,
		WrFastMutex       = 0x22,
		WrGuardedMutex    = 0x23,
		WrRundown         = 0x24,
		MaximumWaitReason = 0x25
	} KWAIT_REASON;
	//
	// NT internal data structrures are very dependable on process bitness; i.e. they're different for x86 and x64;
	// this must be taking into account due to possible memory corruption in runtime mode;
	//
	struct LDR_DATA_TABLE_ENTRY
	{
		LIST_ENTRY        le_load_order;    // InLoadOrder;
		LIST_ENTRY        le_mmry_order;    // InMemoryOrder;
		LIST_ENTRY        le_init_order;    // InInitOrder;
		PVOID             pv_module_base;   // DllBase;
		PVOID             pv_entry_pt;      // EntryPoint;
		ULONG             ul_image_sz;      // SizeOfImage;
		UNICODE_STRING    w_full_path;      // FullDllName;
		UNICODE_STRING    w_base_path;      // BaseDllName;
	};
	typedef LDR_DATA_TABLE_ENTRY  TLdrEntry;
	typedef LDR_DATA_TABLE_ENTRY* PLdrEntry;

	typedef struct _UNICODE_STRING32
	{
		WORD    Length   ;
		WORD    MaxLength;
		DWORD   szBuffer ;
	} UNICODE_STRING32;

	// TODO: original names must be returned back;
	struct LDR_DATA_TABLE_ENTRY32
	{
		LIST_ENTRY32      le_load_order;    // InLoadOrder;
		LIST_ENTRY32      le_mmry_order;    // InMemoryOrder;
		LIST_ENTRY32      le_init_order;    // InInitOrder;
		DWORD             pv_module_base;   // DllBase;
		DWORD             pv_entry_pt;      // EntryPoint;
		ULONG             ul_image_sz;      // SizeOfImage;
		UNICODE_STRING32  w_full_path;      // FullDllName;
		UNICODE_STRING32  w_base_path;      // BaseDllName;
	};
	typedef LDR_DATA_TABLE_ENTRY32* PLdrEntry32;

#ifndef BYTE
#define BYTE UCHAR
#endif

	struct PEB_LDR_DATA
	{
		ULONG           ul_size;                       // structure size/length;
		BYTE            bt_inited;                     // inited flag;
		HANDLE          h_sess;                        // session handle;
		LIST_ENTRY      le_load_order_head; // InLoadOrderModuleListHead; load module order list entry header;
		LIST_ENTRY      le_mmry_order_head; // InMemoryOrderModuleListHead; memory module order list entry header;
		LIST_ENTRY      le_init_order_head; // InInitializationOrderModuleListHead; init module order list entry header;
		PVOID           pv_entry_prog ;                // a pointer to entry poin progress;
		BYTE            pv_shutd_prog ;                // a pointer to shutdown progress;
		HANDLE          h_shutd_thread;                // a handle of shutdown thread;
	};

	struct PEB_LDR_DATA32
	{
		ULONG           ul_size;
		BYTE            bt_inited;
		DWORD           h_sess;
		LIST_ENTRY32    le_load_order_head; // InLoadOrderModuleListHead
		LIST_ENTRY32    le_mmry_order_head; // InMemoryOrderModuleListHead
		LIST_ENTRY32    le_init_order_head; // InInitializationOrderModuleListHead
		DWORD           pv_entry_prog;
		BYTE            pv_shutd_prog;
		DWORD           h_shutd_thread;
	};
	// https://en.wikipedia.org/wiki/Process_Environment_Block
	struct PEB {
		PVOID           pv_Reserved[3];
		PEB_LDR_DATA*   pb_ldr_data;
	};
	typedef PEB* PPEB64;

	struct PEB32 {
		DWORD           Reserved[3];
		DWORD           Ldr;
	};
	typedef PEB32* PPEB32;

	struct PROCESS_BASIC_INFORMATION
	{
		NTSTATUS	    l_exit_status;
		PEB*		    p_peb;
		ULONG_PTR	    p_affinity_mask ;
		LONG		    l_base_priority ;
		HANDLE		    h_uniq_proc_id;
		HANDLE		    h_inherited_uniq_proc_id;
	};

	struct PROCESS_SESSION_INFORMATION {
		ULONG           u_sess_id;
	};
	// docs.microsoft.com/en-us/windows/win32/procthread/zwqueryinformationprocess
	enum _PROCESSINFOCLASS
	{
		ProcessBasicInfo   =  PROCESSINFOCLASS::ProcessBasicInformation  ,
		ProcessSessInfo    =  24,
		ProcessWow64Info   =  PROCESSINFOCLASS::ProcessWow64Information  ,
	};
	typedef _PROCESSINFOCLASS PROCESSINFOCLASS;
	typedef  PROCESSINFOCLASS eProcInfoClass  ;

	class CSysInfoClass {
	public:
		enum _cls : DWORD{
			SystemProcessInformation	= 5,
			SystemHandleInformation		= 16
		};
	};

	typedef struct _SYSTEM_HANDLE_TABLE_ENTRY_INFO
	{
		WORD   UniqueProcessId     ;
		WORD   CreateBackTraceIndex;
		BYTE   ObjectTypeIndex     ;
		BYTE   HandleAttributes    ;
		WORD   HandleValue         ;
		PVOID  Object              ;
		ULONG  GrantedAccess       ;
	} SYSTEM_HANDLE_TABLE_ENTRY_INFO, *PSYSTEM_HANDLE_TABLE_ENTRY_INFO;

	typedef SYSTEM_HANDLE_TABLE_ENTRY_INFO TSysHandleTable ;
	typedef ::std::vector<TSysHandleTable> TSysTableEntries;

	typedef struct _SYSTEM_HANDLE_INFORMATION
	{
		ULONG NumberOfHandles;
		SYSTEM_HANDLE_TABLE_ENTRY_INFO Handles[1];
	}	SYSTEM_HANDLE_INFORMATION, *PSYSTEM_HANDLE_INFORMATION;

	typedef  SYSTEM_HANDLE_INFORMATION  TSysHandleInfo;
	typedef PSYSTEM_HANDLE_INFORMATION  TSysHandleInfoPtr;

	// https://docs.microsoft.com/en-us/windows/win32/api/winternl/nf-winternl-ntquerysysteminformation
	// this declaration is truncated and cannot be used in querying real data;
	// thus new one is defined:
	typedef struct _SYSTEM_PROCESS_INFORMATION
	{
		ULONG			NextEntryOffset             ;
		ULONG			NumberOfThreads             ;
		LARGE_INTEGER	WorkingSetPrivateSize       ;
		ULONG			HardFaultCount              ;
		ULONG			NumberOfThreadsHighWatermark;
		ULONGLONG		CycleTime                   ;
		LARGE_INTEGER	CreateTime                  ;
		LARGE_INTEGER	UserTime                    ;
		LARGE_INTEGER	KernelTime                  ;
		UNICODE_STRING	ImageName                   ;
		KPRIORITY		BasePriority                ;
		HANDLE			UniqueProcessId             ;
		HANDLE			InheritedFromUniqueProcessId;
		ULONG			HandleCount                 ;
		ULONG			SessionId                   ;
		ULONG_PTR		UniqueProcessKey            ;
		SIZE_T			PeakVirtualSize             ;
		SIZE_T			VirtualSize                 ;
		ULONG			PageFaultCount              ;
		SIZE_T 			PeakWorkingSetSize          ;
		SIZE_T			WorkingSetSize              ;
		SIZE_T			QuotaPeakPagedPoolUsage     ;
		SIZE_T 			QuotaPagedPoolUsage         ;
		SIZE_T 			QuotaPeakNonPagedPoolUsage  ;
		SIZE_T 			QuotaNonPagedPoolUsage      ;
		SIZE_T 			PagefileUsage               ;
		SIZE_T 			PeakPagefileUsage           ;
		SIZE_T 			PrivatePageCount            ;
		LARGE_INTEGER	ReadOperationCount          ;
		LARGE_INTEGER	WriteOperationCount         ;
		LARGE_INTEGER	OtherOperationCount         ;
		LARGE_INTEGER 	ReadTransferCount           ;
		LARGE_INTEGER	WriteTransferCount          ;
		LARGE_INTEGER	OtherTransferCount          ;
		SYSTEM_THREAD_INFORMATION Threads[1]        ;
	} SYSTEM_PROCESS_INFORMATION, *PSYSTEM_PROCESS_INFORMATION;

	typedef  SYSTEM_PROCESS_INFORMATION   TSysProcInfo     ;
	typedef PSYSTEM_PROCESS_INFORMATION   TSysProcInfoPtr  ;
	typedef  SYSTEM_THREAD_INFORMATION    TSysThreadInfo   ;
	typedef PSYSTEM_THREAD_INFORMATION    TSysThreadInfoPtr;

	enum THREAD_STATE {
		Running = 0x02,
		Waiting = 0x05
	};

	class CObjectType {
	public:
		enum _OBEJECT_TYPE_INDEX : BYTE
		{
			OTI_Unknown00           = 0x00,
			OTI_Unknown01           = 0x01,
			OTI_Unknown02           = 0x00,
			OTI_Directory           = 0x03,
			OTI_Unknown04           = 0x04,
			OTI_Token               = 0x05,
			OTI_Job                 = 0x06,
			OTI_Process             = 0x07,
			OTI_Thread              = 0x08,
			OTI_Unknown09           = 0x09,
			OTI_IoCompletionReserve = 0x0A,
			OTI_Unknown0B           = 0x0B,
			OTI_Unknown0C           = 0x0C,
			OTI_Unknown0D           = 0x0D,
			OTI_DebugObject         = 0x0E,
			OTI_Event               = 0x0F,
			OTI_Mutant              = 0x10,
			OTI_Unknown11           = 0x11,
			OTI_Semaphore           = 0x12,
			OTI_Timer               = 0x13,
			OTI_IRTimer             = 0x14,
			OTI_Unknown15           = 0x15,
			OTI_Unknown16           = 0x16,
			OTI_WindowStation       = 0x17,
			OTI_Desktop             = 0x18,
			OTI_Composition         = 0x19,
			OTI_RawInputManager     = 0x1A,
			OTI_Unknown1B           = 0x1B,
			OTI_TpWorkerFactory     = 0x1C,
			OTI_Unknown1D           = 0x1D,
			OTI_Unknown1E           = 0x1E,
			OTI_Unknown1F           = 0x1F,
			OTI_Unknown20           = 0x20,
			OTI_IoCompletion        = 0x21,
			OTI_WaitCompletionPacket= 0x22,
			OTI_File                = 0x23,
			OTI_Unknown24           = 0x24,
			OTI_Unknown25           = 0x25,
			OTI_Unknown26           = 0x26,
			OTI_Unknown27           = 0x27,
			OTI_Section             = 0x28,
			OTI_Session             = 0x29,
			OTI_Partition           = 0x2A,
			OTI_Key                 = 0x2B,
			OTI_Unknown2C           = 0x2C,
			OTI_ALPC_Port           = 0x2D,
			OTI_Unknown2E           = 0x2E,
			OTI_WmiGuid             = 0x2F,
			OTI_Unknown30           = 0x30,
			OTI_Unknown31           = 0x31,
			OTI_Unknown32           = 0x32,
			OTI_Unknown33           = 0x33,
			OTI_Unknown34           = 0x34,
			OTI_Unknown35           = 0x35,
		};
	};
	typedef CObjectType::_OBEJECT_TYPE_INDEX OBJECT_TYPE_INDEX;

	bool __lnk_warn_4221_block(void);

}}

#endif/*_SHAREDPROCDEFS_H_649A13B5_A035_4C8E_8899_CFB6ECD40EBE_INCLUDED*/