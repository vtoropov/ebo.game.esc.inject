#ifndef _SHAREDNTSYSERR_H_69509670_113F_4512_B334_DB464BFAFEBE_INCLUDED
#define _SHAREDNTSYSERR_H_69509670_113F_4512_B334_DB464BFAFEBE_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is internal NT error codes declaration file (https://github.com/guided-hacking/GuidedHacking-Injector);
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 28-Feb-2020 at 12:26:43p, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "shared.gen.sys.err.h"
namespace shared { namespace process {
	using shared::sys_core::CError;

#define __no_arg
/////////////////////////////////////////////////////////////////////////////

	class CError_Inj : public CError {
	                  typedef CError TBase;
	public:
		 CError_Inj(void);
		~CError_Inj(void);
	public:
		VOID   Code  (const DWORD);
	public:
		CError_Inj& operator=(const CError&) ;
	};
}}

typedef const shared::process::CError_Inj&   TJErrorRef;

#endif/*_SHAREDNTSYSERR_H_69509670_113F_4512_B334_DB464BFAFEBE_INCLUDED*/