#ifndef _SHAREDPROCINFO_H_1396C6FF_AB10_47DA_AE41_3265A35B353D_INCLUDED
#define _SHAREDPROCINFO_H_1396C6FF_AB10_47DA_AE41_3265A35B353D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Feb-2020 at 0:28:10a, UTC+7, Novosibirsk, Tulenina, Friday;
	This is Ebo Game Dyna injection project process information data interface declaration file;
*/
#include "shared.proc.defs.h"
#include "shared.nt.sys.funs.h"

#include "shared.gen.sys.err.h"
#include "shared.gen.raw.buf.h"
namespace shared { namespace process {
	using shared::sys_core::CError;
	using shared::common::CRawData;

	class CSysProcInfoPtr : public CRawData {
	                       typedef CRawData TData;
	public:
		 CSysProcInfoPtr(void);
		~CSysProcInfoPtr(void);

	public:
		const
		TSysProcInfoPtr Ptr  (void) const;
		TSysProcInfoPtr Ptr  (void)      ;
		HRESULT         Set  (const TSysProcInfo&  );
		HRESULT         Set  (const TSysProcInfoPtr);

	public:
		operator const TSysProcInfoPtr(void) const;
		operator       TSysProcInfoPtr(void)      ;

	public:
		CSysProcInfoPtr& operator= (const TSysProcInfo&  );
		CSysProcInfoPtr& operator= (const TSysProcInfoPtr);
	};

	class CSysThreadInfoPtr : public CRawData {
	                         typedef CRawData TData;
	public:
		 CSysThreadInfoPtr(void);
		~CSysThreadInfoPtr(void);

	public:
		const
		TSysThreadInfoPtr Ptr  (void) const;
		TSysThreadInfoPtr Ptr  (void)      ;
		HRESULT           Set  (const TSysThreadInfo&  );
		HRESULT           Set  (const TSysThreadInfoPtr);

	public:
		operator const TSysThreadInfoPtr(void) const;
		operator       TSysThreadInfoPtr(void)      ;

	public:
		CSysThreadInfoPtr& operator= (const TSysThreadInfo&  );
		CSysThreadInfoPtr& operator= (const TSysThreadInfoPtr);
	};

	class CProcessInfo {
	private:
		CSysProcInfoPtr    m_pCurrentProcess;
		CSysProcInfoPtr    m_pFirstProcess  ;
		CSysThreadInfoPtr  m_pCurrentThread ;

		ULONG  m_BufferSize = 0;
		HANDLE m_hCurrentProcess = nullptr;

		fn_NtQueryProcInfo m_pNtQueryProcInfo = nullptr;
		fn_NtQuerySysInfo  m_pNtQuerySysInfo  = nullptr;
	private:
		CError    m_error;

	public:
		 CProcessInfo(void);
		~CProcessInfo(void);

	public:
		PLdrEntry GetLdrEntry(HINSTANCE hMod);
		PPEB64    GetPEB(void);

	public:
		PVOID   GetEntryPoint (void);
		DWORD   GetPID        (void);
		DWORD   GetTID        (void);
		DWORD   GetThreadId   (void);
		bool    GetThreadState(THREAD_STATE& state, KWAIT_REASON& reason);
		bool    IsNative      (void);
		bool    NextThread    (void);
		bool    RefreshInfo   (void);
		bool    SetProcess    (HANDLE hTargetProc);
		bool    SetThread     (DWORD TID);

	public:
		const TSysProcInfoPtr   GetProcessInfo(void);
		const TSysThreadInfoPtr GetThreadInfo (void);

	public:
		TErrorRef   Error (void) const;
		bool        GetThreadStartAddress(PVOID& start_address);

	public:
#ifdef _WIN64
		PPEB32      GetPEB_WOW64(void);
		PLdrEntry32 GetLdrEntry_WOW64 (HINSTANCE hMod);
#endif
	private:
		PPEB64      GetPEB_Native(void);
		PLdrEntry   GetLdrEntry_Native(HINSTANCE hMod);
	};

}}

#endif/*_SHAREDPROCINFO_H_1396C6FF_AB10_47DA_AE41_3265A35B353D_INCLUDED*/