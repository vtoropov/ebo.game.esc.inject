#ifndef _SHAREDNTSYSTOOLS_H_A6C85810_F109_4424_BD4B_2BABB4E8C7F9_INCLUDED
#define _SHAREDNTSYSTOOLS_H_A6C85810_F109_4424_BD4B_2BABB4E8C7F9_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is NT system tool/helper function interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 10-Mar-2020 at 9:05:47a, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include <TlHelp32.h>
namespace shared { namespace sys_core { namespace tools {

	// a function to get the filepath to the file of this image of the injector;
	bool GetOwnModulePath (
		PWCHAR    pOut           , // a pointer to a widechar buffer to contain the full path;
		size_t    nBufferCchSize   // a size of the buffer in characters (bytes);
	);

	// a function to retrieve the session identifier of a process;
	ULONG GetSessionId (
		HANDLE    hTargetProc    , // a handle to the process; (PROCESS_QUERY_LIMITED_INFORMATION|PROCESS_QUERY_INFORMATION);
		NTSTATUS& ntRetOut         // a reference to an NTSTATUS variable to return a result;
	);

	// a function to quickly check whether a file exists or not;
	bool IsFileExist (
		LPCTSTR   szFile           // a pointer to a string containing the full path to the file;
	);

	// a function used to determine whether a process is running elevated or not (administrator vs. user);
	bool IsElevated(
		HANDLE hTargetProc         // a handle to the desired process; this handle must have the PROCESS_QUERY_INFORMATION access right;
	);

	// a function to determine whether a process runs natively or under WOW64;
	bool IsNative(
		HANDLE hTargetProc         // a handle to the desired process;
		                           // this handle must have the PROCESS_QUERY_LIMITED_INFORMATION | PROCESS_QUERY_INFORMATION access right;
	);

}}}

#endif/*_SHAREDNTSYSTOOLS_H_A6C85810_F109_4424_BD4B_2BABB4E8C7F9_INCLUDED*/