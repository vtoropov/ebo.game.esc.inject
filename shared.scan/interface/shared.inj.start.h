#ifndef _SHAREDINJSTART_H_C00C5585_EBAD_4F79_96D3_29D7696C4CE9_INCLUDED
#define _SHAREDINJSTART_H_C00C5585_EBAD_4F79_96D3_29D7696C4CE9_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is Inject start routine declaration file (https://github.com/guided-hacking/GuidedHacking-Injector);
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 9-Mar-2020 at 9:50:58p, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "shared.proc.info.h"
#include "shared.proc.obj.h"
namespace shared { namespace inject { namespace launch {

#ifdef _WIN64
using f_Routine        = ULONG_PTR(__fastcall*)(PVOID pArg);
using f_Routine_WOW64  = DWORD;
#else
using f_Routine        = ULONG_PTR(__stdcall*)(PVOID pArg);
#endif

//reinterpret_cast = too long to type
#ifndef ReCa
#define ReCa reinterpret_cast
#endif
//Macro to convert 32-bit DWORD into PVOID.
#ifndef MPTR
#define MPTR(d) (PVOID)(ULONG_PTR)d
#endif

//Macro to convert PVOID into 32-bit DWORD.
#ifndef MDWD
#define MDWD(p) (DWORD)((ULONG_PTR)p & 0xFFFFFFFF)
#endif

#define SR_REMOTE_TIMEOUT 2000

	//enum which is used to select the method to execute the shellcode;
	enum LAUNCH_METHOD {

		LM_NtCreateThreadEx,
		LM_HijackThread    ,
		LM_SetWindowsHookEx,
		LM_QueueUserAPC    ,
		LM_SetWindowLong   ,

	};
	typedef LAUNCH_METHOD eLaunch;

	// structure used when LM_SetWindowsHookEx is used;
	struct HookData {
		HHOOK	m_hHook;
		HWND	m_hWnd;
	};
	typedef ::std::vector<HookData>   THookData;

	class CHookData {
	private:
		HookData  m_hk_data;

	public:
		 CHookData (void) ;
		~CHookData (void) ;

	public:
		const HookData& Ref (void) const;
		      HookData& Ref (void)      ;
	public:
		operator const HookData& (void) const;
		operator       HookData& (void)      ;
	};

	// structure used when LM_SetWindowsHookEx is used;
	struct EnumWindowsCallback_Data {
		THookData   m_HookData ;
		DWORD       m_PID      ;
		HOOKPROC    m_pHook    ;
		HINSTANCE   m_hModule  ;
	};

	DWORD StartRoutine(
		HANDLE     hTargetProc , // a handle to the target process. Access rights depend on the launch method. PROCESS_ALL_ACCESS is the best option here;
		f_Routine* pRoutine    , // a pointer to the shellcode in the virtual memory of the target process. The function protoype must be "DWORD CC Func(PVOID pArg);
		PVOID      pArg        , // a pointer to the argument which gets passed to the shellcode;
		eLaunch    eMethod     , // a LAUNCH_METHOD enum which defines the method to be used when executing the shellcode;
		bool       bCloakThread, // a boolean which is only used when the eMethod parameter is LM_NtCreateThreadEx. Then a few cloaking related flags get passed to NtCreateThreadEx;
		DWORD&     LastWin32Err, // a reference to a DWORD which can be used to store an errorcode if something goes wrong. Otherwise it's INJ_ERROR_SUCCESS (0);
		ULONG_PTR& pOut          // a reference to a ULONG_PTR which is used to store the returned value of the shellcode. This can be changed into any datatype (a 32 bit type on x86 and a 64 bit type on x64);
	);                           // returns INJ_ERR_SUCCESS or error code;

#ifdef _WIN64
	DWORD StartRoutine_WOW64(
		HANDLE          hTargetProc ,
		f_Routine_WOW64 pRoutine    ,
		DWORD           pArg        ,
		eLaunch         eMethod     ,
		bool            bCloakThread,
		DWORD&          LastWin32Err,
		DWORD&          pOut
	);
#endif
}}}

#endif/*_SHAREDINJSTART_H_C00C5585_EBD_4F79_96D3_29D7696C4CE9_INCLUDED*/