#ifndef _SHAREDNTSYSFUNS_H_C32DEFF2_EBE0_4E9E_A8AA_04CB771EAB3C_INCLUDED
#define _SHAREDNTSYSFUNS_H_C32DEFF2_EBE0_4E9E_A8AA_04CB771EAB3C_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is internal NT function interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 28-Feb-2020 at 2:14:56a, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "shared.gen.sys.err.h"
#include "shared.proc.defs.h"
namespace shared { namespace process {
	using shared::sys_core::CError;

#define THREAD_CREATE_FLAGS_CREATE_SUSPENDED    0x00000001
#define THREAD_CREATE_FLAGS_SKIP_THREAD_ATTACH  0x00000002
#define THREAD_CREATE_FLAGS_HIDE_FROM_DEBUGGER  0x00000004

#define STATUS_INFO_LENGTH_MISMATCH 0xC0000004
#ifndef NT_FAIL
#define NT_FAIL(status) (status < 0)
#endif

#ifndef NT_SUCCESS
#define NT_SUCCESS(status) (status >= 0)
#endif

	using fn_LdrLoadDll       = NTSTATUS (__stdcall*)(wchar_t* szOptPath, ULONG ulFlags, UNICODE_STRING* pModuleFileName, HANDLE* pOut);
	using fn_NtCreateThreadEx = NTSTATUS (__stdcall*)(
		HANDLE*          pHandle      ,
		ACCESS_MASK      DesiredAccess,
		PVOID            pAttr        ,
		HANDLE           hTargetProc  ,
		PVOID            pFunc        ,
		PVOID            pArg         ,
		ULONG            Flags        ,
		SIZE_T           ZeroBits     ,
		SIZE_T           StackSize    ,
		SIZE_T           MaxStackSize ,
		PVOID            pAttrListOut
	);
#if (0)
	using fn_NtQueryProcessInformation = NTSTATUS (__stdcall*)(
		HANDLE           hTargetProc  ,
		PROCESSINFOCLASS pProcInfoCls ,
		PVOID            pBuffer      ,
		ULONG            BufferSize   ,
		ULONG*           SizeOut
	);
#else
	using fn_NtQueryProcInfo = NTSTATUS (__stdcall*)(
		HANDLE           hTargetProc  ,
		PROCESSINFOCLASS pProcInfoCls ,
		PVOID            pBuffer      ,
		ULONG            BufferSize   ,
		ULONG*           SizeOut
		);
#endif
#if (0)
	using fn_NtQuerySystemInformation = NTSTATUS (__stdcall*)(
		SYSTEM_INFORMATION_CLASS sysInfoCls, PVOID pBuffer, ULONG BufferSize, ULONG* SizeOut
	);
#else
	using fn_NtQuerySystemInformation = NTSTATUS (__stdcall*)(
		CSysInfoClass::_cls sysInfoCls, PVOID pBuffer, ULONG BufferSize, ULONG* SizeOut
		);
	using fn_NtQuerySysInfo = NTSTATUS (__stdcall*)(
		CSysInfoClass::_cls sysInfoCls, PVOID pBuffer, ULONG BufferSize, ULONG* SizeOut
		);
#endif
	using fn_RtlQueueApcWow64Thread = NTSTATUS(__stdcall*)(HANDLE hThread, PVOID pRoutine, PVOID pArg1, PVOID pArg2, PVOID pArg3);

	bool __lnk_warn_4221_block_2(void);
}}

#endif/*_SHAREDNTSYSFUNS_H_C32DEFF2_EBE0_4E9E_A8AA_04CB771EAB3C_INCLUDED*/