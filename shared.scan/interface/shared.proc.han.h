#ifndef _SHAREDPROCHAN_H_796B9098_F710_4816_83D6_B9132091CE93_INCLUDED
#define _SHAREDPROCHAN_H_796B9098_F710_4816_83D6_B9132091CE93_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is base inject handle interface declaration file (https://github.com/guided-hacking/GuidedHacking-Injector);
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 6-Mar-2020 at 5:02:45a, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "shared.nt.sys.err.h"
#include "shared.nt.sys.funs.h"
#include "shared.proc.defs.h"
namespace shared { namespace process {
	using shared::process::CError_Inj;

	class CHandle_Data {
	private:
		DWORD	m_owner_pid;
		WORD	m_value ;
		DWORD	m_access;

	public:
		 CHandle_Data (void);
		 CHandle_Data (const CHandle_Data&);
		~CHandle_Data (void);

	public:
		const DWORD&   Access  (void) const;
		      DWORD&   Access  (void)      ;
		const DWORD&   OwnerPID(void) const;
		      DWORD&   OwnerPID(void)      ;
		const  WORD&   Value   (void) const;
		       WORD&   Value   (void)      ;

	public:
		CHandle_Data&  operator= (const CHandle_Data&);
	};

	typedef ::std::vector <CHandle_Data> THandleData;
	// https://docs.microsoft.com/en-gb/windows/desktop/ProcThread/process-security-and-access-rights
	class CHandle_Enum {
	private:
		CError_Inj    m_j_error;
		THandleData   m_h_data ;

	public:
		 CHandle_Enum (void);
		~CHandle_Enum (void);

	public:
		const
		THandleData&  Data  (void) const;
		TJErrorRef    Error (void) const;
		HRESULT       Find  (const DWORD TargetPID, const DWORD WantedHandleAccess);
	};

}}

#endif/*_SHAREDPROCHAN_H_796B9098_F710_4816_83D6_B9132091CE93_INCLUDED*/