/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Feb-2020 at 2:10:14a, UTC+7, Novosibirsk, Tulenina, Friday;
	This is Ebo Game Dyna injection project process information data interface implementation file;
*/
#include "StdAfx.h"
#include "shared.proc.info.h"

using namespace shared::process;

#include "shared.inj.start.h"

using namespace shared::inject::launch;

#define re_cst reinterpret_cast
/////////////////////////////////////////////////////////////////////////////

CSysProcInfoPtr:: CSysProcInfoPtr(void) : TData() {}
CSysProcInfoPtr::~CSysProcInfoPtr(void)           {}

/////////////////////////////////////////////////////////////////////////////
const
TSysProcInfoPtr CSysProcInfoPtr::Ptr  (void) const { return reinterpret_cast<TSysProcInfoPtr>(this->GetData()); }
TSysProcInfoPtr CSysProcInfoPtr::Ptr  (void)       { return reinterpret_cast<TSysProcInfoPtr>(this->GetData()); }
HRESULT         CSysProcInfoPtr::Set  (const TSysProcInfo&   _ref) { return this->Set((TSysProcInfoPtr)&_ref); }
HRESULT         CSysProcInfoPtr::Set  (const TSysProcInfoPtr _ptr) {
	TData::m_error << __MODULE__ << S_OK;
	if (NULL == _ptr)
		return (TData::m_error = E_POINTER);

	TData::Create((PBYTE)_ptr, sizeof(TSysProcInfo));
	return TData::Error();
}

/////////////////////////////////////////////////////////////////////////////

CSysProcInfoPtr::operator const TSysProcInfoPtr(void) const { return this->Ptr(); }
CSysProcInfoPtr::operator       TSysProcInfoPtr(void)       { return this->Ptr(); }

/////////////////////////////////////////////////////////////////////////////

CSysProcInfoPtr& CSysProcInfoPtr::operator= (const TSysProcInfo&   _ref) { this->Set(_ref); return *this; }
CSysProcInfoPtr& CSysProcInfoPtr::operator= (const TSysProcInfoPtr _ptr) { this->Set(_ptr); return *this; }

/////////////////////////////////////////////////////////////////////////////

CSysThreadInfoPtr:: CSysThreadInfoPtr(void) : TData() {}
CSysThreadInfoPtr::~CSysThreadInfoPtr(void)           {}

/////////////////////////////////////////////////////////////////////////////
const
TSysThreadInfoPtr CSysThreadInfoPtr::Ptr  (void) const { return reinterpret_cast<TSysThreadInfoPtr>(this->GetData()); }
TSysThreadInfoPtr CSysThreadInfoPtr::Ptr  (void)       { return reinterpret_cast<TSysThreadInfoPtr>(this->GetData()); }
HRESULT           CSysThreadInfoPtr::Set  (const TSysThreadInfo&  _ref ) { return this->Set((TSysThreadInfoPtr)&_ref); }
HRESULT           CSysThreadInfoPtr::Set  (const TSysThreadInfoPtr _ptr) {
	TData::m_error << __MODULE__ << S_OK;
	if (NULL == _ptr)
		return (TData::m_error = E_POINTER);

	TData::Create((PBYTE)_ptr, sizeof(TSysThreadInfo));
	return TData::Error();
}

/////////////////////////////////////////////////////////////////////////////

CSysThreadInfoPtr::operator const TSysThreadInfoPtr(void) const { return this->Ptr(); }
CSysThreadInfoPtr::operator       TSysThreadInfoPtr(void)       { return this->Ptr(); }

/////////////////////////////////////////////////////////////////////////////

CSysThreadInfoPtr& CSysThreadInfoPtr::operator= (const TSysThreadInfo&   _ref) { this->Set(_ref); return *this; }
CSysThreadInfoPtr& CSysThreadInfoPtr::operator= (const TSysThreadInfoPtr _ptr) { this->Set(_ptr); return *this; }

/////////////////////////////////////////////////////////////////////////////

#define NEXT_SYSTEM_PROCESS_ENTRY(pCurrent) re_cst<TSysProcInfoPtr>( \
		re_cst<BYTE*>(pCurrent) + pCurrent->NextEntryOffset          \
	)

/////////////////////////////////////////////////////////////////////////////

CProcessInfo:: CProcessInfo(void) { m_error << __MODULE__ << S_OK >> __MODULE__;

	m_BufferSize	= 0x10000;

	HINSTANCE   h_nt_dll = ::GetModuleHandleA("ntdll.dll");
	if (NULL == h_nt_dll) {
		m_error = __LastErrToHresult(); return;
	}

	static LPCSTR lp_prc_fn = "NtQueryInformationProcess";
	static LPCSTR lp_sys_fn = "NtQuerySystemInformation" ;

	m_pNtQueryProcInfo = re_cst<fn_NtQueryProcInfo>	(::GetProcAddress(h_nt_dll, lp_prc_fn));
	m_pNtQuerySysInfo  = re_cst<fn_NtQuerySysInfo >	(::GetProcAddress(h_nt_dll, lp_sys_fn));

	static LPCTSTR lp_sz_pat = _T("Function [%s] cannot be loaded;");
	CAtlString cs_err;

	if (!m_pNtQueryProcInfo ) { cs_err.Format(lp_sz_pat, lp_prc_fn);
		(m_error = E_NOTIMPL) = (LPCTSTR)cs_err; return;
	}

	if (!m_pNtQuerySysInfo  ) { cs_err.Format(lp_sz_pat, lp_sys_fn);
		(m_error = E_NOTIMPL) = (LPCTSTR)cs_err; return;
	}

	RefreshInfo();
}
CProcessInfo::~CProcessInfo(void) {
#if (0)
	if (m_pFirstProcess) {
		try {
			delete[] m_pFirstProcess; m_pFirstProcess = NULL;
		}
		catch(...){}
	}
#endif
}

/////////////////////////////////////////////////////////////////////////////

PVOID   CProcessInfo::GetEntryPoint (void) {
	m_error << __MODULE__ << S_OK;

	if (m_pFirstProcess.IsValid() == false) {
		m_error = OLE_E_BLANK;
		return nullptr;
	}

	PPEB64 ppeb = this->GetPEB();
	if (!ppeb) {
		m_error = E_HANDLE;
		return nullptr;
	}

	PEB	peb = {0};
	if (!::ReadProcessMemory(m_hCurrentProcess, ppeb, &peb, sizeof(PEB), nullptr)) {
		m_error = __LastErrToHresult();
		return nullptr;
	}
	// https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-readprocessmemory
	PEB_LDR_DATA ldrdata = {0};
	if (!::ReadProcessMemory(m_hCurrentProcess, peb.pb_ldr_data, &ldrdata, sizeof(PEB_LDR_DATA), nullptr))
		return nullptr;

	LIST_ENTRY* pCurrentEntry = ldrdata.le_load_order_head.Flink;
	LIST_ENTRY* pLastEntry    = ldrdata.le_load_order_head.Blink;

	wchar_t NameBuffer[MAX_PATH]{ 0 };
	while (true) {
		LDR_DATA_TABLE_ENTRY CurrentEntry{0};
		if (::ReadProcessMemory(m_hCurrentProcess, pCurrentEntry, &CurrentEntry, sizeof(LDR_DATA_TABLE_ENTRY), nullptr))
		{
			if (::ReadProcessMemory(
				m_hCurrentProcess, CurrentEntry.w_base_path.Buffer, NameBuffer, CurrentEntry.w_base_path.Length, nullptr))
			{
				if (NameBuffer[CurrentEntry.w_base_path.Length / 2 - 1] == 'e') {
					return CurrentEntry.pv_entry_pt;
				}
			}
		}

		if (pCurrentEntry == pLastEntry)
			break;
		else
			pCurrentEntry = CurrentEntry.le_load_order.Flink;
	}

	return nullptr;
}

DWORD   CProcessInfo::GetPID        (void) { return ::GetProcessId(m_hCurrentProcess); }
DWORD   CProcessInfo::GetTID        (void) {
	if (m_pFirstProcess.IsValid() ==false)
		return 0;
	else
		return DWORD(reinterpret_cast<ULONG_PTR>(m_pCurrentThread.Ptr()->ClientId.UniqueThread) & 0xFFFFFFFF);
}

DWORD   CProcessInfo::GetThreadId   (void) { return this->GetTID(); }

bool    CProcessInfo::GetThreadState(THREAD_STATE& state, KWAIT_REASON& reason) {
	if (m_pFirstProcess.IsValid() == false)
		return false;

	state  = static_cast<THREAD_STATE>(m_pCurrentThread.Ptr()->ThreadState);
	reason = static_cast<KWAIT_REASON>(m_pCurrentThread.Ptr()->WaitReason );

	return true;
}

bool    CProcessInfo::IsNative      (void) {
	m_error << __MODULE__ << S_OK;
	// https://docs.microsoft.com/en-us/windows/win32/api/wow64apiset/nf-wow64apiset-iswow64process2
#if(1)
	BOOL bOut     = FALSE;
	if (FALSE == ::IsWow64Process(m_hCurrentProcess, &bOut))
		m_error = __LastErrToHresult();
	return (bOut == FALSE);
#else
	if (FALSE == ::IsWow64Process2()) {
		m_error = __LastErrToHresult();
	}
#endif
}

bool    CProcessInfo::NextThread    (void) {
	m_error << __MODULE__ << S_OK;

	bool b_result = false;

	if (m_pFirstProcess.IsValid() == false)
		if (RefreshInfo() == false)
			return b_result;

	for (UINT i_ = 0; i_ < m_pCurrentProcess.Ptr()->NumberOfThreads; i_++)
	{
		if (m_pCurrentProcess.Ptr()->Threads[i_].ClientId.UniqueThread == m_pCurrentThread.Ptr()->ClientId.UniqueThread)
		{
			if (i_ + 1 < m_pCurrentProcess.Ptr()->NumberOfThreads) {
				m_pCurrentThread = m_pCurrentProcess.Ptr()->Threads[1 + i_];
				return (b_result = true);
			}
			else {
				m_pCurrentThread = m_pCurrentProcess.Ptr()->Threads[0 + 0 ];
				return b_result;
			}
		}
	}

	m_pCurrentThread = m_pCurrentProcess.Ptr()->Threads[0];

	return b_result;
}

bool    CProcessInfo::RefreshInfo   (void) {
	m_error << __MODULE__ << S_OK;

	bool b_result = false;

	if (m_pFirstProcess.IsValid() == false) {
		m_pFirstProcess.Create(m_BufferSize);
		if (m_pFirstProcess == false)
			return b_result;
	}
	else {
		return RefreshInfo();
	}

	ULONG size_out = 0;
	NTSTATUS n_result = m_pNtQuerySysInfo(CSysInfoClass::SystemProcessInformation, (PBYTE)m_pFirstProcess, m_BufferSize, &size_out);

	while (n_result == STATUS_INFO_LENGTH_MISMATCH) {

		m_BufferSize = size_out + 0x1000;
		m_pFirstProcess.Create(m_BufferSize);
		if (m_pFirstProcess == false)
			return false;

		n_result = m_pNtQuerySysInfo(CSysInfoClass::SystemProcessInformation, (PBYTE)m_pFirstProcess, m_BufferSize, &size_out);
	}

	if (NT_FAIL(n_result)) {
		m_error = __LastErrToHresult();
		return  b_result;
	}

	m_pCurrentProcess = m_pFirstProcess;
	m_pCurrentThread  = m_pCurrentProcess.Ptr()->Threads[0];
	return (b_result  = true);
}

bool    CProcessInfo::SetProcess    (HANDLE hTargetProc) {

	bool b_result = false;

	if (NULL == hTargetProc)
		return b_result;

	if (NULL == m_pFirstProcess)
		if (NULL == RefreshInfo())
			return b_result;

	m_hCurrentProcess = hTargetProc;

	ULONG_PTR PID = ::GetProcessId(m_hCurrentProcess);

	while (NEXT_SYSTEM_PROCESS_ENTRY(m_pCurrentProcess.Ptr()) != m_pCurrentProcess.Ptr()) {
		if (m_pCurrentProcess.Ptr()->UniqueProcessId == re_cst<PVOID>(PID))
			break;

		m_pCurrentProcess = NEXT_SYSTEM_PROCESS_ENTRY(m_pCurrentProcess.Ptr());
	}

	if (m_pCurrentProcess.Ptr()->UniqueProcessId != re_cst<void*>(PID)){
		m_pCurrentProcess = m_pFirstProcess;
		return false;
	}

	m_pCurrentThread = m_pCurrentProcess.Ptr()->Threads[0];

	return true;
}

bool    CProcessInfo::SetThread     (DWORD TID) {

	bool b_result = false;

	if (NULL == m_pFirstProcess)
		if (b_result == RefreshInfo())
			return b_result;

	m_pCurrentThread = nullptr;

	for (UINT i_ = 0; i_ < m_pCurrentProcess.Ptr()->NumberOfThreads; i_++)
	{
		if (m_pCurrentProcess.Ptr()->Threads[i_].ClientId.UniqueThread == re_cst<PVOID>(ULONG_PTR(TID))) {
			m_pCurrentThread = m_pCurrentProcess.Ptr()->Threads[i_];
			break;
		}
	}

	if (m_pCurrentThread == false) {
		m_pCurrentThread =  m_pCurrentProcess.Ptr()->Threads[0];
		return b_result;
	}

	return (b_result = true);
}

/////////////////////////////////////////////////////////////////////////////

const TSysProcInfoPtr   CProcessInfo::GetProcessInfo(void) { return m_pCurrentProcess; }
const TSysThreadInfoPtr CProcessInfo::GetThreadInfo (void) { return m_pCurrentThread ; }

/////////////////////////////////////////////////////////////////////////////

PLdrEntry CProcessInfo::GetLdrEntry(HINSTANCE hMod) { return GetLdrEntry_Native(hMod); }
PPEB64    CProcessInfo::GetPEB (void) { return this->GetPEB_Native(); }

/////////////////////////////////////////////////////////////////////////////

TErrorRef CProcessInfo::Error (void) const { return m_error; }
bool      CProcessInfo::GetThreadStartAddress(PVOID& start_address) {

	if (m_pFirstProcess.IsValid() == false)
		return false;
	if (m_pCurrentThread.IsValid() == false)
		return false;

	start_address = m_pCurrentThread.Ptr()->StartAddress;

	return true;
}

#ifdef _WIN64
PPEB32       CProcessInfo::GetPEB_WOW64(void)
{
	if (m_pFirstProcess.IsValid() == false)
		return nullptr;
	if (m_pNtQueryProcInfo == NULL)
		return nullptr;
	if (m_hCurrentProcess  == NULL)
		return nullptr;

	ULONG_PTR pPEB = NULL;
	ULONG size_out = 0;
	NTSTATUS ntRet = m_pNtQueryProcInfo(m_hCurrentProcess, eProcInfoClass::ProcessWow64Info, &pPEB, sizeof(pPEB), &size_out);

	if (NT_FAIL(ntRet))
		return nullptr;

	return reinterpret_cast<PPEB32>(pPEB);
}

PLdrEntry32  CProcessInfo::GetLdrEntry_WOW64(HINSTANCE hMod)
{
	if (m_pFirstProcess.IsValid() == false)
		return nullptr;
	
	PPEB32 ppeb = this->GetPEB_WOW64();
	if (NULL == ppeb)
		return nullptr;

	if (NULL == m_hCurrentProcess)
		return nullptr;

	PEB32 peb{ 0 };
	if (FALSE == ::ReadProcessMemory(m_hCurrentProcess, ppeb, &peb, sizeof(PEB32), nullptr))
		return nullptr;
	
	PEB_LDR_DATA32 ldrdata{ 0 };
	if (FALSE == ::ReadProcessMemory(m_hCurrentProcess, MPTR(peb.Ldr), &ldrdata, sizeof(PEB_LDR_DATA32), nullptr))
		return nullptr;
		
	LIST_ENTRY32* pCurrentEntry = ReCa<LIST_ENTRY32*>((ULONG_PTR)ldrdata.le_load_order_head.Flink);
	LIST_ENTRY32* pLastEntry    = ReCa<LIST_ENTRY32*>((ULONG_PTR)ldrdata.le_load_order_head.Blink);

	while (true)
	{
		LDR_DATA_TABLE_ENTRY32 CurrentEntry{ 0 };
		::ReadProcessMemory(m_hCurrentProcess, pCurrentEntry, &CurrentEntry, sizeof(LDR_DATA_TABLE_ENTRY32), nullptr);

		if (CurrentEntry.pv_module_base == MDWD(hMod))
			return ReCa<LDR_DATA_TABLE_ENTRY32*>(pCurrentEntry);

		if (pCurrentEntry == pLastEntry)
			break;

		pCurrentEntry = ReCa<LIST_ENTRY32*>((ULONG_PTR)CurrentEntry.le_load_order.Flink);
	}

	return nullptr;
}
#endif
/////////////////////////////////////////////////////////////////////////////

PPEB64    CProcessInfo::GetPEB_Native()
{
	if (m_pFirstProcess.IsValid() == false)
		return nullptr;
	if (m_pNtQueryProcInfo == NULL)
		return nullptr;
	if (m_hCurrentProcess  == NULL)
		return nullptr;

	PROCESS_BASIC_INFORMATION PBI{ 0 };
	ULONG size_out = 0;
	NTSTATUS ntRet = m_pNtQueryProcInfo(m_hCurrentProcess, eProcInfoClass::ProcessWow64Info, &PBI, sizeof(PROCESS_BASIC_INFORMATION), &size_out);

	if (NT_FAIL(ntRet))
		return nullptr;

	return PBI.p_peb;
}

PLdrEntry CProcessInfo::GetLdrEntry_Native(HINSTANCE hMod)
{
	if (m_pFirstProcess.IsValid() == false)
		return nullptr;
	if (m_hCurrentProcess == NULL)
		return nullptr;

	PPEB64 ppeb = GetPEB();
	if (NULL == ppeb)
		return nullptr;

	PEB	peb{ 0 };
	if (FALSE == ::ReadProcessMemory(m_hCurrentProcess, ppeb, &peb, sizeof(PEB), nullptr))
		return nullptr;

	PEB_LDR_DATA ldrdata{ 0 };
	if (FALSE == ::ReadProcessMemory(m_hCurrentProcess, peb.pb_ldr_data, &ldrdata, sizeof(PEB_LDR_DATA), nullptr))
		return nullptr;

	LIST_ENTRY* pCurrentEntry = ldrdata.le_load_order_head.Flink;
	LIST_ENTRY* pLastEntry    = ldrdata.le_load_order_head.Blink;

	while (true)
	{
		LDR_DATA_TABLE_ENTRY CurrentEntry{ 0 };
		::ReadProcessMemory(m_hCurrentProcess, pCurrentEntry, &CurrentEntry, sizeof(LDR_DATA_TABLE_ENTRY), nullptr);

		if (CurrentEntry.pv_module_base == hMod)
			return ReCa<LDR_DATA_TABLE_ENTRY*>(pCurrentEntry);

		if (pCurrentEntry == pLastEntry)
			break;

		pCurrentEntry = CurrentEntry.le_load_order.Flink;
	}

	return nullptr;
}