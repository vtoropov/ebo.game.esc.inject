#ifndef _SHAREDNTSYSLIB_H_E0D783B2_89BC_47F1_9699_0E0A2F860825_INCLUDED
#define _SHAREDNTSYSLIB_H_E0D783B2_89BC_47F1_9699_0E0A2F860825_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is internal NT function interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 14-Mar-2020 at 12:42:39p, UTC+7, Novosibirsk, Tulenina, Saturday;
*/
#include "shared.nt.sys.funs.h"
#include "shared.inj.base.h"
#include "shared.inj.start.h"
namespace shared { namespace process {
	using shared::inject::launch::eLaunch;
	
#ifndef MAXPATH_IN_TCHAR
#define MAXPATH_IN_TCHAR   MAX_PATH
#endif

	using fn_LoadLibraryExW = decltype(::LoadLibraryExW);

	struct LOAD_LIBRARY_EXW_DATA
	{	
		HINSTANCE           hResult;
		fn_LoadLibraryExW*  pLoadLibraryExW;
		WCHAR               szDll[MAXPATH_IN_TCHAR];
	};
	typedef LOAD_LIBRARY_EXW_DATA TLibLoadData;

	DWORD _LoadLibraryExW (
		PCWCHAR    szDllFile  ,
		HANDLE     hTargetProc,
		eLaunch    eMethod    ,
		DWORD      dwFlags    ,
		HINSTANCE& hOut       ,
		DWORD&     dwLastError
	);

#ifdef _WIN64
	struct LOAD_LIBRARY_EXW_DATA_WOW64
	{	
		DWORD      hResult;
		DWORD      pLoadLibraryExW;
		WCHAR      szDll[MAXPATH_IN_TCHAR];
	};
	typedef LOAD_LIBRARY_EXW_DATA_WOW64 TLibLoadDataW64;

	DWORD _LoadLibrary_WOW64 (
		PCWCHAR    szDllFile  ,
		HANDLE     hTargetProc,
		eLaunch    eMethod    ,
		DWORD      dwFlags    ,
		HINSTANCE& hOut       ,
		DWORD&     dwLastError
	);
#endif
}}

#endif/*_SHAREDNTSYSLIB_H_E0D783B2_89BC_47F1_9699_0E0A2F860825_INCLUDED*/