/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is internal NT function interface implementation file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 14-Mar-2020 at 12:54:52p, UTC+7, Novosibirsk, Tulenina, Saturday;
*/
#include "StdAfx.h"
#include "shared.nt.sys.lib.h"
#include "shared.proc.obj.h"

using namespace shared::process;

#include "shared.inj.error.h"

using namespace shared::inject ;
using namespace shared::inject::launch;

/////////////////////////////////////////////////////////////////////////////

HINSTANCE __stdcall LoadLibraryExW_Shell (TLibLoadData* pData)
{
	if (!pData || !pData->pLoadLibraryExW)
		return NULL;

	pData->hResult = pData->pLoadLibraryExW(pData->szDll, nullptr, NULL);
	pData->pLoadLibraryExW = nullptr;

	return pData->hResult;
}

DWORD LoadLibraryExW_Shell_End() { return 0; }


/////////////////////////////////////////////////////////////////////////////
namespace shared { namespace process {

DWORD _LoadLibraryExW(PCWCHAR szDllFile, HANDLE hTargetProc, eLaunch eMethod, DWORD dwFlags, HINSTANCE& hOut, DWORD& dwLastError)
{
	TLibLoadData data{ 0 };
	::StringCchCopyW(data.szDll, sizeof(data.szDll) / sizeof(WCHAR), szDllFile);

	PVOID pLoadLibraryExW = nullptr;
	char sz_LoadLibName[] = "LoadLibraryExW";

	if (!CObject(hTargetProc).ProcAddress(_T("kernel32.dll"), sz_LoadLibName, pLoadLibraryExW)) {

		dwLastError = ::GetLastError();
		return INJ_ERR_REMOTEFUNC_MISSING;
	}

	data.pLoadLibraryExW = ReCa<fn_LoadLibraryExW*>(pLoadLibraryExW);

	ULONG_PTR ShellSize = (ULONG_PTR)LoadLibraryExW_Shell_End - (ULONG_PTR)LoadLibraryExW_Shell;

	PBYTE pArg = ReCa<PBYTE>(::VirtualAllocEx(
		hTargetProc, nullptr, sizeof(TLibLoadData) + ShellSize + 0x10, MEM_COMMIT|MEM_RESERVE, PAGE_EXECUTE_READWRITE)
		);
	if ( !pArg ) {
		dwLastError = ::GetLastError();
		return INJ_ERR_OUT_OF_MEMORY_EXT;
	}

	if (!::WriteProcessMemory(hTargetProc, pArg, &data, sizeof(TLibLoadData), nullptr))
	{
		dwLastError = ::GetLastError();
		::VirtualFreeEx(hTargetProc, pArg, 0, MEM_RELEASE);

		return INJ_ERR_WPM_FAIL;
	}

	auto* pShell = ReCa<PBYTE>(ALIGN_UP(ReCa<ULONG_PTR>(pArg + sizeof(TLibLoadData)), 0x10));
	if (!::WriteProcessMemory(hTargetProc, pShell, ::LoadLibraryExW_Shell, ShellSize, nullptr))
	{
		dwLastError = ::GetLastError();
		::VirtualFreeEx(hTargetProc, pArg, 0, MEM_RELEASE);

		return INJ_ERR_WPM_FAIL;
	}

	ULONG_PTR remote_ret = 0;
	DWORD dwRet = StartRoutine (
		hTargetProc, ReCa<f_Routine*>(pShell), pArg, eMethod, (dwFlags & INJ_THREAD_CREATE_CLOAKED) != 0, dwLastError, remote_ret
	);

	hOut = ReCa<HINSTANCE>(remote_ret);

	if (eMethod != LM_QueueUserAPC) {
		::VirtualFreeEx(hTargetProc, pArg, 0, MEM_RELEASE);
	}

	return dwRet;
}

#ifdef _WIN64

BYTE LoadLibrary_Shell_WOW64[] = 
{ 
	0x55, 0x8B, 0xEC, 0x56, 0x8B, 0x75, 0x08, 0x85,
	0xF6, 0x74, 0x1F, 0x8B, 0x4E, 0x04, 0x85, 0xC9,
	0x74, 0x18, 0x6A, 0x00, 0x6A, 0x00, 0x8D, 0x46,
	0x08, 0x50, 0xFF, 0xD1, 0x89, 0x06, 0xC7, 0x46,
	0x04, 0x00, 0x00, 0x00, 0x00, 0x5E, 0x5D, 0xC2,
	0x04, 0x00, 0x33, 0xC0, 0x5E, 0x5D, 0xC2, 0x04,
	0x00
};

DWORD _LoadLibrary_WOW64(PCWCHAR szDllFile, HANDLE hTargetProc, eLaunch eMethod, DWORD dwFlags, HINSTANCE& hOut, DWORD& dwLastError)
{
	TLibLoadDataW64 data{ 0 };
	::StringCchCopyW(data.szDll, sizeof(data.szDll) / sizeof(WCHAR), szDllFile);

	PVOID pLoadLibraryExW = nullptr;
	char sz_LoadLibName[] = "LoadLibraryExW";

	if (!CObject_WOW64(hTargetProc).ProcAddress(_T("kernel32.dll"), sz_LoadLibName, pLoadLibraryExW)) {
		dwLastError = ::GetLastError();
		return INJ_ERR_REMOTEFUNC_MISSING;
	}

	data.pLoadLibraryExW = (DWORD)(ULONG_PTR)pLoadLibraryExW;

	PBYTE pArg = ReCa<PBYTE>(
		::VirtualAllocEx(
			hTargetProc, nullptr, sizeof(TLibLoadDataW64) + sizeof(LoadLibrary_Shell_WOW64) + 0x10,
			MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE
		));
	if (!pArg) {
		dwLastError = ::GetLastError();
		return INJ_ERR_OUT_OF_MEMORY_EXT;
	}

	if (!::WriteProcessMemory(hTargetProc, pArg, &data, sizeof(TLibLoadDataW64), nullptr))
	{
		dwLastError = ::GetLastError();
		::VirtualFreeEx(hTargetProc, pArg, 0, MEM_RELEASE);

		return INJ_ERR_WPM_FAIL;
	}

	auto* pShell = ReCa<PBYTE>(ALIGN_UP(ReCa<ULONG_PTR>(pArg + sizeof(TLibLoadDataW64)), 0x10));
	if (!::WriteProcessMemory(hTargetProc, pShell, LoadLibrary_Shell_WOW64, sizeof(LoadLibrary_Shell_WOW64), nullptr))
	{
		dwLastError = ::GetLastError();
		::VirtualFreeEx(hTargetProc, pArg, 0, MEM_RELEASE);

		return INJ_ERR_WPM_FAIL;
	}

	DWORD remote_ret = 0;
	DWORD dwRet = StartRoutine_WOW64(
		hTargetProc, MDWD(pShell), MDWD(pArg), eMethod, (dwFlags & INJ_THREAD_CREATE_CLOAKED) != 0, dwLastError, remote_ret
	);
	hOut = (HINSTANCE)(ULONG_PTR)remote_ret;

	if(eMethod != LM_QueueUserAPC) {
		::VirtualFreeEx(hTargetProc, pArg, 0, MEM_RELEASE);
	}
	return dwRet;
}
#endif
}}