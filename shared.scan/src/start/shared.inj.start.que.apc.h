#ifndef _SHAREDINJSTARTQUEAPC_H_BE0C2B98_8F1B_417A_A02D_72E069A007CB_INCLUDED
#define _SHAREDINJSTARTQUEAPC_H_BE0C2B98_8F1B_417A_A02D_72E069A007CB_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is Inject start routine queue user APC declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 10-Mar-2020 at 2:54:25p, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "shared.inj.start.h"
namespace shared { namespace inject { namespace launch { namespace _impl {

	DWORD QueueUserAPC (
		HANDLE          hTargetProc ,
		f_Routine*      pRoutine    ,
		PVOID           pArg        ,
		DWORD&          LastWin32Err,
		ULONG_PTR&      pOut
	);

#ifdef _WIN64

	DWORD QueueUserAPC_WOW64 (
		HANDLE          hTargetProc ,
		f_Routine_WOW64 pRoutine    ,
		DWORD           pArg        ,
		DWORD&          LastWin32Err,
		DWORD&          pOut
	);

#endif

}}}}

#endif/*_SHAREDINJSTARTQUEAPC_H_BE0C2B98_8F1B_417A_A02D_72E069A007CB_INCLUDED*/