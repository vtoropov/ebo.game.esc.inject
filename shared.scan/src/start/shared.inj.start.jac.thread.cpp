/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is Inject start routine hijack thread implementation file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 10-Mar-2020 at 0:51:06a, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "StdAfx.h"
#include "shared.inj.start.jac.thread.h"

using namespace shared::inject::launch;
using namespace shared::inject::launch::_impl;

#include "shared.inj.error.h"
using namespace shared::inject;

#include "shared.nt.sys.funs.h"
using namespace shared::process;

/////////////////////////////////////////////////////////////////////////////

DWORD HijackThread(HANDLE hTargetProc, f_Routine* pRoutine, PVOID pArg, DWORD& LastWin32Err, ULONG_PTR& Out)
{
	CProcessInfo pi;
	if (!pi.SetProcess(hTargetProc)) {
		return SR_HT_ERR_PROC_INFO_FAIL;
	}

	DWORD ThreadID = pi.GetThreadId();
	do {
		KWAIT_REASON reason;
		THREAD_STATE state ;
		if (!pi.GetThreadState(state, reason))
			continue;

		if (reason != KWAIT_REASON::WrQueue)
		{
			ThreadID = pi.GetThreadId();
			break;
		}

	} while (pi.NextThread());

	if (!ThreadID) {
		return SR_HT_ERR_NO_THREADS;
	}

	HANDLE hThread = OpenThread(THREAD_SET_CONTEXT | THREAD_GET_CONTEXT | THREAD_SUSPEND_RESUME, FALSE, ThreadID);
	if (!hThread) {
		LastWin32Err = ::GetLastError();

		return SR_HT_ERR_OPEN_THREAD_FAIL;
	}

	if (SuspendThread(hThread) == (DWORD)-1)
	{
		LastWin32Err = ::GetLastError();
		::CloseHandle(hThread);

		return SR_HT_ERR_SUSPEND_FAIL;
	}

	CONTEXT OldContext{ 0 };
	OldContext.ContextFlags = CONTEXT_CONTROL;

	if (!::GetThreadContext(hThread, &OldContext))
	{
		LastWin32Err = ::GetLastError();

		::ResumeThread(hThread);
		::CloseHandle (hThread);

		return SR_HT_ERR_GET_CONTEXT_FAIL;
	}

	PVOID pCodecave = ::VirtualAllocEx(hTargetProc, nullptr, 0x100, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	if (! pCodecave )
	{
		LastWin32Err = ::GetLastError();
		::CloseHandle(hThread);

		return SR_HT_ERR_CANT_ALLOC_MEM;
	}

#ifdef _WIN64

	BYTE Shellcode[] =
	{
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,                         // - 0x08           -> returned value

		0x48, 0x83, 0xEC, 0x08,                                                 // + 0x00           -> sub rsp, 0x08

		0xC7, 0x04, 0x24, 0x00, 0x00, 0x00, 0x00,                               // + 0x04 (+ 0x07)  -> mov [rsp], RipLowPart
		0xC7, 0x44, 0x24, 0x04, 0x00, 0x00, 0x00, 0x00,                         // + 0x0B (+ 0x0F)  -> mov [rsp + 0x04], RipHighPart

		0x50, 0x51, 0x52, 0x41, 0x50, 0x41, 0x51, 0x41, 0x52, 0x41, 0x53,       // + 0x13           -> push r(a/c/d)x / r (8 - 11)
		0x9C,                                                                   // + 0x1E           -> pushfq

		0x48, 0xB8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,             // + 0x1F (+ 0x21)  -> mov rax, pRoutine
		0x48, 0xB9, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,             // + 0x29 (+ 0x2B)  -> mov rcx, pArg

		0x48, 0x83, 0xEC, 0x20,                                                 // + 0x33           -> sub rsp, 0x20
		0xFF, 0xD0,                                                             // + 0x37           -> call rax
		0x48, 0x83, 0xC4, 0x20,                                                 // + 0x39           -> add rsp, 0x20

		0x48, 0x8D, 0x0D, 0xB4, 0xFF, 0xFF, 0xFF,                               // + 0x3D           -> lea rcx, [pCodecave]
		0x48, 0x89, 0x01,                                                       // + 0x44           -> mov [rcx], rax

		0x9D,                                                                   // + 0x47           -> popfq
		0x41, 0x5B, 0x41, 0x5A, 0x41, 0x59, 0x41, 0x58, 0x5A, 0x59, 0x58,       // + 0x48           -> pop r(11-8) / r(d/c/a)x

		0xC6, 0x05, 0xA9, 0xFF, 0xFF, 0xFF, 0x00,                               // + 0x53           -> mov byte ptr[$ - 0x57], 0

		0xC3                                                                    // + 0x5A           -> ret
	}; // SIZE = 0x5B (+ 0x08)

	DWORD FuncOffset        = 0x08;
	DWORD CheckByteOffset   = 0x03 + FuncOffset;

	DWORD dwLoRIP = (DWORD)(OldContext.Rip & 0xFFFFFFFF);
	DWORD dwHiRIP = (DWORD)((OldContext.Rip >> 0x20) & 0xFFFFFFFF);

	*ReCa<DWORD*>(Shellcode + 0x07 + FuncOffset) = dwLoRIP;
	*ReCa<DWORD*>(Shellcode + 0x0F + FuncOffset) = dwHiRIP;

	*ReCa<void**>(Shellcode + 0x21 + FuncOffset) = pRoutine;
	*ReCa<void**>(Shellcode + 0x2B + FuncOffset) = pArg;

	OldContext.Rip = ReCa<ULONG_PTR>(pCodecave) + FuncOffset;

#else

	BYTE Shellcode[] =
	{
		0x00, 0x00, 0x00, 0x00,                     // - 0x04 (pCodecave) -> returned value                       ;buffer to store returned value (eax)

		0x83, 0xEC, 0x04,                           // + 0x00             -> sub esp, 0x04                        ;prepare stack for ret
		0xC7, 0x04, 0x24, 0x00, 0x00, 0x00, 0x00,   // + 0x03 (+ 0x06)    -> mov [esp], OldEip                    ;store old eip as return address

		0x50, 0x51, 0x52,                           // + 0x0A             -> psuh e(a/c/d)                        ;save e(a/c/d)x
		0x9C,                                       // + 0x0D             -> pushfd                               ;save flags register

		0xB9, 0x00, 0x00, 0x00, 0x00,               // + 0x0E (+ 0x0F)    -> mov ecx, pArg                        ;load pArg into ecx
		0xB8, 0x00, 0x00, 0x00, 0x00,               // + 0x13 (+ 0x14)    -> mov eax, pRoutine

		0x51,                                       // + 0x18             -> push ecx                             ;push pArg
		0xFF, 0xD0,                                 // + 0x19             -> call eax                             ;call target function

		0xA3, 0x00, 0x00, 0x00, 0x00,               // + 0x1B (+ 0x1C)    -> mov dword ptr[pCodecave], eax        ;store returned value

		0x9D,                                       // + 0x20             -> popfd                                ;restore flags register
		0x5A, 0x59, 0x58,                           // + 0x21             -> pop e(d/c/a)                         ;restore e(d/c/a)x

		0xC6, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00,   // + 0x24 (+ 0x26)    -> mov byte ptr[pCodecave + 0x06], 0x00 ;set checkbyte to 0

		0xC3                                        // + 0x2B             -> ret                                  ;return to OldEip
	}; // SIZE = 0x2C (+ 0x04)

	DWORD FuncOffset        = 0x04;
	DWORD CheckByteOffset   = 0x02 + FuncOffset;

	*ReCa<DWORD*>(Shellcode + 0x06 + FuncOffset) = OldContext.Eip;
	*ReCa<void**>(Shellcode + 0x0F + FuncOffset) = pArg;
	*ReCa<void**>(Shellcode + 0x14 + FuncOffset) = pRoutine;
	*ReCa<BYTE**>(Shellcode + 0x1C + FuncOffset) = ReCa<BYTE*>(pCodecave);
	*ReCa<BYTE**>(Shellcode + 0x26 + FuncOffset) = ReCa<BYTE*>(pCodecave) + CheckByteOffset;

	OldContext.Eip = ReCa<DWORD>(pCodecave) + FuncOffset;

#endif

	if (!::WriteProcessMemory(hTargetProc, pCodecave, Shellcode, sizeof(Shellcode), nullptr))
	{	
		LastWin32Err = ::GetLastError();

		::ResumeThread (hThread);
		::CloseHandle  (hThread);
		::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

		return SR_HT_ERR_WPM_FAIL;
	}

	if (!::SetThreadContext(hThread, &OldContext))
	{		
		LastWin32Err = ::GetLastError();

		::ResumeThread (hThread);
		::CloseHandle  (hThread);
		::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

		return SR_HT_ERR_SET_CONTEXT_FAIL;
	}

	::PostThreadMessage(ThreadID, 0, 0, 0);

	if (::ResumeThread(hThread) == (DWORD)-1)
	{
		LastWin32Err = ::GetLastError();

		::CloseHandle  (hThread);
		::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

		return SR_HT_ERR_RESUME_FAIL;
	}

	::CloseHandle(hThread);

	auto Timer      = GetTickCount64();
	BYTE CheckByte  = 1;

	do {
		::ReadProcessMemory(hTargetProc, ReCa<BYTE*>(pCodecave) + CheckByteOffset, &CheckByte, 1, nullptr);

		if (::GetTickCount64() - Timer > SR_REMOTE_TIMEOUT) {
			return SR_HT_ERR_TIMEOUT;
		}

		::Sleep(10);

	} while (CheckByte != 0);

	::ReadProcessMemory(hTargetProc, pCodecave, &Out, sizeof(Out), nullptr);
	::VirtualFreeEx    (hTargetProc, pCodecave, 0, MEM_RELEASE);

	return SR_ERR_SUCCESS;
}

#ifdef _WIN64

DWORD HijackThread_WOW64(HANDLE hTargetProc, f_Routine_WOW64 pRoutine, DWORD pArg, DWORD& LastWin32Err, DWORD& Out)
{
	CProcessInfo pi;
	if (!pi.SetProcess(hTargetProc)) {
		return SR_HT_ERR_PROC_INFO_FAIL;
	}

	HINSTANCE hRemoteNTDll = CObject_WOW64(hTargetProc).ModuleHandle(TEXT("ntdll.dll"));
	MODULEINFO mi{ 0 };
	::GetModuleInformation(hTargetProc, hRemoteNTDll, &mi, sizeof(mi));
	PBYTE pRemoteNTDll = ReCa<PBYTE> (  hRemoteNTDll);

	DWORD ThreadID = 0;
	do {
		KWAIT_REASON reason;
		THREAD_STATE state ;
		pi.GetThreadState(state, reason);

		if (reason != KWAIT_REASON::WrQueue || state == THREAD_STATE::Running) {
			if (pRemoteNTDll)
			{
				PVOID pStartAddress = nullptr;
				bool  bStartAddress = pi.GetThreadStartAddress(pStartAddress);
				if (  bStartAddress && pStartAddress ) {
					if ((ReCa<PBYTE>(  pStartAddress ) <  pRemoteNTDll) ||
					    (ReCa<PBYTE>(  pStartAddress ) >= pRemoteNTDll + mi.SizeOfImage))
					{
						ThreadID = pi.GetThreadId();
						break;
					}
				}
			}
			ThreadID = pi.GetThreadId();
			break;
		}

	} while (pi.NextThread());

	if (!ThreadID) {
		return SR_HT_ERR_NO_THREADS;
	}

	HANDLE hThread = ::OpenThread(THREAD_SET_CONTEXT | THREAD_GET_CONTEXT | THREAD_SUSPEND_RESUME, FALSE, ThreadID);
	if (  !hThread ) {
		LastWin32Err = ::GetLastError();

		return SR_HT_ERR_OPEN_THREAD_FAIL;
	}

	if (::SuspendThread(hThread) == (DWORD)-1)
	{
		LastWin32Err = ::GetLastError();

		::CloseHandle(hThread);

		return SR_HT_ERR_SUSPEND_FAIL;
	}

	WOW64_CONTEXT OldContext{ 0 };
	OldContext.ContextFlags = CONTEXT_CONTROL;

	if (!::Wow64GetThreadContext(hThread, &OldContext))
	{
		LastWin32Err = ::GetLastError();

		::ResumeThread(hThread);
		::CloseHandle (hThread);

		return SR_HT_ERR_GET_CONTEXT_FAIL;
	}

	PVOID pCodecave = ::VirtualAllocEx(hTargetProc, nullptr, 0x100, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	if ( !pCodecave )
	{
		LastWin32Err = ::GetLastError();

		::CloseHandle(hThread);

		return SR_HT_ERR_CANT_ALLOC_MEM;
	}

	BYTE Shellcode[] =
	{
		0x00, 0x00, 0x00, 0x00,                    // - 0x04 (pCodecave) -> returned value                       ;buffer to store returned value (eax)

		0x83, 0xEC, 0x04,                          // + 0x00             -> sub esp, 0x04                        ;prepare stack for ret
		0xC7, 0x04, 0x24, 0x00, 0x00, 0x00, 0x00,  // + 0x03 (+ 0x06)    -> mov [esp], OldEip                    ;store old eip as return address

		0x50, 0x51, 0x52,                          // + 0x0A             -> psuh e(a/c/d)                        ;save e(a/c/d)x
		0x9C,                                      // + 0x0D             -> pushfd                               ;save flags register

		0xB9, 0x00, 0x00, 0x00, 0x00,              // + 0x0E (+ 0x0F)    -> mov ecx, pArg                        ;load pArg into ecx
		0xB8, 0x00, 0x00, 0x00, 0x00,              // + 0x13 (+ 0x14)    -> mov eax, pRoutine

		0x51,                                      // + 0x18             -> push ecx                             ;push pArg
		0xFF, 0xD0,                                // + 0x19             -> call eax                             ;call target function

		0xA3, 0x00, 0x00, 0x00, 0x00,              // + 0x1B (+ 0x1C)    -> mov dword ptr[pCodecave], eax        ;store returned value

		0x9D,                                      // + 0x20             -> popfd                                ;restore flags register
		0x5A, 0x59, 0x58,                          // + 0x21             -> pop e(d/c/a)                         ;restore e(d/c/a)x

		0xC6, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00,  // + 0x24 (+ 0x26)    -> mov byte ptr[pCodecave + 0x06], 0x00 ;set checkbyte to 0

		0xC3                                       // + 0x2B             -> ret                                  ;return to OldEip
	}; // SIZE = 0x2C (+ 0x04)

	DWORD FuncOffset        = 0x04;
	DWORD CheckByteOffset   = 0x02 + FuncOffset;

	*ReCa<DWORD*>(Shellcode + 0x06 + FuncOffset) = OldContext.Eip;
	*ReCa<DWORD*>(Shellcode + 0x0F + FuncOffset) = pArg;
	*ReCa<DWORD*>(Shellcode + 0x14 + FuncOffset) = pRoutine;
	*ReCa<DWORD*>(Shellcode + 0x1C + FuncOffset) = MDWD(pCodecave);
	*ReCa<DWORD*>(Shellcode + 0x26 + FuncOffset) = MDWD(pCodecave) + CheckByteOffset;

	OldContext.Eip = MDWD(pCodecave) + FuncOffset;

	if (!::WriteProcessMemory(hTargetProc, pCodecave, Shellcode, sizeof(Shellcode), nullptr))
	{	
		LastWin32Err = ::GetLastError();

		::ResumeThread (hThread);
		::CloseHandle  (hThread);
		::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

		return SR_HT_ERR_WPM_FAIL;
	}

	if (!::Wow64SetThreadContext(hThread, &OldContext))
	{		
		LastWin32Err = ::GetLastError();

		::ResumeThread (hThread);
		::CloseHandle  (hThread);
		::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

		return SR_HT_ERR_SET_CONTEXT_FAIL;
	}

	::PostThreadMessage(ThreadID, 0, 0, 0);

	if (::ResumeThread(hThread) == (DWORD)-1)
	{
		LastWin32Err = ::GetLastError();

		::CloseHandle  (hThread);
		::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

		return SR_HT_ERR_RESUME_FAIL;
	}

	::CloseHandle(hThread);

	ULONGLONG Timer = ::GetTickCount64();
	BYTE CheckByte  = 1;

	do {
		::ReadProcessMemory(hTargetProc, ReCa<BYTE*>(pCodecave) + CheckByteOffset, &CheckByte, 1, nullptr);

		if (::GetTickCount64() - Timer > SR_REMOTE_TIMEOUT) {
			return SR_HT_ERR_TIMEOUT;
		}
		::Sleep(10);

	} while (CheckByte != 0);

	::ReadProcessMemory(hTargetProc, pCodecave, &Out, sizeof(Out), nullptr);
	::VirtualFreeEx    (hTargetProc, pCodecave, 0, MEM_RELEASE);

	return SR_ERR_SUCCESS;
}

#endif