#ifndef _SHAREDINJSTARTCRTTHREAD_H_D337AB39_3DDC_4441_B1D0_B3FFE385709D_INCLUDED
#define _SHAREDINJSTARTCRTTHREAD_H_D337AB39_3DDC_4441_B1D0_B3FFE385709D_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is Inject start routine create thread declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 9-Mar-2020 at 10:39:29p, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "shared.inj.start.h"
namespace shared { namespace inject { namespace launch { namespace _impl {

	DWORD NtCreateThreadEx (
		HANDLE          hTargetProc ,
		f_Routine*      pRoutine    ,
		PVOID           pArg        ,
		DWORD&          LastWin32Err,
		bool            bCloakThread,
		ULONG_PTR&      Out
	);

#ifdef _WIN64
	DWORD NtCreateThreadEx_WOW64 (
		HANDLE          hTargetProc ,
		f_Routine_WOW64 pRoutine    ,
		DWORD           pArg        ,
		DWORD&          LastWin32Err,
		bool            bCloakThread,
		DWORD&          Out
	);
#endif
}}}}

#endif/*_SHAREDINJSTARTCRTTHREAD_H_D337AB39_3DDC_4441_B1D0_B3FFE385709D_INCLUDED*/