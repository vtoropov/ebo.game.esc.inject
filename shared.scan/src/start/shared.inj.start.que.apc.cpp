/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is Inject start routine queue user APC declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 10-Mar-2020 at 3:48:50p, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "StdAfx.h"
#include "shared.inj.start.que.apc.h"

using namespace shared::inject::launch;
using namespace shared::inject::launch::_impl;

#include "shared.inj.error.h"
using namespace shared::inject;

#include "shared.nt.sys.funs.h"
#include "shared.proc.obj.h"
using namespace shared::process;

#include "shared.nt.sys.tools.h"
using namespace shared::sys_core::tools;

/////////////////////////////////////////////////////////////////////////////

DWORD QueueUserAPC(HANDLE hTargetProc, f_Routine* pRoutine, PVOID pArg, DWORD& LastWin32Err, ULONG_PTR& pOut)
{
	PVOID pCodecave = VirtualAllocEx(hTargetProc, nullptr, 0x100, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	if ( !pCodecave ) {
		LastWin32Err = ::GetLastError();

		return SR_QUAPC_ERR_CANT_ALLOC_MEM;
	}

#ifdef _WIN64

	BYTE Shellcode[] =
	{
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // - 0x18 -> returned value                       ;buffer to store returned value
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // - 0x10 -> pArg                                 ;buffer to store argument
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // - 0x08 -> pRoutine                             ;pointer to the rouinte to call

		0xEB, 0x00,                                     // + 0x00 -> jmp $+0x02                           ;jump to the next instruction

		0x48, 0x8B, 0x41, 0x10,                         // + 0x02 -> mov rax, [rcx + 0x10]                ;move pRoutine into rax
		0x48, 0x8B, 0x49, 0x08,                         // + 0x06 -> mov rcx, [rcx + 0x08]                ;move pArg into rcx

		0x48, 0x83, 0xEC, 0x28,                         // + 0x0A -> sub rsp, 0x28                        ;reserve stack
		0xFF, 0xD0,                                     // + 0x0E -> call rax                             ;call pRoutine
		0x48, 0x83, 0xC4, 0x28,                         // + 0x10 -> add rsp, 0x28                        ;update stack

		0x48, 0x85, 0xC0,                               // + 0x14 -> test rax, rax                        ;check if rax indicates success/failure
		0x74, 0x11,                                     // + 0x17 -> je pCodecave + 0x2A                  ;jmp to ret if routine failed

		0x48, 0x8D, 0x0D, 0xC8, 0xFF, 0xFF, 0xFF,       // + 0x19 -> lea rcx, [pCodecave]                 ;load pointer to codecave into rcx
		0x48, 0x89, 0x01,                               // + 0x20 -> mov [rcx], rax                       ;store returned value

		0xC6, 0x05, 0xD7, 0xFF, 0xFF, 0xFF, 0x28,       // + 0x23 -> mov byte ptr[pCodecave + 0x18], 0x28 ;hot patch jump to skip shellcode

		0xC3                                            // + 0x2A -> ret                                  ;return
	}; // SIZE = 0x2B (+ 0x10)

	DWORD CodeOffset = 0x18;

	*ReCa<void**>(Shellcode + 0x08) = pArg;
	*ReCa<void**>(Shellcode + 0x10) = pRoutine;

#else

	BYTE Shellcode[] = 
	{
		0x00, 0x00, 0x00, 0x00, // - 0x0C -> returned value                  ;buffer to store returned value
		0x00, 0x00, 0x00, 0x00, // - 0x08 -> pArg                            ;buffer to store argument
		0x00, 0x00, 0x00, 0x00, // - 0x04 -> pRoutine                        ;pointer to the routine to call

		0x55,                   // + 0x00 -> push ebp                        ;x86 stack frame creation
		0x8B, 0xEC,             // + 0x01 -> mov ebp, esp

		0xEB, 0x00,             // + 0x03 -> jmp pCodecave + 0x05 (+ 0x0C)   ;jump to next instruction

		0x53,                   // + 0x05 -> push ebx                        ;save ebx
		0x8B, 0x5D, 0x08,       // + 0x06 -> mov ebx, [ebp + 0x08]           ;move pCodecave into ebx (non volatile)

		0xFF, 0x73, 0x04,       // + 0x09 -> push [ebx + 0x04]               ;push pArg on stack
		0xFF, 0x53, 0x08,       // + 0x0C -> call dword ptr[ebx + 0x08]      ;call pRoutine

		0x85, 0xC0,             // + 0x0F -> test eax, eax                   ;check if eax indicates success/failure
		0x74, 0x06,             // + 0x11 -> je pCodecave + 0x19 (+ 0x0C)    ;jmp to cleanup if routine failed

		0x89, 0x03,             // + 0x13 -> mov [ebx], eax                  ;store returned value
		0xC6, 0x43, 0x10, 0x15, // + 0x15 -> mov byte ptr [ebx + 0x10], 0x15 ;hot patch jump to skip shellcode

		0x5B,                   // + 0x19 -> pop ebx                         ;restore old ebx

		0x5D,                   // + 0x1A -> pop ebp                         ;restore ebp
		0xC2, 0x04, 0x00        // + 0x1B -> ret 0x0004                      ;return
	}; // SIZE = 0x1E (+ 0x0C)

	DWORD CodeOffset = 0x0C;

	*ReCa<void**>(Shellcode + 0x04) = pArg;
	*ReCa<void**>(Shellcode + 0x08) = pRoutine;

#endif

	BOOL bRet = ::WriteProcessMemory(hTargetProc, pCodecave, Shellcode, sizeof(Shellcode), nullptr);
	if (!bRet)
	{
		LastWin32Err = ::GetLastError();
		::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

		return SR_QUAPC_ERR_WPM_FAIL;
	}

	CProcessInfo pi;
	if (!pi.SetProcess(hTargetProc)) {

		return SR_HT_ERR_PROC_INFO_FAIL;
	}

	bool APCQueued	= false;
	PAPCFUNC pShellcode = ReCa<PAPCFUNC>(ReCa<BYTE*>(pCodecave) + CodeOffset);

	do {
		KWAIT_REASON reason;
		THREAD_STATE state ;
		if (!pi.GetThreadState(state, reason))
			continue;

		if (reason == KWAIT_REASON::WrQueue)
			continue;

		DWORD ThreadId = pi.GetTID();
		if ( !ThreadId )
			continue;

		HANDLE hThread = ::OpenThread(THREAD_SET_CONTEXT, FALSE, ThreadId);
		if (!hThread)
			continue;

		if (::QueueUserAPC(pShellcode, hThread, (ULONG_PTR)pCodecave))
		{
			APCQueued = true;
			::PostThreadMessageW(ThreadId, WM_NULL, 0, 0);
		}
		else {
			LastWin32Err = ::GetLastError();
		}

		::CloseHandle(hThread);			

	} while (pi.NextThread());

	if (!APCQueued) {
		::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

		return SR_QUAPC_ERR_NO_APC_THREAD;
	}
	else {
		LastWin32Err = 0;
	}

	auto Timer = ::GetTickCount64();
	pOut = 0;

	do {
		::ReadProcessMemory(hTargetProc, pCodecave, &pOut, sizeof(pOut), nullptr);

		if (::GetTickCount64() - Timer > SR_REMOTE_TIMEOUT) {
			return SR_QUAPC_ERR_TIMEOUT;
		}

		::Sleep(10);
	} while (!pOut);

	return SR_ERR_SUCCESS;
}

#ifdef _WIN64

DWORD QueueUserAPC_WOW64(HANDLE hTargetProc, f_Routine_WOW64 pRoutine, DWORD pArg, DWORD& LastWin32Err, DWORD& pOut)
{
	HINSTANCE h_nt_dll = GetModuleHandleA("ntdll.dll");
	if (!h_nt_dll) {
		LastWin32Err = ::GetLastError();

		return SR_QUAPC_ERR_GET_MODULE_HANDLE_FAIL;
	}

	auto p_RtlQueueApcWow64Thread = reinterpret_cast<fn_RtlQueueApcWow64Thread>(GetProcAddress(h_nt_dll, "RtlQueueApcWow64Thread"));
	if (!p_RtlQueueApcWow64Thread)
	{
		LastWin32Err = ::GetLastError();
		return SR_QUAPC_ERR_RTLQAW64_MISSING;
	}

	PVOID pCodecave = ::VirtualAllocEx(hTargetProc, nullptr, 0x100, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	if ( !pCodecave )
	{
		LastWin32Err = ::GetLastError();
		return SR_QUAPC_ERR_CANT_ALLOC_MEM;
	}

	BYTE Shellcode[] =
	{
		0x00, 0x00, 0x00, 0x00, // - 0x0C -> returned value                  ;buffer to store returned value
		0x00, 0x00, 0x00, 0x00, // - 0x08 -> pArg                            ;buffer to store argument
		0x00, 0x00, 0x00, 0x00, // - 0x04 -> pRoutine                        ;pointer to the routine to call

		0x55,                   // + 0x00 -> push ebp                        ;x86 stack frame creation
		0x8B, 0xEC,             // + 0x01 -> mov ebp, esp

		0xEB, 0x00,             // + 0x03 -> jmp pCodecave + 0x05 (+ 0x0C)   ;jump to next instruction

		0x53,                   // + 0x05 -> push ebx                        ;save ebx
		0x8B, 0x5D, 0x08,       // + 0x06 -> mov ebx, [ebp + 0x08]           ;move pCodecave into ebx (non volatile)

		0xFF, 0x73, 0x04,       // + 0x09 -> push [ebx + 0x04]               ;push pArg on stack
		0xFF, 0x53, 0x08,       // + 0x0C -> call dword ptr[ebx + 0x08]      ;call pRoutine

		0x85, 0xC0,             // + 0x0F -> test eax, eax                   ;check if eax indicates success/failure
		0x74, 0x06,             // + 0x11 -> je pCodecave + 0x19 (+ 0x0C)    ;jmp to cleanup if routine failed

		0x89, 0x03,             // + 0x13 -> mov [ebx], eax                  ;store returned value
		0xC6, 0x43, 0x10, 0x15, // + 0x15 -> mov byte ptr [ebx + 0x10], 0x15 ;hot patch jump to skip shellcode

		0x5B,                   // + 0x19 -> pop ebx                         ;restore old ebx

		0x5D,                   // + 0x1A -> pop ebp                         ;restore ebp
		0xC2, 0x04, 0x00        // + 0x1B -> ret 0x0004                      ;return
	}; // SIZE = 0x1E (+ 0x0C)

	DWORD CodeOffset = 0x0C;

	*ReCa<DWORD*>(Shellcode + 0x04) = pArg;
	*ReCa<DWORD*>(Shellcode + 0x08) = pRoutine;

	BOOL bRet = ::WriteProcessMemory(hTargetProc, pCodecave, Shellcode, sizeof(Shellcode), nullptr);
	if (!bRet) {
		LastWin32Err = ::GetLastError();
		::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

		return SR_QUAPC_ERR_WPM_FAIL;
	}

	CProcessInfo pi;
	if (!pi.SetProcess(hTargetProc)) {
		return SR_HT_ERR_PROC_INFO_FAIL;
	}

	bool     APCQueued  = false;
	PAPCFUNC pShellcode = (PAPCFUNC)((ULONG_PTR)(MDWD((ULONG_PTR)pCodecave + CodeOffset)));

	do {
		KWAIT_REASON reason;
		THREAD_STATE state ;
		if (!pi.GetThreadState(state, reason))
			continue;

		if (reason == KWAIT_REASON::WrQueue)
			continue;

		DWORD ThreadId = pi.GetTID();
		if ( !ThreadId )
			continue;

		HANDLE hThread = ::OpenThread(THREAD_SET_CONTEXT, FALSE, ThreadId);
		if (  !hThread )
			continue;

		if (NT_SUCCESS(p_RtlQueueApcWow64Thread(hThread, pShellcode, pCodecave, nullptr, nullptr)))
		{
			APCQueued = true;
			::PostThreadMessageW(ThreadId, WM_NULL, 0, 0);
		}
		else {
			LastWin32Err = ::GetLastError();
		}

		::CloseHandle(hThread);

	} while (pi.NextThread());

	if (!APCQueued) {
		::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);
		return SR_QUAPC_ERR_NO_APC_THREAD;
	}
	else {
		LastWin32Err = 0;
	}

	ULONGLONG Timer = ::GetTickCount64();
	pOut = 0;

	do {
		::ReadProcessMemory(hTargetProc, pCodecave, &pOut, sizeof(pOut), nullptr);

		if (::GetTickCount64() - Timer > SR_REMOTE_TIMEOUT) {
			return SR_QUAPC_ERR_TIMEOUT;
		}
		::Sleep(10);

	} while (!pOut);

	return SR_ERR_SUCCESS;
}

#endif