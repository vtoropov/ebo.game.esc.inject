/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is Inject start routine window hook implementation file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 10-Mar-2020 at 2:36:07a, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "StdAfx.h"
#include "shared.inj.start.win.hook.h"

using namespace shared::inject::launch;
using namespace shared::inject::launch::_impl;

#include "shared.inj.error.h"
using namespace shared::inject;

#include "shared.nt.sys.funs.h"
#include "shared.proc.obj.h"
using namespace shared::process;

#include "shared.nt.sys.tools.h"
using namespace shared::sys_core::tools;

typedef std::basic_ofstream<wchar_t, ::std::char_traits<wchar_t>> fw_stream;
/////////////////////////////////////////////////////////////////////////////

DWORD SetWindowsHookEx(HANDLE hTargetProc, f_Routine* pRoutine, PVOID pArg, DWORD& LastWin32Err, ULONG TargetSessionId, ULONG_PTR& pOut)
{
	#define w_path_sz_req       (_MAX_PATH * 2)                // in charicters;
	static UINT b_path_sz_req = w_path_sz_req * sizeof(WCHAR); // in bytes;

	WCHAR RootPath[w_path_sz_req] = { 0 };

	if (!::GetOwnModulePath(RootPath, b_path_sz_req)) {
		return SR_SWHEX_ERR_CANT_QUERY_INFO_PATH;
	}

	WCHAR InfoPath[w_path_sz_req] = { 0 };

	::memcpy_s(InfoPath, b_path_sz_req, RootPath, b_path_sz_req);

	::StringCbCatW(InfoPath, sizeof(InfoPath), shared::__get_mod_hand_path());

	if (::IsFileExist(InfoPath))
		::DeleteFileW(InfoPath);

	fw_stream swhex_info(InfoPath, ::std::ios_base::out | ::std::ios_base::app);
	if (swhex_info.rdstate() & std::ofstream::failbit)
	{
		swhex_info.close();
		return SR_SWHEX_ERR_CANT_OPEN_INFO_TXT;
	}

	PVOID pCodecave = ::VirtualAllocEx(hTargetProc, nullptr, 0x1000, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	if ( !pCodecave ) {

		LastWin32Err = ::GetLastError();
		swhex_info.close();

		return SR_SWHEX_ERR_VAE_FAIL;
	}

	PVOID pCallNextHookEx = nullptr;

	CObject(hTargetProc).ProcAddress(_T("user32.dll"), "CallNextHookEx", pCallNextHookEx);
	if (!pCallNextHookEx) {

		::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

		return SR_SWHEX_ERR_CNHEX_MISSING;
	}

#ifdef _WIN64

	BYTE Shellcode[] =
	{
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // - 0x18 -> pArg / returned value / rax  ;buffer
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // - 0x10 -> pRoutine                     ;pointer to target function
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // - 0x08 -> CallNextHookEx               ;pointer to CallNextHookEx

		0x55,                                           // + 0x00 -> push rbp                     ;save important registers
		0x54,                                           // + 0x01 -> push rsp
		0x53,                                           // + 0x02 -> push rbx

		0x48, 0x8D, 0x1D, 0xDE, 0xFF, 0xFF, 0xFF,       // + 0x03 -> lea rbx, [pArg]              ;load pointer into rbx

		0x48, 0x83, 0xEC, 0x20,                         // + 0x0A -> sub rsp, 0x20                ;reserve stack
		0x4D, 0x8B, 0xC8,                               // + 0x0E -> mov r9,r8                    ;set up arguments for CallNextHookEx
		0x4C, 0x8B, 0xC2,                               // + 0x11 -> mov r8, rdx
		0x48, 0x8B, 0xD1,                               // + 0x14 -> mov rdx,rcx
		0xFF, 0x53, 0x10,                               // + 0x17 -> call [rbx + 0x10]            ;call CallNextHookEx
		0x48, 0x83, 0xC4, 0x20,                         // + 0x1A -> add rsp, 0x20                ;update stack

		0x48, 0x8B, 0xC8,                               // + 0x1E -> mov rcx, rax                 ;copy retval into rcx

		0xEB, 0x00,                                     // + 0x21 -> jmp $ + 0x02                 ;jmp to next instruction
		0xC6, 0x05, 0xF8, 0xFF, 0xFF, 0xFF, 0x18,       // + 0x23 -> mov byte ptr[$ - 0x01], 0x1A ;hotpatch jmp above to skip shellcode

		0x48, 0x87, 0x0B,                               // + 0x2A -> xchg [rbx], rcx              ;store CallNextHookEx retval, load pArg
		0x48, 0x83, 0xEC, 0x20,                         // + 0x2D -> sub rsp, 0x20                ;reserver stack
		0xFF, 0x53, 0x08,                               // + 0x31 -> call [rbx + 0x08]            ;call pRoutine
		0x48, 0x83, 0xC4, 0x20,                         // + 0x34 -> add rsp, 0x20                ;update stack

		0x48, 0x87, 0x03,                               // + 0x38 -> xchg [rbx], rax              ;store pRoutine retval, restore CallNextHookEx retval

		0x5B,                                           // + 0x3B -> pop rbx                      ;restore important registers
		0x5C,                                           // + 0x3C -> pop rsp
		0x5D,                                           // + 0x3D -> pop rbp

		0xC3                                            // + 0x3E -> ret                          ;return
	}; // SIZE = 0x3F (+ 0x18)

	DWORD CodeOffset        = 0x18;
	DWORD CheckByteOffset   = 0x19 + CodeOffset;

	*ReCa<void**>(Shellcode + 0x00) = pArg;
	*ReCa<void**>(Shellcode + 0x08) = pRoutine;
	*ReCa<void**>(Shellcode + 0x10) = pCallNextHookEx;

#else

	BYTE Shellcode[] =
	{
		0x00, 0x00, 0x00, 0x00,       // - 0x08          -> pArg                   ;pointer to argument
		0x00, 0x00, 0x00, 0x00,       // - 0x04          -> pRoutine               ;pointer to target function

		0x55,                         // + 0x00          -> push ebp               ;x86 stack frame creation
		0x8B, 0xEC,                   // + 0x01          -> mov ebp, esp

		0xFF, 0x75, 0x10,             // + 0x03          -> push [ebp + 0x10]      ;push CallNextHookEx arguments
		0xFF, 0x75, 0x0C,             // + 0x06          -> push [ebp + 0x0C] 
		0xFF, 0x75, 0x08,             // + 0x09          -> push [ebp + 0x08]
		0x6A, 0x00,                   // + 0x0C          -> push 0x00
		0xE8, 0x00, 0x00, 0x00, 0x00, // + 0x0E (+ 0x0F) -> call CallNextHookEx    ;call CallNextHookEx

		0xEB, 0x00,                   // + 0x13          -> jmp $ + 0x02           ;jmp to next instruction

		0x50,                         // + 0x15          -> push eax               ;save eax (CallNextHookEx retval)
		0x53,                         // + 0x16          -> push ebx               ;save ebx (non volatile)

		0xBB, 0x00, 0x00, 0x00, 0x00, // + 0x17 (+ 0x18) -> mov ebx, pArg          ;move pArg (pCodecave) into ebx
		0xC6, 0x43, 0x1C, 0x14,       // + 0x1C          -> mov [ebx + 0x1C], 0x17 ;hotpatch jmp above to skip shellcode

		0xFF, 0x33,                   // + 0x20          -> push [ebx] (default)   ;push pArg (__cdecl/__stdcall)

		0xFF, 0x53, 0x04,             // + 0x22          -> call [ebx + 0x04]      ;call target function

		0x89, 0x03,                   // + 0x25          -> mov [ebx], eax         ;store returned value

		0x5B,                         // + 0x27          -> pop ebx                ;restore old ebx
		0x58,                         // + 0x28          -> pop eax                ;restore eax (CallNextHookEx retval)

		0x5D,                         // + 0x29          -> pop ebp                ;restore ebp
		0xC2, 0x0C, 0x00              // + 0x2A          -> ret 0x000C             ;return
	}; // SIZE = 0x3D (+ 0x08)

	DWORD CodeOffset        = 0x08;
	DWORD CheckByteOffset   = 0x14 + CodeOffset;

	*ReCa<void**>(Shellcode + 0x00) = pArg;
	*ReCa<void**>(Shellcode + 0x04) = pRoutine;

	*ReCa<DWORD*>(Shellcode + 0x0F + CodeOffset) = ReCa<DWORD>(pCallNextHookEx) - (ReCa<DWORD>(pCodecave) + 0x0E + CodeOffset) - 5;
	*ReCa<void**>(Shellcode + 0x18 + CodeOffset) = pCodecave;

#endif

	if (!::WriteProcessMemory(hTargetProc, pCodecave, Shellcode, sizeof(Shellcode), nullptr))
	{
		LastWin32Err = ::GetLastError();

		::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

		return SR_SWHEX_ERR_WPM_FAIL;
	}

	swhex_info << std::dec << GetProcessId(hTargetProc) << '!' << std::hex << ReCa<ULONG_PTR>(pCodecave) + CodeOffset << std::endl;
	swhex_info.close();

	::StringCbCatW(RootPath, sizeof(RootPath), shared::__get_mod_hand_path());

	PROCESS_INFORMATION pi{ 0 };
	STARTUPINFOW		si{ 0 };
	si.cb			= sizeof(si);
	si.dwFlags		= STARTF_USESHOWWINDOW;
	si.wShowWindow	= SW_HIDE;

	if (TargetSessionId != -1)
	{
		HANDLE hUserToken = nullptr;
		if (!::WTSQueryUserToken(TargetSessionId, &hUserToken))
		{
			LastWin32Err = ::GetLastError();

			::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

			return SR_SWHEX_ERR_WTSQUERY_FAIL;
		}

		HANDLE hNewToken = nullptr;
		if (!::DuplicateTokenEx(hUserToken, MAXIMUM_ALLOWED, nullptr, SecurityIdentification, TokenPrimary, &hNewToken))
		{
			LastWin32Err = ::GetLastError();

			::CloseHandle  (hUserToken);
			::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

			return SR_SWHEX_ERR_DUP_TOKEN_FAIL;
		}

		DWORD SizeOut = 0;
		TOKEN_LINKED_TOKEN admin_token{ 0 };
		if (!::GetTokenInformation(hNewToken, TokenLinkedToken, &admin_token, sizeof(admin_token), &SizeOut))
		{
			LastWin32Err = ::GetLastError();

			::CloseHandle  (hNewToken);
			::CloseHandle  (hUserToken);
			::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

			return SR_SWHEX_ERR_GET_ADMIN_TOKEN_FAIL;
		}
		HANDLE hAdminToken = admin_token.LinkedToken;

		si.cb = sizeof(si);
		si.dwFlags = STARTF_USESHOWWINDOW;
		si.wShowWindow = SW_HIDE;

		if (!::CreateProcessAsUserW(
			hAdminToken, RootPath, nullptr, nullptr, nullptr, FALSE, CREATE_NO_WINDOW, nullptr, nullptr, &si, &pi
		))
		{
			LastWin32Err = ::GetLastError();

			::CloseHandle  (hAdminToken);
			::CloseHandle  (hNewToken  );
			::CloseHandle  (hUserToken );
			::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

			return SR_SWHEX_ERR_CANT_CREATE_PROCESS;
		}

		::CloseHandle(hAdminToken);
		::CloseHandle(hNewToken  );
		::CloseHandle(hUserToken );
	}
	else
	{		
		if (!::CreateProcessW(
			RootPath, nullptr, nullptr, nullptr, FALSE, CREATE_NO_WINDOW, nullptr, nullptr, &si, &pi
		))
		{
			LastWin32Err = ::GetLastError();

			::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

			return SR_SWHEX_ERR_CANT_CREATE_PROCESS;
		}
	}

	DWORD dwWaitRet = ::WaitForSingleObject(pi.hProcess, SR_REMOTE_TIMEOUT);
	if (dwWaitRet  != WAIT_OBJECT_0)
	{
		LastWin32Err = ::GetLastError();

		::TerminateProcess(pi.hProcess, 0);

		return SR_SWHEX_ERR_SWHEX_TIMEOUT;
	}

	DWORD ExitCode = 0;
	::GetExitCodeProcess(pi.hProcess, &ExitCode);

	::CloseHandle(pi.hProcess);
	::CloseHandle(pi.hThread);

	if (ExitCode != SWHEX_ERR_SUCCESS)
	{
		::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

		return ExitCode;
	}

	auto TimeOut    = GetTickCount64();
	BYTE CheckByte  = 0;
	while (!CheckByte)
	{
		::ReadProcessMemory(hTargetProc, ReCa<BYTE*>(pCodecave) + CheckByteOffset, &CheckByte, 1, nullptr);
		::Sleep(10);
		if (::GetTickCount64() - TimeOut > SR_REMOTE_TIMEOUT)
			return SR_SWHEX_ERR_REMOTE_TIMEOUT;
	}

	::ReadProcessMemory(hTargetProc, pCodecave, &pOut, sizeof(pOut), nullptr);
	::VirtualFreeEx    (hTargetProc, pCodecave, 0, MEM_RELEASE);

	return SR_ERR_SUCCESS;
}

#ifdef _WIN64

DWORD SetWindowsHookEx_WOW64(HANDLE hTargetProc, f_Routine_WOW64 pRoutine, DWORD pArg, DWORD & LastWin32Error, ULONG TargetSessionId, DWORD & Out)
{
	#define w_path_sz_req       (_MAX_PATH * 2)                // in charicters;
	static UINT b_path_sz_req = w_path_sz_req * sizeof(WCHAR); // in bytes;

	WCHAR RootPath[w_path_sz_req] = { 0 };

	if (!::GetOwnModulePath(RootPath, b_path_sz_req)) {
		return SR_SWHEX_ERR_CANT_QUERY_INFO_PATH;
	}

	WCHAR InfoPath[w_path_sz_req] = { 0 };

	::memcpy_s(InfoPath, b_path_sz_req, RootPath, b_path_sz_req);

	::StringCbCatW(InfoPath, sizeof(InfoPath), shared::__get_mod_hand_path());

	if (::IsFileExist(InfoPath))
		::DeleteFileW(InfoPath);

	fw_stream swhex_info(InfoPath, ::std::ios_base::out | ::std::ios_base::app);
	if (!swhex_info.good())
	{
		swhex_info.close();
		return SR_SWHEX_ERR_CANT_OPEN_INFO_TXT;
	}

	PVOID pCodecave = ::VirtualAllocEx(hTargetProc, nullptr, 0x1000, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	if ( !pCodecave ) {
		LastWin32Error = ::GetLastError();

		swhex_info.close();

		return SR_SWHEX_ERR_VAE_FAIL;
	}

	PVOID pCallNextHookEx = nullptr;
	CObject_WOW64(hTargetProc).ProcAddress(_T("user32.dll"), "CallNextHookEx", pCallNextHookEx);

	if (!pCallNextHookEx) {
		::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

		return SR_SWHEX_ERR_CNHEX_MISSING;
	}

	BYTE Shellcode[] =
	{
		0x00, 0x00, 0x00, 0x00,       // - 0x08          -> pArg                   ;pointer to argument
		0x00, 0x00, 0x00, 0x00,       // - 0x04          -> pRoutine               ;pointer to target function

		0x55,                         // + 0x00          -> push ebp               ;x86 stack frame creation
		0x8B, 0xEC,                   // + 0x01          -> mov ebp, esp

		0xFF, 0x75, 0x10,             // + 0x03          -> push [ebp + 0x10]      ;push CallNextHookEx arguments
		0xFF, 0x75, 0x0C,             // + 0x06          -> push [ebp + 0x0C] 
		0xFF, 0x75, 0x08,             // + 0x09          -> push [ebp + 0x08]
		0x6A, 0x00,                   // + 0x0C          -> push 0x00
		0xE8, 0x00, 0x00, 0x00, 0x00, // + 0x0E (+ 0x0F) -> call CallNextHookEx    ;call CallNextHookEx

		0xEB, 0x00,                   // + 0x13          -> jmp $ + 0x02           ;jmp to next instruction

		0x50,                         // + 0x15          -> push eax               ;save eax (CallNextHookEx retval)
		0x53,                         // + 0x16          -> push ebx               ;save ebx (non volatile)

		0xBB, 0x00, 0x00, 0x00, 0x00, // + 0x17 (+ 0x18) -> mov ebx, pArg          ;move pArg (pCodecave) into ebx
		0xC6, 0x43, 0x1C, 0x14,       // + 0x1C          -> mov [ebx + 0x1C], 0x17 ;hotpatch jmp above to skip shellcode

		0xFF, 0x33,                   // + 0x20          -> push [ebx] (default)   ;push pArg (__cdecl/__stdcall)

		0xFF, 0x53, 0x04,             // + 0x22          -> call [ebx + 0x04]      ;call target function

		0x89, 0x03,                   // + 0x25          -> mov [ebx], eax         ;store returned value

		0x5B,                         // + 0x27          -> pop ebx                ;restore old ebx
		0x58,                         // + 0x28          -> pop eax                ;restore eax (CallNextHookEx retval)

		0x5D,                         // + 0x29          -> pop ebp                ;restore ebp
		0xC2, 0x0C, 0x00              // + 0x2A          -> ret 0x000C             ;return
	}; // SIZE = 0x3D (+ 0x08)

	DWORD CodeOffset        = 0x08;
	DWORD CheckByteOffset   = 0x14 + CodeOffset;

	*ReCa<DWORD*>(Shellcode + 0x00) = pArg;
	*ReCa<DWORD*>(Shellcode + 0x04) = pRoutine;

	*ReCa<DWORD*>(Shellcode + 0x0F + CodeOffset) = MDWD(pCallNextHookEx) - (MDWD(pCodecave) + 0x0E + CodeOffset) - 5;
	*ReCa<DWORD*>(Shellcode + 0x18 + CodeOffset) = MDWD(pCodecave);

	if (!::WriteProcessMemory(hTargetProc, pCodecave, Shellcode, sizeof(Shellcode), nullptr))
	{
		LastWin32Error = ::GetLastError();

		::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);
		swhex_info.close();

		return SR_SWHEX_ERR_WPM_FAIL;
	}

	swhex_info << std::dec << ::GetProcessId(hTargetProc) << '!' << std::hex << MDWD(pCodecave) + CodeOffset << std::endl;
	swhex_info.close();

	::StringCbCatW(RootPath, sizeof(RootPath), shared::__get_mod_hand_path());

	PROCESS_INFORMATION pi{ 0 };
	STARTUPINFOW si{ 0 };
	si.cb			= sizeof(si);
	si.dwFlags		= STARTF_USESHOWWINDOW;
	si.wShowWindow	= SW_HIDE;

	if (TargetSessionId != -1) 
	{
		HANDLE hUserToken = nullptr;
		if (!::WTSQueryUserToken(TargetSessionId, &hUserToken))
		{
			LastWin32Error = ::GetLastError();

			::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

			return SR_SWHEX_ERR_WTSQUERY_FAIL;
		}

		HANDLE hNewToken = nullptr;
		if (!::DuplicateTokenEx(hUserToken, MAXIMUM_ALLOWED, nullptr, SecurityIdentification, TokenPrimary, &hNewToken))
		{
			LastWin32Error = ::GetLastError();

			::CloseHandle(hUserToken);
			::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

			return SR_SWHEX_ERR_DUP_TOKEN_FAIL;
		}

		DWORD SizeOut = 0;
		TOKEN_LINKED_TOKEN admin_token{ 0 };
		if (!::GetTokenInformation(hNewToken, TokenLinkedToken, &admin_token, sizeof(admin_token), &SizeOut))
		{
			LastWin32Error = ::GetLastError();

			::CloseHandle  (hNewToken );
			::CloseHandle  (hUserToken);
			::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

			return SR_SWHEX_ERR_GET_ADMIN_TOKEN_FAIL;
		}
		HANDLE hAdminToken = admin_token.LinkedToken;

		if (!::CreateProcessAsUserW(
			hAdminToken, RootPath, nullptr, nullptr, nullptr, FALSE, CREATE_NO_WINDOW, nullptr, nullptr, &si, &pi
		))
		{
			LastWin32Error = ::GetLastError();

			::CloseHandle  (hAdminToken);
			::CloseHandle  (hNewToken  );
			::CloseHandle  (hUserToken );
			::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

			return SR_SWHEX_ERR_CANT_CREATE_PROCESS;
		}

		::CloseHandle(hAdminToken);
		::CloseHandle(hNewToken  );
		::CloseHandle(hUserToken );
	}
	else {
		if (!CreateProcessW(RootPath, nullptr, nullptr, nullptr, FALSE, CREATE_NO_WINDOW, nullptr, nullptr, &si, &pi))
		{
			LastWin32Error = ::GetLastError();

			::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

			return SR_SWHEX_ERR_CANT_CREATE_PROCESS;
		}
	}

	DWORD dwWaitRet = ::WaitForSingleObject(pi.hProcess, SR_REMOTE_TIMEOUT);
	if (  dwWaitRet!= WAIT_OBJECT_0)
	{
		LastWin32Error = ::GetLastError ();
		::TerminateProcess(pi.hProcess, 0);

		return SR_SWHEX_ERR_SWHEX_TIMEOUT;
	}

	DWORD ExitCode = 0;
	::GetExitCodeProcess(pi.hProcess, &ExitCode);

	::CloseHandle(pi.hProcess);
	::CloseHandle(pi.hThread);

	if (ExitCode != SWHEX_ERR_SUCCESS) {
		::VirtualFreeEx(hTargetProc, pCodecave, 0, MEM_RELEASE);

		return ExitCode;
	}

	ULONGLONG TimeOut  = GetTickCount64();
	BYTE    CheckByte  = 0;
	while (!CheckByte)
	{
		::ReadProcessMemory(hTargetProc, ReCa<BYTE*>(pCodecave) + CheckByteOffset, &CheckByte, 1, nullptr);
		::Sleep(10);
		if (::GetTickCount64() - TimeOut > SR_REMOTE_TIMEOUT)
			return SR_SWHEX_ERR_REMOTE_TIMEOUT;
	}

	::ReadProcessMemory(hTargetProc, pCodecave, &Out, sizeof(Out), nullptr);
	::VirtualFreeEx    (hTargetProc, pCodecave, 0, MEM_RELEASE);

	return SR_ERR_SUCCESS;
}

#endif