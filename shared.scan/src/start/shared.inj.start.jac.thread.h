#ifndef _SHAREDINJSTARTJACTHREAD_H_EBFFCB4C_313B_4034_96B1_92F7B7CA2A8D_INCLUDED
#define _SHAREDINJSTARTJACTHREAD_H_EBFFCB4C_313B_4034_96B1_92F7B7CA2A8D_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is Inject start routine hijack thread declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 10-Mar-2020 at 0:48:12a, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "shared.inj.start.h"
namespace shared { namespace inject { namespace launch { namespace _impl {

	DWORD HijackThread (
		HANDLE          hTargetProc ,
		f_Routine*      pRoutine    ,
		PVOID           pArg        ,
		DWORD&          LastWin32Err,
		ULONG_PTR&      Out
	);

#ifdef _WIN64
	DWORD HijackThread_WOW64 (
		HANDLE          hTargetProc ,
		f_Routine_WOW64 pRoutine    ,
		DWORD           pArg        ,
		DWORD&          LastWin32Err,
		DWORD&          Out
	);
#endif

}}}}

#endif/*_SHAREDINJSTARTJACTHREAD_H_EBFFCB4C_313B_4034_96B1_92F7B7CA2A8D_INCLUDED*/