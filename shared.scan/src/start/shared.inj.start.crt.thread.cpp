/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is Inject start routine create thread implementation file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 9-Mar-2020 at 10:48:42p, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "StdAfx.h"
#include "shared.inj.start.crt.thread.h"

using namespace shared::inject::launch;
using namespace shared::inject::launch::_impl;

#include "shared.inj.error.h"
using namespace shared::inject;

#include "shared.nt.sys.funs.h"
using namespace shared::process;
 
/////////////////////////////////////////////////////////////////////////////

DWORD NtCreateThreadEx (HANDLE hTargetProc, f_Routine* pRoutine, PVOID pArg, DWORD& LastWin32Err, bool bCloakThread, ULONG_PTR& Out)
{
	auto h_nt_dll = ::GetModuleHandleA("ntdll.dll");
	if (!h_nt_dll)
	{
		LastWin32Err = ::GetLastError();
		return SR_NTCTE_ERR_GET_MODULE_HANDLE_FAIL;
	}

	auto p_NtCreateThreadEx = ReCa<fn_NtCreateThreadEx>(::GetProcAddress(h_nt_dll, "NtCreateThreadEx"));
	if (!p_NtCreateThreadEx)
	{
		LastWin32Err = ::GetLastError();

		return SR_NTCTE_ERR_NTCTE_MISSING;
	}

	PVOID pEntrypoint = nullptr;
	if (bCloakThread)
	{
		CProcessInfo pi;
		pi.SetProcess(hTargetProc);
		pEntrypoint = pi.GetEntryPoint();
	}

	DWORD Flags     = THREAD_CREATE_FLAGS_CREATE_SUSPENDED | THREAD_CREATE_FLAGS_SKIP_THREAD_ATTACH | THREAD_CREATE_FLAGS_HIDE_FROM_DEBUGGER;
	HANDLE hThread  = nullptr;

#ifdef _WIN64

	PVOID pMem = ::VirtualAllocEx(hTargetProc, nullptr, 0x100, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	if (! pMem )
	{
		LastWin32Err = ::GetLastError();
		return SR_NTCTE_ERR_CANT_ALLOC_MEM;
	}

	BYTE Shellcode[] =
	{
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // - 0x10 -> argument / returned value
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // - 0x08 -> pRoutine

		0x48, 0x8B, 0xC1,                               // + 0x00 -> mov rax, rcx
		0x48, 0x8B, 0x08,                               // + 0x03 -> mov rcx, [rax]

		0x48, 0x83, 0xEC, 0x28,                         // + 0x06 -> sub rsp, 0x28
		0xFF, 0x50, 0x08,                               // + 0x0A -> call qword ptr [rax + 0x08]
		0x48, 0x83, 0xC4, 0x28,                         // + 0x0D -> add rsp, 0x28

		0x48, 0x8D, 0x0D, 0xD8, 0xFF, 0xFF, 0xFF,       // + 0x11 -> lea rcx, [pCodecave]
		0x48, 0x89, 0x01,                               // + 0x18 -> mov [rcx], rax
		0x48, 0x31, 0xC0,                               // + 0x1B -> xor rax, rax

		0xC3                                            // + 0x1E -> ret
	}; // SIZE = 0x1F (+ 0x10)

	*ReCa<void**>       (Shellcode + 0x00) = pArg;
	*ReCa<f_Routine**>  (Shellcode + 0x08) = pRoutine;

	DWORD FuncOffset = 0x10;

	BOOL bRet = ::WriteProcessMemory(hTargetProc, pMem, Shellcode, sizeof(Shellcode), nullptr);
	if (!bRet)
	{
		LastWin32Err  = ::GetLastError();

		::VirtualFreeEx(hTargetProc, pMem, 0, MEM_RELEASE);

		return SR_NTCTE_ERR_WPM_FAIL;
	}

	PVOID pRemoteArg   = pMem;
	PVOID pRemoteFunc  = ReCa<BYTE*>(pMem) + FuncOffset;

	NTSTATUS ntRet = p_NtCreateThreadEx(
		&hThread, THREAD_ALL_ACCESS, nullptr    , hTargetProc,
		bCloakThread ? pEntrypoint : pRemoteFunc, pRemoteArg ,
		bCloakThread ? Flags       : NULL, 0, 0 , 0, nullptr
	);
	if (NT_FAIL(ntRet) || !hThread)
	{
		LastWin32Err = ntRet;

		::VirtualFreeEx(hTargetProc, pMem, 0, MEM_RELEASE);

		return SR_NTCTE_ERR_NTCTE_FAIL;
	}

	if (bCloakThread)
	{
		CONTEXT ctx{ 0 };
		ctx.ContextFlags = CONTEXT_INTEGER;

		if (!::GetThreadContext(hThread, &ctx))
		{
			LastWin32Err  = ::GetLastError();

			::TerminateThread(hThread, 0);

			::CloseHandle(hThread);
			::VirtualFreeEx(hTargetProc, pMem, 0, MEM_RELEASE);

			return SR_NTCTE_ERR_GET_CONTEXT_FAIL;
		}

		ctx.Rcx = ReCa<DWORD64>(pRemoteFunc);

		if (!::SetThreadContext(hThread, &ctx))
		{
			LastWin32Err = ::GetLastError();

			::TerminateThread(hThread, 0);
			::CloseHandle(hThread);
			::VirtualFreeEx(hTargetProc, pMem, 0, MEM_RELEASE);

			return SR_NTCTE_ERR_SET_CONTEXT_FAIL;
		}

		if (::ResumeThread(hThread) == (DWORD)-1)
		{
			LastWin32Err = ::GetLastError();

			::TerminateThread(hThread, 0);
			::CloseHandle(hThread);
			::VirtualFreeEx(hTargetProc, pMem, 0, MEM_RELEASE);

			return SR_NTCTE_ERR_RESUME_FAIL;
		}
	}

#else

	NTSTATUS ntRet = p_NtCreateThreadEx(
		&hThread, THREAD_ALL_ACCESS, nullptr, hTargetProc,
		bCloakThread ? pEntrypoint : pRoutine, pArg,
		bCloakThread ? Flags : NULL, 0, 0, 0, nullptr
	);

	if (NT_FAIL(ntRet) || !hThread)
	{
		LastWin32Err = ntRet;
		return SR_NTCTE_ERR_NTCTE_FAIL;
	}

	if (bCloakThread)
	{
		CONTEXT ctx{ 0 };
		ctx.ContextFlags = CONTEXT_INTEGER;

		if (!::GetThreadContext(hThread, &ctx))
		{
			LastWin32Err = ::GetLastError();

			::TerminateThread(hThread, 0);
			::CloseHandle(hThread);

			return SR_NTCTE_ERR_GET_CONTEXT_FAIL;
		}

		ctx.Eax = (DWORD) pRoutine;

		if (!::SetThreadContext(hThread, &ctx))
		{
			LastWin32Err = ::GetLastError();

			::TerminateThread(hThread, 0);
			::CloseHandle(hThread);

			return SR_NTCTE_ERR_SET_CONTEXT_FAIL;
		}

		if (::ResumeThread(hThread) == (DWORD)-1)
		{
			LastWin32Err = ::GetLastError();

			::TerminateThread(hThread, 0);
			::CloseHandle(hThread);

			return SR_NTCTE_ERR_RESUME_FAIL;
		}
	}

#endif

	DWORD dwWaitRet = ::WaitForSingleObject(hThread, SR_REMOTE_TIMEOUT);
	if (  dwWaitRet!= WAIT_OBJECT_0)
	{
		LastWin32Err = ::GetLastError();

		::TerminateThread(hThread, 0);
		::CloseHandle(hThread);

#ifdef _WIN64
		::VirtualFreeEx(hTargetProc, pMem, 0, MEM_RELEASE);
#endif
		return SR_NTCTE_ERR_TIMEOUT;
	}

#ifdef _WIN64

	::CloseHandle(hThread);

	bRet = ::ReadProcessMemory(hTargetProc, pMem, &Out, sizeof(Out), nullptr);

	::VirtualFreeEx(hTargetProc, pMem, 0, MEM_RELEASE);

	if (!bRet) {
		LastWin32Err = ::GetLastError();
		return SR_NTCTE_ERR_RPM_FAIL;
	}

#else

	DWORD dwOut = 0;
	BOOL bRet   = ::GetExitCodeThread(hThread, &dwOut);
	if (!bRet)
	{
		LastWin32Err = ::GetLastError();

		::CloseHandle(hThread);

		return SR_NTCTE_ERR_GECT_FAIL;
	}

	Out = dwOut;

	::CloseHandle(hThread);

#endif

	return SR_ERR_SUCCESS;
}

#ifdef _WIN64

DWORD NtCreateThreadEx_WOW64(HANDLE hTargetProc, f_Routine_WOW64 pRoutine, DWORD pArg, DWORD& LastWin32Err, bool bCloakThread, DWORD & Out)
{
	auto h_nt_dll = ::GetModuleHandleA("ntdll.dll");
	if (!h_nt_dll)
	{
		LastWin32Err = ::GetLastError();
		return SR_NTCTE_ERR_GET_MODULE_HANDLE_FAIL;
	}

	auto p_NtCreateThreadEx = ReCa<fn_NtCreateThreadEx>(GetProcAddress(h_nt_dll, "NtCreateThreadEx"));
	if (!p_NtCreateThreadEx)
	{
		LastWin32Err = ::GetLastError();
		return SR_NTCTE_ERR_NTCTE_MISSING;
	}

	DWORD pEntrypoint = pRoutine;
	if (bCloakThread)
	{
		CProcessInfo pi;
		pi.SetProcess(hTargetProc);
		pEntrypoint = MDWD(pi.GetEntryPoint());
	}

	DWORD Flags     = THREAD_CREATE_FLAGS_CREATE_SUSPENDED | THREAD_CREATE_FLAGS_SKIP_THREAD_ATTACH | THREAD_CREATE_FLAGS_HIDE_FROM_DEBUGGER;
	HANDLE hThread  = nullptr;

	NTSTATUS ntRet = p_NtCreateThreadEx(
		&hThread     , THREAD_ALL_ACCESS , nullptr, hTargetProc,
		bCloakThread ? MPTR(pEntrypoint) : MPTR(pRoutine), MPTR(pArg),
		bCloakThread ? Flags             : NULL, 0, 0, 0, nullptr);

	if (NT_FAIL(ntRet) || !hThread)
	{
		LastWin32Err = ntRet;
		return SR_NTCTE_ERR_NTCTE_FAIL;
	}

	if (bCloakThread)
	{
		WOW64_CONTEXT ctx{ 0 };
		ctx.ContextFlags = CONTEXT_INTEGER;

		if (!::Wow64GetThreadContext(hThread, &ctx))
		{
			LastWin32Err = ::GetLastError();

			::TerminateThread(hThread, 0);
			::CloseHandle(hThread);

			return SR_NTCTE_ERR_GET_CONTEXT_FAIL;
		}

		ctx.Eax = pRoutine;

		if (!::Wow64SetThreadContext(hThread, &ctx))
		{
			LastWin32Err = ::GetLastError();

			::TerminateThread(hThread, 0);
			::CloseHandle(hThread);

			return SR_NTCTE_ERR_SET_CONTEXT_FAIL;
		}

		if (ResumeThread(hThread) == (DWORD)-1)
		{
			LastWin32Err = ::GetLastError();

			::TerminateThread(hThread, 0);
			::CloseHandle(hThread);

			return SR_NTCTE_ERR_RESUME_FAIL;
		}
	}

	::Sleep(200);

	DWORD dwWaitRet = ::WaitForSingleObject(hThread, SR_REMOTE_TIMEOUT);
	if ( dwWaitRet != WAIT_OBJECT_0)
	{
		LastWin32Err = ::GetLastError();

		::TerminateThread(hThread, 0);
		::CloseHandle(hThread);

		return SR_NTCTE_ERR_TIMEOUT;
	}

	BOOL bRet = ::GetExitCodeThread(hThread, &Out);
	if (!bRet)
	{
		LastWin32Err = ::GetLastError();
		::CloseHandle(hThread);

		return SR_NTCTE_ERR_GECT_FAIL;
	}

	::CloseHandle(hThread);

	return SR_ERR_SUCCESS;
}

#endif