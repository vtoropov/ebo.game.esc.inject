#ifndef _SHAREDINJSTARTWINHOOK_H_23AF2F8E_FE35_495C_ABDB_BFB96559CF8F_INCLUDED
#define _SHAREDINJSTARTWINHOOK_H_23AF2F8E_FE35_495C_ABDB_BFB96559CF8F_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is Inject start routine window hook declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 10-Mar-2020 at 1:36:21a, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "shared.inj.start.h"
namespace shared { namespace inject { namespace launch { namespace _impl {

	DWORD SetWindowsHookEx (
		HANDLE          hTargetProc    , // handle to the target process;
		f_Routine*      pRoutine       , // pointer to the shellcode in virtual memory of the target process;
		PVOID           pArg           , // pointer to the argument which gets passed to the shellcode;
		DWORD&          LastWin32Err   , // reference to argument for returning an error code;
		ULONG           TargetSessionId, // target session identifier; 
		ULONG_PTR&      Out              // reference to argument for storing returned value of shellcode.
	);

#ifdef _WIN64
	
	DWORD SetWindowsHookEx_WOW64 (
		HANDLE          hTargetProc    ,
		f_Routine_WOW64 pRoutine       ,
		DWORD           pArg           ,
		DWORD&          LastWin32Err   ,
		ULONG           TargetSessionId,
		DWORD&          Out
	);

#endif

}}}}

#endif/*_SHAREDINJSTARTWINHOOK_H_23AF2F8E_FE35_495C_ABDB_BFB96559CF8F_INCLUDED*/