/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is Inject start routine implementation file (https://github.com/guided-hacking/GuidedHacking-Injector);
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 9-Mar-2020 at 9:56:47p, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "StdAfx.h"
#include "shared.inj.start.h"
#include "shared.inj.error.h"

using namespace shared::inject;
using namespace shared::inject::launch;

#include "shared.inj.start.crt.thread.h"
#include "shared.inj.start.jac.thread.h"
#include "shared.inj.start.que.apc.h"
#include "shared.inj.start.win.hook.h"

using namespace shared::inject::launch::_impl;

#include "shared.nt.sys.tools.h"
using namespace shared::sys_core::tools;
/////////////////////////////////////////////////////////////////////////////

CHookData:: CHookData(void) { ::memset(&m_hk_data, 0, sizeof(HookData)); }
CHookData::~CHookData(void) {}

/////////////////////////////////////////////////////////////////////////////
const
HookData&   CHookData::Ref (void) const { return m_hk_data; }
HookData&   CHookData::Ref (void)       { return m_hk_data; }

/////////////////////////////////////////////////////////////////////////////
CHookData::operator const HookData& (void) const { return this->Ref(); }
CHookData::operator       HookData& (void)       { return this->Ref(); }

/////////////////////////////////////////////////////////////////////////////

DWORD   StartRoutine(
	HANDLE hTargetProc, f_Routine* pRoutine, PVOID pArg, LAUNCH_METHOD eMethod, bool bCloakThread,  DWORD& LastWin32Err, ULONG_PTR& pOut
){
	DWORD dw_result = 0;

	switch (eMethod) {
	case LM_NtCreateThreadEx:
		dw_result = NtCreateThreadEx(hTargetProc, pRoutine, pArg, LastWin32Err, bCloakThread, pOut);
		break;

	case LM_HijackThread:
		dw_result = HijackThread(hTargetProc, pRoutine, pArg, LastWin32Err, pOut);
		break;

	case LM_SetWindowsHookEx: {
			NTSTATUS nt_status  = 0;
			ULONG   OwnSession  = ::GetSessionId(::GetCurrentProcess(), nt_status);
			ULONG TargetSession = ::GetSessionId(hTargetProc, nt_status);

			if (TargetSession  == (ULONG)-1) {
				LastWin32Err    = static_cast<DWORD>(nt_status);
				dw_result       = SR_ERR_CANT_QUERY_SESSION_ID;
				break;
			}
			else if (OwnSession == TargetSession) {
				TargetSession = (ULONG)-1;
			}

			dw_result = SetWindowsHookEx(hTargetProc, pRoutine, pArg, LastWin32Err, TargetSession, pOut);
		}
		break;

	case LM_QueueUserAPC:
		dw_result = QueueUserAPC(hTargetProc, pRoutine, pArg, LastWin32Err, pOut);
		break;

	default:
		dw_result = SR_ERR_INVALID_LAUNCH_METHOD;
		break;

	}

	return dw_result;
}

#ifdef _WIN64

DWORD   StartRoutine_WOW64(
	HANDLE hTargetProc, f_Routine_WOW64 pRoutine, DWORD pArg, LAUNCH_METHOD eMethod, bool bCloakThread, DWORD& LastWin32Err, DWORD& dwOut
){
	DWORD dw_result = 0;

	switch (eMethod) {
	case LM_NtCreateThreadEx:
		dw_result = NtCreateThreadEx_WOW64(hTargetProc, pRoutine, pArg, LastWin32Err, bCloakThread, dwOut);
		break;

	case LM_HijackThread:
		dw_result = HijackThread_WOW64(hTargetProc, pRoutine, pArg, LastWin32Err, dwOut);
		break;

	case LM_SetWindowsHookEx: {
			NTSTATUS nt_status = 0;
			ULONG TargetSession = GetSessionId(hTargetProc, nt_status);
			if (  TargetSession == (ULONG)-1)
			{
				LastWin32Err = static_cast<DWORD>(nt_status);
				dw_result    = SR_ERR_CANT_QUERY_SESSION_ID;
				break;
			}
			else if (TargetSession == ::GetSessionId(::GetCurrentProcess(), nt_status)) {
				TargetSession = (ULONG)-1;
			}
			dw_result = SetWindowsHookEx_WOW64(hTargetProc, pRoutine, pArg, LastWin32Err, TargetSession, dwOut);
		}
		break;

	case LM_QueueUserAPC:
		dw_result = QueueUserAPC_WOW64(hTargetProc, pRoutine, pArg, LastWin32Err, dwOut);
		break;
	}

	return dw_result;
}

#endif