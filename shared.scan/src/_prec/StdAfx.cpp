/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Apr-2019 at 8:08:27a, UTC+7, Phuket, Rawai, Wednesday;
	This is generic shared library precompiled headers implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject library on 27-Feb-2020 at 7:16:19a, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)

namespace shared {

	HINSTANCE&    __get_bas_addr_of_inj(void) {

		static HINSTANCE bas_address = NULL;
		return bas_address;
	}

	LPCTSTR       __get_mod_hand_path  (void) {
		return    __set_mod_hand_path().GetString();
	}

	CAtlString&   __set_mod_hand_path  (void)  {
		static CAtlString cs_mod_path;
		return cs_mod_path;
	}
}