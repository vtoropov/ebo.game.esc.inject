#ifndef _STDAFX_H_C9283464_6654_42A9_99CC_D2DF317CE943_INCLUDED
#define _STDAFX_H_C9283464_6654_42A9_99CC_D2DF317CE943_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Apr-2019 at 7:47:34a, UTC+7, Phuket, Rawai, Thursday;
	This is generic shared library precompiled headers declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject library on 27-Feb-2020 at 7:14:28a, UTC+7, Novosibirsk, Tulenina, Thursday;
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlwin.h >
#include <atlstr.h >
#include <atlsafe.h>
#include <comdef.h >
#include <comutil.h>
#include <vector>
#include <map>

namespace std {
	#include <math.h>
	#include <time.h>
}
#include <ios>
#include <iosfwd>
#include <fstream>

#include <strsafe.h>

#include <winternl.h>
#include <Psapi.h>
#include <wtsapi32.h>

namespace shared {
	// a variable to store the base address of the current image of the injector;
	HINSTANCE&    __get_bas_addr_of_inj(void);
	LPCTSTR       __get_mod_hand_path  (void);  // gets injection process handler module path;
	CAtlString&   __set_mod_hand_path  (void);  // sets injection process handler module path;
}

#pragma warning(disable: 6001) // uninitialized memory (bug with SAL notation)
#pragma warning(disable: 6031) // ignored return value warning
#pragma warning(disable: 6258) // TerminateThread warning

#pragma comment(lib, "wtsapi32.lib")
#pragma comment(lib, "psapi.lib")

#endif/*_STDAFX_H_C9283464_6654_42A9_99CC_D2DF317CE943_INCLUDED*/