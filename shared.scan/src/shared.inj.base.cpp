/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is internal NT function interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 11-Mar-2020 at 3:56:43p, UTC+7, Novosibirsk, Tulenina, Wednesday;
*/
#include "StdAfx.h"
#include "shared.inj.base.h"
#include "shared.inj.error.h"

using namespace shared::inject;

#include "shared.nt.sys.log.h"
#include "shared.inj.log.h"

using namespace shared::log;
using namespace shared::log::inject;

#include "shared.nt.sys.tools.h"

using namespace shared::sys_core::tools;
/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace inject { namespace _impl {

	class CError_Init {
	public:
		 CError_Init(void) {}
		~CError_Init(void) {}

	public:
		DWORD   Do(PCWCHAR szDllPath, INJECTIONDATAW* pData, const bool bNative, const DWORD _err_code) {
			szDllPath; pData; bNative;
			if (_err_code == ERROR_SUCCESS)
				return INJ_ERR_SUCCESS;

			CErrorInfo   err_inf ; err_inf << pData << szDllPath << bNative << _err_code;
			CLog(COption::e_overwrite).Write(err_inf);

			return _err_code;
		}
	};

}}}
using namespace shared::inject::_impl;
/////////////////////////////////////////////////////////////////////////////

DWORD HijackHandle(INJECTIONDATAW* pData);

/////////////////////////////////////////////////////////////////////////////

DWORD __stdcall shared::inject::InjectA(INJECTIONDATAA* pData) {
	#pragma EXPORT_FUNCTION(__FUNCTION__, __FUNCDNAME__)

	if (NULL == pData)
		return ERROR_INVALID_DATA;

	if (NULL == pData->szDllPath)
		return CError_Init().Do(nullptr, ReCa<INJECTIONDATAW*>(pData), false, INJ_ERR_INVALID_FILEPATH);

	INJECTIONDATAW data{ 0 };
	size_t len_out = 0;
	size_t max_len = sizeof(data.szDllPath) / sizeof(wchar_t);
	::StringCchLengthA(pData->szDllPath, max_len, &len_out);
	::mbstowcs_s(&len_out, const_cast<wchar_t*>(data.szDllPath), max_len, pData->szDllPath, max_len);

	data.dwProcessID   = pData->dwProcessID ;
	data.eMode         = pData->eMode  ;
	data.eMethod       = pData->eMethod;
	data.dwFlags       = pData->dwFlags;
	data.hHandleValue  = pData->hHandleValue;

	return InjectW(&data);
}

DWORD __stdcall shared::inject::InjectW(INJECTIONDATAW* pData) {
	#pragma EXPORT_FUNCTION(__FUNCTION__, __FUNCDNAME__)

	if (NULL == pData)
		return ERROR_INVALID_DATA;

	if (NULL == pData->szDllPath)
		return CError_Init().Do(nullptr, pData, false, INJ_ERR_INVALID_FILEPATH);

	if (0 == pData->dwProcessID)
		return CError_Init().Do(pData->szDllPath, pData, false, INJ_ERR_INVALID_PID);

	if (!IsFileExist(pData->szDllPath)) {
		pData->dwLastErrCode = ::GetLastError();

		return CError_Init().Do(pData->szDllPath, pData, false, INJ_ERR_FILE_DOESNT_EXIST);
	}

	HANDLE hTargetProc = nullptr;
	if (pData->dwFlags & INJ_HIJACK_HANDLE) {
		if (pData->hHandleValue) {
			hTargetProc = MPTR(pData->hHandleValue);
		}
		else {
		//	return HijackHandle(pData);
		}
	}
	else {
	}

	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool shared::inject::__lnk_warn_4221_block_3(void) {
	return true;
}