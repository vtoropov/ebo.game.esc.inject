/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is injecting DLL help function interface implementation file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 30-Mar-2020 at 8:24:25p, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "StdAfx.h"
#include "shared.inj.dll.h"
#include "shared.inj.cloak.h"

using namespace shared::inject;
using namespace shared::inject::_impl;
using namespace shared::inject::launch;

#include "shared.nt.sys.lib.h"
#include "shared.nt.sys.ldr.h"
#include "shared.nt.map.man.h"
using namespace shared::process;

/////////////////////////////////////////////////////////////////////////////
namespace shared { namespace inject { namespace _impl {

void EjectDll(HANDLE hTargetProc, HINSTANCE hModBase)
{
	PVOID pFreeLibrary = nullptr;
	CObject(hTargetProc).ProcAddress(_T("kernel32.dll"), "FreeLibrary", pFreeLibrary);

	if (!pFreeLibrary)
		return;

	DWORD win32		= 0;
	ULONG_PTR Out	= 0;
	StartRoutine(hTargetProc, ReCa<f_Routine*>(pFreeLibrary), ReCa<void*>(hModBase), LM_NtCreateThreadEx, true, win32, Out);
}


DWORD InjectDLL(PCWCHAR pszDllFile, HANDLE hTargetProc, eInject eMode, eLaunch eMethod, DWORD dwFlags, DWORD& dwLastError, HINSTANCE& hOut)
{
	if (dwFlags & INJ_LOAD_DLL_COPY)
	{
		size_t len_out = 0;
		::StringCchLengthW(pszDllFile, STRSAFE_MAX_CCH, &len_out);

		PCWCHAR pFileName = pszDllFile;
		pFileName += len_out - 1;
		while (*(pFileName-- - 2) != '\\');
		
		wchar_t new_path[MAXPATH_IN_TCHAR]{ 0 };
		::GetTempPathW  (MAXPATH_IN_TCHAR, new_path);
		::StringCchCatW (new_path, MAXPATH_IN_TCHAR, pFileName);

		::CopyFileW(pszDllFile, new_path, FALSE);

		pszDllFile = new_path;
	}

	if (dwFlags & INJ_SCRAMBLE_DLL_NAME)
	{
		wchar_t new_name[15]{ 0 };
		UINT seed = rand() + dwFlags + LOWORD(hTargetProc);
		LARGE_INTEGER pfc{ 0 };
		::QueryPerformanceCounter(&pfc);
		seed += pfc.LowPart;	
		::srand(seed);

		for (UINT i = 0; i != 10; ++i)
		{
			auto val = rand() % 3;
			if (val == 0)
			{
				val = rand() % 10;
				new_name[i] = wchar_t('0' + val);
			}
			else if (val == 1)
			{
				val = rand() % 26;
				new_name[i] = wchar_t('A' + val);
			}
			else
			{
				val = rand() % 26;
				new_name[i] = wchar_t('a' + val);
			}
		}
		new_name[10] = '.';
		new_name[11] = 'd';
		new_name[12] = 'l';
		new_name[13] = 'l';
		new_name[14] = '\0';

		wchar_t OldFilePath[MAXPATH_IN_TCHAR]{ 0 };
		::StringCchCopyW(OldFilePath, MAXPATH_IN_TCHAR, pszDllFile);

		PWCHAR pFileName = const_cast<PWCHAR>(pszDllFile);
		size_t len_out = 0;
		::StringCchLengthW(pszDllFile, STRSAFE_MAX_CCH, &len_out);
		pFileName += len_out;
		while (*(pFileName-- - 2) != '\\');

		::memcpy(pFileName, new_name, 15 * sizeof(WCHAR));

		::_wrename(OldFilePath, pszDllFile);
	}

	DWORD Ret = 0;

	switch (eMode)
	{
		case IM_LoadLibrary:
			Ret = _LoadLibraryExW(pszDllFile, hTargetProc, eMethod, dwFlags, hOut, dwLastError);
			break;

		case IM_LdrLoadDll:
			Ret = _LdrLoadDll(pszDllFile, hTargetProc, eMethod, dwFlags, hOut, dwLastError);
			break;

		case IM_ManualMap:
			Ret = _ManualMap(pszDllFile, hTargetProc, eMethod, dwFlags, hOut, dwLastError);
	}

	if (Ret != INJ_ERR_SUCCESS) {
		return Ret;
	}

	if (!hOut) {
		return INJ_ERR_REMOTE_CODE_FAILED;
	}

	if (eMode != IM_ManualMap)
	{
		Ret = Cloaking(hTargetProc, dwFlags, hOut, dwLastError);
	}
	return Ret;
}

}}}