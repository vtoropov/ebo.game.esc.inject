#ifndef _SHAREDINJDLL_H_6BFB43CB_9C47_49C7_B728_BA7E3B3FDAB4_INCLUDED
#define _SHAREDINJDLL_H_6BFB43CB_9C47_49C7_B728_BA7E3B3FDAB4_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is injecting DLL help function interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 30-Mar-2020 at 8:16:55p, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "shared.inj.base.h"
#include "shared.inj.start.h"
#include "shared.inj.error.h"
namespace shared { namespace inject { namespace _impl {

	VOID  EjectDll (HANDLE hTargetProc, HINSTANCE hModBase);

	DWORD InjectDLL(
		PCWCHAR pszDllFile, HANDLE hTargetProc, eInject eMode, eLaunch eMethod, DWORD dwFlags, DWORD& dwLastError, HINSTANCE& hOut
	);

}}}

#endif/*_SHAREDINJDLL_H_6BFB43CB_9C47_49C7_B728_BA7E3B3FDAB4_INCLUDED*/