#ifndef _SHAREDINJJACK_H_37D355BF_3EFC_410E_9000_8601FDDE6714_INCLUDED
#define _SHAREDINJJACK_H_37D355BF_3EFC_410E_9000_8601FDDE6714_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is inject event log declaration file (https://github.com/guided-hacking/GuidedHacking-Injector);
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 29-Mar-2020 at 12:39:46p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include "shared.inj.base.h"
#include "shared.inj.error.h"
namespace shared { namespace inject {

	DWORD HijackHandle(INJECTIONDATAW* pData);

}}

#endif/*_SHAREDINJJACK_H_37D355BF_3EFC_410E_9000_8601FDDE6714_INCLUDED*/