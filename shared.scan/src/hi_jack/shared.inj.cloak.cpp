/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is injecting cloak help function interface implementation file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 30-Mar-2020 at 8:53:45p, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "StdAfx.h"
#include "shared.inj.cloak.h"

using namespace shared::inject::_impl;

#include "shared.proc.info.h"
#include "shared.proc.defs.h"

using namespace shared::process;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace inject { namespace _impl {

DWORD Cloaking(HANDLE hTargetProc, DWORD dwFlags, HINSTANCE hModule, DWORD &dwLastError)
{
	if (!dwFlags)
		return INJ_ERR_SUCCESS;

	if (dwFlags & INJ_ERASE_HEADER)
	{
		BYTE Buffer[0x1000]{ 0 };
		DWORD dwOld = 0; 
		BOOL bRet   = ::VirtualProtectEx(hTargetProc, hModule, 0x1000, PAGE_EXECUTE_READWRITE, &dwOld);
		if (!bRet)
		{
			dwLastError = ::GetLastError();
			return INJ_ERR_VPE_FAIL;
		}

		bRet = ::WriteProcessMemory(hTargetProc, hModule, Buffer, 0x1000, nullptr);
		if (!bRet)
		{
			dwLastError = ::GetLastError();
			return INJ_ERR_WPM_FAIL;
		}
	}
	else if (dwFlags & INJ_FAKE_HEADER)
	{
		PVOID pK32 = ReCa<void*>(::GetModuleHandleA("kernel32.dll"));
		if ( !pK32 )
		{
			dwLastError = ::GetLastError();
			return INJ_ERR_GET_MODULE_HANDLE_FAIL;
		}

		DWORD dwOld = 0;
		BOOL bRet   = ::VirtualProtectEx(hTargetProc, hModule, 0x1000, PAGE_EXECUTE_READWRITE, &dwOld);
		if (!bRet)
		{
			dwLastError = ::GetLastError();
			return INJ_ERR_VPE_FAIL;
		}
		
		bRet = ::WriteProcessMemory(hTargetProc, hModule, pK32, 0x1000, nullptr);
		if (!bRet)
		{
			dwLastError = ::GetLastError();
			return INJ_ERR_WPM_FAIL;
		}

		bRet = ::VirtualProtectEx(hTargetProc, hModule, 0x1000, dwOld, &dwOld);
		if (!bRet)
		{
			dwLastError = ::GetLastError();
			return INJ_ERR_VPE_FAIL;
		}
	}

	if (dwFlags & INJ_UNLINK_FROM_PEB)
	{
		CProcessInfo PI;
		PI.SetProcess(hTargetProc);

		PLdrEntry   pEntry = PI.GetLdrEntry(hModule);
		if (NULL == pEntry) {
			return INJ_ERR_CANT_FIND_MOD_PEB;
		}

		TLdrEntry Entry{ 0 };
		if (!::ReadProcessMemory(hTargetProc, pEntry, &Entry, sizeof(Entry), nullptr)) {
			return INJ_ERR_CANT_ACCESS_PEB_LDR;
		}

		auto Unlink = [=](LIST_ENTRY entry)
		{
			LIST_ENTRY list = {0};
			::ReadProcessMemory(hTargetProc, entry.Flink, &list, sizeof(LIST_ENTRY), nullptr);
			list.Blink = entry.Blink;
			::WriteProcessMemory(hTargetProc, entry.Flink, &list, sizeof(LIST_ENTRY), nullptr);
			
			::ReadProcessMemory(hTargetProc, entry.Blink, &list, sizeof(LIST_ENTRY), nullptr);
			list.Flink = entry.Flink;
			::WriteProcessMemory(hTargetProc, entry.Blink, &list, sizeof(LIST_ENTRY), nullptr);
		};

		Unlink(Entry.le_init_order);
		Unlink(Entry.le_load_order);
		Unlink(Entry.le_mmry_order);
	}
	
	return INJ_ERR_SUCCESS;
}

}}}