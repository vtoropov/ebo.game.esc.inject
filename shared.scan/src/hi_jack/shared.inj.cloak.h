#ifndef _SHAREDINJCLOAK_H_19D71EAF_0F75_47AB_BAE9_BC6CD068B65A_INCLUDED
#define _SHAREDINJCLOAK_H_19D71EAF_0F75_47AB_BAE9_BC6CD068B65A_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is injecting cloak help function interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 30-Mar-2020 at 8:47:43p, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "shared.inj.error.h"
namespace shared { namespace inject { namespace _impl {

	DWORD Cloaking(HANDLE hTargetProc, DWORD dwFlags, HINSTANCE hModule, DWORD& dwLastError);

}}}

#endif/*_SHAREDINJCLOAK_H_19D71EAF_0F75_47AB_BAE9_BC6CD068B65A_INCLUDED*/