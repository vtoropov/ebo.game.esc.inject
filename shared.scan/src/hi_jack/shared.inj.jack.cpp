/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is inject event log implementation file (https://github.com/guided-hacking/GuidedHacking-Injector);
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 29-Mar-2020 at 12:51:26p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include "StdAfx.h"
#include "shared.inj.jack.h"
#include "shared.inj.dll.h"
#include "shared.inj.start.h"

using namespace shared::inject;
using namespace shared::inject::_impl;
using namespace shared::inject::launch;

#include "shared.proc.han.h"
#include "shared.proc.obj.h"
#include "shared.nt.sys.tools.h"

using namespace shared::process;
using namespace shared::sys_core::tools;

/////////////////////////////////////////////////////////////////////////////

DWORD shared::inject::HijackHandle(INJECTIONDATAW* pData)
{
	PWCHAR pszDllPath = pData->szDllPath;

	DWORD dw_access_mask = (
		PROCESS_VM_OPERATION | PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_QUERY_INFORMATION | PROCESS_QUERY_LIMITED_INFORMATION
	);
	if (pData->eMethod == eLaunch::LM_NtCreateThreadEx)
		dw_access_mask |= PROCESS_CREATE_THREAD;

	CHandle_Enum  hand_enum;
	HRESULT hr_ = hand_enum.Find(pData->dwProcessID, dw_access_mask);
	if (FAILED(hr_))
		return ((const CError&)hand_enum.Error()).Code();

	CErrorInfo err_info;

	auto handles = hand_enum.Data();
	if ( handles.empty() ) {
		err_info << pszDllPath << pData << true << (DWORD)INJ_ERR_NO_HANDLES;
		return (err_info);
	}

	INJECTIONDATAW hijack_data{ 0 };
	hijack_data.eMode    = eInject::IM_LdrLoadDll;
	hijack_data.eMethod  = eLaunch::LM_NtCreateThreadEx;

	GetOwnModulePath(hijack_data.szDllPath, sizeof(hijack_data.szDllPath) / sizeof(hijack_data.szDllPath[0]));
	::StringCbCatW  (hijack_data.szDllPath, sizeof(hijack_data.szDllPath) , shared::__get_mod_hand_path());
	
	DWORD dwLastErrCode = INJ_ERR_SUCCESS;
	HANDLE hHijackProc  = nullptr;

	for (auto i : handles)
	{
		hHijackProc = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, i.OwnerPID());
		if (NULL == hHijackProc)
		{
			pData->dwLastErrCode = ::GetLastError();
			dwLastErrCode = INJ_ERR_CANT_OPEN_PROCESS;

			continue;
		}
					
		if (!IsElevated(hHijackProc) || !IsNative(hHijackProc))
		{
			pData->dwLastErrCode = ::GetLastError();
			dwLastErrCode = INJ_ERR_HIJACK_NO_NATIVE_HANDLE;

			::CloseHandle(hHijackProc); hHijackProc = NULL;
			
			continue;
		}

		HINSTANCE   hInjectionModuleEx = CObject(hHijackProc).ModuleHandle(shared::__get_mod_hand_path());
		if (NULL == hInjectionModuleEx)
		{
			hijack_data.dwProcessID = i.OwnerPID();
			DWORD inj_ret = InjectW(&hijack_data);

			if (inj_ret || !hijack_data.hDllOut)
			{
				pData->dwLastErrCode = inj_ret;
				dwLastErrCode = INJ_ERR_HIJACK_INJ_FAILED;

				::CloseHandle(hHijackProc);
				continue;
			}

			hInjectionModuleEx = hijack_data.hDllOut;
		}

		PVOID pRemoteInjectW = nullptr;
		bool bLoaded = CObject(hHijackProc).ProcAddress(hInjectionModuleEx, "InjectW", pRemoteInjectW);
		if (!bLoaded)
		{
			dwLastErrCode = INJ_ERR_HIJACK_INJECTW_MISSING;

			EjectDll(hHijackProc, hInjectionModuleEx);
			::CloseHandle(hHijackProc);
			
			continue;
		}


		pData->hHandleValue = (DWORD)i.Value();
		
		PVOID pArg = ::VirtualAllocEx(hHijackProc, nullptr, sizeof(INJECTIONDATAW), MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
		if ( !pArg )
		{
			pData->dwLastErrCode = ::GetLastError();
			dwLastErrCode = INJ_ERR_HIJACK_CANT_ALLOC;

			EjectDll(hHijackProc, hInjectionModuleEx);
			::CloseHandle(hHijackProc);
			
			continue;
		}

		if (!::WriteProcessMemory(hHijackProc, pArg, pData, sizeof(INJECTIONDATAW), nullptr))
		{
			pData->dwLastErrCode = ::GetLastError();
			dwLastErrCode = INJ_ERR_HIJACK_CANT_WPM;

			::VirtualFreeEx(hHijackProc, pArg, 0, MEM_RELEASE);
			EjectDll(hHijackProc, hInjectionModuleEx);
			::CloseHandle(hHijackProc);
			
			continue;
		}

		pData->hHandleValue = 0;

		DWORD win32err        = ERROR_SUCCESS;
		ULONG_PTR hijack_ret  = INJ_ERR_SUCCESS;
		DWORD remote_ret = StartRoutine(hHijackProc, static_cast<f_Routine*>(pRemoteInjectW), pArg, LM_NtCreateThreadEx, false, win32err, hijack_ret);
		
		INJECTIONDATAW data_out{ 0 };
		::ReadProcessMemory(hHijackProc, pArg, &data_out, sizeof(INJECTIONDATAW), nullptr);
		
		::VirtualFreeEx(hHijackProc, pArg, 0, MEM_RELEASE);
		EjectDll(hHijackProc, hInjectionModuleEx);
		::CloseHandle(hHijackProc);

		if (remote_ret != SR_ERR_SUCCESS)
		{
			pData->dwLastErrCode = win32err;
			dwLastErrCode = remote_ret;
			
			continue;
		}
		else if(remote_ret == SR_ERR_SUCCESS)
		{
			if (hijack_ret != INJ_ERR_SUCCESS)
			{
				pData->dwLastErrCode = (DWORD)hijack_ret;
				dwLastErrCode = INJ_ERR_REMOTE_CODE_FAILED;
				
				continue;
			}
		}

		pData->dwLastErrCode = INJ_ERR_SUCCESS;
		dwLastErrCode = INJ_ERR_SUCCESS;

		break;
	}
		
	return (CErrorInfo() << pszDllPath << pData << true << dwLastErrCode);
}