/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is internal NT error codes implementation file (https://github.com/guided-hacking/GuidedHacking-Injector);
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 26-Mar-2020 at 8:57:24p, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "StdAfx.h"
#include "shared.inj.error.h"

using namespace shared::inject;
/////////////////////////////////////////////////////////////////////////////

CErrorInfo:: CErrorInfo(void): m_pro_id(0), m_inj_mode(0), m_lnc_mode(0), m_dw_flags(0), m_err_code(0), m_win_code(0), m_hdl_value(0), m_is_native(false) {
	::GetLocalTime(&m_tm_stamp);
}
CErrorInfo::~CErrorInfo(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT        CErrorInfo::DllPath   (LPCTSTR _p_sz_val) { if (NULL==_p_sz_val || !::_tcslen(_p_sz_val)) return E_INVALIDARG; m_dll_path = _p_sz_val; return S_OK; }
LPCTSTR        CErrorInfo::DllPath   (void) const { return (LPCTSTR)m_dll_path; }
DWORD          CErrorInfo::Flags     (void) const { return m_dw_flags ; }
VOID           CErrorInfo::Flags     (const DWORD _d_val){ m_dw_flags = _d_val; }
DWORD          CErrorInfo::ErrCode   (void) const { return m_err_code ; }
VOID           CErrorInfo::ErrCode   (const DWORD _d_val){ m_err_code = _d_val; }
HRESULT        CErrorInfo::ExePath   (LPCTSTR  _p_sz_val){ if (NULL==_p_sz_val || !::_tcslen(_p_sz_val)) return E_INVALIDARG; m_exe_path = _p_sz_val; return S_OK; }
LPCTSTR        CErrorInfo::ExePath   (void) const { return (LPCTSTR)m_exe_path; }
DWORD          CErrorInfo::Handle    (void) const { return m_hdl_value; }
VOID           CErrorInfo::Handle    (const DWORD _d_val){ m_hdl_value = _d_val;}
DWORD          CErrorInfo::InjectMode(void) const { return m_inj_mode ; }
VOID           CErrorInfo::InjectMode(const DWORD _d_val){ m_inj_mode  = _d_val;}
DWORD          CErrorInfo::LaunchMode(void) const { return m_lnc_mode ; }
VOID           CErrorInfo::LaunchMode(const DWORD _d_val){ m_lnc_mode  = _d_val;}
bool           CErrorInfo::Native    (void) const { return m_is_native; }
VOID           CErrorInfo::Native    (const bool  _b_val){ m_is_native = _b_val;}
DWORD          CErrorInfo::ProcessId (void) const { return m_pro_id   ; }
VOID           CErrorInfo::ProcessId (const DWORD _d_val){ m_pro_id    = _d_val;}
const
SYSTEMTIME&    CErrorInfo::Timestamp (void) const { return m_tm_stamp ; }
DWORD          CErrorInfo::Win32Code (void) const { return m_win_code ; }
VOID           CErrorInfo::Win32Code (const DWORD _d_val){ m_win_code  = _d_val;}

/////////////////////////////////////////////////////////////////////////////

CErrorInfo&   CErrorInfo::operator<<(const bool _b_native) {
	this->Native(_b_native);
	return *this;
}

CErrorInfo&   CErrorInfo::operator<<(const DWORD _err_code ) {
	this->ErrCode(_err_code);
	return *this;
}

CErrorInfo&   CErrorInfo::operator<<(PCWCHAR _p_sz_dll_path) {
	this->DllPath(_p_sz_dll_path);
	return *this;
}

CErrorInfo&   CErrorInfo::operator<<(const INJECTIONDATAW* _p_data) {
	if (NULL == _p_data)
		return *this;

	this->ExePath   (_p_data->lp_sz_Ignored);
	this->ProcessId (_p_data->dwProcessID)  ;
	this->InjectMode(_p_data->eMode  )      ;
	this->LaunchMode(_p_data->eMethod)      ;
	this->Flags     (_p_data->dwFlags)      ;
	this->Win32Code (_p_data->dwLastErrCode);
	this->Handle    (_p_data->hHandleValue );

	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CErrorInfo::operator DWORD (void) const { return this->ErrCode(); }