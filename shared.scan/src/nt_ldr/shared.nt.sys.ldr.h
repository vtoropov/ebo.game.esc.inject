#ifndef _SHAREDNTSYSLDR_H_EA1658E8_1EE0_4098_A886_BC8AD93A3AE5_INCLUDED
#define _SHAREDNTSYSLDR_H_EA1658E8_1EE0_4098_A886_BC8AD93A3AE5_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is internal NT function interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 12-Mar-2020 at 12:10:34p, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "shared.proc.defs.h"
#include "shared.proc.obj.h"
#include "shared.nt.sys.funs.h"
#include "shared.inj.base.h"
#include "shared.inj.start.h"
namespace shared { namespace process {
	using shared::inject::launch::eLaunch;

#ifndef MAXPATH_IN_BYTE_W
#define MAXPATH_IN_BYTE_W   (MAX_PATH * sizeof(WCHAR))
#endif
	struct LDR_LOAD_DLL_DATA
	{
		HANDLE           hResult        ;
		fn_LdrLoadDll    pLdrLoadDll    ;
		NTSTATUS         ntStatus       ;
		UNICODE_STRING   pModuleFileName;
		BYTE             Data[MAXPATH_IN_BYTE_W];
	};
	typedef LDR_LOAD_DLL_DATA  TLdrLoadData;
#ifdef _WIN64
	struct LDR_LOAD_DLL_DATA_WOW64
	{
		DWORD            hResult        ;
		DWORD            pLdrLoadDll    ;
		NTSTATUS         ntStatus       ;
		UNICODE_STRING32 pModuleFileName;
		BYTE             Data[MAXPATH_IN_BYTE_W];
	};
	typedef LDR_LOAD_DLL_DATA_WOW64  TLdrLoadDataW64;
#endif

	DWORD _LdrLoadDll (
		PCWCHAR    szDllFile  ,
		HANDLE     hTargetProc,
		eLaunch    eMethod    ,
		DWORD      dwFlags    ,
		HINSTANCE& hOut       ,
		DWORD&     dwLastError
	);
#ifdef _WIN64
	DWORD _LdrLoadDll_WOW64 (
		PCWCHAR    szDllFile  ,
		HANDLE     hTargetProc,
		eLaunch    eMethod    ,
		DWORD      dwFlags    ,
		HINSTANCE& hOut       ,
		DWORD&     dwLastError
	);
#endif
}}

#endif/*_SHAREDNTSYSLDR_H_EA1658E8_1EE0_4098_A886_BC8AD93A3AE5_INCLUDED*/