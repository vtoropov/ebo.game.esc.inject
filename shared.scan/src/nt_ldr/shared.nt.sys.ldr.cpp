/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is internal NT function interface implementation file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 12-Mar-2020 at 12:51:44p, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "StdAfx.h"
#include "shared.nt.sys.ldr.h"

using namespace shared::process;

#include "shared.inj.error.h"

using namespace shared::inject ;
using namespace shared::inject::launch;

/////////////////////////////////////////////////////////////////////////////

HINSTANCE __stdcall LdrLoadDll_Shell(TLdrLoadData* pData)
{
	if (!pData || !pData->pLdrLoadDll)
		return NULL;

	pData->pModuleFileName.Buffer = ReCa<wchar_t*>(pData->Data);
	pData->ntStatus = pData->pLdrLoadDll(nullptr, 0, &pData->pModuleFileName, &pData->hResult);
	pData->pLdrLoadDll = nullptr;

	return ReCa<HINSTANCE>(pData->hResult);
}

DWORD LdrLoadDll_Shell_End(void) { return 1; }

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace process {

DWORD   _LdrLoadDll(PCWCHAR szDllFile, HANDLE hTargetProc, eLaunch eMethod, DWORD dwFlags, HINSTANCE& hOut, DWORD& dwLastError)
{
	size_t size_out = 0;

	TLdrLoadData data{ 0 };
	data.pModuleFileName.MaximumLength = sizeof(data.Data);
	::StringCbLengthW(szDllFile, data.pModuleFileName.MaximumLength, &size_out);
	::StringCbCopyW  (ReCa<PWCHAR>(data.Data), data.pModuleFileName.MaximumLength, szDllFile);
	data.pModuleFileName.Length = (USHORT)size_out;

	PVOID pLdrLoadDll = nullptr;
	if (!CObject(hTargetProc).ProcAddress(_T("ntdll.dll"), "LdrLoadDll", pLdrLoadDll)) {

		dwLastError = ::GetLastError();
		return INJ_ERR_LDRLOADDLL_MISSING;
	}
	data.pLdrLoadDll    = ReCa<fn_LdrLoadDll>(pLdrLoadDll);

	ULONG_PTR ShellSize	= (ULONG_PTR)LdrLoadDll_Shell_End - (ULONG_PTR)LdrLoadDll_Shell;
	PBYTE pAllocBase    = ReCa<PBYTE>(
		::VirtualAllocEx(hTargetProc, nullptr, sizeof(TLdrLoadData) + ShellSize + 0x10, MEM_COMMIT|MEM_RESERVE, PAGE_EXECUTE_READWRITE)
		);
	PBYTE pArg          = pAllocBase;
	PBYTE pFunc         = ReCa<PBYTE>(ALIGN_UP(ReCa<ULONG_PTR>(pArg) + sizeof(TLdrLoadData), 0x10));

	if (!pArg) {
		dwLastError = ::GetLastError();
		return INJ_ERR_CANT_ALLOC_MEM;
	}

	if (!::WriteProcessMemory(hTargetProc, pArg, &data, sizeof(TLdrLoadData), nullptr)) {

		dwLastError = ::GetLastError();
		::VirtualFreeEx(hTargetProc, pArg, 0, MEM_RELEASE);

		return INJ_ERR_WPM_FAIL;
	}

	if (!::WriteProcessMemory(hTargetProc, pFunc, LdrLoadDll_Shell, ShellSize, nullptr)) {

		dwLastError = ::GetLastError();
		::VirtualFreeEx(hTargetProc, pArg, 0, MEM_RELEASE);

		return INJ_ERR_WPM_FAIL;
	}

	ULONG_PTR remote_ret = 0;
	DWORD dwRet = StartRoutine(
		hTargetProc, ReCa<f_Routine*>(pFunc), pArg, eMethod, (dwFlags & INJ_THREAD_CREATE_CLOAKED) != 0, dwLastError, remote_ret);
	ReadProcessMemory(hTargetProc, pArg, &data, sizeof(data), nullptr);

	hOut = ReCa<HINSTANCE>(remote_ret);

	if (eMethod != LM_QueueUserAPC) {
		::VirtualFreeEx(hTargetProc, pArg, 0, MEM_RELEASE);
	}

	return dwRet;
}

#ifdef _WIN64

BYTE LdrLoadDll_Shell_WOW64[] = 
{ 
	0x55, 0x8B, 0xEC, 0x56, 0x8B, 0x75, 0x08, 0x85,
	0xF6, 0x74, 0x29, 0x8B, 0x46, 0x04, 0x85, 0xC0,
	0x74, 0x22, 0x8D, 0x4E, 0x14, 0x56, 0x89, 0x4E,
	0x10, 0x8D, 0x4E, 0x0C, 0x51, 0x6A, 0x00, 0x6A,
	0x00, 0xFF, 0xD0, 0x89, 0x46, 0x08, 0x8B, 0x06,
	0xC7, 0x46, 0x04, 0x00, 0x00, 0x00, 0x00, 0x5E,
	0x5D, 0xC2, 0x04, 0x00, 0x33, 0xC0, 0x5E, 0x5D,
	0xC2, 0x04, 0x00
};


DWORD   _LdrLoadDll_WOW64(PCWCHAR szDllFile, HANDLE hTargetProc, eLaunch eMethod, DWORD dwFlags, HINSTANCE& hOut, DWORD& dwLastError)
{
	size_t size_out = 0;

	TLdrLoadDataW64 data{ 0 };
	data.pModuleFileName.MaxLength = sizeof(data.Data);
	::StringCbLengthW(szDllFile, data.pModuleFileName.MaxLength, &size_out);
	::StringCbCopyW  (ReCa<wchar_t*>(data.Data), data.pModuleFileName.MaxLength, szDllFile);
	data.pModuleFileName.Length = (WORD)size_out;

	PVOID pLdrLoadDll = nullptr;
	if (!CObject_WOW64(hTargetProc).ProcAddress(_T("ntdll.dll"), "LdrLoadDll", pLdrLoadDll)) {
		dwLastError = ::GetLastError();
		return INJ_ERR_LDRLOADDLL_MISSING;
	}

	data.pLdrLoadDll = (DWORD)(ULONG_PTR)pLdrLoadDll;

	PBYTE pArg = ReCa<PBYTE>(
		::VirtualAllocEx(
			hTargetProc, nullptr, sizeof(TLdrLoadDataW64) + sizeof(LdrLoadDll_Shell_WOW64) + 0x10,
			MEM_COMMIT|MEM_RESERVE, PAGE_EXECUTE_READWRITE
		));
	if ( !pArg ) {
		dwLastError = ::GetLastError();
		return INJ_ERR_CANT_ALLOC_MEM ;
	}

	if (!WriteProcessMemory(hTargetProc, pArg, &data, sizeof(TLdrLoadDataW64), nullptr))
	{
		dwLastError = ::GetLastError();
		::VirtualFreeEx(hTargetProc, pArg, 0, MEM_RELEASE);

		return INJ_ERR_WPM_FAIL;
	}

	auto* pShell = ReCa<PBYTE>(ALIGN_UP(ReCa<ULONG_PTR>(pArg + sizeof(TLdrLoadDataW64)), 0x10));
	if (!::WriteProcessMemory(hTargetProc, pShell, LdrLoadDll_Shell_WOW64, sizeof(LdrLoadDll_Shell_WOW64), nullptr)) {

		dwLastError = ::GetLastError();
		::VirtualFreeEx(hTargetProc, pArg, 0, MEM_RELEASE);

		return INJ_ERR_WPM_FAIL;
	}

	DWORD remote_ret = 0;
	DWORD dwRet = StartRoutine_WOW64(
		hTargetProc, MDWD(pShell), MDWD(pArg), eMethod, (dwFlags & INJ_THREAD_CREATE_CLOAKED) != 0, dwLastError, remote_ret
	);
	hOut = (HINSTANCE)(ULONG_PTR)remote_ret;

	if(eMethod != LM_QueueUserAPC) {
		::VirtualFreeEx(hTargetProc, pArg, 0, MEM_RELEASE);
	}
	return dwRet;
}

#endif
}}