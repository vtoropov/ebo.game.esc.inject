/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is base import handler interface declaration file (https://github.com/guided-hacking/GuidedHacking-Injector);
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 4-Mar-2020 at 6:42:10p, UTC+7, Novosibirsk, Tulenina, Wednesday;
*/
#include "StdAfx.h"
#include "shared.proc.obj.h"

using namespace shared::process;

#include "shared.gen.raw.buf.h"
using namespace shared::common;
/////////////////////////////////////////////////////////////////////////////

CObject_Base:: CObject_Base(const HANDLE _h_target_proc) : m_h_proc(_h_target_proc) { m_j_error << __MODULE__ << S_OK >> __MODULE__; }
CObject_Base::~CObject_Base(void) {}

/////////////////////////////////////////////////////////////////////////////

TJErrorRef     CObject_Base::Error  (void) const { return m_j_error; }
bool           CObject_Base::IsValid(void) const {
	if (NULL == m_h_proc) {
		((CError&)m_j_error = OLE_E_BLANK) = _T("Target procedure name is not specified;");
		return false;
	}
	else
		return true;
}

/////////////////////////////////////////////////////////////////////////////

CObject:: CObject(const HANDLE _h_target_proc) : TImport(_h_target_proc) { }
CObject::~CObject(void) {}

/////////////////////////////////////////////////////////////////////////////

HINSTANCE    CObject::ModuleHandle(LPCTCH  lpModuleName) {
	m_j_error << __MODULE__ << S_OK;

	if (TImport::IsValid() == false) {
		return NULL;
	}
	if (NULL == lpModuleName || !::_tcslen(lpModuleName)) {
		(CError&)m_j_error = E_INVALIDARG;
		return NULL;
	}

	MODULEENTRY32 ME32{ 0 };
	ME32.dwSize = sizeof(ME32);

	HANDLE hSnap  = ::CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, ::GetProcessId(m_h_proc));
	if (hSnap == INVALID_HANDLE_VALUE) {
		while (GetLastError() == ERROR_BAD_LENGTH) {
			hSnap = ::CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, ::GetProcessId(m_h_proc));

			if (hSnap != INVALID_HANDLE_VALUE)
				break;
		}
	}

	BOOL b_result = FALSE;

	if (hSnap == INVALID_HANDLE_VALUE || !hSnap) {
		goto __not_found;
	}

	b_result = ::Module32First(hSnap, &ME32);
	do {
		if (!_tcsicmp(ME32.szModule, lpModuleName))
			break;
		b_result = ::Module32Next(hSnap, &ME32);
	} while (b_result);

	::CloseHandle(hSnap); hSnap = NULL;

	if (b_result == FALSE)
		goto __not_found;
	else
		return ME32.hModule;

__not_found:
	((CError&)m_j_error = __DwordToHresult(ERROR_NOT_FOUND)) = _T("Requested module is not found;");
	return NULL;
}

bool         CObject::ProcAddress (LPCTCH  lpModuleName, LPCCH szProcName, PVOID& pOut) {
	return  this->ProcAddress(this->ModuleHandle(lpModuleName),szProcName, pOut);
}

bool         CObject::ProcAddress (HMODULE hModule     , LPCCH szProcName, PVOID& pOut) {
	m_j_error << __MODULE__ << S_OK;

	if (TImport::IsValid() == false)
		return false;

	BYTE* modBase = reinterpret_cast<BYTE*>(hModule);

	if (NULL == modBase) {
		(CError&)m_j_error = E_INVALIDARG;
		return false;
	}

	static DWORD dw_size = 0x1000;
	static DWORD dw_req  = sizeof(BYTE) * dw_size;

	CRawData pe_header(dw_req);
	if (pe_header.IsValid() == false) {
		(CError&)m_j_error  = pe_header.Error();
		return false;
	}

	if (::ReadProcessMemory(m_h_proc, modBase, (PBYTE)pe_header, (DWORD)pe_header, nullptr) == FALSE) {
		(CError&)m_j_error = __LastErrToHresult();
		return false;
	}

	auto* pNT            = reinterpret_cast<IMAGE_NT_HEADERS*>(
		(PBYTE)pe_header + reinterpret_cast<IMAGE_DOS_HEADER*>((PBYTE)pe_header)->e_lfanew
		);
	auto* pExportEntry  = &pNT->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
	auto  ExportSize    = pExportEntry->Size;
	auto  ExportDirRVA  = pExportEntry->VirtualAddress;

	if (0 == ExportSize) {
		return false;
	}

	CRawData export_data(ExportSize);
	if (export_data.IsValid() == false) {
		(CError&)m_j_error = export_data.Error();
		return false;
	}

	if (!::ReadProcessMemory(m_h_proc, modBase + ExportDirRVA, (PBYTE)export_data, ExportSize, nullptr)) {
		(CError&)m_j_error = __LastErrToHresult();
		return false;
	}

	BYTE* localBase = export_data.GetData() - ExportDirRVA;
	auto pExportDir = reinterpret_cast<IMAGE_EXPORT_DIRECTORY*>((PBYTE)export_data);

	auto Forward = [&](DWORD FuncRVA)->bool
	{
		char pFullExport[MAX_PATH]{ 0 };
		size_t len_out = 0;

		::StringCchLengthA(reinterpret_cast<char*>(localBase + FuncRVA), sizeof(pFullExport), &len_out);
		if (!len_out)
			return false;

		::memcpy(pFullExport, reinterpret_cast<char*>(localBase + FuncRVA), len_out);

		char* pFuncName = strchr(pFullExport, '.');
		*(pFuncName++)  = '\0';
		if (*pFuncName == '#')
		     pFuncName = reinterpret_cast<char*>(LOWORD(::atoi(++pFuncName)));

#ifdef UNICODE
		TCHAR ModNameW[MAX_PATH + 1]{ 0 };
		size_t SizeOut = 0;

		if (::mbstowcs_s(&SizeOut, ModNameW, pFullExport, MAX_PATH))
			return this->ProcAddress(ModNameW, pFuncName, pOut);
		else
			return false;
#else
		return GetProcAddress(pFullExport, pFuncName, pOut);
#endif
	};

	if ((reinterpret_cast<ULONG_PTR>(szProcName) & 0xFFFFFF) <= MAXWORD)
	{
		WORD Base     = LOWORD(pExportDir->Base - 1);
		WORD Ordinal  = LOWORD(szProcName) - Base;
		DWORD FuncRVA = reinterpret_cast<DWORD*>(localBase + pExportDir->AddressOfFunctions)[Ordinal];

		if (FuncRVA >= ExportDirRVA && FuncRVA < ExportDirRVA + ExportSize) {
			return Forward(FuncRVA);
		}
		return !!(modBase + FuncRVA);
	}

	DWORD max      = pExportDir->NumberOfNames - 1;
	DWORD min      = 0;
	DWORD FuncRVA  = 0;

	while (min <= max) {
		DWORD mid = (min + max) / 2;

		DWORD CurrNameRVA = reinterpret_cast<DWORD*>(localBase + pExportDir->AddressOfNames)[mid];
		PCHAR szName      = reinterpret_cast<char* >(localBase + CurrNameRVA);

		int cmp = ::strcmp(szName, szProcName);
		if (cmp < 0)
			min = mid + 1;
		else if (cmp > 0)
			max = mid - 1;
		else {
			WORD Ordinal = reinterpret_cast<WORD* >(localBase + pExportDir->AddressOfNameOrdinals)[mid];
			FuncRVA      = reinterpret_cast<DWORD*>(localBase + pExportDir->AddressOfFunctions)[Ordinal];
			break;
		}
	}

	if (!FuncRVA)
		return false;

	if (FuncRVA >= ExportDirRVA && FuncRVA < ExportDirRVA + ExportSize) {
		return Forward(FuncRVA);
	}

	pOut = modBase + FuncRVA;

	return true;
}

/////////////////////////////////////////////////////////////////////////////

CObject_WOW64:: CObject_WOW64(const HANDLE _h_target_proc) : TImport(_h_target_proc) { }
CObject_WOW64::~CObject_WOW64(void) {}

/////////////////////////////////////////////////////////////////////////////

HINSTANCE    CObject_WOW64::ModuleHandle(LPCTCH  lpModuleName) {
	m_j_error << __MODULE__ << S_OK;

	if (TImport::IsValid() == false) {
		return NULL;
	}
	if (NULL == lpModuleName || !::_tcslen(lpModuleName)) {
		(CError&)m_j_error = E_INVALIDARG;
		return NULL;
	}

	MODULEENTRY32 ME32{ 0 };
	ME32.dwSize = sizeof(ME32);

	HANDLE hSnap  = ::CreateToolhelp32Snapshot(TH32CS_SNAPMODULE32 | TH32CS_SNAPMODULE, ::GetProcessId(m_h_proc));
	if (hSnap == INVALID_HANDLE_VALUE) {
		while (GetLastError() == ERROR_BAD_LENGTH) {
			hSnap = ::CreateToolhelp32Snapshot(TH32CS_SNAPMODULE32 | TH32CS_SNAPMODULE, ::GetProcessId(m_h_proc));

			if (hSnap != INVALID_HANDLE_VALUE)
				break;
		}
	}

	BOOL b_result = FALSE;

	if (hSnap == INVALID_HANDLE_VALUE || !hSnap) {
		goto __not_found;
	}

	b_result = ::Module32First(hSnap, &ME32);
	do {
		if (!::_tcsicmp(ME32.szModule, lpModuleName) && (ME32.modBaseAddr < (BYTE*)0x7FFFF000))
			break;

		b_result = ::Module32Next(hSnap, &ME32);
	} while (b_result);

	::CloseHandle(hSnap); hSnap = NULL;

	if (b_result == FALSE)
		goto __not_found;
	else
		return ME32.hModule;

__not_found:
	((CError&)m_j_error = __DwordToHresult(ERROR_NOT_FOUND)) = _T("Requested module is not found;");
	return NULL;
}

bool         CObject_WOW64::ProcAddress (LPCTCH  lpModuleName, LPCCH szProcName, PVOID& pOut) {
	return ProcAddress(this->ModuleHandle(lpModuleName), szProcName, pOut);
}

bool         CObject_WOW64::ProcAddress (HMODULE hModule     , LPCCH szProcName, PVOID& pOut) {
	m_j_error << __MODULE__ << S_OK;

	if (TImport::IsValid() == false)
		return false;

	BYTE* modBase = reinterpret_cast<BYTE*>(hModule);

	if (NULL == modBase) {
		(CError&)m_j_error = E_INVALIDARG;
		return false;
	}

	static DWORD dw_size = 0x1000;
	static DWORD dw_req  = sizeof(BYTE) * dw_size;

	CRawData pe_header(dw_req);
	if (pe_header.IsValid() == false) {
		(CError&)m_j_error  = pe_header.Error();
		return false;
	}

	CRawData buff(dw_req);
	if (buff.IsValid() == false) {
		(CError&)m_j_error  = buff.Error();
		return false;
	}

	if (::ReadProcessMemory(m_h_proc, modBase, (PBYTE)buff, (DWORD)buff, nullptr) == FALSE) {
		(CError&)m_j_error = __LastErrToHresult();
		return false;
	}

	auto* pNT  = reinterpret_cast<IMAGE_NT_HEADERS32*>(
	             reinterpret_cast<IMAGE_DOS_HEADER*  >((PBYTE)buff)->e_lfanew + (PBYTE)buff
		);
	auto* pDir       = &pNT->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
	auto  ExportSize = pDir->Size;
	auto  DirRVA     = pDir->VirtualAddress;

	if (0 == ExportSize) {
		(CError&)m_j_error = __DwordToHresult(RPC_S_NOTHING_TO_EXPORT);
		return false;
	}

	CRawData pExpDirBuffer(ExportSize);
	if (pExpDirBuffer.IsValid() == false) {
		(CError&)m_j_error = pExpDirBuffer.Error();
		return false;
	}

	auto* pExportDir = reinterpret_cast<IMAGE_EXPORT_DIRECTORY*>((PBYTE)pExpDirBuffer);
	if (::ReadProcessMemory(m_h_proc, modBase + DirRVA, pExpDirBuffer, ExportSize, nullptr) == FALSE)
	{
		(CError&)m_j_error = __LastErrToHresult();
		return false;
	}

	BYTE* pBase  = (PBYTE)pExpDirBuffer - DirRVA;
	auto Forward = [&](DWORD FuncRVA, void* &pForwarded) -> bool
	{
		char pFullExport[MAX_PATH]{ 0 };
		size_t len_out = 0;

		::StringCchLengthA(reinterpret_cast<char*>(pBase + FuncRVA), sizeof(pFullExport), &len_out);
		if (!len_out)
			return false;

		::StringCchCopyA(pFullExport, len_out, reinterpret_cast<char*>(pBase + FuncRVA));
		
		char* pFuncName = ::strchr(pFullExport, '.');
		*pFuncName++ = '\0';
		if (*pFuncName == '#')
		     pFuncName = reinterpret_cast<char*>(LOWORD(atoi(++pFuncName)));

#ifdef UNICODE

		TCHAR ModNameW[MAX_PATH + 1]{ 0 };
		size_t SizeOut = 0;

		if (::mbstowcs_s(&SizeOut, ModNameW, pFullExport, MAX_PATH))
			return this->ProcAddress(ModNameW, pFuncName, pForwarded);
		else
			return false;
#else
		return this->ProcAddress(pFullExport, pFuncName, pForwarded);
#endif
	};

	if (reinterpret_cast<ULONG_PTR>(szProcName) <= MAXWORD)
	{
		WORD Base       = LOWORD(pExportDir->Base - 1);
		WORD Ordinal    = LOWORD(szProcName) - Base;
		DWORD FuncRVA   = reinterpret_cast<DWORD*>(pBase + pExportDir->AddressOfFunctions)[Ordinal];

		if (FuncRVA >= DirRVA && FuncRVA < DirRVA + ExportSize)
		{
			return Forward(FuncRVA, pOut);
		}
			
		pOut = modBase + FuncRVA;
		
		return true;
	}

	DWORD max    = pExportDir->NumberOfNames - 1;
	DWORD min    = 0;
	WORD Ordinal = 0;

	while (min <= max)
	{
		DWORD mid = (min + max) / 2;

		DWORD CurrNameRVA = reinterpret_cast<DWORD*>(pBase + pExportDir->AddressOfNames)[mid];
		char * szName     = reinterpret_cast<char* >(pBase + CurrNameRVA);

		int cmp = ::strcmp(szName, szProcName);
		if (cmp < 0)
			min = mid + 1;
		else if (cmp > 0)
			max = mid - 1;
		else {
			Ordinal = reinterpret_cast<WORD*>(pBase + pExportDir->AddressOfNameOrdinals)[mid];
			break;
		}
	}
	
	if (!Ordinal) {
		return false;
	}
	
	DWORD FuncRVA = reinterpret_cast<DWORD*>(pBase + pExportDir->AddressOfFunctions)[Ordinal];

	if (FuncRVA >= DirRVA && FuncRVA < DirRVA + ExportSize) {
		return Forward(FuncRVA, pOut);
	}

	pOut = modBase + FuncRVA;

	return true;
}

/////////////////////////////////////////////////////////////////////////////
#if (0)
namespace shared { namespace process {
HINSTANCE shared::process::GetModuleHandleEx(HANDLE hTargetProc, const TCHAR* szModuleName)
{
	MODULEENTRY32 ME32{ 0 };
	ME32.dwSize = sizeof(ME32);

	HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, GetProcessId(hTargetProc));
	if (hSnap == INVALID_HANDLE_VALUE) {
		while (GetLastError() == ERROR_BAD_LENGTH) {
			hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, GetProcessId(hTargetProc));

			if (hSnap != INVALID_HANDLE_VALUE)
				break;
		}
	}

	if (hSnap == INVALID_HANDLE_VALUE || !hSnap) {
		return NULL;
	}

	BOOL bRet = Module32First(hSnap, &ME32);
	do {
		if (!_tcsicmp(ME32.szModule, szModuleName))
			break;
		bRet = Module32Next(hSnap, &ME32);
	} while (bRet);

	CloseHandle(hSnap);

	if (!bRet) {
		return NULL;
	}
	return ME32.hModule;
}
#endif
#if (0)
bool shared::process::GetProcAddressEx(HANDLE hTargetProc, const TCHAR* szModuleName, const char* szProcName, void* &pOut)
{
	return GetProcAddressEx(hTargetProc, GetModuleHandleEx(hTargetProc, szModuleName), szProcName, pOut);
}
#endif
#if (0)
bool shared::process::GetProcAddressEx(HANDLE hTargetProc, HINSTANCE hModule, const char* szProcName, void* &pOut)
{
	BYTE* modBase = reinterpret_cast<BYTE*>(hModule);

	if (NULL == modBase)
		return false;

	BYTE* pe_header = new BYTE[0x1000];
	if ( pe_header == NULL)
		return false;

	if (!::ReadProcessMemory(hTargetProc, modBase, pe_header, 0x1000, nullptr)) {
		delete[] pe_header;
		return false;
	}

	auto* pNT           = reinterpret_cast<IMAGE_NT_HEADERS*>(pe_header + reinterpret_cast<IMAGE_DOS_HEADER*>(pe_header)->e_lfanew);
	auto* pExportEntry  = &pNT->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
	auto ExportSize     = pExportEntry->Size;
	auto ExportDirRVA   = pExportEntry->VirtualAddress;

	if (!ExportSize) {
		delete[] pe_header;
		return false;
	}

	BYTE* export_data   = new BYTE[ExportSize];
	if (!export_data) {
		delete[] pe_header;
		return false;
	}

	if (!::ReadProcessMemory(hTargetProc, modBase + ExportDirRVA, export_data, ExportSize, nullptr)) {
		delete[] export_data;
		delete[] pe_header;
		return false;
	}

	BYTE* localBase = export_data - ExportDirRVA;
	auto pExportDir = reinterpret_cast<IMAGE_EXPORT_DIRECTORY*>(export_data);

	auto Forward = [&](DWORD FuncRVA)->bool
	{
		char pFullExport[MAX_PATH]{ 0 };
		size_t len_out = 0;

		StringCchLengthA(reinterpret_cast<char*>(localBase + FuncRVA), sizeof(pFullExport), &len_out);
		if (!len_out)
			return false;

		::memcpy(pFullExport, reinterpret_cast<char*>(localBase + FuncRVA), len_out);

		char* pFuncName = strchr(pFullExport, '.');
		*(pFuncName++)  = '\0';
		if (*pFuncName == '#')
			pFuncName = reinterpret_cast<char*>(LOWORD(atoi(++pFuncName)));

#ifdef UNICODE
		TCHAR ModNameW[MAX_PATH + 1]{ 0 };
		size_t SizeOut = 0;

		if (mbstowcs_s(&SizeOut, ModNameW, pFullExport, MAX_PATH))
			return GetProcAddressEx(hTargetProc, ModNameW, pFuncName, pOut);
		else
			return false;
#else
		return GetProcAddressEx(hTargetProc, pFullExport, pFuncName, pOut);
#endif
	};

	if ((reinterpret_cast<ULONG_PTR>(szProcName) & 0xFFFFFF) <= MAXWORD)
	{
		WORD Base     = LOWORD(pExportDir->Base - 1);
		WORD Ordinal  = LOWORD(szProcName) - Base;
		DWORD FuncRVA = reinterpret_cast<DWORD*>(localBase + pExportDir->AddressOfFunctions)[Ordinal];

		delete[] export_data;
		delete[] pe_header;

		if (FuncRVA >= ExportDirRVA && FuncRVA < ExportDirRVA + ExportSize) {
			return Forward(FuncRVA);
		}
		return modBase + FuncRVA;
	}

	DWORD max      = pExportDir->NumberOfNames - 1;
	DWORD min      = 0;
	DWORD FuncRVA  = 0;

	while (min <= max) {
		DWORD mid = (min + max) / 2;

		DWORD CurrNameRVA = reinterpret_cast<DWORD*>(localBase + pExportDir->AddressOfNames)[mid];
		char * szName     = reinterpret_cast<char*>(localBase + CurrNameRVA);

		int cmp = strcmp(szName, szProcName);
		if (cmp < 0)
			min = mid + 1;
		else if (cmp > 0)
			max = mid - 1;
		else {
			WORD Ordinal = reinterpret_cast<WORD* >(localBase + pExportDir->AddressOfNameOrdinals)[mid];
			FuncRVA      = reinterpret_cast<DWORD*>(localBase + pExportDir->AddressOfFunctions)[Ordinal];
			break;
		}
	}

	delete[] export_data;
	delete[] pe_header;

	if (!FuncRVA)
		return false;

	if (FuncRVA >= ExportDirRVA && FuncRVA < ExportDirRVA + ExportSize)
	{
		return Forward(FuncRVA);
	}

	pOut = modBase + FuncRVA;

	return true;
}
#endif
#if (0)
HINSTANCE GetModuleHandleEx_WOW64(HANDLE hTargetProc, const TCHAR * lpModuleName)
{
	MODULEENTRY32 ME32{ 0 };
	ME32.dwSize = sizeof(ME32);

	HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE32 | TH32CS_SNAPMODULE, GetProcessId(hTargetProc));
	if (hSnap == INVALID_HANDLE_VALUE)
	{
		while (GetLastError() == ERROR_BAD_LENGTH)
		{
			hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE32 | TH32CS_SNAPMODULE, GetProcessId(hTargetProc));

			if (hSnap != INVALID_HANDLE_VALUE)
				break;
		}
	}

	if (hSnap == INVALID_HANDLE_VALUE || !hSnap)
	{
		return NULL;
	}

	BOOL bRet = Module32First(hSnap, &ME32);
	do
	{
		if (!_tcsicmp(ME32.szModule, lpModuleName) && (ME32.modBaseAddr < (BYTE*)0x7FFFF000))
			break;

		bRet = Module32Next(hSnap, &ME32);
	} while (bRet);

	CloseHandle(hSnap);

	if (!bRet)
	{
		return NULL;
	}

	return ME32.hModule;
}
#endif
#if (0)
bool GetProcAddressEx_WOW64(HANDLE hTargetProc, const TCHAR * szModuleName, const char * szProcName, void * &pOut)
{
	return GetProcAddressEx_WOW64(hTargetProc, GetModuleHandleEx_WOW64(hTargetProc, szModuleName), szProcName, pOut);
}
#endif
#if (0)
bool GetProcAddressEx_WOW64(HANDLE hTargetProc, HINSTANCE hModule, const char * szProcName, void * &pOut)
{
	BYTE * modBase = ReCa<BYTE*>(hModule);

	if (!modBase)
		return false;

	BYTE * pe_header = new BYTE[0x1000];
	if (!pe_header)
		return false;

	BYTE * pBuffer = new BYTE[0x1000];
	if (!ReadProcessMemory(hTargetProc, modBase, pBuffer, 0x1000, nullptr))
	{
		delete[] pBuffer;

		return false;
	}

	auto* pNT = ReCa<IMAGE_NT_HEADERS32*>(ReCa<IMAGE_DOS_HEADER*>(pBuffer)->e_lfanew + pBuffer);
	auto* pDir = &pNT->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
	auto ExportSize = pDir->Size;
	auto DirRVA		= pDir->VirtualAddress;

	if (!ExportSize)
	{
		delete[] pBuffer;

		return false;
	}

	BYTE * pExpDirBuffer = new BYTE[ExportSize];
	auto* pExportDir = ReCa<IMAGE_EXPORT_DIRECTORY*>(pExpDirBuffer);
	if (!ReadProcessMemory(hTargetProc, modBase + DirRVA, pExpDirBuffer, ExportSize, nullptr))
	{
		delete[] pExpDirBuffer;
		delete[] pBuffer;

		return false;
	}

	BYTE * pBase = pExpDirBuffer - DirRVA;

	auto Forward = [&](DWORD FuncRVA, void * &pForwarded) -> bool
	{
		char pFullExport[MAX_PATH]{ 0 };
		size_t len_out = 0;

		StringCchLengthA(ReCa<char*>(pBase + FuncRVA), sizeof(pFullExport), &len_out);
		if (!len_out)
			return false;

		StringCchCopyA(pFullExport, len_out, ReCa<char*>(pBase + FuncRVA));

		char * pFuncName = strchr(pFullExport, '.');
		*pFuncName++ = '\0';
		if (*pFuncName == '#')
			pFuncName = ReCa<char*>(LOWORD(atoi(++pFuncName)));

#ifdef UNICODE

		TCHAR ModNameW[MAX_PATH + 1]{ 0 };
		size_t SizeOut = 0;

		if (mbstowcs_s(&SizeOut, ModNameW, pFullExport, MAX_PATH))
			return GetProcAddressEx_WOW64(hTargetProc, ModNameW, pFuncName, pForwarded);
		else
			return false;
#else

		return GetProcAddressEx_WOW64(hTargetProc, pFullExport, pFuncName, pForwarded);

#endif
	};

	if (ReCa<ULONG_PTR>(szProcName) <= MAXWORD)
	{
		WORD Base		= LOWORD(pExportDir->Base - 1);
		WORD Ordinal	= LOWORD(szProcName) - Base;
		DWORD FuncRVA	= ReCa<DWORD*>(pBase + pExportDir->AddressOfFunctions)[Ordinal];

		delete[] pExpDirBuffer;
		delete[] pBuffer;

		if (FuncRVA >= DirRVA && FuncRVA < DirRVA + ExportSize)
		{
			return Forward(FuncRVA, pOut);
		}

		pOut = modBase + FuncRVA;

		return true;
	}

	DWORD max		= pExportDir->NumberOfNames - 1;
	DWORD min		= 0;
	WORD Ordinal	= 0;

	while (min <= max)
	{
		DWORD mid = (min + max) / 2;

		DWORD CurrNameRVA	= ReCa<DWORD*>(pBase + pExportDir->AddressOfNames)[mid];
		char * szName		= ReCa<char*>(pBase + CurrNameRVA);

		int cmp = strcmp(szName, szProcName);
		if (cmp < 0)
			min = mid + 1;
		else if (cmp > 0)
			max = mid - 1;
		else 
		{
			Ordinal = ReCa<WORD*>(pBase + pExportDir->AddressOfNameOrdinals)[mid];
			break;
		}
	}

	if (!Ordinal)
	{
		delete[] pExpDirBuffer;
		delete[] pBuffer;

		return false;
	}

	DWORD FuncRVA = ReCa<DWORD*>(pBase + pExportDir->AddressOfFunctions)[Ordinal];

	delete[] pExpDirBuffer;
	delete[] pBuffer;

	if (FuncRVA >= DirRVA && FuncRVA < DirRVA + ExportSize)
	{
		return Forward(FuncRVA, pOut);
	}

	pOut = modBase + FuncRVA;

	return true;
}
}}
#endif