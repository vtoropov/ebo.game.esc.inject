/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is image header manual mapping interface implementation file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 15-Mar-2020 at 11:34:17p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include "StdAfx.h"
#include "shared.nt.map.man.h"
#include "shared.proc.obj.h"

using namespace shared::process;

#include "shared.inj.base.h"
#include "shared.inj.error.h"
#include "shared.gen.raw.buf.h"

using namespace shared::inject;
using namespace shared::inject::launch;
using namespace shared::common;

/////////////////////////////////////////////////////////////////////////////

HINSTANCE __stdcall ManualMapping_Shell(TManMapData* pData)
{
	if (!pData || !pData->pLoadLibraryA)
		return NULL;

	PBYTE pBase = pData->pModuleBase;
	auto* pOp   = &ReCa<PIMAGE_NT_HEADERS>(pBase + ReCa<PIMAGE_DOS_HEADER>(pBase)->e_lfanew)->OptionalHeader;

	auto pGetProcAddress = pData->pGetProcAddress;
	DWORD dwFlags        = pData->dwFlags;
	auto pDllMain        = ReCa<f_DLL_ENTRY_POINT>(pBase + pOp->AddressOfEntryPoint);

	if (pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size)
	{
		auto*  pImportDescr = ReCa<PIMAGE_IMPORT_DESCRIPTOR>(pBase + pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
		while (pImportDescr && pImportDescr->Name)
		{
			PCHAR szMod = ReCa<PCHAR>(pBase + pImportDescr->Name);
			HINSTANCE hDll = pData->pLoadLibraryA(szMod);

			PULONG_PTR pThunkRef = ReCa<PULONG_PTR>(pBase + pImportDescr->OriginalFirstThunk);
			PULONG_PTR pFuncRef  = ReCa<PULONG_PTR>(pBase + pImportDescr->FirstThunk);

			if (!pImportDescr->OriginalFirstThunk)
				pThunkRef = pFuncRef;

			for (; NULL != pThunkRef && *pThunkRef; ++pThunkRef, ++pFuncRef)
			{
				if (IMAGE_SNAP_BY_ORDINAL(*pThunkRef)) {
					*pFuncRef = pGetProcAddress(hDll, ReCa<PCHAR>(*pThunkRef & 0xFFFF));
				}
				else {
					auto* pImport = ReCa<PIMAGE_IMPORT_BY_NAME>(pBase + (*pThunkRef));
					*pFuncRef = pGetProcAddress(hDll, pImport->Name);
				}
			}
			++pImportDescr;
		}
	}

	if (pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_TLS].Size)
	{
		auto* pTLS = ReCa<PIMAGE_TLS_DIRECTORY>(pBase + pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_TLS].VirtualAddress);
		auto*  pCallback = ReCa<PIMAGE_TLS_CALLBACK*>(pTLS->AddressOfCallBacks);
		for (; pCallback && (*pCallback); ++pCallback)
		{
			auto Callback = *pCallback;
			Callback(pBase, DLL_PROCESS_ATTACH, nullptr);
		}
	}

	pDllMain(pBase, DLL_PROCESS_ATTACH, nullptr);

	if (dwFlags & INJ_CLEAN_DATA_DIR) {
		DWORD  dwSize = pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size;
		if (0!=dwSize)
		{
			auto*  pImportDescr = ReCa<PIMAGE_IMPORT_DESCRIPTOR>(pBase + pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
			while (pImportDescr && pImportDescr->Name)
			{
				PCHAR szMod = ReCa<char*>(pBase + pImportDescr->Name);
				for (;szMod && *szMod++; *szMod = 0);
				pImportDescr->Name = 0;

				PULONG_PTR pThunkRef = ReCa<PULONG_PTR>(pBase + pImportDescr->OriginalFirstThunk);
				PULONG_PTR pFuncRef  = ReCa<PULONG_PTR>(pBase + pImportDescr->FirstThunk);

				if (!pImportDescr->OriginalFirstThunk)
					pThunkRef = pFuncRef;

				for (; NULL!=pThunkRef && *pThunkRef; ++pThunkRef, ++pFuncRef)
				{
					if (!IMAGE_SNAP_BY_ORDINAL(*pThunkRef))
					{
						auto* pImport = ReCa<PIMAGE_IMPORT_BY_NAME>(pBase + (*pThunkRef));
						PCHAR  szFunc = pImport->Name;
						for (; NULL!=szFunc && *szFunc++; *szFunc = 0);
					}
					else {
						*(PWORD)pThunkRef = 0;
					}
				}
				++pImportDescr;
			}
			pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress = 0;
			pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size           = 0;
		}

		dwSize = pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_DEBUG].Size;
		if (dwSize)
		{
			auto* pIDD = reinterpret_cast<PIMAGE_DEBUG_DIRECTORY>(pBase + pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_DEBUG].VirtualAddress);
			PBYTE pDataa  = pBase + pIDD->AddressOfRawData;
			for (UINT  i  = 0; i != pIDD->SizeOfData; ++i) {
				pDataa[i] = 0;
			}
			pIDD->AddressOfRawData = 0;
			pIDD->PointerToRawData = 0;
			pIDD->SizeOfData       = 0;
			pIDD->TimeDateStamp    = 0;
			pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_DEBUG].VirtualAddress = 0;
			pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_DEBUG].Size           = 0;
		}

		dwSize = pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].Size;
		if (dwSize)
		{
			auto*  pRelocData = ReCa<PIMAGE_BASE_RELOCATION>(pBase + pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress);
			while (pRelocData && pRelocData->VirtualAddress)
			{
				pRelocData->VirtualAddress = 0;
				pRelocData = ReCa<PIMAGE_BASE_RELOCATION>(ReCa<PBYTE>(pRelocData) + pRelocData->SizeOfBlock);
			}
			pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress = 0;
			pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].Size           = 0;
		}

		dwSize = pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_TLS].Size;
		if (dwSize)
		{
			auto* pTLS = ReCa<PIMAGE_TLS_DIRECTORY>(pBase + pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_TLS].VirtualAddress);
			auto* pCallback = ReCa<PIMAGE_TLS_CALLBACK*>(pTLS->AddressOfCallBacks);
			for (;pCallback && (*pCallback); ++pCallback) {
			     *pCallback = nullptr;
			}

			pTLS->AddressOfCallBacks    = 0;
			pTLS->AddressOfIndex        = 0;
			pTLS->EndAddressOfRawData   = 0;
			pTLS->SizeOfZeroFill        = 0;
			pTLS->StartAddressOfRawData = 0;
			pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_TLS].VirtualAddress = 0;
			pOp->DataDirectory[IMAGE_DIRECTORY_ENTRY_TLS].Size           = 0;
		}
	}

	if ((dwFlags & INJ_ERASE_HEADER) && pBase) {
		for (UINT i = 0; i < 0x1000; i += sizeof(ULONG64))
		{
			*ReCa<PULONG64>(pBase + i) = 0;
		}
	}

	pData->hResult = ReCa<HINSTANCE>(pBase);

	return pData->hResult;
}

DWORD ManualMapping_Shell_End(void) { return 2; }

/////////////////////////////////////////////////////////////////////////////

DWORD MANUAL_MAPPER::AllocateMemory(DWORD& dwError)
{
	pImageBase = (PBYTE)::VirtualAlloc(nullptr, dwImageSize, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	if (NULL  == pImageBase) {
		dwError = ::GetLastError();
		return INJ_ERR_OUT_OF_MEMORY_INT;
	}

	if (dwFlags & INJ_SHIFT_MODULE) {
		::srand(::GetTickCount64() & 0xFFFFFFFF);
		dwShiftOffset = ALIGN_UP(rand() % 0x1000 + 0x100, 0x10);
	}

	DWORD dwShellcodeSize = (DWORD)((ULONG_PTR)ManualMapping_Shell_End - (ULONG_PTR)ManualMapping_Shell);

	dwAllocationSize = dwShiftOffset + dwImageSize + sizeof(TManMapData) + dwShellcodeSize;

	if (dwFlags & INJ_SHIFT_MODULE)
	{
		pAllocationBase = (PBYTE)::VirtualAllocEx(
			hTargetProcess, nullptr, dwAllocationSize, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE
		);
		if (NULL == pAllocationBase)
		{
			dwError = ::GetLastError();
			::VirtualFree(pImageBase, 0, MEM_RELEASE);

			return INJ_ERR_OUT_OF_MEMORY_EXT;
		}
	}
	else {
		pAllocationBase = (PBYTE)::VirtualAllocEx(
			hTargetProcess, (PVOID)pOptionalHeader->ImageBase, dwAllocationSize, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE
		);
		if (NULL == pAllocationBase)
		{
			pAllocationBase = (PBYTE)::VirtualAllocEx(
				hTargetProcess, nullptr, dwAllocationSize, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE
			);
			if (NULL == pAllocationBase)
			{
				dwError = ::GetLastError();
				::VirtualFree(pImageBase, 0, MEM_RELEASE);

				return INJ_ERR_OUT_OF_MEMORY_EXT;
			}
		}
	}

	pTargetImageBase   = pAllocationBase + dwShiftOffset;
	pManualMappingData = pTargetImageBase + dwImageSize;
	pShellcode         = pManualMappingData + sizeof(TManMapData);

	return INJ_ERR_SUCCESS;
}

DWORD MANUAL_MAPPER::CopyData (DWORD& dwError)
{
	::memcpy(pImageBase, pRawData, pOptionalHeader->SizeOfHeaders);

	auto* pCurrentSectionHeader = IMAGE_FIRST_SECTION(pNtHeaders);
	for (UINT i = 0; pFileHeader && i != pFileHeader->NumberOfSections; ++i, ++pCurrentSectionHeader) {
		if (pCurrentSectionHeader->SizeOfRawData)
		{
			::memcpy(
				pImageBase + pCurrentSectionHeader->VirtualAddress,
				pRawData   + pCurrentSectionHeader->PointerToRawData, pCurrentSectionHeader->SizeOfRawData
			);
		}
	}

	if (dwFlags & INJ_SHIFT_MODULE)
	{
		PDWORD pJunk = 0;
		try {
			pJunk = new DWORD[dwShiftOffset / sizeof(DWORD)];
		}
		catch(const ::std::bad_alloc&) {
			return ERROR_OUTOFMEMORY;
		}
		DWORD  dwSuperJunk = ::GetTickCount64() & 0xFFFFFFFF;

		for (UINT i = 0; i < dwShiftOffset / sizeof(DWORD) - 1; ++i)
		{
			pJunk[i]     = dwSuperJunk;
			dwSuperJunk ^= (i << (i % 32));
			dwSuperJunk -= 0x11111111;
		}

		::WriteProcessMemory(hTargetProcess, pAllocationBase, pJunk, dwShiftOffset, nullptr);

		delete[] pJunk;
	}

	auto pLoadFunctionPointer = [=](HINSTANCE hLib, PCCH szFunc, PVOID& pOut) {
		return CObject(hTargetProcess).ProcAddress(hLib, szFunc, pOut);
	};

	HINSTANCE hK32 = CObject(hTargetProcess).ModuleHandle(_T("kernel32.dll"));

	PVOID pLoadLibraryA = nullptr;
	if (!pLoadFunctionPointer(hK32, "LoadLibraryA", pLoadLibraryA))
		return INJ_ERR_REMOTEFUNC_MISSING;

	PVOID pGetProcAddress = nullptr;
	if (!pLoadFunctionPointer(hK32, "GetProcAddress", pGetProcAddress))
		return INJ_ERR_REMOTEFUNC_MISSING;

	MANUAL_MAPPING_DATA data{ 0 };
	data.pLoadLibraryA   = ReCa<f_LoadLibraryA*>(pLoadLibraryA);
	data.pGetProcAddress = ReCa<f_GetProcAddress>(pGetProcAddress);
	data.pModuleBase     = pTargetImageBase;
	data.dwFlags         = dwFlags;


	if (!::WriteProcessMemory(hTargetProcess, pManualMappingData, &data, sizeof(data), nullptr)) {
		dwError = ::GetLastError();
		return INJ_ERR_WPM_FAIL;
	}

	DWORD dwShellcodeSize = (DWORD)((UINT_PTR)ManualMapping_Shell_End - (UINT_PTR)ManualMapping_Shell);
	if (!::WriteProcessMemory(hTargetProcess, pShellcode, ManualMapping_Shell, dwShellcodeSize, nullptr)) {
		dwError = ::GetLastError();
		return INJ_ERR_WPM_FAIL;
	}

	return INJ_ERR_SUCCESS;
}

DWORD MANUAL_MAPPER::CopyImage(DWORD& dwError)
{
	if (!::WriteProcessMemory(hTargetProcess, pTargetImageBase, pImageBase, dwImageSize, nullptr)) {
		dwError = ::GetLastError();
		return INJ_ERR_WPM_FAIL;
	}

	return INJ_ERR_SUCCESS;
}

DWORD MANUAL_MAPPER::RelocateImage(DWORD& dwError)
{
	PBYTE LocationDelta = pTargetImageBase - pOptionalHeader->ImageBase;
	if ( !LocationDelta )
		return (dwError = INJ_ERR_SUCCESS);

	if (!pOptionalHeader->DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].Size)
		return (dwError = INJ_ERR_IMAGE_CANT_RELOC);

	auto*  pRelocData = ReCa<PIMAGE_BASE_RELOCATION>(
			pImageBase + pOptionalHeader->DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress
		);
	while (pRelocData->VirtualAddress)
	{
		PWORD pRelativeInfo = ReCa<PWORD>(pRelocData + 1);
		for (UINT i = 0; pRelativeInfo
		       && i < ((pRelocData->SizeOfBlock - sizeof(IMAGE_BASE_RELOCATION)) / sizeof(WORD)); ++i, ++pRelativeInfo
		)
		{
			if (RELOC_FLAG(*pRelativeInfo))
			{
				PULONG_PTR pPatch = ReCa<PULONG_PTR>(pImageBase + pRelocData->VirtualAddress + ((*pRelativeInfo) & 0xFFF));
				if (pPatch)
				   *pPatch += ReCa<ULONG_PTR>(LocationDelta);
			}
		}
		pRelocData = ReCa<PIMAGE_BASE_RELOCATION>(ReCa<PBYTE>(pRelocData) + pRelocData->SizeOfBlock);
	}

	return (dwError = INJ_ERR_SUCCESS);
}

/////////////////////////////////////////////////////////////////////////////

DWORD _ManualMap(PCWCHAR szDllFile, HANDLE hTargetProc, eLaunch eMethod, DWORD dwFlags, HINSTANCE& hOut, DWORD& dwLastErr)
{
	MANUAL_MAPPER Module{ 0 };
#if (0)
	PBYTE pRawData{ nullptr };
#endif
	std::ifstream File(szDllFile, std::ios::binary | std::ios::ate);

	auto dwFileSize = File.tellg();
#if (0)
	pRawData = new BYTE[static_cast<size_t>(dwFileSize)];
	if (!pRawData) {
		File.close();
		return INJ_ERR_OUT_OF_MEMORY_NEW;
	}
#else
	CRawData pRawData(static_cast<DWORD>(dwFileSize));
	if (pRawData.IsValid() == false) {
		dwLastErr = pRawData.Error().Code(); File.close();
		return dwLastErr;
	}
#endif
	File.seekg(0, std::ios::beg);
	File.read (ReCa<PCHAR>(pRawData.GetData()), dwFileSize);
	File.close();

	Module.hTargetProcess = hTargetProc;

	Module.pRawData = pRawData.GetData();
	Module.pDosHeader = ReCa<PIMAGE_DOS_HEADER>(Module.pRawData);
	if (NULL == Module.pDosHeader) {
		return (dwLastErr = ERROR_INVALID_DATA);
	}
	Module.pNtHeaders = ReCa<PIMAGE_NT_HEADERS>(Module.pRawData + Module.pDosHeader->e_lfanew);
	Module.pOptionalHeader = &Module.pNtHeaders->OptionalHeader;
	Module.pFileHeader     = &Module.pNtHeaders->FileHeader;
	Module.dwImageSize = Module.pOptionalHeader->SizeOfImage;

	Module.dwFlags = dwFlags;

	DWORD ret = Module.AllocateMemory(dwLastErr);
	if (  ret != INJ_ERR_SUCCESS ) {
#if (0)
		delete[] pRawData;
#endif
		return ret;
	}

	ret = Module.CopyData(dwLastErr);
	if (ret != INJ_ERR_SUCCESS)
	{
		::VirtualFree(Module.pImageBase, 0, MEM_RELEASE);
		::VirtualFreeEx(Module.hTargetProcess, Module.pAllocationBase, 0, MEM_RELEASE);
#if (0)
		delete[] pRawData;
#endif
		return ret;
	}

	ret = Module.RelocateImage(dwLastErr);
	if(ret != INJ_ERR_SUCCESS)
	{
		::VirtualFree(Module.pImageBase, 0, MEM_RELEASE);
		::VirtualFreeEx(Module.hTargetProcess, Module.pAllocationBase, 0, MEM_RELEASE);
#if (0)
		delete[] pRawData;
#endif
		return ret;
	}

	ret = Module.CopyImage(dwLastErr);

	::VirtualFree(Module.pImageBase, 0, MEM_RELEASE);
#if(0)
	delete[] pRawData;
#endif
	if(ret != INJ_ERR_SUCCESS)
	{
		::VirtualFreeEx(Module.hTargetProcess, Module.pAllocationBase, 0, MEM_RELEASE);
		return ret;
	}

	ULONG_PTR remote_ret = 0;
	ret = StartRoutine(
		hTargetProc, ReCa<f_Routine*>(Module.pShellcode), Module.pManualMappingData, eMethod,
		(dwFlags & INJ_THREAD_CREATE_CLOAKED) != 0, dwLastErr, remote_ret
	);
	hOut = ReCa<HINSTANCE>(remote_ret);

	if (eMethod != LM_QueueUserAPC)
	{
		auto zero_size = Module.dwAllocationSize - (Module.pManualMappingData - Module.pAllocationBase);
		CRawData zero_bytes(static_cast<DWORD>(zero_size));
#if(0)
		PBYTE zero_bytes = new BYTE[zero_size];
		::memset(zero_bytes, 0, zero_size);
#endif
		::WriteProcessMemory(hTargetProc, Module.pManualMappingData, zero_bytes.GetData(), zero_size, nullptr);
#if(0)
		delete[] zero_bytes;
#endif
	}

	if (dwFlags & INJ_FAKE_HEADER)
	{
		PVOID pK32 = ReCa<PVOID>(GetModuleHandleA("kernel32.dll"));
		if (  pK32 )
		{
			::WriteProcessMemory(hTargetProc, Module.pTargetImageBase, pK32, 0x1000, nullptr);
		}
	}
	else if (dwFlags & INJ_ERASE_HEADER)
	{
		BYTE zero_bytes[0x1000]{ 0 };
		::WriteProcessMemory(hTargetProc, Module.pTargetImageBase, zero_bytes, 0x1000, nullptr);
	}

	return ret;
}

#include "shared.nt.map.bin.h"
/////////////////////////////////////////////////////////////////////////////

#ifdef _WIN64

DWORD MANUAL_MAPPER_WOW64::AllocateMemory(DWORD& dwError)
{
	pImageBase = (PBYTE)::VirtualAlloc(nullptr, dwImageSize, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	if (NULL == pImageBase)
	{
		dwError = ::GetLastError();
		return INJ_ERR_OUT_OF_MEMORY_INT;
	}

	if (dwFlags & INJ_SHIFT_MODULE)
	{
		::srand(::GetTickCount64() & 0xFFFFFFFF);
		dwShiftOffset = ALIGN_UP(rand() % 0x1000 + 0x100, 0x10);
	}

	ULONG_PTR pShellcodeSize = sizeof(ManualMap_Shell_WOW64);

	dwAllocationSize = dwShiftOffset + dwImageSize + static_cast<DWORD>(sizeof(TManMapDataW64) + pShellcodeSize);

	if (dwFlags & INJ_SHIFT_MODULE)
	{
		pAllocationBase = (PBYTE)::VirtualAllocEx(
			hTargetProcess, nullptr, dwAllocationSize, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE
		);
		if (NULL == pAllocationBase)
		{
			dwError = ::GetLastError();
			::VirtualFree(pImageBase, 0, MEM_RELEASE);

			return INJ_ERR_OUT_OF_MEMORY_EXT;
		}
	}
	else
	{
		// a conversion from double to void pointer needs to be re-viewed;
#pragma warning(disable:4312)
		pAllocationBase = (PBYTE)::VirtualAllocEx(
			hTargetProcess, (PVOID)pOptionalHeader->ImageBase, dwAllocationSize, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE
		);
#pragma warning(default:4312)
		if (NULL == pAllocationBase)
		{
			pAllocationBase = (PBYTE)::VirtualAllocEx(
				hTargetProcess, nullptr, dwAllocationSize, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE
			);
			if (NULL == pAllocationBase || (UINT_PTR)pAllocationBase > 0x7FFFFFFF)
			{
				dwError = ::GetLastError();
				::VirtualFree(pImageBase, 0, MEM_RELEASE);

				return INJ_ERR_OUT_OF_MEMORY_EXT;
			}
		}
	}

	pTargetImageBase   = pAllocationBase    + dwShiftOffset;
	pManualMappingData = pTargetImageBase   + dwImageSize  ;
	pShellcode         = pManualMappingData + sizeof(TManMapDataW64);

	return INJ_ERR_SUCCESS;
}

DWORD MANUAL_MAPPER_WOW64::CopyData(DWORD& dwError)
{
	::memcpy(pImageBase, pRawData, pOptionalHeader->SizeOfHeaders);

	auto* pCurrentSectionHeader = IMAGE_FIRST_SECTION(pNtHeaders);
	for (UINT i = 0; pCurrentSectionHeader && i != pFileHeader->NumberOfSections; ++i, ++pCurrentSectionHeader)
	{
		if (pCurrentSectionHeader->SizeOfRawData) {
			::memcpy(
				pImageBase + pCurrentSectionHeader->VirtualAddress  ,
				pRawData   + pCurrentSectionHeader->PointerToRawData, pCurrentSectionHeader->SizeOfRawData
			);
		}
	}

	if (dwFlags & INJ_SHIFT_MODULE)
	{
#if (0)
		DWORD* pJunk = new DWORD[dwShiftOffset / sizeof(DWORD)];
#else
		CRawData raw_data(dwShiftOffset * sizeof(DWORD));
		DWORD* pJunk = ReCa<PDWORD>(raw_data.GetData());
#endif
		DWORD dwSuperJunk = ::GetTickCount64() & 0xFFFFFFFF;

		for (UINT i = 0; i < dwShiftOffset / sizeof(DWORD) - 1; ++i)
		{
			pJunk[i]     = dwSuperJunk;
			dwSuperJunk ^= (i << (i % 32));
			dwSuperJunk -= 0x11111111;
		}

		::WriteProcessMemory(hTargetProcess, pAllocationBase, pJunk, dwShiftOffset, nullptr);
#if (0)
		delete[] pJunk;
#endif
	}
	auto pLoadFunctionPointer_WOW64 = [=](HINSTANCE hLib, PCCH szFunc, PVOID& pOut)
	{
#if (0)
		if (!GetProcAddressEx_WOW64(hTargetProcess, hLib, szFunc, pOut)) {
			return false;
		}
#endif
		return CObject_WOW64(hTargetProcess).ProcAddress(hLib, szFunc, pOut);
	};

	HINSTANCE hK32 = CObject_WOW64(hTargetProcess).ModuleHandle(_T("kernel32.dll"));

	PVOID pLoadLibraryA = nullptr;
	if (!pLoadFunctionPointer_WOW64(hK32, "LoadLibraryA", pLoadLibraryA))
		return INJ_ERR_REMOTEFUNC_MISSING;

	PVOID pGetProcAddress = nullptr;
	if (!pLoadFunctionPointer_WOW64(hK32, "GetProcAddress", pGetProcAddress))
		return INJ_ERR_REMOTEFUNC_MISSING;

	MANUAL_MAPPING_DATA_WOW64 data{ 0 };
	data.pLoadLibraryA   = (DWORD)(ULONG_PTR)pLoadLibraryA;
	data.pGetProcAddress = (DWORD)(ULONG_PTR)pGetProcAddress;
	data.pModuleBase     = (DWORD)(ULONG_PTR)pTargetImageBase;
	data.dwFlags = dwFlags;

	if (!::WriteProcessMemory(hTargetProcess, pManualMappingData, &data, sizeof(data), nullptr)) {
		dwError = ::GetLastError();
		return INJ_ERR_WPM_FAIL;
	}

	DWORD dwShellcodeSize = sizeof(ManualMap_Shell_WOW64);
	if (!::WriteProcessMemory(hTargetProcess, pShellcode, ManualMap_Shell_WOW64, dwShellcodeSize, nullptr)) {
		dwError = ::GetLastError();
		return INJ_ERR_WPM_FAIL;
	}

	return INJ_ERR_SUCCESS;
}

DWORD MANUAL_MAPPER_WOW64::CopyImage(DWORD& dwError)
{
	if (!::WriteProcessMemory(hTargetProcess, pTargetImageBase, pImageBase, dwImageSize, nullptr)) {
		dwError = ::GetLastError();
		return INJ_ERR_WPM_FAIL;
	}

	return (dwError = INJ_ERR_SUCCESS);
}

DWORD MANUAL_MAPPER_WOW64::RelocateImage(DWORD& dwError)
{
	PBYTE pLocationDelta = pTargetImageBase - pOptionalHeader->ImageBase;
	if ( !pLocationDelta )
		return (dwError = INJ_ERR_SUCCESS);

	if (!pOptionalHeader->DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].Size)
		return (dwError = INJ_ERR_IMAGE_CANT_RELOC);

	auto*  pRelocData = ReCa<PIMAGE_BASE_RELOCATION>(
			pImageBase + pOptionalHeader->DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress
		);
	while (pRelocData && pRelocData->VirtualAddress)
	{
		PWORD pRelativeInfo = ReCa<PWORD>(pRelocData + 1);
		for (UINT i = 0; pRelativeInfo &&
			i < ((pRelocData->SizeOfBlock - sizeof(IMAGE_BASE_RELOCATION)) / sizeof(WORD)); ++i, ++pRelativeInfo
		)
		{
			if (RELOC_FLAG86(*pRelativeInfo)) {
				PULONG_PTR pPatch = ReCa<PULONG_PTR>(pImageBase + pRelocData->VirtualAddress + ((*pRelativeInfo) & 0xFFF));
				if (NULL!= pPatch)
				*pPatch += ReCa<ULONG_PTR>(pLocationDelta) & 0xFFFFFFFF;
			}
		}
		pRelocData = ReCa<PIMAGE_BASE_RELOCATION>(ReCa<PBYTE>(pRelocData) + pRelocData->SizeOfBlock);
	}

	return (dwError = INJ_ERR_SUCCESS);
}

/////////////////////////////////////////////////////////////////////////////

DWORD _ManualMap_WOW64(PCWCHAR szDllFile, HANDLE hTargetProc, eLaunch eMethod, DWORD dwFlags, HINSTANCE& hOut, DWORD& dwLastErr)
{
	TManMapW64 Module{ 0 };
#if (0)
	PBYTE pRawData{ nullptr };
#endif
	std::ifstream File(szDllFile, std::ios::binary | std::ios::ate);

	auto FileSize = File.tellg();
#if (0)
	pRawData = new BYTE[static_cast<size_t>(FileSize)];
#else
	CRawData pRawData(static_cast<DWORD>(FileSize));
#endif
	if (!pRawData.IsValid()) {
		File.close();
		return INJ_ERR_OUT_OF_MEMORY_NEW;
	}

	File.seekg(0, std::ios::beg);
	File.read(ReCa<PCHAR>(pRawData.GetData()), FileSize);
	File.close();

	Module.hTargetProcess = hTargetProc;

	Module.pRawData = pRawData.GetData();
	Module.pDosHeader = ReCa<PIMAGE_DOS_HEADER>(Module.pRawData);
	Module.pNtHeaders = ReCa<PIMAGE_NT_HEADERS32>(Module.pRawData + Module.pDosHeader->e_lfanew);
	Module.pOptionalHeader = &Module.pNtHeaders->OptionalHeader;
	Module.pFileHeader = &Module.pNtHeaders->FileHeader;
	Module.dwImageSize = Module.pOptionalHeader->SizeOfImage;

	Module.dwFlags = dwFlags;

	DWORD ret = Module.AllocateMemory(dwLastErr);
	if (ret != INJ_ERR_SUCCESS)
	{
#if (0)
		delete[] pRawData;
#endif
		return ret;
	}

	ret = Module.CopyData(dwLastErr);
	if (ret != INJ_ERR_SUCCESS)
	{
		::VirtualFree  (Module.pImageBase, 0, MEM_RELEASE);
		::VirtualFreeEx(Module.hTargetProcess, Module.pAllocationBase, 0, MEM_RELEASE);
#if (0)
		delete[] pRawData;
#endif
		return ret;
	}

	ret = Module.RelocateImage(dwLastErr);
	if(ret != INJ_ERR_SUCCESS)
	{
		::VirtualFree  (Module.pImageBase, 0, MEM_RELEASE);
		::VirtualFreeEx(Module.hTargetProcess, Module.pAllocationBase, 0, MEM_RELEASE);
#if (0)
		delete[] pRawData;
#endif
		return ret;
	}

	Module.pRawData = nullptr;
#if (0)
	delete[] pRawData;
#endif
	ret = Module.CopyImage(dwLastErr);
	::VirtualFree(Module.pImageBase, 0, MEM_RELEASE);
	if (ret != INJ_ERR_SUCCESS)
	{
		::VirtualFreeEx(Module.hTargetProcess, Module.pAllocationBase, 0, MEM_RELEASE);
		return ret;
	}
#pragma warning(disable:4311 4302)
	DWORD remote_ret = 0;
	ret = StartRoutine_WOW64(hTargetProc,
		ReCa<f_Routine_WOW64>(Module.pShellcode),
		(DWORD)Module.pManualMappingData, eMethod, (dwFlags & INJ_THREAD_CREATE_CLOAKED) != 0, dwLastErr, remote_ret
	);
	hOut = (HINSTANCE)(ULONG_PTR)remote_ret;
#pragma warning(default:4311 4302)
	if (eMethod != LM_QueueUserAPC)
	{
		auto zero_size = Module.dwAllocationSize - (Module.pManualMappingData - Module.pAllocationBase);
#if (0)
		PBYTE zero_bytes = new BYTE[zero_size];
		::memset(zero_bytes, 0, zero_size);
#else
		CRawData zero_bytes(static_cast<DWORD>(zero_size));
#endif
		::WriteProcessMemory(hTargetProc, Module.pManualMappingData, zero_bytes.GetData(), zero_size, nullptr);
#if (0)
		delete[] zero_bytes;
#endif
	}

	if (dwFlags & INJ_FAKE_HEADER)
	{
		PVOID pK32 = ReCa<PVOID>(CObject_WOW64(hTargetProc).ModuleHandle(_T("kernel32.dll")));
		if (  pK32 )
		{
			::WriteProcessMemory(hTargetProc, Module.pTargetImageBase, pK32, 0x1000, nullptr);
		}
	}
	else if (dwFlags & INJ_ERASE_HEADER)
	{
		BYTE zero_bytes[0x1000]{ 0 };
		::WriteProcessMemory(hTargetProc, Module.pTargetImageBase, zero_bytes, 0x1000, nullptr);
	}

	return ret;
}

#endif