#ifndef _SHAREDNTMAPMAN_H_D6C0149E_6C1A_43D6_8396_3DFA5BD1E4D7_INCLUDED
#define _SHAREDNTMAPMAN_H_D6C0149E_6C1A_43D6_8396_3DFA5BD1E4D7_INCLUDED
/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is image header manual mapping interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 15-Mar-2020 at 4:34:16p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include <WinNT.h>
#include "shared.proc.obj.h"
#include "shared.inj.base.h"
#include "shared.inj.start.h"
namespace shared { namespace process {
	using shared::inject::launch::eLaunch;

	using f_LoadLibraryA    = decltype(::LoadLibraryA);
	using f_GetProcAddress  = ULONG_PTR(WINAPI*)(HINSTANCE hModule, LPCCH lpProcName);
	using f_DLL_ENTRY_POINT = BOOL(WINAPI*)(PVOID hDll, DWORD dwReason ,PVOID pReserved);

	struct  MANUAL_MAPPING_DATA
	{
		HINSTANCE               hResult              ;
		f_LoadLibraryA*         pLoadLibraryA        ;
		f_GetProcAddress        pGetProcAddress      ;
		PBYTE                   pModuleBase          ;
		DWORD                   dwFlags              ;
	};
	typedef MANUAL_MAPPING_DATA TManMapData;

	struct  MANUAL_MAPPER {
		PBYTE                    pRawData            ;
		PBYTE                    pImageBase          ;
		PIMAGE_DOS_HEADER        pDosHeader          ;
		PIMAGE_NT_HEADERS        pNtHeaders          ;
		PIMAGE_OPTIONAL_HEADER   pOptionalHeader     ;
		PIMAGE_FILE_HEADER       pFileHeader         ;
		DWORD                    dwImageSize         ;
		DWORD                    dwAllocationSize    ;
		DWORD                    dwShiftOffset       ;
		PBYTE                    pAllocationBase     ;
		PBYTE                    pTargetImageBase    ;
		PBYTE                    pShellcode          ;
		PBYTE                    pManualMappingData  ;
		DWORD                    dwShellSize         ;
		HANDLE                   hTargetProcess      ;
		DWORD                    dwFlags             ;
	public: // just for distinguishing;
		DWORD                    AllocateMemory(DWORD& dwError);
		DWORD                    CopyData      (DWORD& dwError);
		DWORD                    CopyImage     (DWORD& dwError);
		DWORD                    RelocateImage (DWORD& dwError);
	};
	typedef MANUAL_MAPPER TManMap;

#ifdef _WIN64
	struct  MANUAL_MAPPING_DATA_WOW64
	{
		DWORD                   hResult              ;
		DWORD                   pLoadLibraryA        ;
		DWORD                   pGetProcAddress      ;
		DWORD                   pModuleBase          ;
		DWORD                   dwFlags              ;
	};
	typedef MANUAL_MAPPING_DATA_WOW64 TManMapDataW64 ;

	struct  MANUAL_MAPPER_WOW64 {
		PBYTE                    pRawData            ;
		PBYTE                    pImageBase          ;
		PIMAGE_DOS_HEADER        pDosHeader          ;
		PIMAGE_NT_HEADERS32      pNtHeaders          ;
		PIMAGE_OPTIONAL_HEADER32 pOptionalHeader     ;
		PIMAGE_FILE_HEADER       pFileHeader         ;
		DWORD                    dwImageSize         ;
		DWORD                    dwAllocationSize    ;
		DWORD                    dwShiftOffset       ;
		PBYTE                    pAllocationBase     ;
		PBYTE                    pTargetImageBase    ;
		PBYTE                    pShellcode          ;
		PBYTE                    pManualMappingData  ;
		DWORD                    dwShellSize         ;
		HANDLE                   hTargetProcess      ;
		DWORD                    dwFlags             ;
	public: // just for distinguishing;
		DWORD                    AllocateMemory(DWORD& dwError);
		DWORD                    CopyData      (DWORD& dwError);
		DWORD                    CopyImage     (DWORD& dwError);
		DWORD                    RelocateImage (DWORD& dwError);
	};
	typedef MANUAL_MAPPER_WOW64  TManMapW64;
#endif

	DWORD _ManualMap(PCWCHAR szDllFile, HANDLE hTargetProc, eLaunch eMethod, DWORD dwFlags, HINSTANCE& hOut, DWORD& dwLastErr);
#ifdef _WIN64
	DWORD _ManualMap_WOW64(PCWCHAR szDllFile, HANDLE hTargetProc, eLaunch eMethod, DWORD dwFlags, HINSTANCE& hOut, DWORD& dwLastErr);
#endif
}}
#endif/*_SHAREDNTMAPMAN_H_D6C0149E_6C1A_43D6_8396_3DFA5BD1E4D7_INCLUDED*/