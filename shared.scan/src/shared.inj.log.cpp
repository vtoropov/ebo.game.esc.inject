/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is inject event log implementation file (https://github.com/guided-hacking/GuidedHacking-Injector);
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 26-Mar-2020 at 5:15:38p, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "StdAfx.h"
#include "shared.inj.log.h"

using namespace shared::log::inject;

#include "shared.gen.sys.time.h"

using namespace shared::com_ex;

/////////////////////////////////////////////////////////////////////////////

CLog:: CLog(const DWORD _opts) : TBase(_opts), m_file(NULL) {
	m_error << __MODULE__ << S_OK;

	CAtlString cs_path = TBase::Path();
	if (TBase::m_error)
		return;

	const errno_t err_ = _tfopen_s(
		&m_file, (LPCTSTR)cs_path, (COption().IsOverwrite(m_opts) ? _T("w") : _T("a"))
	);
	if (err_ || NULL == m_file)
		m_error = E_ACCESSDENIED;
}
CLog::~CLog(void) {
	if (NULL != m_file) {
		fclose (m_file); m_file = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

VOID         CLog::Write (const CErrorInfo& _err) { _err;
	
	static LPCTSTR lp_sz_pat = _T(
		"[#error] %s    \r\n"
		"dll   :\t%s    \r\n"
		"exe   :\t%s    \r\n"
		"pid   :\t%d    \r\n"
		"inject:\t%d    \r\n"
		"launch:\t%d    \r\n"
		"flags :\t0x%08x\r\n"
		"code  :\t0x%08x\r\n"
		"win32 :\t0x%08x\r\n"
		"handle:\t0x%08x\r\n"
	);

	if (NULL == m_file) {
		m_error = OLE_E_BLANK; return;
	}

	_ftprintf(
		m_file, lp_sz_pat,
		(LPCTSTR)CDateTime(_err.Timestamp()).GetTimeAsUnix()   ,
		(LPCTSTR)(_err.DllPath() ? _err.DllPath() : _T("#n/a")),
		(LPCTSTR)(_err.ExePath() ? _err.ExePath() : _T("#n/a")),
		_err.ProcessId (),
		_err.InjectMode(),
		_err.LaunchMode(),
		_err.Flags  ()   ,
		_err.ErrCode()   ,
		_err.Win32Code() ,
		_err.Handle ()
		);

	::fflush(m_file);
}

/////////////////////////////////////////////////////////////////////////////

CLog&  CLog::operator << (const CErrorInfo& _err) { this->Write(_err); return *this; }
