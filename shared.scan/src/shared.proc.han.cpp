/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is base inject handle interface implementation file;
	(https://github.com/guided-hacking/GuidedHacking-Injector)
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 6-Mar-2020 at 5:30:19a, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "StdAfx.h"
#include "shared.proc.han.h"

using namespace shared::process;

#include "shared.gen.raw.buf.h"
using namespace shared::common;
/////////////////////////////////////////////////////////////////////////////

CHandle_Data:: CHandle_Data(void) {}
CHandle_Data:: CHandle_Data(const CHandle_Data& _data) : CHandle_Data() { *this = _data; }
CHandle_Data::~CHandle_Data(void) {}

/////////////////////////////////////////////////////////////////////////////

const DWORD&   CHandle_Data::Access  (void) const { return m_access; }
      DWORD&   CHandle_Data::Access  (void)       { return m_access; }
const DWORD&   CHandle_Data::OwnerPID(void) const { return m_owner_pid; }
      DWORD&   CHandle_Data::OwnerPID(void)       { return m_owner_pid; }
const  WORD&   CHandle_Data::Value   (void) const { return m_value ; }
       WORD&   CHandle_Data::Value   (void)       { return m_value ; }

/////////////////////////////////////////////////////////////////////////////

CHandle_Data& CHandle_Data::operator= (const CHandle_Data& _data) {

	this->Access  () = _data.Access  ();
	this->OwnerPID() = _data.OwnerPID();
	this->Value   () = _data.Value   ();

	return *this;
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace inject { namespace _impl {

	class CHandle_Enum_Help {
	private:
		HMODULE   m_module;
		CError    m_error;

	public:
		 CHandle_Enum_Help(void) : m_module(NULL) { m_error << __MODULE__ << S_OK >> __MODULE__;
				m_module = ::GetModuleHandle(_T("ntdll.dll"));
				if (NULL == m_module)
					m_error = __LastErrToHresult();
			}
		~CHandle_Enum_Help(void) {}

	public:
		TErrorRef   Error  (void) const { return m_error; }

		HRESULT     Handles(TSysTableEntries& _entries) {
			if (NULL == m_module)
				return  m_error;

			m_error << __MODULE__ << S_OK;

			if (_entries.empty() == false)
				_entries.clear();

			static LPCSTR lp_proc = "NtQuerySystemInformation";

			auto p_NtQuerySysInfo = reinterpret_cast<fn_NtQuerySysInfo>(::GetProcAddress(m_module, lp_proc));
			if (!p_NtQuerySysInfo)
				return (m_error = E_NOINTERFACE);

			HRESULT  hr_ = m_error;
			NTSTATUS nt_status = STATUS_INFO_LENGTH_MISMATCH;

			DWORD dw_size = 0x2000;

			CRawData raw_info;
			while (nt_status == STATUS_INFO_LENGTH_MISMATCH) {

				hr_ = raw_info.Create(dw_size);
				if (FAILED(hr_))
					return (m_error = raw_info.Error());

				nt_status = p_NtQuerySysInfo(
					CSysInfoClass::SystemHandleInformation, (PBYTE)raw_info, dw_size, &dw_size
				);
			}
			if (NT_FAIL(nt_status))
				return (m_error = __LastErrToHresult());

			const auto* p_sys_info = reinterpret_cast<TSysHandleInfoPtr>((PBYTE)raw_info);
			if (NULL == p_sys_info)
				return (m_error = E_UNEXPECTED);

			for (ULONG i_ = 0; i_ < p_sys_info->NumberOfHandles; i_++) {
				if (p_sys_info->Handles[i_].ObjectTypeIndex != CObjectType::OTI_Process)
					continue;
				try {
					_entries.push_back(p_sys_info->Handles[i_]);
				}
				catch(const ::std::bad_alloc&) {
					m_error = E_OUTOFMEMORY; break;
				}
			}
			return m_error;
		}
	};
}}}
using namespace shared::inject::_impl;
/////////////////////////////////////////////////////////////////////////////

CHandle_Enum:: CHandle_Enum(void) { m_j_error << __MODULE__ << S_OK >> __MODULE__; }
CHandle_Enum::~CHandle_Enum(void) {}

/////////////////////////////////////////////////////////////////////////////
const
THandleData&  CHandle_Enum::Data  (void) const { return m_h_data; }
TJErrorRef    CHandle_Enum::Error (void) const { return m_j_error; }
HRESULT       CHandle_Enum::Find  (const DWORD TargetPID, const DWORD WantedHandleAccess) {
	m_j_error << __MODULE__ << S_OK; WantedHandleAccess;
	if (0 == TargetPID)
		return ((CError&)m_j_error = __DwordToHresult(ERROR_INVALID_HANDLE));

	TSysTableEntries procs;
	CHandle_Enum_Help help_;

	HRESULT hr_ = help_.Handles(procs);
	if (FAILED(hr_))
		m_j_error = help_.Error();

	if (m_h_data.empty() == false)
		m_h_data.clear();

	DWORD dw_owner_pid  = 0;
	HANDLE h_owner_proc = nullptr;

	for (size_t i_ = 0; i_ < procs.size(); i_++) {
		const TSysHandleTable& table_ = procs[i_];

		if (table_.UniqueProcessId != dw_owner_pid
		 && table_.UniqueProcessId != TargetPID
		 && table_.UniqueProcessId != ::GetCurrentProcessId()) {

			if (h_owner_proc) {
				::CloseHandle(h_owner_proc); h_owner_proc = nullptr;
			}
			dw_owner_pid = table_.UniqueProcessId;
			h_owner_proc = ::OpenProcess(PROCESS_DUP_HANDLE, FALSE, dw_owner_pid);
			if (NULL == h_owner_proc){
				continue;
			}
		} else if (NULL == h_owner_proc)
			continue;

		HANDLE h_duplicate = nullptr;
		HANDLE h_original  = reinterpret_cast<HANDLE>(table_.HandleValue);
		NTSTATUS nt_status = ::DuplicateHandle(
			h_owner_proc, h_original, ::GetCurrentProcess(), &h_duplicate, PROCESS_QUERY_LIMITED_INFORMATION, 0, 0
		);
		if (NT_FAIL(nt_status))
			continue;

		if (::GetProcessId(h_duplicate) == TargetPID
		 &&(table_.GrantedAccess - (table_.GrantedAccess ^ WantedHandleAccess) == WantedHandleAccess )) {
			try {
				CHandle_Data h_data;
				h_data.OwnerPID() = dw_owner_pid;
				h_data.Access  () = table_.GrantedAccess;
				h_data.Value   () = table_.HandleValue  ;

				m_h_data.push_back(h_data);

			}
			catch (const ::std::bad_alloc&) {
				(CError&)m_j_error = E_OUTOFMEMORY; break;
			}
		}
		::CloseHandle(h_duplicate); h_duplicate = nullptr;
	}

	return m_j_error;
}

/////////////////////////////////////////////////////////////////////////////