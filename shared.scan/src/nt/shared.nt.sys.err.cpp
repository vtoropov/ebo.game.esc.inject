/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Feb-2020 at 1:42:50p, UTC+7, Novosibirsk, Tulenina, Friday;
	This is Ebo Game Dyna Inject project system error extended wrapper interface implementation file.
*/
#include "StdAfx.h"
#include "shared.nt.sys.err.h"

using namespace shared::process;

#include "shared.inj.error.h"

using namespace shared::inject;

/////////////////////////////////////////////////////////////////////////////

CError_Inj:: CError_Inj(void) : TBase() { TBase() << __MODULE__ << S_OK; }
CError_Inj::~CError_Inj(void) {}

/////////////////////////////////////////////////////////////////////////////

VOID   CError_Inj::Code  (const DWORD _err_code) {
	_err_code;
	switch (_err_code) {
	case INJ_ERR_SUCCESS                : { TBase() = S_OK; } break;
	case INJ_ERR_INVALID_PROC_HANDLE    : { TBase() = __LastErrToHresult(); } break;
	case INJ_ERR_FILE_DOESNT_EXIST      : { TBase() = __LastErrToHresult(); } break;
	case INJ_ERR_OUT_OF_MEMORY_EXT      : { TBase() = __LastErrToHresult(); } break;
	case INJ_ERR_OUT_OF_MEMORY_INT      : { TBase() = __LastErrToHresult(); } break;
	case INJ_ERR_IMAGE_CANT_RELOC       : { TBase().Code(_err_code); TBase() = _T("Base relocation directory empty;"); } break;
	case INJ_ERR_LDRLOADDLL_MISSING     : { TBase().Code(_err_code); TBase() = _T("Pointer to LdrLoadDll cannot be found;"); } break;
	case INJ_ERR_REMOTEFUNC_MISSING     : { TBase().Code(_err_code); TBase() = _T("Remote function cannot be found;"); } break;
	case INJ_ERR_CANT_FIND_MOD_PEB      : { TBase().Code(_err_code); TBase() = _T("Module is not linked to PEB;"); } break;
	case INJ_ERR_WPM_FAIL               : { TBase() = __LastErrToHresult(); } break;
	case INJ_ERR_CANT_ACCESS_PEB        : { TBase() = __LastErrToHresult(); } break;
	case INJ_ERR_CANT_ACCESS_PEB_LDR    : { TBase() = __LastErrToHresult(); } break;
	case INJ_ERR_VPE_FAIL               : { TBase() = __LastErrToHresult(); } break;
	case INJ_ERR_CANT_ALLOC_MEM         : { TBase() = __LastErrToHresult(); } break;
	case INJ_ERR_CT32S_FAIL             : { TBase().Code(_err_code); TBase() = _T("Creating process snapshot is failed;"); } break;
	case INJ_ERR_RPM_FAIL               : { TBase() = __LastErrToHresult(); } break;  
	case INJ_ERR_INVALID_PID            : { TBase().Code(_err_code); TBase() = _T("Process identifier cannot be invalid;"); } break;
	case INJ_ERR_INVALID_FILEPATH       : { TBase().Code(_err_code); TBase() = _T("Target executable path is invalid;");} break;
	case INJ_ERR_CANT_OPEN_PROCESS      : { TBase() = __LastErrToHresult(); } break;
	case INJ_ERR_PLATFORM_MISMATCH      : { TBase().Code(_err_code); TBase() = _T("Target file format cannot be evaluated;"); } break;
	case INJ_ERR_NO_HANDLES             : { TBase().Code(_err_code); TBase() = _T("No process handle for injection;"); } break;
	case INJ_ERR_HIJACK_NO_NATIVE_HANDLE: { TBase().Code(_err_code); TBase() = _T("Process handle is invalid; Injection is not available;"); } break;
	case INJ_ERR_HIJACK_INJ_FAILED      : { TBase().Code(_err_code); TBase() = _T("Cannot made an injection to target process;"); } break;
	case INJ_ERR_HIJACK_CANT_ALLOC      : { TBase() = __LastErrToHresult(); } break;
	case INJ_ERR_HIJACK_CANT_WPM        : { TBase() = __LastErrToHresult(); } break;
	case INJ_ERR_HIJACK_INJMOD_MISSING  : { TBase().Code(_err_code); TBase() = _T("Cannot finde a target injection module;"); } break;
	case INJ_ERR_HIJACK_INJECTW_MISSING : { TBase().Code(_err_code); TBase() = _T("Cannot finde a target injection function;"); } break;
	case INJ_ERR_GET_MODULE_HANDLE_FAIL : { TBase() = __LastErrToHresult(); } break;
	case INJ_ERR_OUT_OF_MEMORY_NEW      : { TBase().Code(_err_code); TBase() = _T("Memory allocation is unpredictably failed;"); } break;
	case INJ_ERR_REMOTE_CODE_FAILED     : { TBase().Code(_err_code); TBase() = _T("Remote code is not able to load target module;"); } break;
	}
}

/////////////////////////////////////////////////////////////////////////////

CError_Inj& CError_Inj::operator = (const CError& _err) { (CError&)*this = _err; return *this; }