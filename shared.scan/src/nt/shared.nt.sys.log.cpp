/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Mar-2020 at 0:41:23a, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Ebo Game Dyna Inject project error log interface implementation file.
*/
#include "StdAfx.h"
#include "shared.nt.sys.log.h"

using namespace shared::log;

#include "shared.gen.syn.obj.h"

using namespace shared::sys_core;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace log { namespace _impl {

	CSyncObject& EventLog_SafeGuard(void)
	{
		static CSyncObject cs_guard;
		return cs_guard;
	}

}}}
using namespace shared::log::_impl;
/////////////////////////////////////////////////////////////////////////////

#define EVT_LOG_SAFE_LOCK() SAFE_LOCK(EventLog_SafeGuard());

CLocator:: CLocator(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CLocator::~CLocator(void) {}

/////////////////////////////////////////////////////////////////////////////

CAtlString  CLocator::File  (void) const {
	m_error << __MODULE__ << S_OK;

	static const INT nSize = _MAX_PATH + _MAX_DRIVE + _MAX_DIR;

	TCHAR path__[nSize] = {0};

	const BOOL ret__ = ::GetModuleFileName(NULL, path__, _countof(path__));
	if (FALSE == ret__) {
		m_error = __LastErrToHresult();
		return CAtlString();
	}

	TCHAR file_part__[_MAX_FNAME] = {0};
	_tsplitpath_s(path__, NULL, 0, NULL, 0, file_part__, _MAX_FNAME, NULL, 0);

	SYSTEMTIME st = {0};
	::GetLocalTime(&st);

	::ATL::CAtlString file__;
	file__.Format(_T("%s_%04d_%02d_%02d_%02d_%02d_%02d_%04d.log"), 
		file_part__, 
		st.wYear   ,
		st.wMonth  ,
		st.wDay    ,
		st.wHour   ,
		st.wMinute ,
		st.wSecond ,
		st.wMilliseconds
	);
	return file__;
}

CAtlString  CLocator::Folder(void) const {
	m_error << __MODULE__ << S_OK;

	static const INT nSize = _MAX_PATH + _MAX_DRIVE + _MAX_DIR;

	TCHAR path__[nSize] = {0};
	const BOOL ret__ = ::GetModuleFileName(NULL, path__, _countof(path__));
	if (FALSE == ret__) {
		m_error = __LastErrToHresult();
		return CAtlString();
	}

	::ATL::CAtlString folder__(path__);
	INT ptr__ = folder__.ReverseFind(_T('\\'));
	if (-1 == ptr__){
		m_error = E_UNEXPECTED;
		return CAtlString();
	}

	folder__ = folder__.Left(ptr__ + 1);
	folder__+= _T("logs\\");
	if (!::PathFileExists(folder__)) {
		if (NULL == ::CreateDirectory(folder__, NULL))
		{
			const DWORD dwError = ::GetLastError();
			if (ERROR_ALREADY_EXISTS != dwError) {
				m_error = __DwordToHresult(dwError);
			}
		}
	}
	return folder__;
}

TErrorRef   CLocator::Error (void) const { return m_error; }

/////////////////////////////////////////////////////////////////////////////

CLog_Base:: CLog_Base(const DWORD _opts) : m_opts(_opts) { m_error << __MODULE__ << S_OK >> __MODULE__;}
CLog_Base::~CLog_Base(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef  CLog_Base::Error (void) const { return m_error; }
CAtlString CLog_Base::Path  (void) const {
	m_error << __MODULE__ << S_OK;
	CLocator loc_;
	CAtlString cs_dir = loc_.Folder();
	if (loc_.Error()) {
		m_error = loc_.Error(); return CAtlString();
	}
	CAtlString cs_name = loc_.File();
	if (loc_.Error()) {
		m_error = loc_.Error(); return CAtlString();
	}
	CAtlString cs_path; cs_path += cs_dir; cs_path += cs_name;
	return cs_name;
}

/////////////////////////////////////////////////////////////////////////////

CDump:: CDump (const DWORD _opts) : TBase(_opts) {
	m_error << __MODULE__ << S_OK;

	CAtlString cs_path = TBase::Path();
	if (TBase::m_error)
		return;
	// https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-createfilea
	m_file = ::CreateFile(
		(LPCTSTR)cs_path, FILE_GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL
	);
	if (m_file.IsValid() == false)
		m_error = __LastErrToHresult();
}
CDump::~CDump (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CDump::Write (const _variant_t& _var) {

	switch (_var.vt) {
	case VARENUM::VT_BSTR: {
			if (NULL == _var.bstrVal)
				m_error = __DwordToHresult(ERROR_INVALID_DATA);
			else {
				CRawData raw_(_var);
				this->Write(raw_);
			}
		} break;
	default:
		m_error = DISP_E_BADVARTYPE;
	}
	return m_error;
}

HRESULT   CDump::Write (const CRawData& _raw) {

	if (_raw.IsValid() == false)
		return (m_error = _raw.Error());

	return this->Write(_raw.GetData(), _raw.GetSize());
}

HRESULT   CDump::Write (const PBYTE _data, const DWORD _sz) {
	m_error << __MODULE__ << S_OK;

	if (NULL == _data)
		return (m_error = E_POINTER);

	if (0 == _sz)
		return (m_error = __DwordToHresult(ERROR_EMPTY));

	if (m_file.IsValid() == false)
		return (m_error = OLE_E_BLANK);

	DWORD dwBytesWritten = 0;
	const BOOL b_result  = ::WriteFile(
		m_file, _data, _sz, &dwBytesWritten, NULL
	);
	if (b_result == FALSE)
		m_error = __LastErrToHresult();

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CDump&    CDump::operator << (const CRawData& _raw) { this->Write(_raw); return *this; }

/////////////////////////////////////////////////////////////////////////////

bool COption::IsOverwrite(const DWORD _ops) const { return (0!=(_opt::e_overwrite & _ops)); }