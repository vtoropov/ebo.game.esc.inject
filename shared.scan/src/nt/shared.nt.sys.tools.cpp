/*
	Original code is created by Broihon (https://guidedhacking.com/) on 17-Jul-2019;
	This is NT system tool/helper function interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 10-Mar-2020 at 9:16:12a, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "StdAfx.h"
#include "shared.nt.sys.tools.h"

using namespace shared::sys_core::tools;

#include "shared.proc.defs.h"
#include "shared.nt.sys.funs.h"

using namespace shared::process;
/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace sys_core { namespace tools {

bool   GetOwnModulePath(PWCHAR pOut, size_t nBufferCchSize)
{
	MODULEENTRY32W ME32{ 0 };
	ME32.dwSize = sizeof(ME32);

	HANDLE hSnap = ::CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, ::GetCurrentProcessId());

	BOOL   bRet  = ::Module32FirstW(hSnap, &ME32);
	while (bRet)
	{
		if (ME32.hModule == ::shared::__get_bas_addr_of_inj())
			break;
		bRet = ::Module32NextW(hSnap, &ME32);
	}

	::CloseHandle(hSnap);

	if (!bRet)
		return false;

	size_t size_out = 0;
	::StringCchLengthW(ME32.szExePath, sizeof(ME32.szExePath), &size_out);

	PWCHAR temp_path  = ME32.szExePath;
	       temp_path += size_out;

	while (*temp_path -- != '\\');
	      *(temp_path + 2)= '\0' ;

	::StringCchCopyW(pOut, nBufferCchSize, ME32.szExePath);

	return true;	
}

ULONG  GetSessionId(HANDLE hTargetProc, NTSTATUS & ntRetOut)
{	
	auto h_nt_dll = ::GetModuleHandleA("ntdll.dll");
	if (!h_nt_dll)
		return 0;

	static LPCSTR lp_sz_fun_name = "NtQueryInformationProcess";

	auto p_NtQueryInformationProcess = reinterpret_cast<fn_NtQueryProcInfo>(::GetProcAddress(h_nt_dll, lp_sz_fun_name));
	if (!p_NtQueryInformationProcess)
		return 0;

	PROCESS_SESSION_INFORMATION psi{ 0 };
	ntRetOut = p_NtQueryInformationProcess(hTargetProc, eProcInfoClass::ProcessBasicInfo, &psi, sizeof(psi), nullptr);
	if (NT_FAIL(ntRetOut))
		return (ULONG)-1;

	return psi.u_sess_id;
}

bool   IsFileExist(LPCTSTR szFile) {
	return (::GetFileAttributes(szFile) != INVALID_FILE_ATTRIBUTES);
}

bool   IsElevated(HANDLE hTargetProc)
{
	HANDLE hToken = nullptr;
	if (!::OpenProcessToken(hTargetProc, TOKEN_QUERY, &hToken))
		return false;

	TOKEN_ELEVATION te{ 0 };
	DWORD dwSizeOut = 0;
	// https://docs.microsoft.com/en-us/windows/win32/api/securitybaseapi/nf-securitybaseapi-gettokeninformation
	if (FALSE == ::GetTokenInformation(hToken, TokenElevation, &te, sizeof(te), &dwSizeOut))
		return false;

	::CloseHandle(hToken);
	
	return (te.TokenIsElevated != ERROR_SUCCESS);
}

}}}