/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Apr-2019 on 10:23:21p, UTC+7, Phuket, Rawai, Thursday;
	This is sound-bin-trans receiver desktop console application entry point file.
	-----------------------------------------------------------------------------
	Adopted to FakeGPS project on 13-Dec-2019 at 12:11:50p, UTC+7, Novosibirsk, Tulenina, Friday;
	Adopted to File Touch App project on 02-Feb-2020 at 7:54:18p, UTC+7, Novosibirsk, Tulenina, Sunday;
	Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 6:00:55a, UTC+7, Novosibirsk, Tulenina, Thursday; 
*/
#include "StdAfx.h"
#include "shared.gen.sys.com.h"
#include "shared.gen.sys.err.h"

using namespace shared::sys_core;

#include "shared.gen.cmd.arg.h"
#include "shared.gen.con.bas.h"

using namespace shared::common;
using namespace shared::common::ui;

#include "ebo.gam.esc.con.res.h"

#include "shared.gen.proc.h"

using namespace shared::process;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace game { namespace console {

	class CConsole_Rcv : public shared::common::ui::CConsole_Ex  {
	                    typedef shared::common::ui::CConsole_Ex  TBase;
	private:
		CError        m_error;

	public:
		CConsole_Rcv(void) {
			m_error << __MODULE__ << S_OK;
			CAtlString cs_title(_T("Ebo Game Dyna Inject Console"));
#if defined(WIN64)
			cs_title += _T(" [x64]");
#else
			cs_title += _T(" [x86]");
#endif
			TBase::OnCreate(cs_title.GetString());
			TBase::SetIcon (IDR_GAM_ESC_ICO);
		}
	private:
	};
}}}
using namespace ebo::game::console;
/////////////////////////////////////////////////////////////////////////////

INT _tmain(VOID) {
	INT n_res = 0;

	::SetConsoleOutputCP(CP_UTF8);

	CConsole_Rcv con_;

	CCoIniter com_lib(false);
	if (com_lib == false) {
		con_.WriteErr(com_lib.Error());
		return (n_res = 1);
	}

#pragma region _sec
	CCoSecurityProvider sec_;
	HRESULT hr_ = sec_.InitNoIdentity();
	if (FAILED(hr_)) {
		con_.WriteErr(sec_.Error());
		return (n_res = 1);
	}
#pragma endregion

	CError sys_err; sys_err << __MODULE__ << S_OK;
	CCommandLine cmd_line;

	if (SUCCEEDED(hr_)) {
	}

	CGenericProcessEnum proc_enum;
	hr_ = proc_enum.Enumerate();
	if (FAILED(hr_)) {
		con_.WriteErr(proc_enum.Error());
	}
	else {
		CAtlString cs_msg(_T("\t|Id|Name|Desc"));

		const TGenericProcessList& procs_ = proc_enum.List();
		for (size_t i_ = 0; i_ < procs_.size(); i_++) {

			const CGenericProcess& proc_ = procs_[i_];
			cs_msg += _T("\t\n");
			cs_msg += proc_.IdAsText().GetString();
			cs_msg += _T("\t");
			cs_msg += proc_.Name();
			cs_msg += _T("\t");
			cs_msg += proc_.Desc();

		}
		con_.WriteInfo(cs_msg);
	}

	con_.OnWait(_T("Press any key or click [x] window button to exit.\n"));
	return n_res;
}