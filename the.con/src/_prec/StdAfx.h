#ifndef _STDAFX_H_A2AD1503_5554_4E5D_9B38_341E6E697B7E_INCLUDED
#define _STDAFX_H_A2AD1503_5554_4E5D_9B38_341E6E697B7E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Apr-2019 at 10:12:24p, UTC+7, Phuket, Rawai, Thursday;
	This is sound-bin-trans receiver desktop console application precompiled header include file.
	-----------------------------------------------------------------------------
	Adopted to FakeGPS project on 13-Dec-2019 at 8:19:23a, UTC+7, Novosibirsk, Tulenina, Friday;
	Adopted to File Touch App project on 02-Feb-2020 at 1:37:29p, UTC+7, Novosibirsk, Tulenina, Sunday;
	Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 5:29:57a, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "ebo.gam.esc.con.ver.h"

#ifndef STRICT
#define STRICT
#endif

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe
#pragma warning(disable: 4458)  // declaration of '{func_name}' hides class member (GDI+)

#define _ATL_APARTMENT_THREADED
#define _ATL_NO_AUTOMATIC_NAMESPACE
//
// ATL/OLE2-based header;
//
#include <atlbase.h>
#include <atlwin.h >
#include <atlcom.h >
#include <comdef.h >
#include <atlstr.h >
#include <atlsafe.h>

using namespace ATL;

#ifdef _DEBUG
	#define _ATL_DEBUG_INTERFACES
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif
//
// STL-based headers;
//
#include <vector>
#include <map>
#include <time.h>
#include <typeinfo>

#if (0)
#if defined WIN64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined WIN32
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

#pragma comment(lib, "__shared.lite_v15.lib")

#pragma comment(lib, "shared.gen_v15.lib"  )
#pragma comment(lib, "shared.gui_v15.lib"  )

#pragma comment(lib, "ebo.mem.scan_v15.lib")
#pragma comment(lib, "ebo.sys.proc_v15.lib")

#pragma comment(lib, "_ntfs_v15.lib"    )
#pragma comment(lib, "_user.32_v15.lib" )

#endif/*_STDAFX_H_A2AD1503_5554_4E5D_9B38_341E6E697B7E_INCLUDED*/