#ifndef UDDGENDLGSETBAS_H_9110A96E_7D32_45DC_9283_F1DA0C0839CD_INCLUDED
#define UDDGENDLGSETBAS_H_9110A96E_7D32_45DC_9283_F1DA0C0839CD_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Nov-2018 at 1:31:43p, UTC+7, Novosibirsk, Tulenina, Friday;
	This is USB Drive Detective (bitsphereinc.com) desktop app tabbed page base layout interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google push notification app on 9-Sep-2019 at 10:38:36p, UTC+7, Novosibirsk, Tulenina, Monday;
	Adopted to File Touch app on 11-Feb-2020 at 12:13:42p, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "shared.gen.sys.err.h"
#include "shared.gui.page.pane.h"

namespace shared { namespace gui { namespace layout {

	using shared::sys_core::CError;

	class CPage_Layout_Base {
	protected:
		CWindow     m_host;
		UINT        m_active;    // identifier of currently active or selected control within this layer;
	public:
		 CPage_Layout_Base(const HWND _host);
		~CPage_Layout_Base(void);

	public:
		 CPage_Layout_Base& operator= (const HWND);  // assigns a host handle;

	public:
		//
		// TODO: operator signs must be reviewed for better readability;
		//
		RECT        operator =(const UINT _ctl_id) const; // gets a rectangle of a control in host client area coordinates;
		HWND        operator<<(const UINT _ctl_id);       // sets an active control and returns its window handle; 
		CAtlString  operator<=(const UINT _ctl_id) const; // gets control window text;
	public:
		operator    RECT (void) const; // returns host window client rectangle;
	};

	typedef CPage_Layout_Base Lay_;

	class CPage_Layout {
	public:
		static const UINT u_sm_gap = 0x03;
		static const UINT u_md_gap = 0x05;
		static const UINT u_margin = 0x10; // a gap between a control window border and page client area edge;
	protected:
		mutable
		CError        m_error;
		CWindow       m_host ;   // a handle of tab page dialog;
		RECT          m_area ;   // a host client area rectangle;

	public:
		 CPage_Layout(const HWND _host);
		~CPage_Layout(void);

	public:
		HRESULT       AdjustBan (const CPage_Ban&) const;
		HRESULT       AdjustImg (const CPage_Ava&) const;
		TErrorRef     Error (void) const;

	public:
		HRESULT       AlignComboHeightTo(const UINT _combo_id, const UINT _height);
		HRESULT       AlignCtrlsByHeight(const UINT _ctl_src , const UINT _ctl_tag, const SIZE& _shifts);

	protected:
		const RECT    ImageArea(const CPage_Ban&) const;
		const RECT    ImageText(const CPage_Ban&) const;
	};

}}}

#endif/*UDDGENDLGSETBAS_H_9110A96E_7D32_45DC_9283_F1DA0C0839CD_INCLUDED*/