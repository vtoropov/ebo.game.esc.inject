#ifndef _UIXNOTIFYTRAYAREA_H_FC78667C_1725_4b96_8D6E_D8E80A16522C_INCLUDED
#define _UIXNOTIFYTRAYAREA_H_FC78667C_1725_4b96_8D6E_D8E80A16522C_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Sep-2013 at 5:57:21pm, GMT+3, Taganrog, Saturday;
	This is UIX library notify tray area wrapper class declaration file.
	-----------------------------------------------------------------------------
	Adopted to File Touch app project on 02-Feb-2020 at 3:19:13p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include <shellapi.h>

#include "shared.gen.sys.err.h"

namespace shared { namespace gui {

	using shared::sys_core::CError;
	// https://docs.microsoft.com/en-us/windows/win32/api/shellapi/ns-shellapi-notifyicondataa
	class CNotifyPopupType
	{
	public:
		enum _e {
			NPT_Info     = 0,   // default
			NPT_Warning  = 1,
			NPT_Error    = 2
		};
	public:
		static DWORD EnumToDword(const _e); // converts enumeration value to tray flag;
	};

	typedef CNotifyPopupType::_e TNotifyType;

	interface INotifyTrayAreaCallback
	{
		virtual HRESULT  NotifyTray_OnClickEvent(const UINT eventId)       { eventId; return E_NOTIMPL; } // a user clicks
		virtual HRESULT  NotifyTray_OnContextMenuEvent(const UINT eventId) { eventId; return E_NOTIMPL; } // requests to show context menu
		virtual HRESULT  NotifyTray_OnMouseMoveEvent(const UINT eventId)   { eventId; return E_NOTIMPL; } // notifies about mouse movement over the tray icon
	};

	class CNotifyTrayArea
	{
	public:
		enum {
			NTA_CallbackMessageId = WM_APP + 5
		};
	public:
		class CNotifyIcon {
			friend class CNotifyTrayArea;
		private:
			NOTIFYICONDATA&
			          m_not_data;
			bool      m_shown;
			CError    m_error;
		public:
			 CNotifyIcon (NOTIFYICONDATA&);
			~CNotifyIcon (void);

		public:
			HRESULT   Hide (void);
			TErrorRef Error(void) const;
			bool      Is   (void) const;
			HRESULT   Show (const UINT _n_ico_res_id, LPCTSTR _lp_sz_tips);
			HRESULT   Tips (LPCTSTR _lp_sz_tips);
		};

		class CNotifyPopup {
			friend class CNotifyTrayArea;
			enum _dflt {
				e_timeout = 11000, // default timeout in msec;
			};
		private:
			NOTIFYICONDATA&
			         m_not_data;
			DWORD    m_timeout ;
			CError   m_error;
		public:
			 CNotifyPopup (NOTIFYICONDATA&);
			~CNotifyPopup (void);

		public:
			TErrorRef Error(void) const;
			HRESULT   Show (LPCTSTR _lp_sz_title, LPCTSTR _lp_sz_msg, const TNotifyType);
			HRESULT   Timeout(const DWORD _dw_msec); // seems to be not supported by Win 10;
		};

	private:
		class CMessageHandler:
			public  ATL::CWindowImpl<CMessageHandler> {
			typedef ATL::CWindowImpl<CMessageHandler> TWindow;
			friend class CNotifyTrayArea;
		private:
			INotifyTrayAreaCallback&   m_sink_ref;
			const UINT                 m_event_id;
		public:
			 CMessageHandler(INotifyTrayAreaCallback&, const UINT eventId);
			~CMessageHandler(void);
		public:
			BEGIN_MSG_MAP(CMessageHandler)
				MESSAGE_HANDLER(CNotifyTrayArea::NTA_CallbackMessageId, OnNotify)
			END_MSG_MAP()
		private:
			LRESULT  OnNotify(UINT, WPARAM, LPARAM, BOOL&);
		};
	private:
		NOTIFYICONDATA   m_data   ;
		CMessageHandler  m_handler;
		CError           m_error  ;
		CNotifyIcon      m_icon   ;
		CNotifyPopup     m_popup  ;

	public:
		 CNotifyTrayArea(INotifyTrayAreaCallback&, const UINT eventId);
		~CNotifyTrayArea(void);

	public:
		TErrorRef       Error(void) const;
		const HWND      Host (void) const;    // return handle of message handler window for traking context menu;
		CNotifyIcon&    Icon (void) ;
		const bool      Is   (void) const;    // returns true  if message handler window exists; 
		CNotifyPopup&   Popup(void) ;
		HRESULT         Turn (const bool _b_on);
	};

}}

typedef shared::gui::CNotifyTrayArea::CNotifyIcon  TNotifyIcon;
typedef shared::gui::CNotifyTrayArea::CNotifyPopup TNotifyPopup;
typedef shared::gui::CNotifyTrayArea  TNotifyArea;

#endif/*_UIXNOTIFYTRAYAREA_H_FC78667C_1725_4b96_8D6E_D8E80A16522C_INCLUDED*/