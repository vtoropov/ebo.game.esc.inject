#ifndef _SHAREDLITEGENERICAPPLICATIONOBJECT_H_9D10F860_5FE9_4b74_91D1_9E9D329E142F_INCLUDED
#define _SHAREDLITEGENERICAPPLICATIONOBJECT_H_9D10F860_5FE9_4b74_91D1_9E9D329E142F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jun-2016 at 1:28:28p, GMT+7, Phuket, Rawai, Saturday;
	This is Shared Lite Library (thefileguardian.com) Generic Application class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject 29-Feb-2020 at 10:10:36a, UTC+7, Novosibirsk, Tulenina, Saturday;
*/
#include <Shellapi.h>
#include <map>
#pragma comment(lib, "version.lib")

#include "shared.gen.cmd.arg.h"

namespace shared { namespace user32
{
	using shared::common::CCommandLine;
	using shared::common::TCmdLineArgs;
	
	class CApplication
	{
	public:
		class CObject
		{
		private:
			CONST
			CApplication& m_app_obj_ref;
			HANDLE        m_mutex;
			CAtlString    m_mutex_name;
			DWORD         m_proc_state;

		public:
			 explicit
			 CObject(CONST CApplication&);
			~CObject(VOID);
		public:
			bool          IsSingleton(void) const;
			HRESULT       RegisterSingleton(LPCTSTR pMutexName);
			HRESULT       UnregisterSingleton(VOID);
		};
		class CVersion
		{
		private:
			mutable
			HRESULT       m_hResult;     // last result
			LPVOID        m_pVerInfo;    // file version info pointer
		public:
			 CVersion(void);
			 CVersion(LPCTSTR pszModulePath);
			~CVersion(void);
		public:
			CAtlString    Comments   (void)        const;
			CAtlString    CompanyName(void)        const;
			CAtlString    CopyRight  (void)        const;
			CAtlString    CustomValue(LPCTSTR lpszValueName)const;
			CAtlString    FileDescription(void)    const;
			CAtlString    FileVersion(void)        const;
			CAtlString    FileVersionFixed(void)   const;
			HRESULT       GetLastResult(void)      const;
			CAtlString    InternalName (void)      const;
			CAtlString    OriginalFileName(void)   const;
			CAtlString    ProductName   (void)     const;
			CAtlString    ProductVersion(void)     const;
			CAtlString    ProductVersionFixed(void)const;
		};

	private:
		mutable 
		CAtlString        m_app_name;
		HRESULT           m_hResult;     // last result
		CVersion          m_version;
		CObject           m_object;
		CCommandLine      m_cmd_line;
	public:
		 CApplication(void);
		~CApplication(void);
	public:
		const
		CCommandLine&     CommandLine  (void) const;
		CCommandLine&     CommandLine  (void);
		CAtlString        GetFileName  (const bool bNoExtension = false) const;
		HRESULT           GetLastResult(void) const;
		LPCTSTR           GetName (void) const;
		HRESULT           GetPath (CAtlString& __in_out_ref) const;
		HRESULT           GetPathFromAppFolder(LPCTSTR lpszPattern, CAtlString& __in_out_ref) const;
		const CObject &   Instance(VOID)const;
		      CObject &   Instance(VOID);
		const CVersion&   Version (VOID)const;
	public:
		static HRESULT    GetFullPath (CAtlString&);
		static bool       Is64bit(VOID);
		static bool       IsRelatedToAppFolder(LPCTSTR pPath);
	private:
		CApplication(const CApplication&);
		CApplication& operator= (const CApplication&);
	};

	CApplication&  GetAppObjectRef(void); // gets the static object reference; it is used in a client of version 2
}}

#endif/*_SHAREDLITEGENERICAPPLICATIONOBJECT_H_9D10F860_5FE9_4b74_91D1_9E9D329E142F_INCLUDED*/