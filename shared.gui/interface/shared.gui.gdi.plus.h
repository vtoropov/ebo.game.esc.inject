#ifndef _TMLNCUSTOMGDIHLP_H_C75B326C_B2E1_43E4_BD2E_6550386B89D9_INCLUDED
#define _TMLNCUSTOMGDIHLP_H_C75B326C_B2E1_43E4_BD2E_6550386B89D9_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Feb-2019 at 7:12:25a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is time line custom WTL-based control GDI+ wrapper interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to serial port project on 26-May-2019 at 11:06:19a, UTC+7, Phuket, Rawai, Sunday;
	Adopted to File Touch app on 11-Feb-2020 at 6:03:10p, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#pragma warning(push)
#pragma warning(disable:4458) // for getting Level 4 of the warnings, otherwise, getting mess of warnings from the gdi+ header(s);
#include <gdiplus.h>
#pragma warning(pop)
#define __HRESULT_FROM_LASTERROR()   HRESULT_FROM_WIN32(::GetLastError())
#define __H(rc__)    (rc__.bottom - rc__.top)
#define __W(rc__)    (rc__.right - rc__.left)

#define RGBA(r,g,b,a)	( RGB(r,g,b) | (((DWORD)(BYTE)(a))<<24) )
#define GetAValue(_clr)	( 0 == LOBYTE((_clr)>>24) ? 255 : LOBYTE((_clr)>>24) )

#define __DwordToHresult(_word)  HRESULT_FROM_WIN32(_word)
#pragma comment(lib, "gdiplus.lib")

namespace shared { namespace gui { namespace draw {

	class CGdiPlusLib_Guard {
	public:
		 CGdiPlusLib_Guard(void);
		~CGdiPlusLib_Guard(void);
	public:
		HRESULT   Capture(void);
		HRESULT   Release(void);
		bool      Secured(void) const;
	private:
		CGdiPlusLib_Guard(const CGdiPlusLib_Guard&);
		CGdiPlusLib_Guard& operator= (const CGdiPlusLib_Guard&);
	};

	class CGdiPlusPng_Loader {
	public:
		 CGdiPlusPng_Loader(void);
		~CGdiPlusPng_Loader(void);
	public:
		HRESULT     LoadImage(const ATL::_U_STRINGorID RID, Gdiplus::Bitmap*&);
		HRESULT     LoadImage(const ATL::_U_STRINGorID RID, HBITMAP&);
		HRESULT     LoadImage(const ATL::_U_STRINGorID RID, const HMODULE hModule, Gdiplus::Bitmap*&);
	};

	class CGdiPlusPng_Draw {
	public:
		CGdiPlusPng_Draw(void);
		~CGdiPlusPng_Draw(void);
	public:
		HRESULT     Draw(const HDC& _h_dc, const RECT& _rc_area, Gdiplus::Bitmap* const);
	};

	#define _c_clr const COLORREF
	class CGdiClass_Draw {
	public:
		 CGdiClass_Draw(void);
		~CGdiClass_Draw(void);
	public:
		HRESULT     Gradient(
		               const HDC& _h_dc, const RECT& _rc_area, _c_clr& _from, _c_clr& _upto, const bool _b_horz = true
		            ); // no alpha channel at this time;
	};

}}}

#endif/*_TMLNCUSTOMGDIHLP_H_C75B326C_B2E1_43E4_BD2E_6550386B89D9_INCLUDED*/