#ifndef _TMTLCUSTOMWTLAUX_H_DF24A00C_9866_4D21_90A8_9F1B72C7392A_INCLUDED
#define _TMTLCUSTOMWTLAUX_H_DF24A00C_9866_4D21_90A8_9F1B72C7392A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Feb-2019 at 9:54:32a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is time line custom control WTL test desktop app UI auxiliary interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Soset project on 04-Mar-2019 at 05:51:59a, UTC+7, Novosibirsk, Tulenina, Monday;
	Adopted to File Touch project on 02-Feb-2020 at 11:00:40p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/

namespace shared { namespace gui {

	class CIconLoader {
	private:
		HICON   m_small;
		HICON   m_large;
	public:
		 CIconLoader(void);
		 CIconLoader(const UINT nIconId );
		~CIconLoader(void);
	public:
		VOID    Destroy(void);
		HICON   DetachLarge (void);
		HICON   DetachSmall (void);
		HRESULT Load   (const UINT nIconId);
	};
	//
	// TODO: this class should be excluded/deprecated due to having the same functionality in WTL::CTheme;
	//
	class CTheme {
	public:
		 CTheme(void);
		~CTheme(void);
	public:
		bool        IsCompositionEnabled(const bool bReset =false); // checks for composition enabling
	public:
		static bool IsEnabled(void);                                // checks a theme is enabled at all
	};
}}

#endif/*_TMTLCUSTOMWTLAUX_H_DF24A00C_9866_4D21_90A8_9F1B72C7392A_INCLUDED*/