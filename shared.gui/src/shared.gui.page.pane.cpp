/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Nov-2018 at 7:14:02p, UTC+7, Novosibirsk, Tulenina, Friday;
	This is USB Drive Detective (bitsphereinc.com) desktop app property page base panes interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google push notification app on 10-Sep-2019 at 11:16:08a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	Adopted to Ebo Pack personal account app on 18-Oct-2019 at 10:56:11a, UTC+7, Novosibirsk, Tulenina, Friday;
	Adopted to File Touch app on 11-Feb-2020 at 4:09:31p, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "StdAfx.h"
#include "shared.gui.page.pane.h"
#include "shared.gui.page.layout.h"

using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

CPage_Ava:: CPage_Ava(void) : m_ctl(0), m_res(0) {}
CPage_Ava:: CPage_Ava(const UINT _ctl_id, const UINT _res_id) : m_ctl(_ctl_id), m_res(_res_id) {}
CPage_Ava::~CPage_Ava(void) {}

/////////////////////////////////////////////////////////////////////////////

const UINT&   CPage_Ava::Control (void) const { return m_ctl; }
      UINT&   CPage_Ava::Control (void)       { return m_ctl; }
const UINT&   CPage_Ava::Resource(void) const { return m_res; }
      UINT&   CPage_Ava::Resource(void)       { return m_res; }

/////////////////////////////////////////////////////////////////////////////

const bool    CPage_Ava::IsValid(void) const { return (0 < m_ctl && 0 < m_res); }

/////////////////////////////////////////////////////////////////////////////

CPage_Ava::operator bool(void) const { return this->IsValid(); }

/////////////////////////////////////////////////////////////////////////////

CPage_Text:: CPage_Text(void) : m_res(0), m_ctl(0) {}
CPage_Text:: CPage_Text(const UINT _ctl_id, const UINT _res_id) : m_ctl(_ctl_id), m_res(_res_id) {}
CPage_Text::~CPage_Text(void) {}

/////////////////////////////////////////////////////////////////////////////
const
bool     CPage_Text::IsValid (void) const      { return (0  < m_ctl && 0 < m_res); }
UINT     CPage_Text::Label   (void) const      { return m_ctl; }
HRESULT  CPage_Text::Label   (const UINT _v)   { if (0 == _v) return E_INVALIDARG; m_ctl = _v; return S_OK; }
UINT     CPage_Text::Resource(void) const      { return m_res; }
UINT&    CPage_Text::Resource(void)            { return m_res; }
HRESULT  CPage_Text::Resource(const UINT _v)   { if (0 == _v) return E_INVALIDARG; m_res = _v; return S_OK; }

/////////////////////////////////////////////////////////////////////////////

CPage_Text::operator bool (void) const { return this->IsValid(); }

/////////////////////////////////////////////////////////////////////////////

CPage_Ban:: CPage_Ban(void) {}
CPage_Ban:: CPage_Ban(const UINT _img_id, const UINT _lab_id) : m_image(_img_id, 0), m_label(_lab_id, 0) {}
CPage_Ban::~CPage_Ban(void) {}

/////////////////////////////////////////////////////////////////////////////
const
CPage_Ava &   CPage_Ban::Image(void) const { return m_image; }
CPage_Ava &   CPage_Ban::Image(void)       { return m_image; }
const
CPage_Text&   CPage_Ban::Label(void) const { return m_label; }
CPage_Text&   CPage_Ban::Label(void)       { return m_label; }

/////////////////////////////////////////////////////////////////////////////

const bool
CPage_Ban::IsValid      (void) const { return (m_image.IsValid() && m_label.IsValid()); }
CPage_Ban::operator bool(void) const { return this->IsValid(); }

/////////////////////////////////////////////////////////////////////////////

CPage_Info:: CPage_Info(void) {}
CPage_Info::~CPage_Info(void) {}

/////////////////////////////////////////////////////////////////////////////

const
CPage_Ava & CPage_Info::Image(void) const { return m_logo; }
CPage_Ava & CPage_Info::Image(void)       { return m_logo; }
const
CPage_Text& CPage_Info::Text (void) const { return m_text; }
CPage_Text& CPage_Info::Text (void)       { return m_text; }

/////////////////////////////////////////////////////////////////////////////

CPage_Pane:: CPage_Pane(const HWND _host) : m_host(_host) {}
CPage_Pane::~CPage_Pane(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CPage_Pane::Error(const CPage_Info& _info, TErrorRef _err) {
	_err; _info;
	HRESULT hr_ = S_OK;

	CPage_Layout lay_(m_host);
	hr_ = lay_.AdjustImg(_info.Image()) ;
	if (FAILED(hr_))
		return hr_;

	CWindow desc_ = Lay_(m_host) << _info.Text().Label();
	if (NULL == desc_) {
		return (hr_ = HRESULT_FROM_WIN32(ERROR_NOT_FOUND));
	}
	desc_.SetWindowText(
		_err.Format(_T("\n")).GetString()
	);

	return  hr_;
}

HRESULT       CPage_Pane::Info (const CPage_Info& _info) {
	_info;
	HRESULT hr_ = S_OK;

	CPage_Layout lay_(m_host);
	hr_ = lay_.AdjustImg(_info.Image()) ;
	if (FAILED(hr_))
		return hr_;

	CWindow desc_ = Lay_(m_host) << _info.Text().Label();
	if (NULL == desc_) {
		return (hr_ = HRESULT_FROM_WIN32(ERROR_NOT_FOUND));
	}
	CAtlString cs_desc;
	cs_desc.LoadString(
		_info.Text().Resource()
	);
	if (cs_desc.IsEmpty())
		return (hr_ = HRESULT_FROM_WIN32(ERROR_RESOURCE_DATA_NOT_FOUND));

	desc_.SetWindowText(
		cs_desc.GetString()
	);

	return  hr_;
}