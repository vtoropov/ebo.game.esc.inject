/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Feb-2019 at 9:58:40a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is time line custom control WTL test desktop app UI auxiliary interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Soset project on 04-Mar-2019 at 05:52:36a, UTC+7, Novosibirsk, Tulenina, Monday;
	Adopted to File Touch project on 02-Feb-2020 at 11:05:37p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include "StdAfx.h"
#include "shared.gui.aux.h"

using namespace shared::gui;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace gui { namespace details {

	SIZE App_IconSize(const bool bTreatAsLargeIcon)
	{
		const SIZE szIcon = {
					::GetSystemMetrics(bTreatAsLargeIcon ? SM_CXICON : SM_CXSMICON), 
					::GetSystemMetrics(bTreatAsLargeIcon ? SM_CYICON : SM_CYSMICON)
				};
		return szIcon;
	}

	HICON App_LoadIcon(const UINT nIconResId, const bool bTreatAsLargeIcon)
	{
		const SIZE szIcon = App_IconSize(bTreatAsLargeIcon);
		const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
		const HICON hIcon = (HICON)::LoadImage(
					hInstance, MAKEINTRESOURCE(nIconResId), IMAGE_ICON, szIcon.cx, szIcon.cy, LR_DEFAULTCOLOR
				);
		return hIcon;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CIconLoader:: CIconLoader(void) : m_small(NULL), m_large(NULL) {}
CIconLoader:: CIconLoader(const UINT nIconId ) : CIconLoader() { this->Load(nIconId); }
CIconLoader::~CIconLoader(void) {
	this->Destroy();
}

/////////////////////////////////////////////////////////////////////////////


VOID    CIconLoader::Destroy(void) {
	if (NULL != m_large) {
		::DestroyIcon(m_large); m_large = NULL;
	}
	if (NULL != m_small) {
		::DestroyIcon(m_small); m_small = NULL;
	}
}

HICON   CIconLoader::DetachLarge (void) { HICON ret_ = m_large; m_large = NULL; return ret_; }

HICON   CIconLoader::DetachSmall (void) { HICON ret_ = m_small; m_small = NULL; return ret_; }

HRESULT CIconLoader::Load        (const UINT nIconId) {
	HRESULT hr_ = S_OK;

	if (0 == nIconId)
		return (hr_ = E_INVALIDARG);
	
	m_large = details::App_LoadIcon(nIconId, true ); if (!m_large) hr_ = HRESULT_FROM_WIN32(::GetLastError());
	m_small = details::App_LoadIcon(nIconId, false); if (!m_small) hr_ = HRESULT_FROM_WIN32(::GetLastError());

	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace gui {

CTheme:: CTheme(void) {}
CTheme::~CTheme(void) {}

/////////////////////////////////////////////////////////////////////////////

bool     CTheme::IsCompositionEnabled(const bool bReset)
{
	static int nResult   = -1;
	if (false == bReset && -1 != nResult)
		return ( 0!= nResult);

	nResult = 0;

	HMODULE hLib = ::LoadLibrary(_T("dwmapi.dll"));
	if (0 != hLib)
	{
		typedef HRESULT WINAPI IsComposed(PBOOL);
		IsComposed* pComposed = reinterpret_cast<IsComposed*>(::GetProcAddress(hLib, "DwmIsCompositionEnabled"));

		if (NULL != pComposed)
		{
			BOOL bResult = FALSE;
			bResult = SUCCEEDED(pComposed(&bResult)) && bResult;
			nResult = static_cast<INT>(bResult);
		}
		if(!::FreeLibrary(hLib)) {
			ATLASSERT(FALSE);
		}
	}
	return (0 != nResult);
}

/////////////////////////////////////////////////////////////////////////////

bool     CTheme::IsEnabled(void)
{
	bool bResult = false;

	HMODULE hModule = ::LoadLibrary(_T("uxtheme.dll"));
	if (NULL==hModule)
		return bResult;

	typedef BOOL WINAPI IsAppThemed();
	typedef BOOL WINAPI IsThemeActive();

	IsAppThemed* pAppThemed = reinterpret_cast<IsAppThemed*>(::GetProcAddress(hModule,"IsAppThemed"));
	IsThemeActive* pThemeActive = reinterpret_cast<IsThemeActive*>(::GetProcAddress(hModule,"IsThemeActive"));

	const BOOL b_res_ = (NULL != pAppThemed && NULL != pThemeActive && TRUE == (*pThemeActive)());
	::FreeLibrary(hModule); hModule = NULL;

	return (b_res_ != FALSE);
}

}}