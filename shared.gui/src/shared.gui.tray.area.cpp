/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Sep-2013 at 6:11:38pm, GMT+3, Taganrog, Saturday;
	This is UIX library notify tray area wrapper class implementation file.
	-----------------------------------------------------------------------------
	Adopted to File Touch App project on 02-Feb-2020 at 3:23:13p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include "StdAfx.h"
#include "shared.gui.tray.area.h"
#include "shared.gui.aux.h"

using namespace shared::gui;

#if !defined(NIF_SHOWTIP)
	#define  NIF_SHOWTIP     0x00000080
#endif
#if !defined(NOTIFYICON_VERSION_4)
	#define  NOTIFYICON_VERSION_4   4
#endif

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace gui { namespace _impl
{
	class __declspec(uuid("{34CCFCEF-AB28-45bc-B9A9-A29C8839414E}")) NotifyTrayArea_IconIdentifier;
}}}
using namespace shared::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

DWORD CNotifyPopupType::EnumToDword(const _e _e_val) {

	DWORD  dw_flag = NIIF_INFO;
	switch (_e_val) {
	case _e::NPT_Error  : { dw_flag = NIIF_ERROR;   } break;
	case _e::NPT_Info   : { dw_flag = NIIF_INFO ;   } break;
	case _e::NPT_Warning: { dw_flag = NIIF_WARNING; } break;
	}
	return dw_flag;
}

/////////////////////////////////////////////////////////////////////////////

CNotifyTrayArea::CMessageHandler::CMessageHandler(INotifyTrayAreaCallback& sink_ref, const UINT eventId):
	m_sink_ref(sink_ref),
	m_event_id(eventId) {
}

CNotifyTrayArea::CMessageHandler::~CMessageHandler(void) {
}

/////////////////////////////////////////////////////////////////////////////

LRESULT  CNotifyTrayArea::CMessageHandler::OnNotify(UINT, WPARAM, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	switch (LOWORD(lParam)) {
	case WM_MOUSEMOVE  : { m_sink_ref.NotifyTray_OnMouseMoveEvent(m_event_id);  } break;
	case WM_LBUTTONDOWN: { m_sink_ref.NotifyTray_OnClickEvent(m_event_id);      } break;
	case WM_RBUTTONDOWN:
	case WM_CONTEXTMENU: { m_sink_ref.NotifyTray_OnContextMenuEvent(m_event_id);} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CNotifyTrayArea::CNotifyIcon:: CNotifyIcon(NOTIFYICONDATA& _not_data) :
	m_not_data(_not_data), m_shown(false) {
	m_error << __MODULE__ << S_OK;
}
CNotifyTrayArea::CNotifyIcon::~CNotifyIcon(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CNotifyTrayArea::CNotifyIcon::Hide (void) {
	m_error << __MODULE__ << S_OK;

	if (this->Is() == false)
		return m_error;

	if (!::Shell_NotifyIcon(NIM_DELETE, &m_not_data))
		return (m_error = ::GetLastError());

	m_shown = false;
	return m_error;
}

TErrorRef CNotifyTrayArea::CNotifyIcon::Error(void) const { return m_error; }
bool      CNotifyTrayArea::CNotifyIcon::Is   (void) const { return m_shown; }

HRESULT   CNotifyTrayArea::CNotifyIcon::Show (const UINT _n_ico_res_id, LPCTSTR _lp_sz_tips) {
	m_error << __MODULE__ << S_OK;

	if (this->Is())
		return m_error;

	CIconLoader   ico_load;
	HRESULT hr_ = ico_load.Load(_n_ico_res_id);
	if (FAILED(hr_))
		return (m_error = hr_);
	// enabling NIF_GUID flag leads to access violation error while icon is being added to tray;
	m_not_data.uFlags = (NIF_ICON | NIF_MESSAGE | NIF_TIP/* | NIF_GUID*/);
	m_not_data.hIcon  = ico_load.DetachSmall();

	if (_lp_sz_tips && ::_tcslen(_lp_sz_tips)) {
		const errno_t err_ = ::_tcscpy_s(
			m_not_data.szTip,  ARRAYSIZE(m_not_data.szTip), _lp_sz_tips
		);
	}

	if (::Shell_NotifyIcon(NIM_ADD, &m_not_data) == FALSE) {
		::DestroyIcon(m_not_data.hIcon);
		m_not_data.hIcon = NULL;
		m_error = ::GetLastError();
		if (ERROR_ACCESS_DENIED == m_error.Code()) m_error = _T("Access denied");
		else m_error = _T("Internal shell error");
	}
	else
		m_shown = true;

	return m_error;
}

HRESULT   CNotifyTrayArea::CNotifyIcon::Tips (LPCTSTR _lp_sz_tips) {
	m_error << __MODULE__ << S_OK;

	if (NULL == _lp_sz_tips || 0 == ::_tcslen(_lp_sz_tips))
		return (m_error = E_INVALIDARG);

	const errno_t err_ = ::_tcscpy_s(
		m_not_data.szTip,  ARRAYSIZE(m_not_data.szTip), _lp_sz_tips
	);
	if (NO_ERROR != err_)
		return (m_error = E_OUTOFMEMORY);

	m_not_data.uFlags = NIF_TIP;
	
	if (::Shell_NotifyIcon(NIM_MODIFY, &m_not_data) == FALSE)
		m_error = ::GetLastError();
	
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CNotifyTrayArea::CNotifyPopup:: CNotifyPopup(NOTIFYICONDATA& _not_data) :
	m_not_data(_not_data), m_timeout(_dflt::e_timeout) {
	m_error << __MODULE__ << S_OK;
}
CNotifyTrayArea::CNotifyPopup::~CNotifyPopup(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef CNotifyTrayArea::CNotifyPopup::Error(void) const { return m_error; }
HRESULT   CNotifyTrayArea::CNotifyPopup::Show (LPCTSTR _lp_sz_title, LPCTSTR _lp_sz_msg, const TNotifyType _type) {
	m_error << __MODULE__ << S_OK;

	if (NULL == _lp_sz_title || 0 == ::_tcslen(_lp_sz_title))
		return (m_error = E_INVALIDARG) = _T("Title argument is invalid;");
	if (NULL == _lp_sz_msg   || 0 == ::_tcslen(_lp_sz_msg  ))
		return (m_error = E_INVALIDARG) = _T("Message argument is invalid;");

	errno_t err_ = NO_ERROR;
	err_ = ::_tcscpy_s(m_not_data.szInfoTitle, ARRAYSIZE(m_not_data.szInfoTitle), _lp_sz_title);
	if (NO_ERROR != err_)
		return (m_error = E_OUTOFMEMORY);

	err_ = ::_tcscpy_s(m_not_data.szInfo, ARRAYSIZE(m_not_data.szInfo), _lp_sz_msg);
	if (NO_ERROR != err_)
		return (m_error = E_OUTOFMEMORY);


	m_not_data.uFlags      = NIF_INFO;
	m_not_data.dwInfoFlags = CNotifyPopupType::EnumToDword(_type);
	m_not_data.uTimeout    = m_timeout;

	if (::Shell_NotifyIcon(NIM_MODIFY, &m_not_data) == FALSE)
		m_error = ::GetLastError();

	return m_error;
}

HRESULT   CNotifyTrayArea::CNotifyPopup::Timeout(const DWORD _dw_msec) {
	m_error << __MODULE__ << S_OK;
	m_not_data.uTimeout = (m_timeout = _dw_msec);
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CNotifyTrayArea:: CNotifyTrayArea(INotifyTrayAreaCallback& sink_ref, const UINT eventId): 
	m_handler(sink_ref, eventId), m_icon(m_data), m_popup(m_data) {

	::memset((void*)&m_data, 0, sizeof(NOTIFYICONDATA));
	m_error << __MODULE__ << S_OK;
}

CNotifyTrayArea::~CNotifyTrayArea(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef     CNotifyTrayArea::Error(void) const { return   m_error;  }
const HWND    CNotifyTrayArea::Host (void) const { return   m_handler;}
TNotifyIcon&  CNotifyTrayArea::Icon (void)       { return   m_icon ;  }
const bool    CNotifyTrayArea::Is   (void) const { return !!m_handler.IsWindow(); }
TNotifyPopup& CNotifyTrayArea::Popup(void)       { return   m_popup;  }

HRESULT       CNotifyTrayArea::Turn (const bool _b_on) {
	m_error << __MODULE__ << S_OK;

	if ( this->Is() &&  _b_on) return m_error; // already initialized;
	if (!this->Is() && !_b_on) return m_error; // already destroyed;

	if (_b_on) {

		const HWND h_hand = m_handler.Create(HWND_MESSAGE);
		if (NULL == h_hand)
			return (m_error = ::GetLastError());

		m_data.cbSize   = sizeof(NOTIFYICONDATA);
		m_data.uVersion = NOTIFYICON_VERSION_4  ; ::Shell_NotifyIcon(NIM_SETVERSION, &m_data);
		m_data.uID      = m_handler.m_event_id  ;
		m_data.uCallbackMessage = NTA_CallbackMessageId;
		m_data.guidItem = __uuidof(NotifyTrayArea_IconIdentifier);
		m_data.hWnd     = m_handler;
	
	}
	else {
		m_handler.SendMessage(WM_CLOSE);
	}

	return  m_error;
}