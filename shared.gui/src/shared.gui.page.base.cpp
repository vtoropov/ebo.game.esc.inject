/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Jan-2016 at 11:50:01am, GMT+7, Phuket, Rawai, Thursday;
	This is File Watcher (thefileguardian.com) desktop app UI tabbed page interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to USB Drive Detective (bitsphereinc.com) on 16-Aug-2018 at 7:02:23p, UTC+7, Novosibirsk, Rodniki, Tulenina, Friday;
	Adopted to Ebo Pack Google push notification app on 9-Sep-2019 at 10:34:04p, UTC+7, Novosibirsk, Tulenina, Monday;
	Adopted to Ebo Pack personal account app on 18-Oct-2019 at 10:56:11a, UTC+7, Novosibirsk, Tulenina, Friday;
	Adopted to File Touch desktop app on 11-Feb-2020 at 1:04:08p, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "StdAfx.h"
#include "shared.gui.page.base.h"

using namespace shared::gui;
#pragma warning(disable: 4458)  // declaration of '{func_name}' hides class member (GDI+)
#include "shared.uix.gdi.object.h"

using namespace ex_ui::draw;
/////////////////////////////////////////////////////////////////////////////

CTabPageBase::CTabPageBase(const UINT RID, ITabPageEvents& _evt_snk, ITabSetCallback& _set_snk) :
	IDD(RID), m_evt_snk(_evt_snk), m_bInited(false), m_nIndex(-1), m_set_snk(_set_snk) {
}

CTabPageBase::~CTabPageBase(void) { }

/////////////////////////////////////////////////////////////////////////////

LRESULT   CTabPageBase::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	return 0;
}

LRESULT   CTabPageBase::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	RECT rcArea = {0};

	WTL::CTabCtrl tab_ctl = (HWND)TDialog::GetParent();

	tab_ctl.GetWindowRect(&rcArea);
	tab_ctl.ScreenToClient(&rcArea);
	tab_ctl.AdjustRect(false, &rcArea);
	TDialog::SetWindowPos(
		0, &rcArea, SWP_NOZORDER|SWP_NOACTIVATE
	);
	if (CTheme::IsThemingSupported())
	{
		HRESULT hr_ = ::EnableThemeDialogTexture(*this, ETDT_ENABLETAB);
		ATLVERIFY(SUCCEEDED(hr_));
	}
	m_evt_snk.TabPage_OnEvent(uMsg, wParam, lParam, bHandled);
	return 0;
}

LRESULT   CTabPageBase::OnPagePaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled = FALSE;
	::WTL::CPaintDC dc(TDialog::m_hWnd);
	return 0;
}

LRESULT   CTabPageBase::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

BOOL      CTabPageBase::TabPage_OnCommand(const WORD wCmdId) {
	wCmdId;
	return FALSE; // not handled
}

/////////////////////////////////////////////////////////////////////////////

INT       CTabPageBase::Index(void)   const { return m_nIndex; }
VOID      CTabPageBase::Index(const INT _v) { m_nIndex   = _v; }

/////////////////////////////////////////////////////////////////////////////

const
HFONT     CTabPageBase::SectionCapFont (void) {
	ex_ui::draw::CFont fnt_(
		_T("Verdana"),
		eCreateFontOption::eRelativeSize,
		+1
	);
	return fnt_.Detach();
}

/////////////////////////////////////////////////////////////////////////////
#if (0)

namespace udd { namespace ui
{
	//
	// it is necessary to keep this class implementation in namespace, which the class belongs to;
	// otherwise, class name coincidence appears with ATL window class;
	//
	CWindowEx::CWindowEx(const CWindow& _wnd) { *this = _wnd; }

/////////////////////////////////////////////////////////////////////////////

	CWindowEx& CWindowEx::operator=(const CWindow& _wnd) { TWindow::m_hWnd = _wnd; return *this; }

/////////////////////////////////////////////////////////////////////////////

	RECT      CWindowEx::GetDlgItemRect(const UINT _ctrl_id)const
	{
		RECT rc_ = {0};
		if (!*this)
			return rc_;

		CWindow ctrl_ = TWindow::GetDlgItem(_ctrl_id);
		if (!ctrl_)
			return rc_;

		ctrl_.GetWindowRect(&rc_);
		::MapWindowPoints(HWND_DESKTOP, *this, (LPPOINT)&rc_, 0x2);

		return rc_;
	}

}}

/////////////////////////////////////////////////////////////////////////////

CCtrlAutoState::CCtrlAutoState(const CWindow& _host, const UINT ctrlId, const bool bEnabled):
m_host(_host), m_ctrlId(ctrlId), m_bEnabled(bEnabled)
{
	CWindow ctrl_ = m_host.GetDlgItem(m_ctrlId);
	if (ctrl_)
	{
		m_bEnabled = !!ctrl_.IsWindowEnabled();
		if (m_bEnabled != bEnabled)
			ctrl_.EnableWindow(static_cast<BOOL>(bEnabled));
	}
}

CCtrlAutoState::~CCtrlAutoState(void)
{
	CWindow ctrl_ = m_host.GetDlgItem(m_ctrlId);
	if (ctrl_)
		ctrl_.EnableWindow(static_cast<BOOL>(m_bEnabled));
}
#endif