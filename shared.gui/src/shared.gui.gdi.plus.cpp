/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Feb-2019 at 7:20:17a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is time line custom WTL-based control GDI+ wrapper interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to serial port project library on 26-May-2019 at 11:07:02a, UTC+7, Phuket, Rawai, Sunday;
	Adopted to File Touch app on 11-Feb-2020 at 6:14:48p, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "StdAfx.h"
#include "shared.gui.gdi.plus.h"

using namespace shared::gui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace gui { namespace draw { namespace details {

	ULONG_PTR&
	       GdiPlus_Guard(void) {
		static ULONG_PTR token_ = NULL;
		return token_;
	}

	struct GdiPlus_ResourceInfo  {
		PVOID   __res;
		DWORD   __size;
		GdiPlus_ResourceInfo(void) : __res(0), __size(0) {}
	};

	HRESULT GdiPlus_GetResourcePtr(const ATL::_U_STRINGorID RID, const HMODULE hModule, GdiPlus_ResourceInfo& res__) {
		if (!RID.m_lpstr)
			return E_INVALIDARG;

		HRSRC hResource = ::FindResource(hModule, RID.m_lpstr, _T("PNG"));
		if (!hResource)
			return OLE_E_CANT_BINDTOSOURCE;

		res__.__size = ::SizeofResource(hModule, hResource);
		if (!res__.__size)
			return ERROR_EMPTY;

		res__.__res = ::LockResource(::LoadResource(hModule, hResource));
		if (!res__.__res)
			return STG_E_LOCKVIOLATION;
		else
			return S_OK;
	}

	class   GdiPlus_GlobalAlloca {
	private:
		HGLOBAL    m_hGlobal;
		HRESULT    m_hResult;
		DWORD      m_dwSize;
		PVOID      m_pVoid;
	public:
		// error C4596: '{ctor}': illegal qualified name in member declaration
		// GdiProvider_GlobalAlloca::
		GdiPlus_GlobalAlloca(const DWORD size__, const bool fixed__ = false):
			m_hGlobal(NULL),
			m_hResult(OLE_E_BLANK),
			m_dwSize (size__),
			m_pVoid  (NULL) {
			try {
				m_hGlobal = ::GlobalAlloc(fixed__ ? GMEM_FIXED : GMEM_MOVEABLE, m_dwSize);
				m_hResult = (m_hGlobal != NULL ? S_OK : E_OUTOFMEMORY);
				if (S_OK == m_hResult)
				{
					m_pVoid = ::GlobalLock(m_hGlobal);
					if (!m_pVoid)
						 m_hResult = HRESULT_FROM_WIN32(::GetLastError());
				}
			}
			catch(...) {
				m_hResult = E_OUTOFMEMORY;
			}
		}
		// error C4596: '{dtor}': illegal qualified name in member declaration
		// GdiProvider_GlobalAlloca::
		~GdiPlus_GlobalAlloca(void) {
			m_pVoid = NULL;
			if (m_hGlobal) {
				::GlobalUnlock(m_hGlobal);
				::GlobalFree(m_hGlobal);
				m_hGlobal = NULL;
			}
		}
	public:
		HGLOBAL    GetHandle(void) const     { return m_hGlobal; }
		HRESULT    GetLastResult(void) const { return m_hResult; }
		PVOID      GetPtr(void) const        { return m_pVoid;   }
		DWORD      GetSize(void) const       { return m_dwSize;  }
	private:
		GdiPlus_GlobalAlloca(const GdiPlus_GlobalAlloca&);
		GdiPlus_GlobalAlloca& operator=(const GdiPlus_GlobalAlloca&);
	};
}}}}
using namespace shared::gui::draw::details;
/////////////////////////////////////////////////////////////////////////////

CGdiPlusLib_Guard:: CGdiPlusLib_Guard(void) {}
CGdiPlusLib_Guard::~CGdiPlusLib_Guard(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CGdiPlusLib_Guard::Capture(void) {
	HRESULT hr_ = S_OK;
	if (NULL != GdiPlus_Guard())
		return (hr_ = __DwordToHresult(ERROR_ALREADY_INITIALIZED));

	Gdiplus::GdiplusStartupInput input_ = {0};
	Gdiplus::GdiplusStartup(&GdiPlus_Guard(), &input_, NULL);

	return  hr_;
}

HRESULT   CGdiPlusLib_Guard::Release(void){
	HRESULT hr_ = S_OK;
	if (NULL == GdiPlus_Guard())
		return (hr_ = __DwordToHresult(ERROR_INVALID_STATE));

	Gdiplus::GdiplusShutdown(GdiPlus_Guard()); GdiPlus_Guard() = NULL;

	return  hr_;
}

bool      CGdiPlusLib_Guard::Secured(void) const { return (NULL != GdiPlus_Guard()); }

/////////////////////////////////////////////////////////////////////////////

CGdiPlusPng_Loader:: CGdiPlusPng_Loader(void) {}
CGdiPlusPng_Loader::~CGdiPlusPng_Loader(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CGdiPlusPng_Loader::LoadImage(const ATL::_U_STRINGorID RID, Gdiplus::Bitmap*& ptr_ref) {
	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
	return this->LoadImage(RID, hInstance, ptr_ref);
}

HRESULT     CGdiPlusPng_Loader::LoadImage(const ATL::_U_STRINGorID RID, HBITMAP& _h_bit) {
	HRESULT hr_ = S_OK;
	if (_h_bit)
		return (hr_ = __DwordToHresult(ERROR_OBJECT_ALREADY_EXISTS));

	Gdiplus::Bitmap* pBitmap = NULL;
	hr_ = this->LoadImage(RID, pBitmap);
	if (FAILED(hr_))
		return hr_;

	if (NULL == pBitmap)
		return (hr_ = E_UNEXPECTED);

	pBitmap->GetHBITMAP(0, &_h_bit);
	try
	{
		delete pBitmap; pBitmap = NULL;
	}
	catch(...){ return E_OUTOFMEMORY; }
	return hr_;
}

HRESULT     CGdiPlusPng_Loader::LoadImage(const ATL::_U_STRINGorID RID, const HMODULE hModule, Gdiplus::Bitmap*& ptr_ref) {
	if (ptr_ref)
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);

	GdiPlus_ResourceInfo res__;
	HRESULT hr_ = GdiPlus_GetResourcePtr(RID, hModule, res__);
	if (FAILED(hr_))
		return hr_;

	GdiPlus_GlobalAlloca ga__(res__.__size, false);
	if (FAILED(hr_ = ga__.GetLastResult()))
		return hr_;

	::CopyMemory(ga__.GetPtr(), res__.__res, res__.__size);

	::ATL::CComPtr<IStream> pStream;
	hr_ = ::CreateStreamOnHGlobal(ga__.GetHandle(), FALSE, &pStream);
	if (SUCCEEDED(hr_)) {
		ptr_ref = Gdiplus::Bitmap::FromStream(pStream);
		if (ptr_ref) {
			hr_ = ((*ptr_ref).GetLastStatus() == Gdiplus::Ok ? S_OK : E_OUTOFMEMORY);
		}
		else {
			hr_ = E_OUTOFMEMORY;
		}
	}
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

CGdiPlusPng_Draw:: CGdiPlusPng_Draw(void) {}
CGdiPlusPng_Draw::~CGdiPlusPng_Draw(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CGdiPlusPng_Draw::Draw(const HDC& _h_dc, const RECT& _rc_area, Gdiplus::Bitmap* const _p_bit) {
	HRESULT hr_ = S_OK;
	if (NULL == _p_bit)
		return (hr_ = E_POINTER);
	if (NULL == _h_dc) return (hr_ = E_INVALIDARG);
	if (::IsRectEmpty(&_rc_area)) return (hr_ = OLE_E_INVALIDRECT);

	Gdiplus::Graphics gr__(_h_dc);

	const SIZE sz_bit = {
		static_cast<LONG>(_p_bit->GetWidth()), static_cast<LONG>(_p_bit->GetHeight())
	};
	const POINT pt_bit = {
		_rc_area.left + (__W(_rc_area) - sz_bit.cx) / 2,
		_rc_area.top  + (__H(_rc_area) - sz_bit.cy) / 2
	};
	
	gr__.DrawImage(_p_bit, pt_bit.x, pt_bit.y, sz_bit.cx, sz_bit.cy);
			
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

CGdiClass_Draw:: CGdiClass_Draw(void) {}
CGdiClass_Draw::~CGdiClass_Draw(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CGdiClass_Draw::Gradient(const HDC& _h_dc, const RECT& _rc_area, _c_clr& _from, _c_clr& _upto, const bool _b_horz) {
	_h_dc; _rc_area; _from; _upto; _b_horz;
	HRESULT hr_ = S_OK;
	if (NULL == _h_dc)
		return (hr_ = E_HANDLE);
	if (::IsRectEmpty(&_rc_area))
		return (hr_ = OLE_E_INVALIDRECT);

	TRIVERTEX        vert[2] = {0};
	GRADIENT_RECT    gRect   = {0};
	vert [0] .x      = _rc_area.left;
	vert [0] .y      = _rc_area.top;
	vert [0] .Red    = GetRValue(_from)<<8;
	vert [0] .Green  = GetGValue(_from)<<8;
	vert [0] .Blue   = GetBValue(_from)<<8;
	vert [0] .Alpha  = 0x0000;

	vert [1] .x      = _rc_area.right;
	vert [1] .y      = _rc_area.bottom; 
	vert [1] .Red    = GetRValue(_upto)<<8;
	vert [1] .Green  = GetGValue(_upto)<<8;
	vert [1] .Blue   = GetBValue(_upto)<<8;
	vert [1] .Alpha  = 0x0000;

	gRect.UpperLeft  = 0;
	gRect.LowerRight = 1;
	::GradientFill(_h_dc, vert, 2, &gRect, 1, _b_horz ? GRADIENT_FILL_RECT_H : GRADIENT_FILL_RECT_V);

	return  hr_;
}