/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Nov-2018 at 5:44:25p, UTC+7, Novosibirsk, Tulenina, Friday;
	This is USB Drive Detective (bitsphereinc.com) desktop app tabbed page base layout interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google push notification app on 9-Sep-2019 at 10:40:56p, UTC+7, Novosibirsk, Tulenina, Monday;
	Adopted to File Touch app on 11-Feb-2020 at 12:17:06p, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "StdAfx.h"
#include "shared.gui.page.layout.h"
#include "shared.gui.gdi.plus.h"

using namespace shared::gui::layout;
using namespace shared::gui::draw  ;

/////////////////////////////////////////////////////////////////////////////

CPage_Layout_Base:: CPage_Layout_Base(const HWND _host) : m_active(0) { *this = _host; }
CPage_Layout_Base::~CPage_Layout_Base(void) {}

/////////////////////////////////////////////////////////////////////////////

CPage_Layout_Base& CPage_Layout_Base::operator= (const HWND _v) { m_host = _v; return *this; }

/////////////////////////////////////////////////////////////////////////////

RECT        CPage_Layout_Base::operator=(const UINT _ctl_id) const {
	_ctl_id;
	RECT rec_ctl = {0};

	if (NULL == m_host.m_hWnd)
		return rec_ctl;

	CWindow ctl_itm = m_host.GetDlgItem(_ctl_id);
	if (NULL == ctl_itm.m_hWnd)
		return rec_ctl;
	else
		ctl_itm.GetWindowRect(&rec_ctl);

	::MapWindowPoints(HWND_DESKTOP, m_host, (LPPOINT)&rec_ctl, 0x2);

	return rec_ctl;
}

HWND        CPage_Layout_Base::operator<<(const UINT _ctl_id) { return m_host.GetDlgItem(m_active = _ctl_id); }

CAtlString  CPage_Layout_Base::operator<=(const UINT _ctl_id) const {
	CAtlString cs_cap;

	CWindow ctl_ = m_host.GetDlgItem(_ctl_id);
	if (ctl_)
		ctl_.GetWindowText(cs_cap);

	return cs_cap;
}

/////////////////////////////////////////////////////////////////////////////

CPage_Layout_Base::operator    RECT (void) const {
	RECT rec_cli = {0};

	if (NULL == m_host.m_hWnd)
		return rec_cli;

	m_host.GetWindowRect(&rec_cli);
	::MapWindowPoints(HWND_DESKTOP, m_host, (LPPOINT)&rec_cli, 0x2);

	return rec_cli;
}

/////////////////////////////////////////////////////////////////////////////

CPage_Layout:: CPage_Layout(const HWND _host) : m_host(_host) { m_error << __MODULE__ << S_OK;
if (NULL == m_host || !m_host.GetClientRect(&m_area))
::SetRectEmpty(&m_area);
}
CPage_Layout::~CPage_Layout(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CPage_Layout::AdjustBan (const CPage_Ban& _ban) const {
	m_error << __MODULE__ << S_OK;

	if (false == _ban)
		return (m_error = E_INVALIDARG);

	const RECT rc_image = this->ImageArea(_ban);
	const RECT rc_label = this->ImageText(_ban);

	CWindow((Lay_(m_host) << _ban.Image().Control())).MoveWindow(&rc_image);
	CWindow((Lay_(m_host) << _ban.Label().Label())).MoveWindow(&rc_label);

	return m_error;
}

HRESULT       CPage_Layout::AdjustImg (const CPage_Ava& _ava) const {
	m_error << __MODULE__ << S_OK;

	if (false == _ava)
		return (m_error = E_INVALIDARG);

	HBITMAP hBitmap = NULL;
	HRESULT hr_ = CGdiPlusPng_Loader().LoadImage (
			_ava.Resource(), hBitmap
	);
	if (SUCCEEDED(hr_))
	{
		::WTL::CStatic ctrl_ = (Lay_(m_host) << _ava.Control());
		if (ctrl_)
			ctrl_.SetBitmap(hBitmap);

		::DeleteObject(hBitmap);
		hBitmap = NULL;
	}

	return m_error;
}

TErrorRef     CPage_Layout::Error (void) const { return m_error; }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CPage_Layout::AlignComboHeightTo(const UINT _combo_id, const UINT _height) {
	m_error << __MODULE__ << S_OK;

	CWindow ctrl_ = Lay_(m_host) << _combo_id;
	if (NULL == ctrl_)
		return (m_error = __DwordToHresult(ERROR_NOT_FOUND));
	if (NULL == _height)
		return (m_error = __DwordToHresult(ERROR_INVALID_PARAMETER));

	static const INT _COMBOBOX_EDIT_MARGIN = 3;

	const INT pixHeight = _height - _COMBOBOX_EDIT_MARGIN * 2;
	LRESULT res__ = ERROR_SUCCESS;
	res__ = ctrl_.SendMessage(CB_SETITEMHEIGHT, (WPARAM)-1, pixHeight); if (res__ == CB_ERR) m_error = E_INVALIDARG;
	res__ = ctrl_.SendMessage(CB_SETITEMHEIGHT, (WPARAM) 0, pixHeight); if (res__ == CB_ERR) m_error = E_INVALIDARG;

	return m_error;
}

HRESULT       CPage_Layout::AlignCtrlsByHeight(const UINT _ctl_src  , const UINT _ctl_tag, const SIZE& _shifts) {

	CWindow  ctrl_0 = Lay_(m_host) << _ctl_src;
	const RECT rc_0 = Lay_(m_host) =  _ctl_src;
	
	CWindow  ctrl_1 = Lay_(m_host) << _ctl_tag;
	const RECT rc_1 = Lay_(m_host) =  _ctl_tag;

	if (ctrl_1 && !::IsRectEmpty(&rc_0))
	{
		RECT result   = rc_1;
		result.bottom = rc_0.bottom + _shifts.cy;
		ctrl_1.SetWindowPos(
			NULL, &result, SWP_NOZORDER|SWP_NOACTIVATE
		);
	}
	return S_OK; // ignors any possible errors;
}

/////////////////////////////////////////////////////////////////////////////
const
RECT     CPage_Layout::ImageArea(const CPage_Ban& _ban) const {
	RECT rc_ctl = (Lay_(m_host) = _ban.Image().Control());
	if (::IsRectEmpty(&rc_ctl))
		return rc_ctl;
	::OffsetRect(&rc_ctl, 0, m_area.bottom - rc_ctl.bottom - CPage_Layout::u_margin);
	return rc_ctl;
}
const
RECT     CPage_Layout::ImageText(const CPage_Ban& _ban) const {
	RECT rc_ctl = this->ImageArea(_ban);
	if (::IsRectEmpty(&rc_ctl))
		return rc_ctl;
	rc_ctl.left   = rc_ctl.right;
	rc_ctl.right  = m_area.right  - CPage_Layout::u_margin;
	return rc_ctl;
}