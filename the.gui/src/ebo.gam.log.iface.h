#ifndef _EBOGAMLOGIFACE_H_22902E86_AF94_4A93_A215_6E10FFD3A6A7_INCLUDED
#define _EBOGAMLOGIFACE_H_22902E86_AF94_4A93_A215_6E10FFD3A6A7_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Mar-2020 at 11:27:25p, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is Ebo Game Dyna Inject desktop app global logger interface declaration file.
*/

namespace ebo { namespace game { namespace gui {

	interface ILogger {

		virtual VOID ILog_Text (LPCTSTR _lp_sz_text) PURE;

	};

}}}

#endif/*_EBOGAMLOGIFACE_H_22902E86_AF94_4A93_A215_6E10FFD3A6A7_INCLUDED*/