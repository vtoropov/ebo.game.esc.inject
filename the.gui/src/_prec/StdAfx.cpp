/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-May-2019 at 6:19:45p, UTC+7, Phuket, Rawai, Tuesday;
	This is communication data exchange receiver desktop console app precompiled header creation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack virtual printer console on 5-Sep-2019 at 3:10:17p, UTC+7, Novosibirsk, Tulenina, Thursday;
	Adopted to Ebo Pack notify Google service system tray app on 7-Sep-2019 at 1:31:59p, UTC+7, Novosibirsk, Tulenina, Saturday;
	Adopted to Ebo Pack personal account app on 13-Oct-2019 at 9:25:48a, UTC+7, Novosibirsk, Light Coloured, Sunday;
	Adopted to Ebo Pack 2D shape draw app on 25-Feb-2020 at 11:08:49a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 3:45:34a, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/

#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)

#include "shared.gen.syn.obj.h"

namespace _internal {

	using shared::sys_core::CSyncObject;
	using ebo::game::gui::ILogger; typedef ILogger* TLoggerPtr;

	CSyncObject&  __int_get_guard(void) {
		static CSyncObject guard_;
		return guard_;
	}

	TLoggerPtr&  __int_get_log_ptr(void) {
		static TLoggerPtr p_log = NULL;
		return p_log;
	}

	class CLogger_Dummy : public ebo::game::gui::ILogger {
	public:
#pragma warning(disable:4481)
		virtual VOID ILog_Text (LPCTSTR _lp_sz_text) override sealed {
			_lp_sz_text; // does nothing;
		}
#pragma warning(default:4481)
	};

	CLogger_Dummy&  __int_get_log_dummy(void) {
		static CLogger_Dummy log_dum;
		return log_dum;
	}
}

namespace _in  {
	VOID   _set_logger_ptr(TLoggerPtr _p_log) {
		SAFE_LOCK(_internal::__int_get_guard());
		_internal::__int_get_log_ptr() = _p_log;
	}
}

namespace _out {

	TLoggerRef _get_logger_ref(void) {
		SAFE_LOCK(_internal::__int_get_guard());
		_in::TLoggerPtr p_logger = _internal::__int_get_log_ptr();

		if (p_logger == NULL) {
			return _internal::__int_get_log_dummy();
		}
		else // possible it's could be dangerous; but the pointer will be destroyed on app exit;
			return *p_logger;
	}
}

::WTL::CMessageLoop* get_MessageLoop(){ return _Module.GetMessageLoop(); }
HINSTANCE get_HINSTANCE() { return _Module.GetResourceInstance(); }