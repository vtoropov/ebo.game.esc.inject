#ifndef _EBOBOOUSREPRFDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED
#define _EBOBOOUSREPRFDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Dec-2018 at 5:05:16p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is USB Drive Detective (bitsphereinc.com) desktop app scan dialog interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google notify service on 14-Sep-2019 at 3:53:05p, UTC+7, Novosibirsk, Tulenina, Saturday;
	Adopted to Ebo Pack personal account app on 22-Oct-2019 at 11:53:25a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	Adopted to Ebo Game Dyna Inject project on 2-Mar-2020 at 3:03:42p, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "shared.gen.sys.err.h"
#include "shared.gen.proc.flt.h"
#include "shared.uix.frms.img.ban.h"

#include "ebo.gam.esc.flt.tab.set.h"

namespace ebo { namespace game { namespace gui {

	using shared::sys_core::CError;

	using ex_ui::frames::CImageBanner;
	using ex_ui::draw::defs::IRenderer;

	using shared::gui::ITabSetCallback;
	using shared::process::CProcessFilter;

	class CFilterAct {
	public:
		enum _act {
			e_none  = 0,
			e_clear = 1,
			e_apply = 2
		};
	};

	class CFilterDlg {
	private:
		class CFilterDlgImpl : public ::ATL::CDialogImpl<CFilterDlgImpl>, ITabSetCallback, IRenderer {
		                      typedef ::ATL::CDialogImpl<CFilterDlgImpl>  TDialog;
			friend class CFilterDlg;
		private:
			CImageBanner       m_banner;
			CProcessFilter     m_filter;
			CFilterTabSet      m_tabset;
		public :
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CFilterDlgImpl)
				MESSAGE_HANDLER     (WM_COMMAND   ,   OnBtnCmd )
				MESSAGE_HANDLER     (WM_DESTROY   ,   OnDestroy)
				MESSAGE_HANDLER     (WM_INITDIALOG,   OnInitDlg)
				MESSAGE_HANDLER     (WM_KEYDOWN   ,   OnKeyDown) // does not work in modeless dialogs;
				MESSAGE_HANDLER     (WM_SYSCOMMAND,   OnSysCmd )
				NOTIFY_CODE_HANDLER (TCN_SELCHANGE ,  OnNotify )
			END_MSG_MAP()

		public:
			 CFilterDlgImpl (void) ;
			~CFilterDlgImpl (void) ;

		private:
			LRESULT OnBtnCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnInitDlg(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnSysCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		private:
			LRESULT OnNotify (INT, LPNMHDR, BOOL&);

		private: // ITabSetCallback
#pragma warning(disable:4481)
			virtual HRESULT TabSet_OnDataChanged (const UINT pageId, const bool bChanged) override sealed;
			virtual HRESULT TabSet_OnDataRequest (const UINT ctrlId, RECT& _rc_ctrl) override sealed;
		private: // IRenderer
			HRESULT DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea) const override sealed;
#pragma warning(default:4481)
		};
	private:
		CFilterDlgImpl   m_dlg;

	public:
		 CFilterDlg (void);
		~CFilterDlg (void);

	public:
		CFilterAct::_act DoModal(void);
		const
		CProcessFilter&  Filter (void) const;
		CProcessFilter&  Filter (void)      ;

	private:
		CFilterDlg (const CFilterDlg&);
		CFilterDlg& operator= (const CFilterDlg&);
	};

}}}

#endif/*_EBOBOOUSREPRFDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED*/