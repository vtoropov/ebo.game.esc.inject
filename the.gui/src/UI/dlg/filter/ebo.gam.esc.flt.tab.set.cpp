/*
	Created by Tech_dog (VToropov) on 16-Jan-2016 at 3:58:22pm, GMT+7, Phuket, Rawai, Saturday;
	This is USB Drive Detective (bitsphereinc.com) app database option tab set interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 17-Aug-2018 at 6:57:00p, UTC+7, Novosibirsk, Rodniki, Tulenina, Friday;
	Adopted to Ebo Pack Google push notification app on 15-Sep-2019 at 1:12:36p, UTC+7, Novosibirsk, Tulenina, Sunday;
	Adopted to Ebo Pack personal account app on 22-Oct-2019 at 11:48:25a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	Adopted to Ebo Game Dyna Inject project on 2-Mar-2020 at 2:55:49p, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "StdAfx.h"
#include "ebo.gam.esc.flt.tab.set.h"
#include "ebo.gam.flt.dlg.res.h"

using namespace ebo::game::gui;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace game { namespace gui { namespace _impl
{
	class CFilterTabSet_Layout
	{
	private:
		RECT    m_area;
	public:
		CFilterTabSet_Layout(const RECT& rcArea) : m_area(rcArea) {}

	public:
		RECT    TabsArea (void) const {
			RECT rcTabs = m_area;
			::InflateRect(&rcTabs, -5, -3);
			return rcTabs;
		}
	};
}}}}

using namespace ebo::game::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CFilterTabSet:: CFilterTabSet(ITabSetCallback& _snk, CProcessFilter& _flt) :
	m_e_active(_pages::e_include), m_include(_snk, _flt), m_exclude(_snk, _flt) {
}
CFilterTabSet::~CFilterTabSet(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CFilterTabSet::Create(const HWND hParent, const RECT& rcArea) {
	hParent; rcArea;
	HRESULT hr_ = S_OK;

	if(FALSE == ::IsWindow(hParent) || ::IsRectEmpty(&rcArea))
		return (hr_ = E_INVALIDARG);

	CFilterTabSet_Layout layout(rcArea);
	RECT rcTabs  = layout.TabsArea();

	m_cTabCtrl.Create(
			hParent,
			rcTabs ,
			0,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
			WS_EX_CONTROLPARENT,
			IDC_EBO_GAM_ESC_FLT_DLG_TAB
		);
	m_cTabCtrl.SetFont(::ATL::CWindow(hParent).GetFont());

	INT nIndex = 0; nIndex;
	const DWORD dwFlags = SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW; // auto-placement is made by default implementation of create/initialize page handler;

	m_cTabCtrl.AddItem(m_include.GetPageTitle());
	{
		m_include.Create(m_cTabCtrl.m_hWnd);
		m_include.SetWindowPos(
				HWND_TOP,
				0, 0, 0 , 0,
				dwFlags
			);
		if (m_include.IsWindow()) m_include.Index(nIndex++);
	}
	m_cTabCtrl.AddItem(m_exclude.GetPageTitle());
	{
		m_exclude.Create(m_cTabCtrl.m_hWnd);
		m_exclude.SetWindowPos(
			HWND_TOP,
			0, 0, 0 , 0,
			dwFlags
		);
		if (m_exclude.IsWindow()) m_exclude.Index(nIndex++);
	}
	m_cTabCtrl.SetCurSel(m_e_active);

	this->UpdateLayout();

	return S_OK;
}

HRESULT       CFilterTabSet::Destroy(void) {
	if (m_include.IsWindow()){
		m_include.DestroyWindow();  m_include.m_hWnd = NULL;
	}
	if (m_exclude.IsWindow()){
		m_exclude.DestroyWindow();  m_exclude.m_hWnd = NULL;
	}
	return S_OK;
}

CFilterTabSet::_pages
              CFilterTabSet::Selected(void) const      { return m_e_active; }
HRESULT       CFilterTabSet::Selected(const _pages _v) {
	HRESULT hr_ = S_OK;
	const bool b_changed = (_v != m_e_active);
	m_e_active  = _v;
	if (b_changed && m_cTabCtrl.IsWindow()) {
		m_cTabCtrl.SetCurSel(m_e_active);
		this->UpdateLayout();
	}
	return  hr_;
}

void          CFilterTabSet::UpdateLayout(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();

	if (m_include.IsWindow()) m_include.ShowWindow(nTabIndex == m_include.Index() ? SW_SHOW : SW_HIDE);
	if (m_exclude.IsWindow()) m_exclude.ShowWindow(nTabIndex == m_exclude.Index() ? SW_SHOW : SW_HIDE);
}