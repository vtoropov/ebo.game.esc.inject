/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Dec-2018 at 6:16:09p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is USB Drive Detective (bitsphereinc.com) desktop app scan dialog interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google notify service on 14-Sep-2019 at 4:21:37p, UTC+7, Novosibirsk, Tulenina, Saturday;
	Adopted to Ebo Pack personal account app on 22-Oct-2019 at 11:58:53a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	Adopted to Ebo Game Dyna Inject project on 2-Mar-2020 at 3:09:25p, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "StdAfx.h"
#include "ebo.gam.esc.flt.dlg.h"
#include "ebo.gam.flt.dlg.res.h"

using namespace ebo::game::gui;

#include "shared.gui.page.layout.h"

using namespace shared::gui::layout;

#include "shared.gen.app.res.h"
#include "shared.uix.gdi.provider.h"

using namespace shared::user32;
using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace game { namespace gui { namespace _impl {

	class CFilterDlg_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_bann  = IDC_EBO_GAM_ESC_FLT_DLG_BAN,
			e_ctl_sepa  = IDC_EBO_GAM_ESC_FLT_DLG_SEP,
			e_ctl_clear = IDNO                       ,
			e_ctl_apply = IDOK                       , 
			e_ctl_close = IDCANCEL  
		};
	};
	typedef CFilterDlg_Ctl This_Ctl;

	class CFilterDlg_Initer {
	private:
		CWindow&     m_dlg_ref;

	public:
		 CFilterDlg_Initer (CWindow&  _dlg_ref) : m_dlg_ref(_dlg_ref) {}
		~CFilterDlg_Initer (void) {}

	public:
		HRESULT      OnCreate(void) {
			HRESULT hr_ = S_OK;
			return  hr_;
		}
	};

	class CFilterDlg_Layout {
	private:
		enum _e {
			e_gap = 0x04,
			e_ban = 0x3c,  // 60x750px is taken from PNG banner file; this is a hight of the banner;
		};

	private:
		CWindow&     m_dlg_ref;
		RECT         m_dlg_rec;

	public:
		CFilterDlg_Layout(CWindow&  _dlg_ref) : m_dlg_ref(_dlg_ref) {

			if (m_dlg_ref.GetClientRect(&m_dlg_rec) == FALSE)
				::SetRectEmpty(&m_dlg_rec);
		}

	public:
		RECT     BanArea(VOID) {
			__no_args;
			RECT rc_ban  = {0};
			CWindow ban_ = (Lay_(m_dlg_ref) << This_Ctl::e_ctl_bann);
			if (NULL == ban_)
				return rc_ban;

			rc_ban = (Lay_(m_dlg_ref) = This_Ctl::e_ctl_bann);
			return rc_ban;
		}

		RECT     TabArea(VOID) {
			const
			RECT rc_ban = this->BanArea();
			RECT rc_sep = (Lay_(m_dlg_ref) = This_Ctl::e_ctl_sepa);

			RECT rc_tab = {0};
			::SetRect(
				&rc_tab, m_dlg_rec.left + _e::e_gap, rc_ban.bottom - _e::e_gap/2, m_dlg_rec.right - _e::e_gap, rc_sep.top - _e::e_gap
			);

			return rc_tab;
		}
	};

	class CFilterDlg_Handler {
	private:
		CWindow&     m_dlg_ref;

	public:
		CFilterDlg_Handler(CWindow&  _dlg_ref) : m_dlg_ref(_dlg_ref) {}

	public:
		VOID   OnCommand(WORD wNotify, WORD ctrlId) {
			wNotify; ctrlId;

			switch (ctrlId) {
			case This_Ctl::e_ctl_apply:
			case This_Ctl::e_ctl_clear:
			case This_Ctl::e_ctl_close:
				::EndDialog(m_dlg_ref, ctrlId); break;
			}
		}
	};

}}}}

using namespace ebo::game::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CFilterDlg::CFilterDlgImpl:: CFilterDlgImpl(void) : 
	IDD(IDD_EBO_GAM_ESC_FLT_DLG), m_banner(This_Ctl::e_ctl_bann), m_tabset(*this, m_filter) {
	m_banner.Renderer(this);	
}
CFilterDlg::CFilterDlgImpl::~CFilterDlgImpl(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CFilterDlg::CFilterDlgImpl::OnBtnCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const WORD wNotify = HIWORD(wParam); wNotify;
	const WORD ctrlId  = LOWORD(wParam); ctrlId ;

	CFilterDlg_Handler hand_(*this);
	hand_.OnCommand(wNotify, ctrlId);

	return 0;
}

LRESULT   CFilterDlg::CFilterDlgImpl::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled = FALSE;
	m_tabset.Destroy();
	m_banner.Destroy();
	return 0;
}

LRESULT   CFilterDlg::CFilterDlgImpl::OnInitDlg(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	TDialog::CenterWindow();
	{
		CApplicationIconLoader   loader_(IDR_EBO_GAM_ESC_FLT_DLG_ICO);
		TDialog::SetIcon(loader_.DetachLargeIcon(), TRUE );
		TDialog::SetIcon(loader_.DetachSmallIcon(), FALSE);
	}
	HRESULT hr_ = m_banner.Create(TDialog::m_hWnd, This_Ctl::e_ctl_bann);
	if (SUCCEEDED(hr_)){}
	CFilterDlg_Initer   initer_(*this); initer_.OnCreate();

	CFilterDlg_Layout   layout_(*this);
	const RECT rc_tabs = layout_.TabArea();

	hr_ = m_tabset.Create(*this, rc_tabs);

	return 0;
}

LRESULT   CFilterDlg::CFilterDlgImpl::OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	switch (wParam)  {
	case VK_RETURN : {
			CWindow sav_but = (Lay_(*this)) << This_Ctl::e_ctl_apply;
			if (sav_but.IsWindowEnabled()) {
				CFilterDlg_Handler hand_(*this); hand_.OnCommand(0, This_Ctl::e_ctl_apply);
			}
		} break;
	}
	return 0;
}

LRESULT   CFilterDlg::CFilterDlgImpl::OnSysCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			TDialog::EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CFilterDlg::CFilterDlgImpl::OnNotify (INT idCtrl, LPNMHDR pnmh, BOOL& bHandled) {
	idCtrl; pnmh; bHandled = TRUE;
	m_tabset.UpdateLayout();
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CFilterDlg::CFilterDlgImpl::TabSet_OnDataChanged (const UINT pageId, const bool bChanged ) {
	pageId; bChanged;
	HRESULT hr_ = S_OK;

	CWindow bt_app = TDialog::GetDlgItem(IDOK);
	if (NULL != bt_app) {
		bt_app.EnableWindow(bChanged);
	}

	return  hr_;
}

HRESULT   CFilterDlg::CFilterDlgImpl::TabSet_OnDataRequest (const UINT ctrlId, RECT& _rc_ctrl) {
	ctrlId; _rc_ctrl;
	HRESULT hr_ = S_OK;
	switch (ctrlId) {
	case IDYES   : case IDOK:
	case IDCANCEL:
	{
		CWindow ctl_btn = TDialog::GetDlgItem(ctrlId);
		if (NULL != ctl_btn) {
			ctl_btn.GetWindowRect(&_rc_ctrl);
		}
	} break;
	default:
	hr_ = DISP_E_PARAMNOTFOUND;
	}
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CFilterDlg::CFilterDlgImpl::DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& _rc_area) const {
	hChild;
	if (NULL == hSurface)
		return __DwordToHresult(ERROR_INVALID_HANDLE);
	if (::IsRectEmpty(&_rc_area))
		return OLE_E_INVALIDRECT;
	// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getsyscolor
	CZBuffer dc_(hSurface, _rc_area);
	dc_.FillSolidRect(
		&_rc_area, ::GetSysColor(COLOR_3DFACE)
	);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CFilterDlg:: CFilterDlg(void) {}
CFilterDlg::~CFilterDlg(void) {}

/////////////////////////////////////////////////////////////////////////////

CFilterAct::_act CFilterDlg::DoModal(void) {

	const INT_PTR result = m_dlg.DoModal();
	switch (result) {
	case IDNO : return CFilterAct::e_clear;
	case IDOK : return CFilterAct::e_apply;
	default   : return CFilterAct::e_none ;  
	}
}

const
CProcessFilter& CFilterDlg::Filter (void) const { return m_dlg.m_filter; }
CProcessFilter& CFilterDlg::Filter (void)       { return m_dlg.m_filter; }