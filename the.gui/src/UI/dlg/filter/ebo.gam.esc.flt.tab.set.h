#ifndef _EBOBOOUSRPRFTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
#define _EBOBOOUSRPRFTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Apr-2018 at 3:51:20p, UTC+7, Novosibirsk, Rodniki, Monday;
	This is USB Drive Detective (bitsphereinc.com) app database option tab set interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 17-Aug-2018 at 6:55:35p, UTC+7, Novosibirsk, Rodniki, Tulenina, Friday;
	Adopted to Ebo Pack Google push notification app on 15-Sep-2019 at 1:05:15p, UTC+7, Novosibirsk, Tulenina, Sunday;
	Adopted to Ebo Pack personal account app on 22-Oct-2019 at 11:38:13a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	Adopted to Ebo Game Dyna Inject project on 2-Mar-2020 at 2:54:27p, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "shared.gui.page.base.h"
#include "shared.gen.proc.flt.h"

#include "ebo.gam.esc.flt.pag.1st.h"
#include "ebo.gam.esc.flt.pag.2nd.h"

namespace ebo { namespace game { namespace gui {

	using shared::gui::ITabSetCallback;
	using shared::process::CProcessFilter;

	class CFilterTabSet
	{
	public:
		enum _pages : INT {
			e_include  = 0,  // filter inclusion criteria;
			e_exclude  = 1,  // filter exclusion criteria;
		};
	private:
		WTL::CTabCtrl m_cTabCtrl;
		_pages        m_e_active;

	private: //tab page(s)
		CPageFilter_1st  m_include;
		CPageFilter_2nd  m_exclude;

	public:
		 CFilterTabSet (ITabSetCallback&, CProcessFilter&);
		~CFilterTabSet (void);

	public:
		HRESULT       Create  (const HWND hParent, const RECT& rcArea);
		HRESULT       Destroy (void)        ;
		_pages        Selected(void)   const;
		HRESULT       Selected(const _pages);
		VOID          UpdateLayout(void);
	public:
	};
}}}

#endif/*_EBOBOOUSRPRFTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED*/