/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Sep-2018 at 11:44:40a, UTC+7, Novosibirsk, Rodniki, Tulenina, Sunday;
	This is USB Drive Detective (bitsphereinc.com) main view options dialog page interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google push notification app on 15-Sep-2019 at 2:43:52p, UTC+7, Novosibirsk, Tulenina, Sunday;
	Adopted to Ebo Pack personal account app on 22-Oct-2019 at 8:29:40a, UTC+7, Novosibirsk, Light Coloured, Tuesday;
	Adopted to Ebo Game Dyna Inject project on 2-Mar-2020 at 7:47:14p, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "StdAfx.h"
#include "ebo.gam.esc.flt.pag.2nd.h"
#include "ebo.gam.flt.dlg.res.h"

using namespace ebo::game::gui;

#include "shared.gui.page.layout.h"

using namespace shared::gui::layout;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

#include "shared.reg.hive.h"

using namespace shared::registry;
/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace game { namespace gui { namespace _impl
{
	class CPageFilter_2nd_Ctl {
	public:
		enum _ctl : WORD {
			e_ava_ctl = IDC_EBO_GAM_ESC_FLT_2ND_AVA,    // page avatar ;
			e_ava_txt = IDC_EBO_GAM_ESC_FLT_2ND_CAP,    // page caption;
			e_exc_lst = IDC_EBO_GAM_ESC_FLT_2ND_LST,    // list of include parts/patterns;
			e_but_add = IDC_EBO_GAM_ESC_FLT_2ND_ADD,    // button of adding criterion to list;
			e_edt_add = IDC_EBO_GAM_ESC_FLT_2ND_PRT,    // criterion edit box control;
		};
	};
	typedef CPageFilter_2nd_Ctl This_Ctl;
	typedef ::std::vector<CAtlString>  TFltValues;

	class CPageFilter_2nd_Data {
	private:
		CError         m_error;
		CRegistryStg   m_registry;

	public:
		CPageFilter_2nd_Data (void) : m_registry(HKEY_CURRENT_USER, CRegOptions::eAutoCompletePath) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		TFltValues   Default(void) const {
			TFltValues  def_vals;
			def_vals.push_back(CAtlString(_T("svchost")));
			return   def_vals;
		}
		TErrorRef    Error  (void) const { return m_error; }
		
	};

	class CPageFilter_2nd_Layout
	{
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPageFilter_2nd_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ctl, This_Ctl::e_ava_ctl) );

		/*	CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_inf_img, This_Ctl::e_inf_img) );
			CWindow  inf_ctl = Lay_(m_page_ref) << This_Ctl::e_inf_txt;
			if (0 != inf_ctl) {
				CAtlString cs_info;   cs_info.LoadString(This_Ctl::e_inf_txt);
				inf_ctl.SetWindowText(cs_info);
			}*/
			CWindow  cap_ava = Lay_(m_page_ref) << This_Ctl::e_ava_txt;
			if (0 != cap_ava) {
				cap_ava.SetFont(CTabPageBase::SectionCapFont());
			}
			/*CWindow  cap_ctl = Lay_(m_page_ref) << This_Ctl::e_prv_cap;
			if (0 != cap_ctl) {
				cap_ctl.SetFont(CTabPageBase::SectionCapFont());
			}*/
		}
		VOID   OnSize  (void) {
			/*CPage_Ban pan_(This_Ctl::e_inf_img, This_Ctl::e_inf_txt);
			pan_.Image().Resource() = This_Ctl::e_inf_img;
			pan_.Label().Resource() = This_Ctl::e_inf_txt;
			CPage_Layout(m_page_ref).AdjustBan(
				pan_
			);*/
		}
	};

	class CPageFilter_2nd_Init
	{
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;
		
	public:
		CPageFilter_2nd_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (CProcessFilter& _flt) {
			m_error << __MODULE__ << S_OK; 

			CListViewCtrl lst_exc = Lay_(m_page_ref) << This_Ctl::e_exc_lst;
			if (lst_exc) {
				lst_exc.SetExtendedListViewStyle(LVS_EX_FULLROWSELECT|LVS_EX_DOUBLEBUFFER|LVS_EX_CHECKBOXES, 0);
				lst_exc.AddColumn(_T(" ")       , 0); lst_exc.SetColumnWidth(0,  35);
				lst_exc.AddColumn(_T("Patterns"), 1); lst_exc.SetColumnWidth(1, 210);

				TFltValues defs_ = CPageFilter_2nd_Data().Default();

				if (_flt.Exclude().Empty()) {
					for (size_t i_ = 0; i_ <  defs_.size(); i_++) {
						_flt.Exclude().Append(defs_[i_], false);
						lst_exc.AddItem(static_cast<INT>(i_), 0, _T(""), 1);
						lst_exc.AddItem(static_cast<INT>(i_), 1, defs_[i_].GetString());
					}
				}
				else {
					INT ndx_ = 0; 
					const TCriteria& cri_ = _flt.Exclude().Criteria();
					for ( TCriteria::const_iterator it_ = cri_.begin(); it_ != cri_.end(); ++it_) {

						lst_exc.AddItem(ndx_, 0, _T(""), 1);
						lst_exc.AddItem(ndx_, 1, it_->first.GetString());

						lst_exc.SetCheckState(ndx_, it_->second);
						ndx_ += 1;
					}
				}

				lst_exc.SelectItem(0);
				lst_exc.SetFocus();

				::SendMessage(lst_exc,  WM_CHANGEUISTATE, MAKELONG(UIS_SET, UISF_HIDEFOCUS), 0);
				::SendMessage(lst_exc,  WM_UPDATEUISTATE, MAKELONG(UIS_SET, UISF_HIDEFOCUS), 0);
			}

			return m_error;
		}

		HRESULT   OnUpdate  (LPCTSTR _lp_sz_param) {
			m_error << __MODULE__ << S_OK;

			CListViewCtrl lst_exc = Lay_(m_page_ref) << This_Ctl::e_exc_lst;
			if (NULL == lst_exc)
				return (m_error = __DwordToHresult(ERROR_NOT_FOUND));

			if (NULL == _lp_sz_param || !::_tcslen(_lp_sz_param))
				return (m_error = E_INVALIDARG);

			INT n_ndx = lst_exc.GetItemCount();

			lst_exc.AddItem(n_ndx, 0, _T(""), 1);
			lst_exc.AddItem(n_ndx, 1, _lp_sz_param);
			lst_exc.SetCheckState(n_ndx, TRUE);

			return m_error;
		}
	};

	class CPageFilter_2nd_Handler {
	private:
		CWindow&         m_page_ref;
		CProcessFilter&  m_proc_flt;

	public:
		CPageFilter_2nd_Handler(CWindow& dlg_ref, CProcessFilter& _flt) : m_page_ref(dlg_ref), m_proc_flt(_flt) {}

	public:
		BOOL   OnCommand (const WORD u_ctl_id, const WORD u_ntf_cd) {
			u_ntf_cd; u_ctl_id;

			BOOL   bHandled = FALSE;

			switch (u_ctl_id) {
			case This_Ctl::e_but_add : {

					CWindow edt_add = Lay_(m_page_ref) << This_Ctl::e_edt_add;
					if (NULL == edt_add)
						return bHandled;
					
					CAtlString cs_data; edt_add.GetWindowText(cs_data);
					m_proc_flt.Exclude().Append((LPCTSTR)cs_data);

					CPageFilter_2nd_Init(m_page_ref).OnUpdate((LPCTSTR)cs_data);

					return (bHandled = TRUE);
				} break;
			}

			bHandled = (EN_CHANGE == u_ntf_cd); if (FALSE==bHandled) return bHandled;

			WTL::CEdit edt_ctl = Lay_(m_page_ref) << u_ctl_id;
			if (NULL== edt_ctl)
				return (bHandled = FALSE);

			CAtlString cs_data; edt_ctl.GetWindowText(cs_data); if (cs_data.GetLength()) cs_data.Trim();
			const bool b_enabled = (0 < cs_data.GetLength());

			WTL::CButton bt_add = Lay_(m_page_ref) << This_Ctl::e_but_add;
			if ( NULL != bt_add )
				bt_add.EnableWindow(static_cast<BOOL>(b_enabled));
			return bHandled;
		}

		BOOL   OnNotify  (WPARAM _w_p, LPARAM _l_p) {
			_w_p; _l_p;

			BOOL bHandled = FALSE;

			const LPNMHDR p_header = reinterpret_cast<LPNMHDR>(_l_p);
			if ( NULL == p_header )
				return bHandled;

			if (LVN_ITEMCHANGED != p_header->code)
				return bHandled;

			CListViewCtrl lv_cri = Lay_(m_page_ref) << This_Ctl::e_exc_lst;
			if ( NULL  == lv_cri )
				return bHandled;

			const INT n_cnt = lv_cri.GetItemCount();
			for (INT i_ = 0; i_ < n_cnt; i_++) {
				const bool bChecked = lv_cri.GetCheckState(i_);
				TCHAR sz_buffer[MAX_PATH] = {0};

				lv_cri.GetItemText(i_, 1, sz_buffer, _countof(sz_buffer) - 1);

				m_proc_flt.Exclude().Use(sz_buffer, bChecked);
			}

			return (bHandled = TRUE);
		}
	};
}}}}
using namespace ebo::game::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageFilter_2nd:: CPageFilter_2nd(ITabSetCallback& _set_snk, CProcessFilter& _flt):
       TPage(IDD_EBO_GAM_ESC_FLT_2ND_PAG, *this, _set_snk) , m_filter(_flt) {
}

CPageFilter_2nd::~CPageFilter_2nd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageFilter_2nd::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TPage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPageFilter_2nd::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TPage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPageFilter_2nd_Layout layout_(*this, TPage::m_set_snk);
	layout_.OnCreate();

	CPageFilter_2nd_Init init_(*this);
	init_.OnCreate(m_filter);
#if (0)
	TFltValues vals_ = CPageFilter_2nd_Data().Default();
	for (size_t i_ = 0; i_ < vals_.size(); i_++) {
		m_filter.Exclude().Append(vals_[i_].GetString());
	}
#endif
	TPage::m_bInited = true;
	return l_res;
}

LRESULT    CPageFilter_2nd::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TPage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageFilter_2nd_Layout layout_(*this, TPage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageFilter_2nd::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
			if (!TPage::m_bInited)
				break;
			const WORD u_ntf_cd = HIWORD(wParam);
			const WORD u_ctl_id = LOWORD(wParam);

			CPageFilter_2nd_Handler handler_(*this, m_filter);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(u_ctl_id, u_ntf_cd);
			if (bHandled == TRUE ) {
				TPage::m_set_snk.TabSet_OnDataChanged(0, true);
			}
		} break;
	case WM_NOTIFY : {
		const LPNMHDR p_header = reinterpret_cast<LPNMHDR>(lParam);
		if ( NULL ==  p_header)
			return FALSE;

		if (TPage::m_bInited && LVN_ITEMCHANGED == p_header->code) {
			CPageFilter_2nd_Handler hand_(*this, m_filter);
			bHandled = hand_.OnNotify(wParam, lParam);
		}

	} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageFilter_2nd::IsChanged   (void) const {
#if(0)
	CWindow lvw_wnd(Lay_(*this) << This_Ctl::e_col_vis);
	CListViewColEnum cols_ex(lvw_wnd);
#endif
	return false;
}

CAtlString CPageFilter_2nd::GetPageTitle(void) const {  static CAtlString cs_title(_T(" Exclude ")); return cs_title; }

VOID       CPageFilter_2nd::UpdateData  (const DWORD _opt) {
	_opt;
}