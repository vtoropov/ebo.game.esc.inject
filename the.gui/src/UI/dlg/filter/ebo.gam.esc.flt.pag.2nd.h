#ifndef _EBOBOOUSRPRFPAG2ND_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
#define _EBOBOOUSRPRFPAG2ND_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Sep-2018 at 11:38:45a, UTC+7, Novosibirsk, Rodniki, Tulenina, Sunday;
	This is USB Drive Detective (bitsphereinc.com) main view options dialog page interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google push notification app on 15-Sep-2019 at 1:26:03p, UTC+7, Novosibirsk, Tulenina, Sunday;
	Adopted to Ebo Pack personal account app on 22-Oct-2019 at 8:19:34a, UTC+7, Novosibirsk, Light Coloured, Tuesday;
	Adopted to Ebo Game Dyna Inject project on 2-Mar-2020 at 7:55:26p, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "shared.gui.page.base.h"
#include "shared.gen.proc.flt.h"

namespace ebo { namespace game { namespace gui {

	using shared::gui::ITabPageEvents ;
	using shared::gui::ITabSetCallback;
	using shared::gui::CTabPageBase;

	using shared::process::CProcessFilter;
	using shared::process::TCriteria;

	class CPageFilter_2nd : public CTabPageBase, public  ITabPageEvents {
	                       typedef CTabPageBase  TPage;
	private:
		CProcessFilter&     m_filter;
	public :
		 CPageFilter_2nd(ITabSetCallback&, CProcessFilter&);
		~CPageFilter_2nd(void);

	private:
#pragma warning(disable:4481)
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	private: // ITabPageEvents
		virtual LRESULT     TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	public : // TPage
		virtual bool        IsChanged   (void) const override sealed;
		virtual CAtlString  GetPageTitle(void) const override sealed;
		virtual VOID        UpdateData  (const DWORD _opt = 0) override sealed; // temporarily disabled due to possible invocation after control(s) destroying;
#pragma warning(default:4481)
	public:
	};

}}}

#endif/*_EBOBOOUSRPRFPAG2ND_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED*/