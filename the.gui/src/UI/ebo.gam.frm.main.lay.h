#ifndef _EBOBOOFRMMAINLAY_H_B915121B_1007_4EF9_ADE3_EFD8BB4B3169_INCLUDED
#define _EBOBOOFRMMAINLAY_H_B915121B_1007_4EF9_ADE3_EFD8BB4B3169_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Aug-2018 at 8:10:08a, UTC+7, Novosibirsk, Rodniki, Tulenina, Friday;
	This is USB Drive Detective (bitsphereinc.com) desktop application main form layout interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 27-Oct-2019 at 3:05:30p, UTC+7, Novosibirsk, Tulenina, Sunday;
	Adopted to Ebo Pack 2D shape draw app on 25-Feb-2020 at 11:43:12a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 5:07:44a, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "ebo.gam.frm.main.h"

namespace ebo { namespace game { namespace gui { namespace _impl {

	using ebo::game::gui::CMainForm;

	class CMainForm_Layout {
	private:
		RECT       m_area;
		CMainForm& m_main;
		
	public:
		CMainForm_Layout(CMainForm& _main);

	public:
		RECT    GetViewRect (void) const;                // calculates main view panel rectangle;
		VOID    RecalcLayout(void);                      // adjust positions of all UI elements of main window;

	public:
		static RECT   AdjustRect(const RECT& _rc);       // checks a rectangle against boundaries of main monitor work screen area;
		static RECT   CenterArea(const RECT& _rc);
		static RECT   FormDefPlace(void);

	private:
		static RECT   GetAvailableArea(void);
	};
}}}}

#endif/*_EBOBOOFRMMAINLAY_H_B915121B_1007_4EF9_ADE3_EFD8BB4B3169_INCLUDED*/