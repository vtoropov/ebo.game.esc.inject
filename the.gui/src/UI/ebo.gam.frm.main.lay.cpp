/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Aug-2018 at 4:23:18p, UTC+7, Novosibirsk, Rodniki, Tulenina, Friday;
	This is USB Drive Detective (bitsphereinc.com) desktop application main form layout interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 27-Oct-2019 at 3:09:59p, UTC+7, Novosibirsk, Tulenina, Sunday;
	Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 5:08:22a, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "StdAfx.h"
#include "ebo.gam.frm.main.lay.h"
#include "ebo.gam.esc.res.h"

using namespace ebo::game::gui;
using namespace ebo::game::gui::_impl;

#include "shared.uix.gdi.provider.h"
#include "shared.uix.frms.img.ban.h"

using namespace ex_ui::draw;
using namespace ex_ui::frames;

#include "shared.gui.page.layout.h"

using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

CMainForm_Layout::CMainForm_Layout(CMainForm& _main) : m_main(_main) {

	::SetRectEmpty(&m_area);
	if (m_main.Window().IsWindow())
		m_main.Window().GetClientRect(&m_area);
}

/////////////////////////////////////////////////////////////////////////////

RECT    CMainForm_Layout::GetViewRect(void) const {

	RECT rc_view = m_area;

	const CSelector& sel_     = m_main.Selector ();

	if (sel_.Window().IsWindow() && sel_.Window().IsWindowVisible()) {
		RECT rc_sel = Lay_(m_main.Window()) = sel_.Id();
		if ( rc_sel.left == 0) // attached to the left side;
			rc_view.left  = rc_sel.right;
		else
			rc_view.right = rc_sel.left;
	}
	return rc_view;
}

VOID    CMainForm_Layout::RecalcLayout(void) {

	CWindow main_frm = m_main.Window();

	if (main_frm.IsWindow() == FALSE)
		return;

	RECT rc_area = m_area;
#if(1)
	if (FALSE == main_frm.GetClientRect(&rc_area)) // is already assigned via class constructor;
		return;
#endif
	if (::IsRectEmpty(&rc_area))
		return;
	//
	// (1) gets banner size;
	//
	// (4) selector placement;
	m_main.Selector().Layout() << rc_area;
	//
	// (5) gets main frame views' rectangle and moves them;
	//
	const RECT rc_view = this->GetViewRect();
	if (m_main.LogView ().IsValid()) { m_main.LogView ().UpdateLayout(rc_view); }
	if (m_main.ProcView().IsValid()) { m_main.ProcView().UpdateLayout(rc_view); }
}

/////////////////////////////////////////////////////////////////////////////

RECT   CMainForm_Layout::AdjustRect(const RECT& _rc) {

	RECT rc_adjusted = _rc;
	const RECT rc_area = CMainForm_Layout::GetAvailableArea();

	if (_rc.left < rc_area.left)
		::OffsetRect(&rc_adjusted, rc_area.left - _rc.left, 0);

	if (_rc.top  < rc_area.top )
		::OffsetRect(&rc_adjusted, 0, rc_area.top - _rc.top);

	const LONG x_delta = rc_adjusted.right  - rc_area.right ;  if (0 < x_delta) rc_adjusted.right  = rc_area.right ;
	const LONG y_delta = rc_adjusted.bottom - rc_area.bottom;  if (0 < y_delta) rc_adjusted.bottom = rc_area.bottom;

	return rc_adjusted;
}

RECT   CMainForm_Layout::CenterArea(const RECT& _rc) {

	const RECT  screen_ = CMainForm_Layout::GetAvailableArea();
	const POINT left_top = {
		(screen_.right - screen_.left) / 2  - (_rc.right - _rc.left) / 2,
		(screen_.bottom - screen_.top) / 2  - (_rc.bottom - _rc.top) / 2,
	};
	RECT center_ = {
		left_top.x,
		left_top.y,
		left_top.x + (_rc.right - _rc.left),
		left_top.y + (_rc.bottom - _rc.top)
	};

	return center_;
}

RECT   CMainForm_Layout::FormDefPlace(void) {

	RECT rc_ = {0, 0, 700, 400};

	return CMainForm_Layout::CenterArea(rc_);
}

/////////////////////////////////////////////////////////////////////////////

RECT   CMainForm_Layout::GetAvailableArea(void)
{
	const POINT ptZero = {0};
	const HMONITOR hMonitor = ::MonitorFromPoint(ptZero, MONITOR_DEFAULTTOPRIMARY);
	MONITORINFO mInfo  = {0};
	mInfo.cbSize = sizeof(MONITORINFO);
	::GetMonitorInfo(hMonitor, &mInfo);
	return mInfo.rcWork;
}