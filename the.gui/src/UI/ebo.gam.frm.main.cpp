	/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Jul-2018 on 10:31:47a, UTC+7, Novosibirsk, Tulenina, Friday;
	This is Ebo Bot HTTP/NET test desktop application main form interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 18-Oct-2019 at 7:19:46a, UTC+7, Novosibirsk, Tulenina, Friday;
	Adopted to Ebo Pack 2D shape draw app on 25-Feb-2020 at 11:34:36a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 5:04:47a, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "StdAfx.h"
#include "ebo.gam.frm.main.h"
#include "ebo.gam.frm.main.lay.h"

#include "ebo.gam.esc.flt.dlg.h"
#include "ebo.gam.esc.res.h"

using namespace ebo::game::gui;

#include "shared.gen.app.res.h"
#include "shared.gen.app.obj.h"

using namespace shared::user32;

#include "shared.uix.gen.clrs.h"
#include "shared.uix.gdi.renderer.h"

using namespace ex_ui::draw;
using namespace ex_ui::draw::renderers;
using namespace ex_ui::controls;
/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace game { namespace gui { namespace _impl {

	class CMainForm_Init {
	private:
		CMainForm&  m_main ;
		CError      m_error;

	public:
		CMainForm_Init(CMainForm& _main_frm) : m_main(_main_frm) { m_error << __MODULE__ << S_OK;}

	public:
		TErrorRef   Error (void) const { return m_error; }

		HRESULT     OnCreate(void){
			m_error << __MODULE__ << S_OK;

			CSelector& sel_ = m_main.Selector();
			CCmpClrs_Enum clrs_;
			const CCmpClrs& pair_ = clrs_[0];

			sel_.Format().BackColour(pair_.Dark());
			sel_.Format().ForeColour(pair_.Light());
			sel_.Format().ItmImages (IDR_EBO_GAM_PROC_IMG);
			sel_.Layout().Width(150);
			sel_.Layout().HAlign() = eHorzAlign::eLeft;
			sel_.Layout().Margins().Left() = 7;

			static LPCTSTR lp_sz_caps[] = {
				_T("Processes"), _T("Scanning"), _T("Filter"), _T("Log")
			};
			static UINT  u_itm_cmds[] = {
				IDC_EBO_GAM_PROC_LST, IDC_EBO_GAM_PROC_SCN, IDC_EBO_GAM_PROC_FLT, IDC_EBO_GAM_PROC_LOG
			};

			CSel_Item_Enum& items_= sel_.Items();
			for (UINT i_ = 0; i_ < _countof(lp_sz_caps) && i_ < _countof(u_itm_cmds); i_ ++) {
				CSel_Item itm_;
				itm_.Id () = u_itm_cmds[i_];
				itm_.Cap() = lp_sz_caps[i_];

				switch (i_) {
				case 0: { itm_.Img() = 4;} break;
				case 1: { itm_.Img() = 3;} break;
				case 3: { itm_.Img() = 5;} break;
				}

				items_.Append(itm_);
			}
			RECT rc_ = {0}; m_main.Window().GetClientRect(&rc_);
			HRESULT hr_ = sel_.Create(m_main.Window(), rc_, _T("ebo_pack::controls::selector"), IDC_EBO_GAM_PROC_LST);
			if (FAILED(hr_)) {
				m_error = sel_.Error();
				m_error.Show();
			}
			items_.Selected(0);
			return m_error;
		}
		VOID        SetMenu(void) { }
	};
}}}}
using namespace ebo::game::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CMainForm::CMainFormWnd:: CMainFormWnd(CMainForm& _frm) : m_form(_frm), m_selector(*this) { m_error << __MODULE__ << S_OK; }
CMainForm::CMainFormWnd::~CMainFormWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CMainForm::CMainFormWnd::OnCreate (UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	wParam; lParam; bHandled = TRUE;

	CApplicationIconLoader loader_(IDR_EBO_GAM_ICO);
	{
		TWindow::SetIcon(loader_.DetachLargeIcon(), TRUE );
		TWindow::SetIcon(loader_.DetachSmallIcon(), FALSE);
	}
	const CApplication& the_app = GetAppObjectRef();

	CAtlString cs_title;
	cs_title.Format (
		_T("%s [ver.%s]"),
		(LPCTSTR)the_app.Version().CustomValue(_T("Comments"   )),
		(LPCTSTR)the_app.Version().CustomValue(_T("FileVersion"))
	);

	CMainForm_Init init_(m_form);
	HRESULT hr_ =  init_.OnCreate();
	if (FAILED(hr_)) {
		m_error = init_.Error(); m_error.Show();
	}
	RECT rc_view = CMainForm_Layout(m_form).GetViewRect();

	hr_ = m_log_view.Create(*this, rc_view, false);
	hr_ = m_pro_view.Create(*this, rc_view, true );

	CMainForm_Layout(m_form).RecalcLayout();

	TWindow::SetWindowText(cs_title.GetString());
	return 0;
}

LRESULT   CMainForm::CMainFormWnd::OnDestroy(UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	wParam; lParam; bHandled = FALSE;
	if (m_log_view.IsValid())
		m_log_view.Destroy();
	if (m_pro_view.IsValid())
		m_pro_view.Destroy();
	if (m_selector.Window ().IsWindow()) {
		m_selector.Destroy();
	}
	return 0;
}

LRESULT   CMainForm::CMainFormWnd::OnDraw   (UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	wParam; lParam; bHandled = FALSE;
	return 0;
}

LRESULT   CMainForm::CMainFormWnd::OnErase  (UINT, WPARAM wParam, LPARAM, BOOL& b_hand) {
	b_hand = TRUE; wParam;
	static bool  b_fst_time = false;
	if (false == b_fst_time) {
		HBRUSH brush = ::CreateSolidBrush(RGB(0, 0, 0));
		::SetClassLongPtr(*this, GCLP_HBRBACKGROUND, (LONG_PTR)brush);
		b_fst_time = false;
	}

	static const LRESULT l_is_drawn = 1;
	{
		RECT rc_view = CMainForm_Layout(m_form).GetViewRect();

		CZBuffer buf_((HDC)wParam, rc_view);
		buf_.FillSolidRect(&rc_view, 0);
	}
	return l_is_drawn;
}

LRESULT   CMainForm::CMainFormWnd::OnMnuInit(UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	wParam; lParam; bHandled = TRUE;
	return 0;
}

LRESULT   CMainForm::CMainFormWnd::OnSize   (UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	wParam; lParam; bHandled = FALSE;
	RECT rc_ = {0, 0, LOWORD(lParam), HIWORD(lParam)}; rc_;

	CMainForm_Layout(m_form).RecalcLayout();

	return 0;
}

LRESULT   CMainForm::CMainFormWnd::OnSysCmd (UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	wParam; lParam; bHandled = FALSE;
	switch (wParam) {
	case SC_CLOSE:  {
		::PostQuitMessage(0);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CMainForm::CMainFormWnd::ISelector_OnBkgChanged (void) { return S_OK;  }
HRESULT   CMainForm::CMainFormWnd::ISelector_OnFontChanged(void) { return S_OK;  }
HRESULT   CMainForm::CMainFormWnd::ISelector_OnItemClick  (const UINT _u_itm_id) {
	switch (_u_itm_id) {
	case IDC_EBO_GAM_PROC_LST: {

			m_pro_view.IsVisible(true );
			m_log_view.IsVisible(false);

		} break;
	case IDC_EBO_GAM_PROC_SCN: {

			m_pro_view.IsVisible(true );
			m_log_view.IsVisible(false);
			m_pro_view.Scan();
			
		} break;
	case IDC_EBO_GAM_PROC_FLT: {

			m_pro_view.IsVisible(true );
			m_log_view.IsVisible(false);
			m_pro_view.Filter();

		} break;
	case IDC_EBO_GAM_PROC_LOG: {
			m_log_view.IsVisible(true );
			m_pro_view.IsVisible(false);
		} break;
	}
	return S_OK;
}
HRESULT   CMainForm::CMainFormWnd::ISelector_OnItemImages (const UINT _u_res_id) { _u_res_id; return S_OK; }

/////////////////////////////////////////////////////////////////////////////

LRESULT   CMainForm::CMainFormWnd::OnCommand(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled) {
	bHandled = TRUE; wNotifyCode; hWndCtl; wID;
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CMainForm:: CMainForm(void) : m_wnd(*this) { }
CMainForm::~CMainForm(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CMainForm::Create  (const HWND hParent, const RECT& rcArea)
{
	if (m_wnd.IsWindow())
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	if (::IsRectEmpty(&rcArea))
		return OLE_E_INVALIDRECT;

	RECT rc_ = CMainForm_Layout::CenterArea(rcArea);
	HWND hView = m_wnd.Create(hParent, &rc_, NULL, /*WS_VISIBLE|*/WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN);
	if (!hView)
		return HRESULT_FROM_WIN32(::GetLastError());
	else
		return S_OK;
}

HRESULT       CMainForm::Destroy (void) {
	if (m_wnd.IsWindow())
		m_wnd.SendMessage(WM_CLOSE);
	return S_OK;
}
const
CLogView &    CMainForm::LogView (void) const { return m_wnd.m_log_view; }
CLogView &    CMainForm::LogView (void)       { return m_wnd.m_log_view; }
const
CProcView&    CMainForm::ProcView(void) const { return m_wnd.m_pro_view; }
CProcView&    CMainForm::ProcView(void)       { return m_wnd.m_pro_view; }
const
CSelector&    CMainForm::Selector(void) const { return m_wnd.m_selector; }
CSelector&    CMainForm::Selector(void)       { return m_wnd.m_selector; }

HRESULT       CMainForm::Visible (const bool _vis) {

	HRESULT hr_ = S_OK;
	if (m_wnd.IsWindow())
		m_wnd.ShowWindow(_vis ? SW_SHOW : SW_HIDE);
	else
		hr_ = OLE_E_BLANK;

	return hr_;
}

const
CWindow&      CMainForm::Window  (void) const { return m_wnd; }
CWindow&      CMainForm::Window  (void)       { return m_wnd; }

/////////////////////////////////////////////////////////////////////////////

CMainForm::operator  CWindow&    (void) { return this->Window(); }