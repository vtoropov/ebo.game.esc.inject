#ifndef __EBOBOOFRMMAIN_H_DFF7313A_9C01_46B4_8BD1_E8DCED919AB7_INCLUDED
#define __EBOBOOFRMMAIN_H_DFF7313A_9C01_46B4_8BD1_E8DCED919AB7_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Jul-2018 at 10:07:10a, UTC+7, Novosibirsk, Tulenina, Friday;
	This is Ebo Bot HTTP/NET test desktop application main form interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 18-Oct-2019 at 2:16:31a, UTC+7, Novosibirsk, Tulenina, Friday;
	Adopted to Ebo Pack 2D shape draw app on 25-Feb-2020 at 11:30:55a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 5:02:38a, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "shared.gen.sys.err.h"
#include "ebo.frm.main.view.pro.h"
#include "ebo.frm.main.view.log.h"

#include "shared.uix.gdi.draw.bkg.h"
#include "shared.uix.ctrl.selector.h"

#include "shared.uix.gen.clrs.h"
#include "IGridInterface.thm.h"

namespace ebo { namespace game { namespace gui {

	using shared::sys_core::CError;

	using ex_ui::draw::CBitmapInfo    ;
	using ex_ui::draw::CBkgImages     ;
	using ex_ui::draw::CBkgLayout     ;
	using ex_ui::draw::CBkgRenderer   ;
	using ex_ui::draw::CBkgSegment    ;
	using ex_ui::draw::CBkgSegmentMap ;

	using ex_ui::controls::CSelector;
	using ex_ui::controls::ISelector_Events;

	using ebo::game::gui::view::CProcView;
	using ebo::game::gui::view::CLogView;

	class CMainForm {
	private:
		class CMainFormWnd :
			public  ::ATL::CWindowImpl<CMainFormWnd>, public ISelector_Events {
			typedef ::ATL::CWindowImpl<CMainFormWnd>  TWindow; friend class CMainForm;
		private:
			CMainForm&    m_form    ;
			CError        m_error   ;
			CBkgRenderer  m_render  ;
			CSelector     m_selector;
			CProcView     m_pro_view;
			CLogView      m_log_view;

		public:
			#define WM_MENUPOPUP  WM_INITMENUPOPUP
			#define WM_ERASE      WM_ERASEBKGND

			DECLARE_WND_CLASS(_T("ebo::game::escape::MainForm"));
			BEGIN_MSG_MAP(CMainFormWnd)
				MESSAGE_HANDLER(WM_CREATE       , OnCreate )
				MESSAGE_HANDLER(WM_DESTROY      , OnDestroy)
				MESSAGE_HANDLER(WM_DRAWITEM     , OnDraw   )
				MESSAGE_HANDLER(WM_MENUPOPUP    , OnMnuInit)
				MESSAGE_HANDLER(WM_ERASE        , OnErase  )
				MESSAGE_HANDLER(WM_SIZE         , OnSize   )
				MESSAGE_HANDLER(WM_SYSCOMMAND   , OnSysCmd )
			END_MSG_MAP()
		private:
			LRESULT   OnCreate (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnDestroy(UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnDraw   (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnErase  (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnMnuInit(UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnSize   (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnSysCmd (UINT, WPARAM, LPARAM, BOOL&);
		public:
			 CMainFormWnd(CMainForm&);
			~CMainFormWnd(void);
		private:
			LRESULT   OnCommand(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
		private: // ISelector_Events
#pragma warning(disable: 4481)
			virtual HRESULT   ISelector_OnBkgChanged (void) override sealed;
			virtual HRESULT   ISelector_OnFontChanged(void) override sealed;
			virtual HRESULT   ISelector_OnItemClick  (const UINT _u_itm_id) override sealed;
			virtual HRESULT   ISelector_OnItemImages (const UINT _u_res_id) override sealed;
#pragma warning(default: 4481)
		};
	private:
		CMainFormWnd   m_wnd;
	public:
		 CMainForm(void);
		~CMainForm(void);
	public:
		HRESULT       Create  (const HWND hParent, const RECT& rcArea);
		HRESULT       Destroy (void);
		const
		CLogView&     LogView (void) const;
		CLogView&     LogView (void)      ;
		const
		CProcView&    ProcView(void) const;
		CProcView&    ProcView(void)      ;
		const
		CSelector&    Selector(void) const;
		CSelector&    Selector(void)      ;
		HRESULT       Visible (const bool);
		const
		CWindow&      Window  (void) const;   // main form window handle reference;
		CWindow&      Window  (void)      ;
	public:
		operator  CWindow&    (void)      ;
	};

}}}

#endif/*__EBOBOOFRMMAIN_H_DFF7313A_9C01_46B4_8BD1_E8DCED919AB7_INCLUDED*/