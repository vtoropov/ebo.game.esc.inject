/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Dec-2019 at 5:47:05p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app accountant view interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 1-Mar-2020 at 10:41:40p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include "StdAfx.h"
#include "ebo.frm.main.view.pro.h"
#include "ebo.gam.esc.res.h"

#include "shared.gui.page.layout.h"

using namespace ebo::game::gui;
using namespace ebo::game::gui::view;

#include "shared.uix.gdi.renderer.h"

using namespace ex_ui::draw::renderers;
using namespace ex_ui::controls;

using namespace shared::process;
/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace game { namespace gui { namespace _impl {

	class CProcView_Data  {
	private:
		CWindow&    m_v_ref;
		CError      m_error;

	public:
		CProcView_Data (CWindow& _view) : m_v_ref(_view) { m_error << __MODULE__ << S_OK >> __MODULE__; }

	public:
		HRESULT     Active  (UILayer::IGrid* _p_grid) {
			m_error << __MODULE__ << S_OK;

			if (NULL == _p_grid)
				return (m_error = E_POINTER);

			UILayer::ICell* p_act = _p_grid->GetActiveCell();
			if (NULL == p_act)
				return m_error;

			RECT rc_act = {0};
			UILayer::IColumn* p_col = _p_grid->GetColumn(1);
			UILayer::IRowSelector* pRS = _p_grid->GetRowSelector();
			if (pRS) {
				UILayer::IRow* p_row = _p_grid->GetRow(pRS->GetTopRow() + pRS->GetVisibleRows() - 1);
				if (p_col && p_row) {
					::SetRect(&rc_act,
						p_col->GetLeft(), p_row->GetTop(), p_col->GetLeft() + p_col->GetDefWidth(), p_row->GetTop() + p_row->GetHeight()
					);
				}
			}
			p_act->SetPlacement(rc_act);
			return m_error;
		}

		TErrorRef   Error   (void) const { return m_error; }

		HRESULT     Selected(UILayer::IGrid* _p_grid, DWORD& _dw_proc_pid) {
			m_error << __MODULE__ << S_OK;  _dw_proc_pid = 0;

			if (NULL == _p_grid)
				return (m_error = E_POINTER);

			ICell* p_act = _p_grid->GetActiveCell();
			if (0==p_act) {
				return (m_error = __DwordToHresult(ERROR_INVALID_STATE)) = _T("No process is selected;");
			}
			const UINT n_row = p_act->GetRowIndex();

			CAtlString cs_pid;

			HRESULT hr_ = _p_grid->GetText(n_row, 1, cs_pid);
			if (FAILED(hr_)) {
				return (m_error = hr_) = _T("Reading PID is failed;");
			}
			_dw_proc_pid = ::_tstol((LPCTSTR)cs_pid);
			if (_dw_proc_pid == 0) {
				return (m_error = __DwordToHresult(ERROR_INVALID_DATA)) = _T("Selected PID is invalid;");
			}

			return m_error;
		}

		HRESULT     Update  (UILayer::IGrid* _p_grid, const CProcessFilter& _flt) {
			m_error << __MODULE__ << S_OK;

			if (NULL == _p_grid)
				return (m_error = E_POINTER);

			CGenericProcessEnum proc_enum;
			HRESULT hr_ = proc_enum.Enumerate();
			if (FAILED(hr_)) {
				m_error = proc_enum.Error();
				m_error.Show();
			}

			TGenericProcessList result_;

			if (_flt.Exclude().Empty() == false || 
			    _flt.Include().Empty() == false)
			proc_enum.Apply(_flt, result_);
			else
				result_ = proc_enum.List();

			const UINT n_procs = static_cast<UINT>(result_.size());
			if (0 == n_procs) {
				_p_grid->SetRows(1);
				UILayer::IRow* p_row = _p_grid->GetRow(0);
				if (p_row) {
					_p_grid->SetText(0, 1, (LPCTSTR)_T("#n/a"));
					_p_grid->SetText(0, 2, (LPCTSTR)_T("[No data]"));
					_p_grid->SetText(0, 3, (LPCTSTR)_T("    "));
				}
				this->Active(_p_grid);
				return m_error;
			}
			_p_grid->SetRows(n_procs);

			for (UINT i_ = 0; i_ < n_procs; i_++){

				UILayer::IRow* p_row = _p_grid->GetRow(i_);
				if (NULL == p_row)
					continue;

				const CGenericProcess& proc_ = result_[i_];
				
				_p_grid->SetText(i_, 1, (LPCTSTR)proc_.IdAsText());
				_p_grid->SetText(i_, 2, (LPCTSTR)proc_.Name());
				_p_grid->SetText(i_, 3, (LPCTSTR)proc_.Module());
			}
			this->Active(_p_grid);
			return m_error;
		}
	};

	typedef
	      CProcView THost;
	class CProcView_Layout {
	private:
		THost&      m_view;
		RECT        m_cli_rec;

	public:
		CProcView_Layout (THost& _view) : m_view(_view) {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_cli_rec);
			else
				::SetRectEmpty(&m_cli_rec);
		}

		RECT     Lab_Area  (LPCTSTR _lp_sz_txt, const CLab_Format& _fmt) {
			static LONG l_req_h = this->Lab_Req_Height();
			RECT rc_ = {
				m_cli_rec.left, m_cli_rec.top,
				m_cli_rec.left + CLabel_Ex::ReqSize(_lp_sz_txt, _fmt).cx, m_cli_rec.top + l_req_h
			};
			return rc_;
		}

		RECT     Rfs_Area(void) {

			const TLabel&    lab_cap = m_view.Header_Lab () ;
			const SIZE sz_ = lab_cap.ReqSize(lab_cap.Text());

			TButton& but_rfs  = m_view.Button_Rfs();
			const SIZE sz_req = but_rfs.Renderer().RequiredSize();

			RECT rc_rfs  = {sz_.cx, m_cli_rec.top, sz_.cx + sz_req.cx, m_cli_rec.top + this->Lab_Req_Height()};

			return rc_rfs;
		}
	public:
		LONG     Lab_Req_Height (void) const {
			static LONG l_gap =  3;
			static LONG l_but = 48; // images that are used for buttons are expected to have 48x48px size;
			return l_gap * 2 + l_but;
		}
		LONG     Lab_Req_Width  (void) const {
			static LONG l_but = 48;
			return __W(m_cli_rec) - l_but * 2;
		}
	};

	class CProcView_Initer {
	private:
		THost&    m_view ;
		RECT      m_cli_rec;
		CError    m_error;

	public:
		CProcView_Initer (THost& _view) : m_view(_view) { m_error << __MODULE__ << S_OK >> __MODULE__;
		if (m_view.Window().IsWindow())
			m_view.Window().GetClientRect(&m_cli_rec);
		else
			::SetRectEmpty(&m_cli_rec);
		}

		TErrorRef Error (void) const { return m_error; } 

		HRESULT   OnCreate(void) {
			m_error << __MODULE__ << S_OK;

			TLabel&  lab_cap = m_view.Header_Lab();

			CCmpClrs_Enum clrs_;
			const CCmpClrs& pair_ = clrs_[0];

			CAtlString cs_cap(_T("Process List [All]"));

			CProcView_Layout lay_(m_view);

			lab_cap.Format().FontSize(16); 
			lab_cap.Format().ForeColour(pair_.Light());
			lab_cap.Format().BkgColour (pair_.Dark ());
			lab_cap.Format().Margins().Left () = 10;
			lab_cap.Format().Margins().Right() = 10;

			const RECT rc_lab = lay_.Lab_Area((LPCTSTR)cs_cap, lab_cap.Format());

			lab_cap.Create(m_view.Window(), rc_lab, (LPCTSTR)cs_cap);

			TButton& but_rfs = m_view.Button_Rfs();
			if (but_rfs.Window())
				return (m_error = __DwordToHresult(ERROR_ALREADY_EXISTS));

			but_rfs.Runtime().CtrlId(IDC_EBO_GAM_FLX_GRD_RFRS);
			but_rfs.Renderer().Images().Add(CControlState::eDisabled, IDC_EBO_GAM_FLX_GRD_RFS3);
			but_rfs.Renderer().Images().Add(CControlState::eHovered , IDC_EBO_GAM_FLX_GRD_RFS1);
			but_rfs.Renderer().Images().Add(CControlState::eNormal  , IDC_EBO_GAM_FLX_GRD_RFS0);
			but_rfs.Renderer().Images().Add(CControlState::ePressed , IDC_EBO_GAM_FLX_GRD_RFS2);
			but_rfs.Runtime ().BackColor(pair_.Dark());

			RECT rc_rfs = lay_.Rfs_Area();
			HRESULT hr_ = but_rfs.Create(m_view.Window(), &rc_rfs, false);
			if (FAILED(hr_))
				m_error = hr_;

			return m_error;
		}
	};

}}}}
using namespace ebo::game::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CProcView::CProcViewWnd:: CProcViewWnd(CProcView& _view) : m_view(_view), m_grid(NULL), m_bt_r(*this) {}
CProcView::CProcViewWnd::~CProcViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CProcView::CProcViewWnd::OnCreate  (UINT, WPARAM, LPARAM, BOOL& _b_hand) {
	_b_hand;

	CProcView_Initer init_(m_view);
	init_.OnCreate();

	m_grid = UILayer::CreateGridInstance(*this, 10, IDC_EBO_GAM_FLX_GRD_PROC);
	if (m_grid == NULL)
		return 0;
	
	UILayer::IThemeManager& them_man = m_grid->GetThemeManagerRef();

	const UILayer::ePredefinedTheme e_theme = ePredefinedTheme::eSolidBlack;

	m_grid->ApplyObject(
		&(them_man.GetPredefinedTheme(e_theme))
	);

	UILayer::IRowSelector* p_rs_sel = m_grid->GetRowSelector();
	if (p_rs_sel) {
		p_rs_sel->SetVisible(true);
		p_rs_sel->UpdateLayout();

		UILayer::IRowSelectorTheme& rs_them = p_rs_sel->GetTheme();
		UILayer::IColorTheme& rs_cust = rs_them.Customize(e_theme);
		p_rs_sel->ApplyTheme (rs_cust);
	}

	UILayer::ICell* p_act_cell = m_grid->GetActiveCell();
	if (p_act_cell) {
		UILayer::IActiveCellTheme& act_them = p_act_cell->GetTheme();
		UILayer::IColorTheme&  act_cust = act_them.Customize(e_theme);
		p_act_cell->ApplyTheme(act_cust);
		p_act_cell->SetOptions(UILayer::eCellOption::eSingleBorder);
	}
	UILayer::IColumnHeader* p_head = m_grid->GetColumnHeader();
	if (p_head == NULL)
		return 0;
	else {
		UILayer::THeadTheme&   had_them = p_head->GetTheme();
		UILayer::IColorTheme&  had_cust = had_them.Customize(e_theme);
		p_head->ApplyTheme    (had_cust);
	}
	UILayer::IScrollBars& scrolls = m_grid->ScrollBars();
	UILayer::TScrollTheme& scr_theme = scrolls.Theme();
	UILayer::IColorTheme& scr_cust = scr_theme.Customize(e_theme);
	scrolls.Theme(scr_cust);

	static LPCTSTR lp_sz_caps[] = {
		_T(" "  ) ,
		_T("PID") , _T("Name"), _T("Path")
	};

	static const UILayer::eColumnHAlignTypes e_col_aligns[] = {
		UILayer::e2_clmnHAlignCenter,
		UILayer::e2_clmnHAlignRight , UILayer::e2_clmnHAlignLeft , UILayer::e2_clmnHAlignLeft
	};

	static const UILayer::eColumnDataTypes e_col_types[] = {
		UILayer::e2_clmnDTypeLong   ,
		UILayer::e2_clmnDTypeLong   , UILayer::e2_clmnDTypeText  , UILayer::e2_clmnDTypeText
	};

	static const UINT u_col_width[] = { 30, 70, 160, 440 };
	static const bool u_col_accss[] = {
		false, false, false, false
	};

	static const DWORD e_grp_style  = UILayer::e2_GroupStyleImmovable;

	UINT u_grp_id_tag = 0x0C000000;
	UINT u_col_id_tag = 0x10000000;

	for ( INT i_ = 0; i_ < _countof(lp_sz_caps  ) && i_ < _countof(e_col_aligns)
	               && i_ < _countof(e_col_types ) && i_ < _countof(u_col_width ); i_++ ){

		UILayer::IColumnGroup* pGroup = p_head->AddGroup(lp_sz_caps[i_]);
		if (pGroup == NULL)
			continue;
		u_grp_id_tag += 1;

		pGroup->SetID   (u_grp_id_tag);
		pGroup->SetStyle(e_grp_style );
		pGroup->SetTag  (u_grp_id_tag);

		UILayer::IColumn* pColumn = pGroup->AddColumn(NULL, ++u_col_id_tag);
		if (pColumn == NULL)
			continue;

		pColumn->SetTag     (u_col_id_tag);
		pColumn->SetHAlign  (e_col_aligns[i_]);
		pColumn->SetDataType(e_col_types [i_]);
		pColumn->SetWidth   (u_col_width [i_]);

		UILayer::__OverlayData& over__  = (*pColumn).AccessOverlay();
		over__.__c__0 = RGB(17, 17, 17);
		over__.__c__1 = RGB(27, 27, 27);

		pColumn->SetLock(true);
		pGroup ->Update();
	}
	m_grid->ScrollBars().Show(UILayer::eGridScrollBarShow::e2_ScrollBarShowAuto);
	m_grid->EnableRowGrouping(true);
	m_grid->SetRows(10);
	m_grid->UpdateLayout(*this);

	CProcessFilter empty_;

	CProcView_Data(*this).Update(m_grid, empty_);

	CAtlString cs_rec; cs_rec.Format(_T("Process(es) has been enumerated: [rec(s): %d]"), m_grid->GetRowCount());

	_out::_get_logger_ref().ILog_Text((LPCTSTR)cs_rec);

	return 0;
}

LRESULT   CProcView::CProcViewWnd::OnDestroy (UINT, WPARAM, LPARAM, BOOL&) {

	UILayer::DestroyGrid_Safe(&m_grid); m_grid = NULL;
	m_bt_r.Destroy();
	m_laba.Destroy();
	return 0;
}

LRESULT   CProcView::CProcViewWnd::OnErase   (UINT, WPARAM _w_dc, LPARAM, BOOL& _b_hand) {
	_b_hand = TRUE; _w_dc;
	static const LRESULT l_is_drawn = 1;
	RECT rc_ = {0};
	TPane::GetClientRect(&rc_);
	CZBuffer z_buf((HDC)_w_dc, rc_);

	CCmpClrs_Enum clrs_;
	const CCmpClrs& pair_ = clrs_[0];

	z_buf.DrawSolidRect(rc_, pair_.Dark());
	
	return l_is_drawn;
}

LRESULT   CProcView::CProcViewWnd::OnSize    (UINT, WPARAM, LPARAM _lp, BOOL&) {
	
	CProcView_Layout lay_(m_view);

	const SIZE sz_ = {0, lay_.Lab_Req_Height()};
	const RECT rc_ = {0, sz_.cy, LOWORD(_lp), HIWORD(_lp)};
	if (m_grid) {
		m_grid->UpdateLayout(rc_);
		CWindow w_grid = m_grid->GetSafeHwnd();
		w_grid.RedrawWindow();
	}
	const RECT rc_lab =  lay_.Lab_Area(m_laba.Text(), m_laba.Format());
	const RECT rc_rfs =  lay_.Rfs_Area();

	m_laba.Window ().MoveWindow(&rc_lab);
	m_laba.Refresh();
	m_bt_r.Window ().MoveWindow(&rc_rfs);
	m_bt_r.Refresh();
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CProcView::CProcViewWnd::IControlEvent_OnClick(const UINT ctrlId) {
	CProcView_Data(*this).Update(m_grid, m_filter);
	ctrlId; return S_OK;
}
HRESULT    CProcView::CProcViewWnd::IControlEvent_OnClick(const UINT ctrlId, const LONG_PTR nData) { ctrlId; nData; return S_OK; }

/////////////////////////////////////////////////////////////////////////////

CProcView:: CProcView(void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CProcView::~CProcView(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CProcView::Refresh(void) {
	TView::m_error << __MODULE__ << S_OK;

	HRESULT hr_ = TView::Refresh();
	if (m_wnd.m_grid == NULL)
		return (TView::m_error = OLE_E_BLANK);

	m_wnd.m_laba.Refresh();

	UILayer::IThemeManager& them_man = m_wnd.m_grid->GetThemeManagerRef();
	const UILayer::ePredefinedTheme e_theme = ePredefinedTheme::eSolidBlack;

	m_wnd.m_grid->ApplyObject(
		&(them_man.GetPredefinedTheme(e_theme))
	);

	UILayer::IRowSelector* p_rs_sel = m_wnd.m_grid->GetRowSelector();
	if (p_rs_sel) {
		
		UILayer::IRowSelectorTheme& rs_them = p_rs_sel->GetTheme();
		UILayer::IColorTheme& rs_cust = rs_them.Customize(e_theme);
		p_rs_sel->ApplyTheme (rs_cust);
		p_rs_sel->Refresh();
	}

	UILayer::ICell* p_act_cell = m_wnd.m_grid->GetActiveCell();
	if (p_act_cell) {
		UILayer::IActiveCellTheme& act_them = p_act_cell->GetTheme();
		UILayer::IColorTheme&  act_cust = act_them.Customize(e_theme);
		p_act_cell->ApplyTheme(act_cust);
	}

	CWindow w_grid = m_wnd.m_grid->GetSafeHwnd();
	w_grid.RedrawWindow();

	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CProcView::Filter (void) {
	TView::m_error << __MODULE__ << S_OK;

	CFilterDlg dlg_; dlg_.Filter() = m_wnd.m_filter;

	CFilterAct::_act e_result = dlg_.DoModal();

	if (CFilterAct::e_apply  == e_result) { this->Apply(dlg_.Filter()); }
	if (CFilterAct::e_clear  == e_result) { this->Clear(); }

	return TView::m_error;
}

HRESULT     CProcView::Scan   (void) {
	TView::m_error << __MODULE__ << S_OK;

	CProcView_Data data_prov(m_wnd);

	DWORD dw_pid = 0;
	HRESULT  hr_ = data_prov.Selected(m_wnd.m_grid, dw_pid);
	if (FAILED(hr_)) {
		m_error  = data_prov.Error(); m_error.Show();
	}

	return TView::m_error;
}

CWindow&    CProcView::Window (void) { return m_wnd;        }

/////////////////////////////////////////////////////////////////////////////

TButton&    CProcView::Button_Rfs(void) { return m_wnd.m_bt_r; }
TLabel &    CProcView::Header_Lab(void) { return m_wnd.m_laba; }

/////////////////////////////////////////////////////////////////////////////

HRESULT     CProcView::Apply  (const CProcessFilter& _flt) {
	TView::m_error << __MODULE__ << S_OK;

	m_wnd.m_filter = _flt;

	CProcView_Data(m_wnd).Update(m_wnd.m_grid, _flt);

	CAtlString cs_cap(_T("Process List [Filtered]"));

	m_wnd.m_laba.Text((LPCTSTR)cs_cap);
	m_wnd.SendMessageW(WM_SIZE);

	if (m_wnd.m_grid == NULL)
		return (m_error = E_POINTER);

	CAtlString cs_rec; cs_rec.Format(_T("Process filter has applied: [rec(s): %d]"), m_wnd.m_grid->GetRowCount());

	_out::_get_logger_ref().ILog_Text((LPCTSTR)cs_rec);

	return m_error;
}

HRESULT     CProcView::Clear  (void) {
	TView::m_error << __MODULE__ << S_OK;

	CProcessFilter empty_;
	CProcView_Data(m_wnd).Update(m_wnd.m_grid, empty_);

	CAtlString cs_cap(_T("Process List [All]"));

	m_wnd.m_laba.Text((LPCTSTR)cs_cap);
	m_wnd.SendMessageW(WM_SIZE);

	if (m_wnd.m_grid == NULL)
		return (m_error = E_POINTER);

	CAtlString cs_rec; cs_rec.Format(_T("Process filter is cleared: [rec(s): %d]"), m_wnd.m_grid->GetRowCount());

	_out::_get_logger_ref().ILog_Text((LPCTSTR)cs_rec);

	return TView::m_error;
}