/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Oct-2019 at 8:51:42p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account app base view interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 1-Mar-2020 at 10:32:07p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include "StdAfx.h"
#include "ebo.frm.main.view.bas.h"

using namespace ebo::game::gui::view;

/////////////////////////////////////////////////////////////////////////////

CViewWnd_Base:: CViewWnd_Base(void) {}
CViewWnd_Base::~CViewWnd_Base(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CViewWnd_Base::OnCommand (UINT, WPARAM, LPARAM, BOOL&) { return 0; }
LRESULT   CViewWnd_Base::OnCreate  (UINT, WPARAM, LPARAM, BOOL&) { return 0; }
LRESULT   CViewWnd_Base::OnDestroy (UINT, WPARAM, LPARAM, BOOL&) { return 0; }
LRESULT   CViewWnd_Base::OnErase   (UINT, WPARAM, LPARAM, BOOL& _b_hand) { _b_hand = FALSE; return 0; }
LRESULT   CViewWnd_Base::OnSize    (UINT, WPARAM, LPARAM, BOOL&) { return 0; }

/////////////////////////////////////////////////////////////////////////////

CView_Base:: CView_Base(CViewWnd_Base& _wnd) : m_wnd(_wnd) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CView_Base::~CView_Base(void) {}

/////////////////////////////////////////////////////////////////////////////

bool        CView_Base::CanAccept(const WORD nCmdId ) const {
	nCmdId;
	return false;
}
HRESULT     CView_Base::Create   (const HWND hParent, const RECT& _area, const bool bVisible) {
	m_error << __MODULE__ << S_OK;

	if (!::IsWindow(hParent))
		return (m_error = OLE_E_INVALIDHWND);

	const DWORD dwStyle = WS_CHILD | WS_CLIPCHILDREN | (bVisible ? WS_VISIBLE : NULL);

	RECT area_ = _area;

	m_wnd.Create(hParent, area_, NULL, dwStyle);
	if (m_wnd == NULL)
		m_error = ::GetLastError();

	return m_error;
}
HRESULT     CView_Base::Destroy  (void) {
	m_error << __MODULE__ << S_OK;

	if (NULL == m_wnd || FALSE == m_wnd.IsWindow())
		return (m_error = OLE_E_BLANK);

	m_wnd.SendMessage(WM_CLOSE);

	return m_error;
}
TErrorRef   CView_Base::Error    (void) const { return m_error; }
bool        CView_Base::IsChecked(const UINT nCmdId) const {
	nCmdId; return false;
}
bool        CView_Base::IsValid  (void) const { return (NULL != m_wnd.m_hWnd && !!m_wnd.IsWindow()); }
bool        CView_Base::IsVisible(void) const { return (this->IsValid() && !!m_wnd.IsWindowVisible()); }
HRESULT     CView_Base::IsVisible(const bool _v) {
	m_error << __MODULE__ << S_OK;
	HRESULT hr_ = m_error;
	if (this->IsValid() == false)
		return (hr_ = OLE_E_BLANK);

	if (_v) m_wnd.ShowWindow(SW_SHOW);
	else    m_wnd.ShowWindow(SW_HIDE);

	return  hr_;
}

HRESULT     CView_Base::Refresh  (void) {
	m_error << __MODULE__ << S_OK;
	HRESULT hr_ = m_error;
	if (this->IsValid() == false)
		return (hr_ = OLE_E_BLANK);

	m_wnd.RedrawWindow();

	return  hr_;
}

HRESULT     CView_Base::OnCommand(const WORD cmdId) {
	cmdId;
	m_error << __MODULE__ << S_OK;
#if (0)
	_out::SetIdleMessage();
#endif
	return  m_error;
}
HRESULT     CView_Base::OnPrepare(const WORD cmdId) {
	m_error << __MODULE__ << S_FALSE;    cmdId;
	return  m_error;
}
HRESULT     CView_Base::UpdateLayout(const RECT& _area) {
	m_error << __MODULE__ << S_OK;

	if (::IsRectEmpty(&_area) == TRUE)
		return (m_error = OLE_E_INVALIDRECT);

	m_wnd.MoveWindow(&_area, TRUE);

	return  m_error;
}

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////