/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Dec-2019 at 5:47:05p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app accountant view interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 3-Mar-2020 at 6:44:26p, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "StdAfx.h"
#include "ebo.frm.main.view.log.h"
#include "ebo.gam.esc.res.h"

#include "shared.gui.page.layout.h"

using namespace ebo::game::gui;
using namespace ebo::game::gui::view;

#include "shared.gen.sys.time.h"
using namespace shared::com_ex;

#include "shared.uix.gdi.renderer.h"

using namespace ex_ui::draw::renderers;
/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace game { namespace gui { namespace _impl {

	class CLogView_Data  {
	private:
		CWindow&    m_v_ref;
		CError      m_error;

	public:
		CLogView_Data (CWindow& _view) : m_v_ref(_view) { m_error << __MODULE__ << S_OK >> __MODULE__; }

	public:
		HRESULT     Update(UILayer::IGrid* _p_grid) {
			m_error << __MODULE__ << S_OK;

			if (NULL == _p_grid)
				return (m_error = E_POINTER);

			return m_error;
		}
	};

	class CLogView_Layout {
	private:
		CWindow&    m_v_ref;

	public:
		CLogView_Layout (CWindow& _view) : m_v_ref(_view) {}
	};

}}}}
using namespace ebo::game::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CLogView::CLogViewWnd:: CLogViewWnd(void) : m_grid(NULL) {}
CLogView::CLogViewWnd::~CLogViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CLogView::CLogViewWnd::OnCreate  (UINT, WPARAM, LPARAM, BOOL& _b_hand) {
	_b_hand;
	m_grid = UILayer::CreateGridInstance(*this, 10, IDC_EBO_GAM_FLX_GRD_LOG);
	if (m_grid == NULL)
		return 0;
	
	UILayer::IThemeManager& them_man = m_grid->GetThemeManagerRef();

	const UILayer::ePredefinedTheme e_theme = ePredefinedTheme::eSolidBlack;

	m_grid->ApplyObject(
		&(them_man.GetPredefinedTheme(e_theme))
	);

	UILayer::IRowSelector* p_rs_sel = m_grid->GetRowSelector();
	if (p_rs_sel) {
		p_rs_sel->SetVisible(true);
		p_rs_sel->UpdateLayout();

		UILayer::IRowSelectorTheme& rs_them = p_rs_sel->GetTheme();
		UILayer::IColorTheme& rs_cust = rs_them.Customize(e_theme);
		p_rs_sel->ApplyTheme (rs_cust);
	}

	UILayer::ICell* p_act_cell = m_grid->GetActiveCell();
	if (p_act_cell) {
		UILayer::IActiveCellTheme& act_them = p_act_cell->GetTheme();
		UILayer::IColorTheme&  act_cust = act_them.Customize(e_theme);
		p_act_cell->ApplyTheme(act_cust);
		p_act_cell->SetOptions(UILayer::eCellOption::eSingleBorder);
	}
	UILayer::IColumnHeader* p_head = m_grid->GetColumnHeader();
	if (p_head == NULL)
		return 0;
	else {
		UILayer::THeadTheme&   had_them = p_head->GetTheme();
		UILayer::IColorTheme&  had_cust = had_them.Customize(e_theme);
		p_head->ApplyTheme    (had_cust);
	}
	UILayer::IScrollBars& scrolls = m_grid->ScrollBars();
	UILayer::TScrollTheme& scr_theme = scrolls.Theme();
	UILayer::IColorTheme& scr_cust = scr_theme.Customize(e_theme);
	scrolls.Theme(scr_cust);

	static LPCTSTR lp_sz_caps[] = {
		_T(" " ), _T("Timestamp"), _T("Content")
	};

	static const UILayer::eColumnHAlignTypes e_col_aligns[] = {
		UILayer::e2_clmnHAlignRight, UILayer::e2_clmnHAlignCenter, UILayer::e2_clmnHAlignLeft
	};

	static const UILayer::eColumnDataTypes e_col_types[] = {
		UILayer::e2_clmnDTypeLong   ,
		UILayer::e2_clmnDTypeLong   , UILayer::e2_clmnDTypeText  , UILayer::e2_clmnDTypeText
	};

	static const UINT u_col_width[] = { 30, 220, 450 };
	static const bool u_col_accss[] = {
		false, false, false
	};

	static const DWORD e_grp_style  = UILayer::e2_GroupStyleImmovable;

	UINT u_grp_id_tag = 0x0C000000;
	UINT u_col_id_tag = 0x10000000;

	CAtlString cs_cap;

	for ( INT i_ = 0; i_ < _countof(lp_sz_caps  ) && i_ < _countof(e_col_aligns)
	               && i_ < _countof(e_col_types ) && i_ < _countof(u_col_width ); i_++ ){

		UILayer::IColumnGroup* pGroup = p_head->AddGroup(lp_sz_caps[i_]);
		if (pGroup == NULL)
			continue;
		u_grp_id_tag += 1;

		pGroup->SetID   (u_grp_id_tag);
		pGroup->SetStyle(e_grp_style );
		pGroup->SetTag  (u_grp_id_tag);

		UILayer::IColumn* pColumn = pGroup->AddColumn(NULL, ++u_col_id_tag);
		if (pColumn == NULL)
			continue;

		pColumn->SetTag     (u_col_id_tag);
		pColumn->SetHAlign  (e_col_aligns[i_]);
		pColumn->SetDataType(e_col_types [i_]);
		pColumn->SetWidth   (u_col_width [i_]);

		UILayer::__OverlayData& over__  = (*pColumn).AccessOverlay();
		over__.__c__0 = RGB(17, 17, 17);
		over__.__c__1 = RGB(27, 27, 27);

		pColumn->SetLock(true);
		pGroup ->Update();
	}
	m_grid->ScrollBars().Show(UILayer::eGridScrollBarShow::e2_ScrollBarShowAuto);
	m_grid->EnableRowGrouping(true);
	m_grid->SetRows(1);
	m_grid->UpdateLayout(*this);

	m_grid->SetText(0, 2, _T("[No record]"));

	_in::_set_logger_ptr(this);

	return 0;
}

LRESULT   CLogView::CLogViewWnd::OnDestroy (UINT, WPARAM, LPARAM, BOOL&) {
	_in::_set_logger_ptr(NULL);
	UILayer::DestroyGrid_Safe(&m_grid); m_grid = NULL;
	return 0;
}

LRESULT   CLogView::CLogViewWnd::OnErase   (UINT, WPARAM _w_dc, LPARAM, BOOL& _b_hand) {
	_b_hand = TRUE; _w_dc;
	static const LRESULT l_is_drawn = 1;
	RECT rc_ = {0};
	TPane::GetClientRect(&rc_);
	CZBuffer z_buf((HDC)_w_dc, rc_);

	CCmpClrs_Enum clrs_;
	const CCmpClrs& pair_ = clrs_[0];

	z_buf.DrawSolidRect(rc_, pair_.Dark());
	
	return l_is_drawn;
}

LRESULT   CLogView::CLogViewWnd::OnSize    (UINT, WPARAM, LPARAM _lp, BOOL&) {

	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, 0, LOWORD(_lp), HIWORD(_lp)};
	if (m_grid) {
		m_grid->UpdateLayout(rc_);
		CWindow w_grid = m_grid->GetSafeHwnd();
		w_grid.RedrawWindow();
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

VOID      CLogView::CLogViewWnd::ILog_Text (LPCTSTR _lp_sz_text) {
	if (NULL == _lp_sz_text || 0 == ::_tcslen(_lp_sz_text))
		return;
	if (m_grid == NULL)
		return;

	CDateTime timestamp;
	static bool b_1st_time = true;

	UINT n_row_cnt = m_grid->GetRowCount();
	if (!b_1st_time) m_grid->SetRows(++n_row_cnt); 

	m_grid->SetText(n_row_cnt - 1, 1, (LPCTSTR)timestamp.GetTimeAsUnix());
	m_grid->SetText(n_row_cnt - 1, 2, (LPCTSTR)_lp_sz_text);
	b_1st_time =false;
}

/////////////////////////////////////////////////////////////////////////////

CLogView:: CLogView(void) : TView(m_wnd) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CLogView::~CLogView(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CLogView::Refresh(void) {
	TView::m_error << __MODULE__ << S_OK;

	HRESULT hr_ = TView::Refresh();
	if (m_wnd.m_grid == NULL)
		return (TView::m_error = OLE_E_BLANK);

	UILayer::IThemeManager& them_man = m_wnd.m_grid->GetThemeManagerRef();
	const UILayer::ePredefinedTheme e_theme = ePredefinedTheme::eSolidBlack;

	m_wnd.m_grid->ApplyObject(
		&(them_man.GetPredefinedTheme(e_theme))
	);

	UILayer::IRowSelector* p_rs_sel = m_wnd.m_grid->GetRowSelector();
	if (p_rs_sel) {
		
		UILayer::IRowSelectorTheme& rs_them = p_rs_sel->GetTheme();
		UILayer::IColorTheme& rs_cust = rs_them.Customize(e_theme);
		p_rs_sel->ApplyTheme (rs_cust);
		p_rs_sel->Refresh();
	}

	UILayer::ICell* p_act_cell = m_wnd.m_grid->GetActiveCell();
	if (p_act_cell) {
		UILayer::IActiveCellTheme& act_them = p_act_cell->GetTheme();
		UILayer::IColorTheme&  act_cust = act_them.Customize(e_theme);
		p_act_cell->ApplyTheme(act_cust);
	}

	CWindow w_grid = m_wnd.m_grid->GetSafeHwnd();
	w_grid.RedrawWindow();

	return  hr_;
}