#ifndef _EBOFRMMAINVIEWLOG_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
#define _EBOFRMMAINVIEWLOG_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Dec-2019 at 5:44:43p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app accountant view interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 3-Mar-2020 at 6:37:23p, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "shared.gen.sys.err.h"
#include "ebo.frm.main.view.bas.h"
#include "IGridInterface.h"

#include "ebo.gam.log.iface.h"

namespace ebo { namespace game { namespace gui { namespace view {

	using shared::sys_core::CError;

	using UILayer::ePredefinedTheme;
	using ebo::game::gui::ILogger;

	class CLogView : public CView_Base {
	                typedef CView_Base TView;
	private:
		class CLogViewWnd : public  CViewWnd_Base , public ILogger {
		                    typedef CViewWnd_Base TPane;
			friend class CLogView;
		private:
			UILayer::IGrid* m_grid;
			
		public:
			 CLogViewWnd(void);
			~CLogViewWnd(void);
		private:
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnErase   (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
		public:
			VOID      ILog_Text (LPCTSTR _lp_sz_text) override sealed;
#pragma warning (default: 4481)
		};

	private:
		CLogViewWnd   m_wnd  ;

	public:
		 CLogView(void);
		~CLogView(void);

	public:
#pragma warning (disable: 4481)
		HRESULT     Refresh  (void) override sealed;
#pragma warning (default: 4481)
	};

}}}}

#endif/*_EBOFRMMAINVIEWLOG_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED*/