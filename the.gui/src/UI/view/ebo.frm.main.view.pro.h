#ifndef _EBOBOOMAINVIEWSACC_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
#define _EBOBOOMAINVIEWSACC_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Dec-2019 at 5:44:43p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app accountant view interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 1-Mar-2020 at 10:39:45p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.label.h"
#include "shared.uix.ctrl.button.h"

#include "ebo.gam.esc.flt.dlg.h"
#include "ebo.frm.main.view.bas.h"
#include "IGridInterface.h"

#include "shared.gen.proc.h"

namespace ebo { namespace game { namespace gui { namespace view {

	using shared::sys_core::CError;

	using ex_ui::controls::CLabel_Ex;
	using ex_ui::controls::CLab_Format;

	using ex_ui::controls::CButton_Ex ;
	using ex_ui::controls::IControlEvent;

	using UILayer::ePredefinedTheme;
	using UILayer::IGrid;
	using UILayer::ICell;

	using shared::process::CProcessFilter; typedef CProcessFilter TProcFilter;

	using ebo::game::gui::CFilterDlg;

	typedef CButton_Ex TButton;
	typedef CLabel_Ex  TLabel ;

	class CProcView : public CView_Base {
	                 typedef CView_Base TView;
	private:
		class CProcViewWnd : public  CViewWnd_Base, public IControlEvent {
		                     typedef CViewWnd_Base  TPane;
			friend class CProcView;
		private:
			CProcView&   m_view;
			IGrid*       m_grid;
			CLabel_Ex    m_laba;
			CButton_Ex   m_bt_r;  // refresh button;
			TProcFilter  m_filter;
			
		public:
			 CProcViewWnd(CProcView&);
			~CProcViewWnd(void);

		private:   //  IControlEvent
#pragma warning (disable: 4481)
			HRESULT    IControlEvent_OnClick(const UINT ctrlId) override sealed;
			HRESULT    IControlEvent_OnClick(const UINT ctrlId, const LONG_PTR nData) override sealed;

		private:  // CViewWnd_Base
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnErase   (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};

	private:
		CProcViewWnd   m_wnd  ;

	public:
		 CProcView(void);
		~CProcView(void);

	public: // CView_Base
#pragma warning (disable: 4481)
		HRESULT     Refresh  (void) override sealed;
#pragma warning (default: 4481)
	public:
		HRESULT     Filter   (void);
		HRESULT     Scan     (void);
		CWindow&    Window   (void);
	public:
		TButton&    Button_Rfs(void);
		TLabel &    Header_Lab(void);

	private:
		HRESULT     Apply    (const CProcessFilter&);
		HRESULT     Clear    (void); // removes a filter if any;
	};

}}}}

#endif/*_EBOBOOMAINVIEWSACC_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED*/