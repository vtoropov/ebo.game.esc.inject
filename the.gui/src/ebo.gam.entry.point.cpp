/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Jul-2018 at 9:33:01a, UTC+7, Novosibirsk, Tulenina, Friday;
	This is Ebo Bot data provider test desktop application entry point implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Boo project on 18-Oct-2019 at 07:04:38a, UTC+7, Novosibirsk, Tulenina, Friday;
	Adopted to Ebo Pack 2D shape draw app on 25-Feb-2020 at 11:13:07a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	Adopted to Ebo Game Dyna Inject on 1-Mar-2020 at 6:44:04p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include "StdAfx.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

#include "shared.gen.sys.com.h"

using namespace shared::sys_core;

CAppModule _Module;

#include "ebo.gam.frm.main.h"

using namespace ebo::game::gui;
/////////////////////////////////////////////////////////////////////////////

INT RunModal(VOID) {
	INT nRet = 0; return nRet;
}

INT RunModeless(VOID)
{
	INT result_ = 0;

	if (true) {

		::WTL::CMessageLoop pump_;
		_Module.AddMessageLoop(&pump_);

		RECT rc_def_ = {0, 0, 900, 500};

		CMainForm frame_;
		const HRESULT hr_ = frame_.Create(HWND_DESKTOP, rc_def_);
		if (SUCCEEDED(hr_))
		{
			frame_.Visible(true);
			result_ = pump_.Run();
		}
		_Module.RemoveMessageLoop();
	}
	return result_;
}

INT WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpstrCmdLine, INT nCmdShow)
{
	hInstance; hPrevInstance; lpstrCmdLine; nCmdShow;

	CCoIniter com(false);
	CCoSecurityProvider sec_;

	HRESULT hr_ = sec_.InitDefault();
	ATLVERIFY(SUCCEEDED(hr_));
	/*
		Tech_dog commented on 09-Feb-2010 at 12:47:50pm:
		________________________________________________
		we need to assign lib id to link ATL DLL statically,
		otherwise we get annoying fkn message "Did you forget to pass the LIBID to CComModule::Init?"
	*/
	_Module.m_libid = LIBID_ATLLib;
	INT nResult = 0;
	// this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used
	::DefWindowProc(NULL, 0, 0, 0L);

	AtlInitCommonControls(ICC_BAR_CLASSES);	// add flags to support other controls

	hr_ = _Module.Init(NULL, hInstance);
	if(!SUCCEEDED(hr_))
	{
		ATLASSERT(FALSE);
		return -1;
	}

	ex_ui::draw::CGdiPlusLib_Guard  guard_;
#if defined(_MODAL)
	nResult = ::RunModal();
#else
	nResult = ::RunModeless();
#endif
	_Module.Term();

	return nResult;
}