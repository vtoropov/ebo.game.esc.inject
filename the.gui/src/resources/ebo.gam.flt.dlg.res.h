//
// Created by Tech_dog (ebontrop@gmail.com) on 3-Dec-2018 at 6:50:00a, UTC+7, Novosibirsk, Tulenina, Monday;
// This is USB Drive Detective (bitsphereinc.com) desktop app data wizard UI control identifier declaration file.
// -----------------------------------------------------------------------------
// Adopted to Ebo Pack Google push notification app on 14-Sep-2019 at 3:46:28p, UTC+7, Novosibirsk, Tulenina, Saturday;
// Adopted to Ebo Pack personal account app on 22-Oct-2019 at 7:40:18a, UTC+7, Novosibirsk, Light Coloured, Tuesday;
// Adopted to Ebo Game Dyna Inject project on 2-Mar-2020 at 1:42:36p, UTC+7, Novosibirsk, Tulenina, Monday;
//

#pragma region __filter_dlg

#define IDD_EBO_GAM_ESC_FLT_DLG          2101
#define IDR_EBO_GAM_ESC_FLT_DLG_ICO      2103
#define IDC_EBO_GAM_ESC_FLT_DLG_SAV      2105
#define IDC_EBO_GAM_ESC_FLT_DLG_BAN      2107
#define IDR_EBO_GAM_ESC_FLT_DLG_BAN      IDC_EBO_GAM_ESC_FLT_DLG_BAN
#define IDC_EBO_GAM_ESC_FLT_DLG_TAB      2109
#define IDC_EBO_GAM_ESC_FLT_DLG_SEP      2111

#pragma region __filter_1st_page
#define IDD_EBO_GAM_ESC_FLT_1ST_PAG      2201
#define IDC_EBO_GAM_ESC_FLT_1ST_IMG      2207
#define IDR_EBO_GAM_ESC_FLT_1ST_IMG      IDC_EBO_GAM_ESC_FLT_1ST_IMG
#define IDC_EBO_GAM_ESC_FLT_1ST_INF      2209
#define IDS_EBO_GAM_ESC_FLT_1ST_INF      IDC_EBO_GAM_ESC_FLT_1ST_INF
#define IDC_EBO_GAM_ESC_FLT_1ST_PRT      2211
#define IDC_EBO_GAM_ESC_FLT_1ST_ADD      2213
#define IDC_EBO_GAM_ESC_FLT_1ST_AVA      2215
#define IDR_EBO_GAM_ESC_FLT_1ST_AVA      IDC_EBO_GAM_ESC_FLT_1ST_AVA
#define IDC_EBO_GAM_ESC_FLT_1ST_CAP      2217
#define IDC_EBO_GAM_ESC_FLT_1ST_SEP      2219
#define IDC_EBO_GAM_ESC_FLT_1ST_LST      2221
#define IDC_EBO_GAM_ESC_FLT_1ST_DEL      2223
#pragma endregion


#pragma region __filter_2nd_page
#define IDD_EBO_GAM_ESC_FLT_2ND_PAG      2301
#define IDC_EBO_GAM_ESC_FLT_2ND_IMG      2307
#define IDR_EBO_GAM_ESC_FLT_2ND_IMG      IDC_EBO_GAM_ESC_FLT_2ND_IMG
#define IDC_EBO_GAM_ESC_FLT_2ND_INF      2309
#define IDS_EBO_GAM_ESC_FLT_2ND_INF      IDC_EBO_GAM_ESC_FLT_2ND_INF
#define IDC_EBO_GAM_ESC_FLT_2ND_PRT      2311
#define IDC_EBO_GAM_ESC_FLT_2ND_ADD      2313
#define IDC_EBO_GAM_ESC_FLT_2ND_AVA      2315
#define IDR_EBO_GAM_ESC_FLT_2ND_AVA      IDC_EBO_GAM_ESC_FLT_2ND_AVA
#define IDC_EBO_GAM_ESC_FLT_2ND_CAP      2317
#define IDC_EBO_GAM_ESC_FLT_2ND_SEP      2319
#define IDC_EBO_GAM_ESC_FLT_2ND_LST      2321
#define IDC_EBO_GAM_ESC_FLT_2ND_DEL      2323
#pragma endregion

#pragma endregion


