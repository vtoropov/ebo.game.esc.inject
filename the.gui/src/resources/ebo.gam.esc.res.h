//
//   Created by Tech_dog (ebontrop@gmail.com) on 21-May-2019 at 6:21:36p, UTC+7, Phuket, Rawai, Tuesday;
//   This is communication data exchange receiver desktop console application resource declaration file.
//   -----------------------------------------------------------------------------
//   Adopted to Ebo Pack virtual printer console on 5-Sep-2019 at 3:14:50p, UTC+7, Novosibirsk, Tulenina, Thursday;
//   Adopted to Ebo Pack notify Google service system tray app on 7-Sep-2019 at 1:32:02p, UTC+7, Novosibirsk, Tulenina, Saturday;
//   Adopted to Ebo Pack personal account app on 13-Oct-2019 at 9:27:52a, UTC+7, Novosibirsk, Light Coloured, Sunday;
//   Adopted to Ebo Pack 2D shape draw app on 25-Feb-2020 at 11:15:43a, UTC+7, Novosibirsk, Tulenina, Tuesday;
//   Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 3:51:31a, UTC+7, Novosibirsk, Tulenina, Tuesday;
//

#pragma region __generic

#define IDR_EBO_GAM_ICO                1001

#pragma region __selector_cmds

#define IDC_EBO_GAM_PROC_LST           1101
#define IDR_EBO_GAM_PROC_IMG           IDC_EBO_GAM_PROC_LST
#define IDC_EBO_GAM_PROC_SCN           1103
#define IDC_EBO_GAM_PROC_FLT           1105
#define IDC_EBO_GAM_PROC_LOG           1107

#pragma endregion

#pragma region __proc_view
#define IDC_EBO_GAM_FLX_GRD_PROC       1201
#define IDC_EBO_GAM_FLX_GRD_RFRS       1203
#define IDC_EBO_GAM_FLX_GRD_RFS0       1205
#define IDC_EBO_GAM_FLX_GRD_RFS1       1207
#define IDC_EBO_GAM_FLX_GRD_RFS2       1209
#define IDC_EBO_GAM_FLX_GRD_RFS3       1211
#pragma endregion

#pragma region __log_view
#define IDC_EBO_GAM_FLX_GRD_LOG        1301
#pragma endregion

#pragma endregion


